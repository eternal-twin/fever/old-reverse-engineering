package {
import flash.text.TextField;

import package_10.Unserializer;
import package_10.Serializer;
import package_10.class_886;

import package_27.package_28.Bytes;

import package_32.App;
import package_32.class_899;

/**
 * JPEXS Obfuscated name: `§M3\x02\x16\x01§`
 * JPEXS name: `class_811`
 */
public class MtCodec {
  public static var var_370:Boolean;

  /**
   * JPEXS name: `var_1055`
   */
  public static var fl_disableEncryption:Boolean = false;

  /**
   * JPEXS name: `var_1056`
   */
  public static var BASE64_URL:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";

  /**
   * JPEXS name: `var_1057`
   */
  public static var codecSalt:String = null;

  public static var var_936:class_720;

  public static var var_1058:TextField;

  public static function var_1059(param1:*):void {
    var _loc2_:Shape = null;
    if (MtCodec.var_1058 == null) {
      _loc2_ = new Shape();
      _loc2_.graphics.beginFill(16777215);
      _loc2_.graphics.drawRect(0, 0, 1000, 1000);
      App.rootMc.addChild(_loc2_);
      MtCodec.var_1058 = new TextField();
      MtCodec.var_1058.width = 500;
      MtCodec.var_1058.height = 1000;
      App.rootMc.addChild(MtCodec.var_1058);
      MtCodec.var_1058.selectable = true;
      MtCodec.var_1058.multiline = true;
      MtCodec.var_1058.wordWrap = true;
      MtCodec.var_1058.textColor = 16711680;
    }
    MtCodec.var_1058.text = MtCodec.var_1058.text + (class_691.method_97(param1) + "\n");
  }

  public static var name_36:class_886 = null;

  /**
   * JPEXS name: `var_64`
   */
  public var keyBuffer:Bytes;

  /**
   * JPEXS name: `var_1061`
   */
  public var codecKey:String;

  public function MtCodec() {
    var _loc4_:int = 0;
    var _loc6_:int = 0;
    var _loc7_:int = 0;
    if (class_899.var_239) {
      return;
    }
    if (MtCodec.codecSalt == null) {
      MtCodec.codecSalt = "";
    }
    codecKey = MtCodec.getPlayerParam("k");
    if (codecKey == null) {
      class_899.var_317 = new Error();
      throw "Missing key";
    }
    var _loc1_:String = MtCodec.codecSalt + codecKey;
    var buf:Bytes = Bytes.alloc(256);
    var _loc3_:int = 0;
    while (_loc3_ < 256) {
      _loc3_++;
      _loc4_ = _loc3_;
      buf.b[_loc4_] = _loc4_ & 127;
    }
    _loc3_ = 0;
    _loc4_ = _loc1_.length;
    var _loc5_:int = 0;
    while (_loc5_ < 256) {
      _loc5_++;
      _loc6_ = _loc5_;
      _loc3_ = int(_loc3_ + int(buf.b[_loc6_]) + _loc1_.charCodeAt(int(_loc6_ % _loc4_))) & 127;
      _loc7_ = int(buf.b[_loc6_]);
      buf.b[_loc6_] = int(buf.b[_loc3_]);
      buf.b[_loc3_] = _loc7_;
    }
    keyBuffer = buf;
  }

  public static function method_743(param1:String, param2:*):void {
    MtCodec.var_936.method_216(param1, param2);
  }

  /**
   * JPEXS name: `method_744`
   */
  public static function getPlayerParam(name:String):String {
    return Reflect.field(App.rootMc.stage.loaderInfo.parameters, name);
  }

  /**
   * JPEXS name: `method_184`
   */
  public static function apiAction(param1:String, param2:*, param3:Function):void {
    var var_1060:Function = param3;
    var _loc4_:class_886 = new class_886(param1);
    var var_68:MtCodec = new MtCodec();
    _loc4_.method_746("__d", var_68.encodeData(param2));
    _loc4_.var_1060 = function (param1:String):void {
      var _loc3_:* = null;
      try {
        _loc3_ = var_68.method_747(param1);
      } catch (_loc4_:*) {
        if (null as Error) {
          class_899.var_317 = null;
        }
        MtCodec.var_1059(null);
        return;
      }
      var_1060(_loc3_);
    };
    _loc4_.name_95 = MtCodec.var_1059;
    _loc4_.method_748(true);
    MtCodec.name_36 = _loc4_;
  }

  public static function method_70(param1:String):* {
    var _loc2_:String = MtCodec.getPlayerParam(param1);
    if (_loc2_ == null) {
      class_899.var_317 = new Error();
      throw "Missing data \'" + param1 + "\'";
    }
    return new MtCodec().method_747(_loc2_);
  }

  public function method_747(param1:String):* {
    var _loc3_:Bytes = null;
    var _loc4_:StringBuf = null;
    var _loc5_:int = 0;
    var _loc6_:int = 0;
    var _loc7_:int = 0;
    var _loc8_:int = 0;
    var _loc9_:int = 0;
    var _loc10_:* = null;
    var _loc11_:int = 0;
    var _loc12_:int = 0;
    var _loc13_:class_722 = null;
    var _loc2_:Boolean = param1.substr(0, codecKey.length) == codecKey;
    if (_loc2_) {
      MtCodec.fl_disableEncryption = true;
    }
    if (MtCodec.fl_disableEncryption) {
      if (param1.substr(0, codecKey.length) != codecKey) {
        class_899.var_317 = new Error();
        throw "Invalid key";
      }
      param1 = param1.substr(codecKey.length);
    } else {
      _loc3_ = keyBuffer;
      _loc4_ = new StringBuf();
      _loc5_ = param1.length - 4;
      _loc6_ = int(_loc3_.b[0]);
      _loc7_ = int(_loc3_.b[1]);
      _loc8_ = 0;
      while (_loc8_ < _loc5_) {
        _loc8_++;
        _loc9_ = _loc8_;
        _loc10_ = param1.charCodeAt(_loc9_);
        _loc11_ = _loc10_ ^ int(_loc3_.b[_loc9_ & 255]);
        _loc12_ = _loc11_ == 0 ? _loc10_ : _loc11_;
        _loc4_.b = _loc4_.b + String.fromCharCode(_loc12_);
        if (_loc11_ == 0) {
          _loc10_ = 0;
        }
        _loc6_ = int((int(_loc6_ + _loc10_)) % 65521);
        _loc7_ = int((_loc7_ + _loc6_) % 65521);
      }
      _loc8_ = _loc6_ ^ _loc7_ << 8;
      _loc5_++;
      if (param1.charCodeAt(_loc5_) != MtCodec.BASE64_URL.charCodeAt(_loc8_ & 63) || param1.charCodeAt(_loc5_) != MtCodec.BASE64_URL.charCodeAt(_loc8_ >> 6 & 63) || param1.charCodeAt(_loc5_) != MtCodec.BASE64_URL.charCodeAt(_loc8_ >> 12 & 63) || param1.charCodeAt(_loc5_) != MtCodec.BASE64_URL.charCodeAt(_loc8_ >> 18 & 63)) {
        if (int(param1.indexOf("Maximum POST data")) != -1) {
          class_899.var_317 = new Error();
          throw param1;
        }
        if (param1.substr(0, 5) == "<!DOC") {
          _loc13_ = new class_722("id=\"error_msg\">([^<]+)</", "");
          if (_loc13_.method_432(param1)) {
            class_899.var_317 = new Error();
            throw _loc13_.method_431(1);
          }
        }
        class_899.var_317 = new Error();
        throw "FCHK";
      }
      param1 = _loc4_.b;
    }
    var _loc14_:Unserializer = new Unserializer(param1);
    _loc14_.setResolver({
      "resolveEnum": function (param1:String):* {
        var _loc2_:* = MtCodec.var_936.method_98(param1);
        if (_loc2_ != null) {
          return _loc2_;
        }
        return Type.method_749(param1);
      },
      "resolveClass": function (param1:String):* {
        var _loc2_:* = MtCodec.var_936.method_98(param1);
        if (_loc2_ != null) {
          return _loc2_;
        }
        return Type.method_750(param1);
      }
    });
    return _loc14_.unserialize();
  }

  /**
   * JPEXS name: `method_752`
   */
  public function encryptData(dataString:String):String {
    var _loc8_:int = 0;
    var _loc9_:* = null;
    var _loc10_:int = 0;
    var _loc11_:int = 0;
    if (MtCodec.fl_disableEncryption) {
      return codecKey + dataString;
    }
    var _loc2_:Bytes = keyBuffer;
    var _loc3_:StringBuf = new StringBuf();
    var _loc4_:int = int(_loc2_.b[0]);
    var _loc5_:int = int(_loc2_.b[1]);
    var _loc6_:int = 0;
    var _loc7_:int = dataString.length;
    while (_loc6_ < _loc7_) {
      _loc6_++;
      _loc8_ = _loc6_;
      _loc9_ = dataString.charCodeAt(_loc8_);
      _loc10_ = _loc9_ ^ int(_loc2_.b[_loc8_ & 255]);
      _loc11_ = _loc10_ == 0 ? _loc9_ : _loc10_;
      _loc3_.b = _loc3_.b + String.fromCharCode(_loc11_);
      _loc4_ = int((_loc4_ + _loc10_) % 65521);
      _loc5_ = int((_loc5_ + _loc4_) % 65521);
    }
    _loc6_ = _loc4_ ^ _loc5_ << 8;
    _loc7_ = MtCodec.BASE64_URL.charCodeAt(_loc6_ & 63);
    _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
    _loc7_ = MtCodec.BASE64_URL.charCodeAt(_loc6_ >> 6 & 63);
    _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
    _loc7_ = MtCodec.BASE64_URL.charCodeAt(_loc6_ >> 12 & 63);
    _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
    _loc7_ = MtCodec.BASE64_URL.charCodeAt(_loc6_ >> 18 & 63);
    _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
    return _loc3_.b;
  }

  /**
   * JPEXS name: `method_745`
   */
  public function encodeData(param1:*, param2:Object = undefined):String {
    var _loc3_:Serializer = new Serializer();
    _loc3_.useEnumIndex = true;
    if (param2) {
      _loc3_.serializeException(param1);
    } else {
      _loc3_.serialize(param1);
    }
    return encryptData(_loc3_.toString());
  }
}
}
