package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_717 extends class_645
   {
       
      
      public var var_655:Array;
      
      public var var_351:Number;
      
      public var name_61:MovieClip;
      
      public var var_657:int;
      
      public var var_562:Object;
      
      public var var_656:Array;
      
      public var var_25:MovieClip;
      
      public var var_69:MovieClip;
      
      public var var_142:int;
      
      public var var_658:Boolean;
      
      public var var_659:MovieClip;
      
      public var var_248:Number;
      
      public function class_717()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:MovieClip = null;
         var _loc4_:class_892 = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               name_61.x = Number(name_61.x + var_351);
               if(name_61.x > var_142)
               {
                  method_81(false,20);
                  name_3 = 2;
                  class_319.play();
                  _loc1_ = 0;
                  _loc2_ = var_656;
                  while(_loc1_ < int(_loc2_.length))
                  {
                     _loc3_ = _loc2_[_loc1_];
                     _loc1_++;
                     _loc3_.gotoAndPlay(27 + int(class_691.method_114(7)));
                  }
                  return;
               }
               var_69.x = name_61.x;
               var_69.scaleX = (class_742.var_216 - name_61.x) * 0.01;
               _loc2_ = var_655.method_78();
               _loc1_ = 0;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc4_ = _loc2_[_loc1_];
                  _loc1_++;
                  if(_loc4_.var_245 > name_61.y)
                  {
                     if(Number(Math.abs(name_61.x - _loc4_.var_244)) < 10)
                     {
                        name_61.gotoAndPlay("smoke");
                        method_81(true,20);
                        name_3 = 2;
                     }
                     method_411(_loc4_.var_244,name_61.y,_loc4_.var_278);
                     _loc4_.method_89();
                     var_655.method_116(_loc4_);
                  }
               }
               if(name_5)
               {
                  if(var_562 == null)
                  {
                     var_562 = 0;
                     break;
                  }
                  var_562 = Number(Math.min(Number(var_562 + 0.5),10));
                  var_25.gotoAndStop(int(Math.round(Number(var_562 + 20))));
                  break;
               }
               if(var_562 != null)
               {
                  method_312();
                  break;
               }
               break;
         }
      }
      
      public function method_312() : void
      {
         var _loc1_:class_892 = null;
         var _loc2_:class_892 = null;
         if(var_562 > 2.5)
         {
            _loc2_ = new class_892(var_232.method_84(class_899.__unprotect__("x\x14s\x02"),class_645.var_209));
            _loc2_.var_233 = 0.99;
            _loc1_ = _loc2_;
            _loc1_.var_244 = var_25.x - 39;
            _loc1_.var_245 = var_25.y - 63;
            _loc1_.var_278 = Math.cos(var_248) * var_562 * 0.8;
            _loc1_.var_279 = Math.sin(var_248) * var_562 * 0.8;
            _loc1_.var_250 = 0.5;
            _loc1_.method_118();
            _loc1_.var_243.scaleX = 0.6;
            _loc1_.var_243.scaleY = 0.6;
            var_655.push(_loc1_);
         }
         var_25.gotoAndStop("20");
         var_562 = null;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [540 - param1 * 300];
         super.method_95(param1);
         var_142 = 169;
         var_657 = 10;
         var_248 = -Math.PI * 0.75;
         var_351 = Number(0.5 + param1 * 1.5);
         var_655 = [];
         method_117();
         method_72();
      }
      
      public function method_411(param1:Number, param2:Number, param3:Number) : void
      {
         var _loc5_:int = 0;
         var _loc6_:class_892 = null;
         var _loc7_:class_892 = null;
         var _loc4_:int = 0;
         while(_loc4_ < 10)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc7_ = new class_892(var_232.method_84(class_899.__unprotect__("\x01h1N"),class_645.var_209));
            _loc7_.var_233 = 0.99;
            _loc6_ = _loc7_;
            _loc6_.var_244 = param1;
            _loc6_.var_245 = param2;
            _loc6_.var_278 = Number(5 * (Math.random() * 2 - 1) + param3);
            _loc6_.var_279 = -(3 + Math.random() * 8);
            _loc6_.var_344 = Number(0.4 + Math.random() * 0.6);
            _loc6_.var_250 = 0.5;
            _loc6_.var_251 = 10 + int(class_691.method_114(10));
            _loc6_.var_272 = 0;
            _loc6_.var_243.alpha = 0.6;
            _loc6_.method_118();
         }
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\';~ \x01"),0);
         var_659 = Reflect.field(class_319,"_bomb");
         var_25 = Reflect.field(class_319,"_monster");
         name_61 = Reflect.field(class_319,"_spark");
         var_69 = Reflect.field(class_319,"_mask");
         var_656 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 8)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = Reflect.field(class_319,"_p" + _loc2_);
            if(_loc3_ != null)
            {
               _loc3_.gotoAndPlay(10);
               var_656.push(_loc3_);
            }
         }
      }
   }
}
