package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_667
   {
       
      
      public var var_371:Number;
      
      public var var_365:Boolean;
      
      public var var_372:Boolean;
      
      public var var_357:Array;
      
      public var var_375:Boolean;
      
      public var var_374:Number;
      
      public var var_373:class_654;
      
      public var var_376:Number;
      
      public var var_377:Boolean;
      
      public var var_232:class_755;
      
      public var var_378:int;
      
      public var var_379:int;
      
      public function class_667(param1:Sprite = undefined, param2:int = 0, param3:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(param1 == null)
         {
            class_899.var_317 = new Error();
            throw "root is null";
         }
         var_379 = 0;
         var_373 = new class_654();
         var_371 = param2;
         var_376 = param3;
         var_232 = new class_755(param1);
      }
      
      public function method_79(param1:Number, param2:Number, param3:Number) : void
      {
         var _loc5_:* = null;
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         if(param1 == 0 && param3 == 0)
         {
            return;
         }
         var _loc4_:* = var_373.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc6_ = _loc5_.var_357;
            _loc7_ = 0;
            while(_loc7_ < int(_loc6_.length))
            {
               _loc8_ = _loc6_[_loc7_];
               _loc7_++;
               if(param1 != 0)
               {
                  if(_loc8_.x <= 0 && !_loc8_.name_31)
                  {
                     method_124(_loc5_,Number(_loc8_.x + _loc8_.width));
                     _loc8_.name_31 = true;
                  }
                  if(Number(_loc8_.x + _loc8_.width) <= 0)
                  {
                     _loc6_.method_116(_loc8_);
                     _loc8_.parent.removeChild(_loc8_);
                  }
                  _loc8_.x = _loc8_.x - (Number(param1 + (param2 < 0 && Number(Math.abs(param2)) > param1?Number(0):param2)));
               }
               if(param3 != 0)
               {
                  if(Number(Math.abs(Number(_loc8_.name_32))) < var_374)
                  {
                     _loc8_.y = _loc8_.y - param3;
                     _loc8_.name_32 = Number(Number(_loc8_.name_32) + param3);
                  }
                  else if(Number(_loc8_.name_32) <= 0 && Number(_loc8_.name_32) <= var_374 && param3 > 0)
                  {
                     if(_loc8_.y < var_374)
                     {
                        _loc8_.y = _loc8_.y - param3;
                        _loc8_.name_32 = Number(Number(_loc8_.name_32) + param3);
                     }
                  }
                  else if(Number(_loc8_.name_32) >= 0 && Number(_loc8_.name_32) >= var_374 && param3 < 0 && _loc8_.y <= 0)
                  {
                     _loc8_.y = _loc8_.y - param3;
                     _loc8_.name_32 = Number(Number(_loc8_.name_32) + param3);
                  }
               }
            }
         }
      }
      
      public function method_266() : void
      {
         var _loc3_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_357;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            var_357.method_116(_loc3_);
            _loc3_.parent.removeChild(_loc3_);
         }
         var_357 = null;
      }
      
      public function method_263(param1:String, param2:Number = 0.0, param3:Boolean = false, param4:Boolean = true) : void
      {
         if(param1 == null)
         {
            class_899.var_317 = new Error();
            throw "linkage needed";
         }
         var _loc6_:int = var_379 + 1;
         var_379 = _loc6_;
         var_373.method_216(_loc6_,{
            "d\x0ft\x01":var_379,
            "xISj\x01":[],
            "\x14&`\x02\x03":param1,
            "\x07\fbd\x01":param2,
            "P\x07){\x03":param3,
            "a\x13s\x01":param4
         });
         method_124(var_373.method_98(var_379),0);
      }
      
      public function method_124(param1:Object, param2:Number) : void
      {
         if(param1 == null)
         {
            class_899.var_317 = new Error();
            throw "unknown layer";
         }
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__(param1.name_33),int(param1.name_34));
         var _loc4_:Number = param2;
         _loc3_.x = _loc4_;
         _loc3_.x = _loc4_;
         if(!var_377)
         {
            if(_loc3_.height >= var_376)
            {
               var_374 = (_loc3_.height - var_376) / 2;
            }
            else
            {
               var_374 = (var_376 - _loc3_.height) / 2;
            }
            var_377 = true;
         }
         _loc4_ = -var_374;
         _loc3_.y = _loc4_;
         _loc3_.y = _loc4_;
         _loc3_.name_32 = 0;
         param1.var_357.push(_loc3_);
      }
   }
}
