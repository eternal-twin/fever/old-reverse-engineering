package
{
   import flash.utils.Dictionary;
   import package_32.class_899;
   
   public class Xml
   {
      
      public static var class_738:String = "element";
      
      public static var var_1062:String = "pcdata";
      
      public static var var_1063:String = "cdata";
      
      public static var var_1064:String = "comment";
      
      public static var var_1065:String = "doctype";
      
      public static var var_1066:String = "prolog";
      
      public static var class_771:String = "document";
       
      
      public var var_428:Xml;
      
      public var var_1067:String;
      
      public var var_880:String;
      
      public var var_1068:String;
      
      public var _node:XML;
      
      public var _map:Dictionary;
      
      public function Xml()
      {
      }
      
      public static function method_654(param1:String) : Xml
      {
         var _loc5_:TypeError = null;
         var _loc6_:class_722 = null;
         var _loc7_:String = null;
         XML.ignoreWhitespace = false;
         XML.ignoreProcessingInstructions = false;
         XML.ignoreComments = false;
         var _loc4_:XML = null;
         while(_loc4_ == null)
         {
            try
            {
               _loc4_ = new XML("<__document" + ">" + param1 + "</__document>");
            }
            catch(:TypeError;)
            {
               _loc5_ = ;
               if(_loc5_ as Error)
               {
                  class_899.var_317 = _loc5_;
               }
               _loc6_ = new class_722("\"([^\"]+)\"","");
               if(_loc5_.errorID == 1083 && Boolean(_loc6_.method_432(_loc5_.message)))
               {
                  _loc7_ = _loc6_.method_431(1);
                  var _loc3_:String = "<__document" + (" xmlns:" + _loc7_ + "=\"@" + _loc7_ + "\"");
                  continue;
               }
               class_899.var_317 = new Error();
               throw _loc5_;
            }
         }
         return Xml.method_754(null,_loc4_,Xml.class_771);
      }
      
      public static function method_674(param1:String) : Xml
      {
         return Xml.method_754(null,new XML("<" + param1 + "/>"),Xml.class_738);
      }
      
      public static function method_677(param1:String) : Xml
      {
         XML.ignoreWhitespace = false;
         return Xml.method_754(null,new XML(param1),Xml.var_1062);
      }
      
      public static function method_755(param1:String) : Xml
      {
         return Xml.method_754(null,new XML("<![CDATA[" + param1 + "]]>"),Xml.var_1063);
      }
      
      public static function method_756(param1:String) : Xml
      {
         XML.ignoreComments = false;
         return Xml.method_754(null,new XML("<!--" + param1 + "-->"),Xml.var_1064);
      }
      
      public static function NGV(param1:String) : Xml
      {
         return Xml.method_754(null,new XML("<!DOCTYPE " + param1 + ">"),Xml.var_1065);
      }
      
      public static function method_757(param1:String) : Xml
      {
         XML.ignoreProcessingInstructions = false;
         return Xml.method_754(null,new XML("<?" + param1 + "?>"),Xml.var_1066);
      }
      
      public static function method_673() : Xml
      {
         return Xml.method_754(null,<__document/>,Xml.class_771);
      }
      
      public static function method_758(param1:XML) : String
      {
         var _loc2_:String = param1.nodeKind();
         if(_loc2_ == "element")
         {
            return Xml.class_738;
         }
         if(_loc2_ == "text")
         {
            return Xml.var_1062;
         }
         if(_loc2_ == "processing-instruction")
         {
            return Xml.var_1066;
         }
         if(_loc2_ == "comment")
         {
            return Xml.var_1064;
         }
         class_899.var_317 = new Error();
         throw "unimplemented node type: " + param1.nodeType;
      }
      
      public static function method_754(param1:Dictionary, param2:XML, param3:String = undefined) : Xml
      {
         if(param1 == null)
         {
            param1 = new Dictionary();
         }
         var _loc4_:Xml = param1[param2];
         if(_loc4_ == null)
         {
            _loc4_ = new Xml();
            _loc4_._node = param2;
            _loc4_._map = param1;
            _loc4_.var_880 = param3 != null?param3:Xml.method_758(param2);
            param1[param2] = _loc4_;
         }
         return _loc4_;
      }
      
      public function method_759(param1:XMLList) : Array
      {
         var _loc5_:int = 0;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = int(param1.length());
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_.push(Xml.method_754(_map,param1[_loc5_]));
         }
         return _loc2_;
      }
      
      public function toString() : String
      {
         var _loc1_:String = null;
         XML.prettyPrinting = false;
         if(var_880 == Xml.class_771)
         {
            _loc1_ = _node.toXMLString();
            _loc1_ = _loc1_.substr(int(_loc1_.indexOf(">")) + 1);
            _loc1_ = _loc1_.substr(0,_loc1_.length - 13);
            return _loc1_;
         }
         return _node.toXMLString();
      }
      
      public function method_760(param1:String) : String
      {
         var _loc5_:int = 0;
         var _loc6_:XMLList = null;
         var _loc2_:String = var_880;
         var _loc3_:Xml = null;
         if(_loc2_ == Xml.class_738 || _loc2_ == Xml.class_771)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         if(_loc2_ == Xml.var_1062)
         {
            _loc3_ = Xml.method_677(param1);
         }
         else if(_loc2_ == Xml.var_1063)
         {
            _loc3_ = Xml.method_755(param1);
         }
         else if(_loc2_ == Xml.var_1064)
         {
            _loc3_ = Xml.method_756(param1);
         }
         else if(_loc2_ == Xml.var_1065)
         {
            _loc3_ = Xml.NGV(param1);
         }
         else
         {
            _loc3_ = Xml.method_757(param1);
         }
         var _loc4_:XML = _node.parent();
         if(_loc4_ != null)
         {
            _loc4_.insertChildAfter(_node,_loc3_._node);
            _loc5_ = int(_node.childIndex());
            _loc6_ = _loc4_.children();
            delete _loc6_[Reflect.method_649(_loc6_)[_loc5_]];
         }
         _node = _loc3_._node;
         _map[_node] = this;
         return param1;
      }
      
      public function method_761(param1:String) : String
      {
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _node.setLocalName(param1);
         }
         else
         {
            _node.setLocalName(_loc2_[1]);
            _node.setNamespace(_node.namespace(_loc2_[0]));
         }
         return param1;
      }
      
      public function method_216(param1:String, param2:String) : void
      {
         var _loc4_:Namespace = null;
         var _loc5_:XMLList = null;
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc3_:Array = param1.split(":");
         if(_loc3_[0] == "xmlns")
         {
            _loc4_ = _node.namespace(_loc3_[1] == null?"":_loc3_[1]);
            if(_loc4_ != null)
            {
               class_899.var_317 = new Error();
               throw "Can\'t modify namespace";
            }
            if(_loc3_[1] == null)
            {
               class_899.var_317 = new Error();
               throw "Can\'t set default namespace";
            }
            _node.addNamespace(new Namespace(_loc3_[1],param2));
            return;
         }
         if(int(_loc3_.length) == 1)
         {
            _node["@" + param1] = param2;
         }
         else
         {
            _loc5_ = method_762(_node,_loc3_);
            _loc5_[0] = param2;
         }
      }
      
      public function method_763(param1:Xml) : Boolean
      {
         var _loc2_:XMLList = _node.children();
         if(_loc2_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         if(_node != param1._node.parent())
         {
            return false;
         }
         var _loc3_:int = int(param1._node.childIndex());
         delete _loc2_[Reflect.method_649(_loc2_)[_loc3_]];
         return true;
      }
      
      public function method_116(param1:String) : void
      {
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            Reflect.method_740(_node,"@" + param1);
         }
         else
         {
            delete method_762(_node,_loc2_)[0];
         }
      }
      
      public function method_73() : Object
      {
         var _loc1_:XMLList = _node.children();
         if(_loc1_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         var var_1069:Array = method_759(_loc1_);
         var var_452:int = 0;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_452 < int(var_1069.length);
            },
            "?\x05\x10;\x01":function():Xml
            {
               var _loc1_:int = var_452;
               var_452 = var_452 + 1;
               return var_1069[_loc1_];
            }
         };
      }
      
      public function method_764(param1:Xml, param2:int) : void
      {
         var _loc3_:XMLList = _node.children();
         if(_loc3_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         if(param2 < int(_loc3_.length()))
         {
            _node.insertChildBefore(_loc3_[param2],param1._node);
         }
         else
         {
            _node.appendChild(param1._node);
         }
      }
      
      public function method_765() : Xml
      {
         return Xml.method_754(_map,_node.parent());
      }
      
      public function method_766() : String
      {
         var _loc1_:String = var_880;
         if(_loc1_ == Xml.class_738 || _loc1_ == Xml.class_771)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         if(_loc1_ == Xml.var_1064)
         {
            return _node.toString().substr(4,-7);
         }
         return _node.toString();
      }
      
      public function method_664() : String
      {
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc1_:Namespace = _node.namespace();
         return _loc1_.prefix == ""?_node.localName():_loc1_.prefix + ":" + _node.localName();
      }
      
      public function method_762(param1:XML, param2:Array) : XMLList
      {
         var _loc4_:XML = null;
         var _loc3_:Namespace = param1.namespace(param2[0]);
         if(_loc3_ == null)
         {
            _loc4_ = param1.parent();
            if(_loc4_ == null)
            {
               _loc3_ = new Namespace(param2[0],"@" + param2[0]);
               param1.addNamespace(_loc3_);
            }
            else
            {
               return method_762(_loc4_,param2);
            }
         }
         return _node.attribute(new QName(_loc3_,param2[1]));
      }
      
      public function method_98(param1:String) : String
      {
         var _loc3_:Namespace = null;
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            _loc3_ = _node.namespace(_loc2_[1] == null?"":_loc2_[1]);
            return _loc3_ == null?null:_loc3_.uri;
         }
         if(int(_loc2_.length) == 1)
         {
            if(!Reflect.method_679(_node,"@" + param1))
            {
               return null;
            }
            return Reflect.field(_node,"@" + param1);
         }
         var _loc4_:XMLList = method_762(_node,_loc2_);
         return int(_loc4_.length()) == 0?null:_loc4_.toString();
      }
      
      public function method_656() : Xml
      {
         var _loc1_:XMLList = _node.elements();
         if(_loc1_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.method_754(_map,_loc1_[0]);
      }
      
      public function method_767() : Xml
      {
         var _loc1_:XMLList = _node.children();
         if(_loc1_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.method_754(_map,_loc1_[0]);
      }
      
      public function method_217(param1:String) : Boolean
      {
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            return _node.namespace(_loc2_[1] == null?"":_loc2_[1]) != null;
         }
         if(int(_loc2_.length) == 1)
         {
            return Boolean(Reflect.method_679(_node,"@" + param1));
         }
         return int(method_762(_node,_loc2_).length()) > 0;
      }
      
      public function method_768(param1:String) : Object
      {
         var _loc3_:XMLList = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:Xml = null;
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _loc3_ = _node.elements(param1);
         }
         else
         {
            _loc3_ = _node.elements();
         }
         if(_loc3_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         var var_1069:Array = method_759(_loc3_);
         if(int(_loc2_.length) != 1)
         {
            _loc4_ = 0;
            _loc5_ = var_1069.method_78();
            while(_loc4_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc4_];
               _loc4_++;
               if(_loc6_._node.localName() != _loc2_[1] || _loc6_._node.namespace().prefix != _loc2_[0])
               {
                  var_1069.method_116(_loc6_);
               }
            }
         }
         var var_452:int = 0;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_452 < int(var_1069.length);
            },
            "?\x05\x10;\x01":function():Xml
            {
               var _loc1_:int = var_452;
               var_452 = var_452 + 1;
               return var_1069[_loc1_];
            }
         };
      }
      
      public function name_78() : Object
      {
         var _loc1_:XMLList = _node.elements();
         if(_loc1_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         var var_1069:Array = method_759(_loc1_);
         var var_452:int = 0;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_452 < int(var_1069.length);
            },
            "?\x05\x10;\x01":function():Xml
            {
               var _loc1_:int = var_452;
               var_452 = var_452 + 1;
               return var_1069[_loc1_];
            }
         };
      }
      
      public function name_106() : Object
      {
         if(var_880 != Xml.class_738)
         {
            class_899.var_317 = new Error();
            throw "bad nodeType";
         }
         var name_106:XMLList = _node.attributes();
         var var_1070:Array = Reflect.method_649(name_106);
         var var_452:int = 0;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_452 < int(var_1070.length);
            },
            "?\x05\x10;\x01":function():String
            {
               var _loc1_:int = var_452;
               var_452 = var_452 + 1;
               return name_106[class_691.method_352(var_1070[_loc1_])].name();
            }
         };
      }
      
      public function method_672(param1:Xml) : void
      {
         var _loc2_:XMLList = _node.children();
         if(_loc2_ == null)
         {
            class_899.var_317 = new Error();
            throw "bad nodetype";
         }
         _node.appendChild(param1._node);
      }
   }
}
