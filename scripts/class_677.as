package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_32.class_899;
   import package_47.package_48.name_43;
   import package_47.package_48.name_44;
   
   public class class_677 extends class_645
   {
      
      public static var var_456:int = 10;
      
      public static var var_457:int = 10;
       
      
      public var var_351:int;
      
      public var var_464:Number;
      
      public var name_30:Array;
      
      public var var_460:int;
      
      public var package_15:Number;
      
      public var var_338:BitmapData;
      
      public var var_288:class_892;
      
      public var var_462:Number;
      
      public var var_461:Array;
      
      public var var_459:MovieClip;
      
      public var var_458:MovieClip;
      
      public function class_677()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_298() : void
      {
         var _loc3_:class_892 = null;
         var _loc1_:int = 0;
         var _loc2_:Array = name_30;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_244 = _loc3_.var_244 - var_351;
            _loc3_.var_244 = int(_loc3_.var_244);
            _loc3_.var_245 = int(_loc3_.var_245);
         }
         var_458.x = var_458.x - var_351 * 0.25;
         var_459.x = var_459.x - var_351 * 0.5;
         if(var_458.x < -100)
         {
            var_458.x = Number(var_458.x + 100);
         }
         if(var_459.x < -100)
         {
            var_459.x = Number(var_459.x + 100);
         }
      }
      
      public function method_299() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:class_892 = null;
         var _loc10_:class_892 = null;
         var _loc12_:ColorTransform = null;
         var _loc13_:Matrix = null;
         if(var_288 == null)
         {
            return;
         }
         var_288.var_243.gotoAndStop(2);
         if(name_5)
         {
            var_288.var_279 = var_288.var_279 - 0.18;
            var_288.var_243.gotoAndStop(1);
         }
         var _loc1_:int = int((var_288.var_244 + var_460 - 0.5) / class_677.var_456);
         var _loc2_:int = int((var_288.var_245 - 0.5) / class_677.var_456);
         if(var_461[_loc1_][_loc2_] == 1 || var_288.var_245 < -class_677.var_456 || var_288.var_245 > 100 + class_677.var_456)
         {
            _loc3_ = 24;
            _loc4_ = 0;
            while(_loc4_ < _loc3_)
            {
               _loc4_++;
               _loc5_ = _loc4_;
               _loc6_ = (_loc5_ + Number(Math.random())) / _loc3_ * 6.28;
               _loc7_ = Math.random() * 1.5;
               _loc8_ = 2;
               _loc10_ = new class_892(var_232.method_84(class_899.__unprotect__("xE6p\x02"),class_645.var_209));
               _loc10_.var_233 = 0.99;
               _loc9_ = _loc10_;
               _loc9_.var_278 = Number(Math.cos(_loc6_) * _loc7_ + Math.random() * 2);
               _loc9_.var_279 = Math.sin(_loc6_) * _loc7_;
               _loc9_.var_244 = Number(var_288.var_244 + _loc9_.var_278 * _loc8_);
               _loc9_.var_245 = Number(var_288.var_245 + _loc9_.var_279 * _loc8_);
               _loc9_.var_251 = 10 + int(class_691.method_114(10));
               _loc9_.var_272 = 0;
               _loc9_.var_250 = Number(0.05 + Math.random() * 0.05);
               _loc9_.var_243.gotoAndStop(int(class_691.method_114(2)) + 1);
               name_30.push(_loc9_);
            }
            var_288.method_89();
            var_288 = null;
            method_81(false,10);
            method_102(32);
         }
         if(var_288 == null)
         {

         }
      }
      
      override public function method_79() : void
      {
         var_464 = var_464 - (Number(0.01 + 0.03 * var_237[0]));
         if(var_464 < 2)
         {
            var_464 = 2;
         }
         var_460 = var_460 + var_351;
         var_338.scroll(-var_351,0);
         while(var_460 > class_677.var_456)
         {
            var_460 = var_460 - class_677.var_456;
            var_461.shift();
            method_300();
         }
         method_299();
         method_298();
         if(var_227[0] == 0)
         {
            method_81(true);
         }
         super.method_79();
      }
      
      public function method_300() : void
      {
         var _loc1_:Array = null;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc6_:* = null;
         var _loc7_:Number = NaN;
         var _loc8_:Array = null;
         var _loc10_:Matrix = null;
         package_15 = Number(package_15 + 0.2);
         if(int(class_691.method_114(6)) == 0)
         {
            package_15 = Number(package_15 + 0.5 * var_237[0]);
         }
         if(int(var_461.length) > 0)
         {
            _loc1_ = var_461[int(var_461.length) - 1];
         }
         else
         {
            _loc1_ = [];
            _loc2_ = 0;
            _loc3_ = class_677.var_457;
            while(_loc2_ < _loc3_)
            {
               _loc2_++;
               _loc4_ = _loc2_;
               _loc1_.push(0);
            }
         }
         var _loc5_:Array = _loc1_.method_78();
         while(package_15 >= 1)
         {
            package_15 = package_15 - 1;
            _loc2_ = 0;
            _loc3_ = 0;
            while(_loc3_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc3_];
               _loc3_++;
               if(_loc6_ == 0)
               {
                  _loc2_++;
               }
            }
            _loc7_ = _loc2_ - var_464;
            if(Math.random() * 2 - 1 > _loc7_)
            {
               _loc8_ = method_301(_loc5_,0);
               _loc3_ = int(_loc8_[int(class_691.method_114(int(_loc8_.length)))]);
               _loc5_[_loc3_] = 0;
            }
            if(Math.random() * 2 - 1 < _loc7_)
            {
               _loc8_ = method_301(_loc5_,1);
               _loc3_ = int(_loc8_[int(class_691.method_114(int(_loc8_.length)))]);
               _loc5_[_loc3_] = 1;
            }
         }
         var _loc9_:MovieClip = var_232.method_84(class_899.__unprotect__("[dN[\x02"),0);
         _loc2_ = int(var_461.length) * class_677.var_456 - var_460;
         var_338.fillRect(new Rectangle(_loc2_,0,_loc2_ + class_677.var_456,100),0);
         _loc3_ = 0;
         while(_loc3_ < 10)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            if(_loc5_[_loc4_] == 1)
            {
               _loc10_ = new Matrix();
               _loc10_.translate(_loc2_,_loc4_ * class_677.var_456);
               _loc9_.gotoAndStop(int(class_691.method_114(_loc9_.totalFrames)) + 1);
               var_338.draw(_loc9_,_loc10_);
            }
         }
         _loc9_.parent.removeChild(_loc9_);
         var_461.push(_loc5_);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_229 = [true];
         package_15 = 0;
         var_460 = 0;
         var_351 = 3;
         var_464 = 10 - param1 * 2;
         var_462 = 0;
         name_30 = [];
         method_117();
      }
      
      public function method_301(param1:Array, param2:int) : Array
      {
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc8_:* = null;
         var _loc9_:* = null;
         var _loc10_:* = null;
         var _loc11_:* = null;
         var _loc12_:Boolean = false;
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         var _loc5_:int = class_677.var_457;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = param1[_loc6_ - 2];
            _loc8_ = param1[_loc6_ - 1];
            _loc9_ = param1[_loc6_];
            _loc10_ = param1[_loc6_ + 1];
            _loc11_ = param1[_loc6_ + 2];
            switch(param2)
            {
               case 0:
                  if(_loc9_ == 1 && (_loc8_ == 0 || _loc10_ == 0))
                  {
                     _loc3_.push(_loc6_);
                  }
                  continue;
               case 1:
                  _loc12_ = int(var_461.length) > 9 && int(class_691.method_114(15)) == 0 && var_237[0] > 0 && var_464 > 5 - var_237[0] * 2;
                  if(_loc9_ == 0 && (_loc8_ == 1 && _loc7_ != 0 || _loc10_ == 1 && _loc11_ != 0 || _loc8_ == null || _loc10_ == null || _loc12_))
                  {
                     _loc3_.push(_loc6_);
                  }

            }
         }
         return _loc3_;
      }
      
      public function method_117() : void
      {
         var_201.scaleY = Number(4);
         var_201.scaleX = 4;
         class_319 = var_232.method_84(class_899.__unprotect__("\x17,-,\x01"),0);
         var_458 = var_232.method_84(class_899.__unprotect__("!\x12?\x12"),0);
         var_459 = var_232.method_84(class_899.__unprotect__("\x07O\x1ff\x02"),0);
         var_338 = new BitmapData(100 + class_677.var_456,100,true,0);
         var_232.method_92(0).addChild(new Bitmap(var_338));
         var_461 = [];
         while(int(var_461.length) < 11)
         {
            method_300();
         }
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("gA\'\x15\x03"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var_288 = _loc2_;
         var_288.var_244 = 2.5 * class_677.var_456;
         var_288.var_245 = 50;
         var_288.var_250 = 0.1;
         var_288.var_233 = 0.98;
         var_288.var_279 = -2;
         var_288.method_118();
      }
   }
}
