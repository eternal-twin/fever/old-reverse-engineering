package
{
   import package_10.class_741;
   
   public class class_808 implements class_741
   {
      
      public static var var_370:Boolean;
      
      public static var var_1016:Array = ["Cocktail","Grimoire","Chaussures","Miroir","Trèfle","Poupée vaudou","Masque vaudou","Baguette","Lunettes","Radar","Prisme","FeverX","Parapluie","Dé","Moulin à vent","Glace chimique","Sablier","ChromaX","Bague magique","Fourchette enchantée","Fil d\'ariane","Rune Ini","Rune Sed","Rune Aarg","Rune Olh","Rune Al","Rune Laf","Rune Hep"];
      
      public static var var_1017:Array;
      
      public static var var_312:Array = ["Fulgo","Ignik","Rasen"];
      
      public static var var_1018:Array = ["Détruit le monstre le plus proche","Détruit tous les monstres sur l\'île","Détruit les monstres sur une ligne"];
      
      public static var var_313:Array = ["Camembeurk","Vol-o-vent","Burilame"];
      
      public static var var_1019:Array;
      
      public static var var_1020:Array = ["abonnement arc-en-ciel","abonnement glaçon"];
      
      public static var var_1021:Array;
      
      public static var var_722:Array = ["Koan","Barchenold","Piluvien","Dumerost","Chankron","Malvenel","Lifolet","Tarabluff","Sidron","Chomniber","Pata","Droenix","Lancurno","Jomil","Tokepo","Grazuli"];
      
      public static var var_721:String = "L\'oeil de %1 veille sur toi";
      
      public static var var_1022:String = "objet utilisé en permanence";
      
      public static var var_1023:String = "vous n\'avez plus de glaçons !";
      
      public static var var_619:String = "vous n\'avez plus d\'arc-en-ciel !";
      
      public static var var_1024:String = "Pas de cartouche de jeu !";
      
      public static var var_1025:String = "Aucune statue découverte !";
      
      public static var var_1026:String = "vous en avez déjà assez !";
      
      public static var var_1027:String = "Vous avez besoin d\'une clé !";
      
      public static var var_1028:String = "Vous avez besoin d\'une baguette !";
      
      public static var var_1029:Array = ["réceptacle cardiaque","Espace de stockage pour coeurs abandonnés","x quarts de coeurs","Retrouver les quarts manquants pour gagner une vie supplémentaire"];
      
      public static var var_1030:String = "Le grand bakélite Sargon anéanti, le portail dimensionnel tend désormais les bras à Pousty.\nLe plus valeureux des pingouins n\'hésite pas : malgré les blessures de son dernier combat, c\'est d\'un pied palmé décidé qu\'il franchit la mystérieuse porte...\nUn maelström de couleurs enveloppe Pousty ! Peu à peu, les énergies refluent. A quelques mètres, le passage semble ouvrir sur un autre monde ! Un monde semblable mais...\nMême d\'ici, on sent une atmosphère bien moins accueillante. Notre héros palmipède ressent déjà la menace Bakélite, et cette fois, ça ne sera pas une partie de plaisir !";
      
      public static var var_1031:String = "Voulez-vous rester sur l\'archipel de %1 pour éliminer vos derniers ennemis, ou faire le grand saut et rejoindre %2 pour une nouvelle aventure ?";
      
      public static var var_1032:String = "Retourner à %1";
      
      public static var var_1033:String = "Partir à %1";
      
      public static var var_1034:Array = ["Gonkrogme","Sultura","Baniflok","Grizantol","Marshoukrev","Dishigan","Lakulite","Koleporsh","Murumuru","Frisantheme","Zulebi"];
      
      public static var var_325:String = "Une erreur est survenue. Merci de relancer le jeu.";
      
      public static var var_838:String = "génération du monde...";
      
      public static var var_1035:String = "voir l\'inventaire";
      
      public static var var_1036:String = "retourner au jeu";
      
      public static var var_1037:String = "ile";
      
      public static var var_1038:String = "pas de monstre en vue !";
      
      public static var var_806:Array = ["jouer","étape"];
      
      public static var var_808:String = "selectionnez\n  une étape";
      
      public static var var_835:String = "connexion au serveur...";
       
      
      public function class_808()
      {
      }
      
      public static function package_21(param1:String, param2:String) : String
      {
         return "<font color=\'" + param2 + "\'>" + param1 + "</font>";
      }
      
      public static function method_248(param1:String) : String
      {
         return class_808.package_21(param1,"#777777");
      }
      
      public static function var_72(param1:String) : String
      {
         return class_808.package_21(param1,"#FF00FF");
      }
      
      public static function method_466(param1:String, param2:String, param3:String = undefined, param4:String = undefined, param5:String = undefined) : String
      {
         if(param3 == null)
         {
            param3 = "b";
         }
         if(param4 == null)
         {
            param4 = "c";
         }
         if(param5 == null)
         {
            param5 = "d";
         }
         param1 = class_761.name_54(param1,"%1",param2);
         param1 = class_761.name_54(param1,"%2",param3);
         param1 = class_761.name_54(param1,"%3",param4);
         param1 = class_761.name_54(param1,"%4",param5);
         return param1;
      }
      
      public static function method_95() : void
      {
         var _loc3_:Array = null;
         var _loc4_:String = null;
      }
   }
}
