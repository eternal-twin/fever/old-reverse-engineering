package {
import package_32.class_899;

/**
 * JPEXS name: `class_927`
 */
public class StringBuf {
  /**
   * JPEXS name: `var_12`
   */
  public var b:String;

  public function StringBuf() {
    if (class_899.var_239) {
      return;
    }
    b = "";
  }

  public function toString():String {
    return b;
  }

  /**
   * JPEXS name: `method_1119`
   */
  public function addSub(s:String, pos:int, len:Object = undefined):void {
    if (len == null) {
      b = b + s.substr(pos);
    } else {
      b = b + s.substr(pos, len);
    }
  }

  /**
   * JPEXS name: `method_1120`
   */
  public function addChar(param1:int):void {
    b = b + String.fromCharCode(param1);
  }

  /**
   * JPEXS name: `method_124`
   */
  public function add(param1:*):void {
    b = b + param1;
  }
}
}
