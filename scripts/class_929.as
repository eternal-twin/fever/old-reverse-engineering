package
{
   import flash.events.MouseEvent;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_32.class_899;
   import package_8.package_9.class_660;
   
   public class class_929 extends class_645
   {
       
      
      public var name_55:Array;
      
      public var var_1519:Array;
      
      public var var_254:Number;
      
      public var var_1176:Array;
      
      public function class_929()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_928 = null;
         var _loc6_:int = 0;
         var _loc7_:class_366 = null;
         super.method_79();
         if(var_220)
         {
            var_254 = (var_254 + 0.05) % 1;
            _loc1_ = var_254;
            _loc2_ = [];
            _loc3_ = 0;
            _loc4_ = var_1176;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc6_ = int(class_657.method_138(_loc1_));
               class_657.name_18(_loc5_,_loc6_,-100);
               _loc1_ = Number(_loc1_ + 0.05);
            }
            _loc3_ = 0;
            _loc4_ = var_1519;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc7_ = _loc4_[_loc3_];
               _loc3_++;
               _loc6_ = int(class_657.method_138(_loc1_));
               class_657.name_18(_loc7_,_loc6_,-100);
               _loc1_ = Number(_loc1_ + 0.05);
            }
         }
      }
      
      public function method_1122(param1:class_928, param2:class_928) : int
      {
         if(param1.var_215 < param2.var_215)
         {
            return -1;
         }
         return 1;
      }
      
      public function method_1016() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:class_928 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Boolean = false;
         var _loc12_:class_366 = null;
         if(name_55 != null)
         {
            _loc1_ = 0;
            _loc2_ = name_55;
            while(_loc1_ < int(_loc2_.length))
            {
               _loc3_ = _loc2_[_loc1_];
               _loc1_++;
               _loc3_.method_824();
            }
         }
         name_55 = [];
         var _loc4_:Boolean = true;
         _loc1_ = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc5_ = _loc1_;
            _loc6_ = 0;
            while(_loc6_ < 3)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               _loc8_ = 0;
               _loc9_ = 0;
               while(_loc9_ < 3)
               {
                  _loc9_++;
                  _loc10_ = _loc9_;
                  switch(_loc5_)
                  {
                     case 0:
                        _loc8_ = _loc8_ + (var_1176[_loc7_ * 3 + _loc10_].var_439 + 1);
                        continue;
                     case 1:
                        _loc8_ = _loc8_ + (var_1176[_loc10_ * 3 + _loc7_].var_439 + 1);

                  }
               }
               _loc11_ = _loc8_ == 15;
               _loc12_ = var_1519[_loc5_ * 3 + _loc7_];
               if(_loc12_ != null)
               {
                  _loc12_.gotoAndStop(!!_loc11_?2:1);
                  _loc12_.GLWA.text = class_691.method_97(_loc8_);
                  if(!_loc11_)
                  {
                     _loc4_ = false;
                  }
               }
            }
         }
         if(_loc4_)
         {
            method_81(true,30);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc5_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_366 = null;
         var_227 = [800];
         super.method_95(param1);
         var _loc2_:int = 1 + int(Math.round(param1 * 5));
         class_319 = new class_346();
         var_232.method_124(class_319,0);
         var_1176 = [];
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         while(_loc4_ < 9)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc3_.push(_loc5_);
         }
         class_659.method_241(_loc3_);
         _loc4_ = 0;
         while(_loc4_ < 9)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            var var_631:Array = [new class_928(int(_loc5_ % 3),int(_loc5_ / 3),int(_loc3_[_loc5_]),_loc5_)];
            var_1176.push(var_631[0]);
            var_232.method_124(var_631[0],1);
            var var_214:Array = [this];
            if(_loc5_ != 4)
            {
               var_631[0].addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
               {
                  var var_214:Array = param1;
                  var var_631:Array = param2;
                  return function(param1:*):void
                  {
                     var_214[0].method_1123(var_631[0]);
                  };
               }(var_214,var_631));
            }
         }
         _loc4_ = class_928.var_1010;
         _loc5_ = class_928.var_333;
         var_1519 = [];
         var _loc6_:int = 0;
         while(_loc6_ < _loc2_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc8_ = new class_366();
            var_232.method_124(_loc8_,1);
            if(_loc7_ < 3)
            {
               _loc8_.x = _loc4_ + _loc5_ * 3 - 6;
               _loc8_.y = _loc4_ + _loc7_ * _loc5_;
            }
            else
            {
               _loc8_.x = _loc4_ + (_loc7_ - 3) * _loc5_;
               _loc8_.y = _loc4_ + _loc5_ * 3 - 10;
            }
            _loc8_.var_106.gotoAndStop(_loc7_ < 3?1:2);
            var_1519.push(_loc8_);
         }
         var_254 = 0;
         method_1016();
      }
      
      public function method_1123(param1:class_928) : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:class_928 = null;
         var _loc6_:class_660 = null;
         if(int(name_55.length) == 2)
         {
            return;
         }
         param1.var_10();
         name_55.push(param1);
         if(int(name_55.length) == 2)
         {
            _loc2_ = [name_55[1].var_215,name_55[0].var_215];
            _loc3_ = 0;
            while(_loc3_ < 2)
            {
               _loc3_++;
               _loc4_ = _loc3_;
               _loc5_ = name_55[_loc4_];
               var_232.method_110(_loc5_);
               _loc6_ = new class_660(name_55[_loc4_],name_55[1 - _loc4_].x,name_55[1 - _loc4_].y);
               _loc6_.method_122();
               if(_loc4_ == 0)
               {
                  _loc6_.var_258 = method_1016;
               }
               _loc5_.var_215 = int(_loc2_[_loc4_]);
            }
            var_1176.sort(method_1122);
         }
      }
   }
}
