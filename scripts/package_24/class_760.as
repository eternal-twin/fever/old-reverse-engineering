package package_24
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import flash.filters.ColorMatrixFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import package_32.class_899;
   
   public class class_760 extends Bitmap
   {
      
      public static var var_214:class_760;
       
      
      public var var_280:Sprite;
      
      public var var_344:Number;
      
      public var var_812:Array;
      
      public function class_760(param1:Sprite = undefined, param2:int = 0, param3:int = 0, param4:Number = 1.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         class_760.var_214 = this;
         super();
         var_280 = param1;
         var_344 = param4;
         bitmapData = new BitmapData(int(param2 / param4),int(param3 / param4),false,0);
         scaleX = param4;
         scaleY = param4;
         var_812 = [];
      }
      
      public function method_79() : void
      {
         var _loc1_:* = null;
         bitmapData.fillRect(bitmapData.rect,-65536);
         bitmapData.draw(var_280,null,null,null,null,false);
         while(int(var_812.length) > 0)
         {
            _loc1_ = var_812.shift();
            bitmapData.applyFilter(bitmapData,bitmapData.rect,new Point(0,0),_loc1_);
         }
      }
      
      public function method_555(param1:Array, param2:Number) : void
      {
         var _loc7_:int = 0;
         var _loc3_:Array = [1,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0];
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         var _loc6_:int = int(_loc3_.length);
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc4_.push(Number(_loc3_[_loc7_] * (1 - param2) + param1[_loc7_] * param2));
         }
         var_812.push(new ColorMatrixFilter(_loc4_));
      }
      
      public function method_89() : void
      {
         bitmapData.dispose();
         if(parent != null)
         {
            parent.removeChild(this);
         }
      }
   }
}
