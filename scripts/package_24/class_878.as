package package_24
{
   import package_32.class_899;
   
   public class class_878
   {
       
      
      public var var_745:Array;
      
      public var var_733:class_874;
      
      public var var_970:Number;
      
      public var var_258:Function;
      
      public var var_743:Boolean;
      
      public var var_205:Number;
      
      public function class_878(param1:class_874 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(!var_258)
         {
            var_258 = function():void
            {
            };
         }
         var_733 = param1;
         var_745 = [0];
         var_205 = 0;
         var_970 = 1;
         var_743 = true;
      }
      
      public function method_79() : void
      {
         var_205 = Number(var_205 + var_970);
         if(var_205 >= int(var_745.length))
         {
            if(var_743)
            {
               var_205 = var_205 - int(var_745.length);
            }
            else
            {
               var_205 = int(var_745.length) - 1;
               var_970 = 0;
            }
            var_258();
         }
         if(var_205 < 0)
         {
            if(var_743)
            {
               var_205 = Number(var_205 + int(var_745.length));
            }
            else
            {
               var_205 = 0;
               var_970 = 0;
            }
            var_258();
         }
      }
      
      public function method_134() : void
      {
         name_8(0);
      }
      
      public function method_120() : void
      {
         var_970 = -var_970;
         var_205 = int(var_745.length) - (Number(1 + var_205));
      }
      
      public function name_8(param1:Number = 1.0) : void
      {
         var_970 = param1;
      }
      
      public function method_140() : void
      {
         var_205 = int(class_691.method_114(int(var_745.length)));
      }
      
      public function method_133(param1:Number) : void
      {
         var_205 = param1;
      }
      
      public function method_1002() : class_750
      {
         var _loc1_:int = int(var_745[int(class_691.method_114(int(var_745.length)))]);
         return var_733.method_98(_loc1_);
      }
      
      public function method_485() : class_750
      {
         var _loc1_:int = int(var_745[int(var_205)]);
         return var_733.method_98(_loc1_);
      }
   }
}
