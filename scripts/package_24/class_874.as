package package_24
{
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.filters.GlowFilter;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import package_32.class_899;
   
   public class class_874
   {
       
      
      public var var_1312:class_720;
      
      public var var_741:BitmapData;
      
      public var var_1314:int;
      
      public var var_295:class_720;
      
      public var var_1313:Array;
      
      public var var_737:int;
      
      public var var_736:int;
      
      public function class_874(param1:BitmapData = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_741 = param1;
         var_1313 = [];
         var_736 = 0;
         var_737 = 0;
         var_1314 = 0;
         var_295 = new class_720();
         var_1312 = new class_720();
      }
      
      public function method_991(param1:BitmapData) : void
      {
         var _loc4_:class_750 = null;
         var_741 = param1;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1313;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.var_741 = var_741;
         }
      }
      
      public function method_992(param1:int, param2:int, param3:int, param4:int, param5:int = 1, param6:int = 1) : void
      {
         var _loc8_:int = 0;
         var _loc7_:int = 0;
         while(_loc7_ < 4)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            method_366(param1,param2,param3,param4,param5,param6,false,false,_loc8_ * 1.57);
         }
      }
      
      public function method_366(param1:int, param2:int, param3:int, param4:int, param5:int = 1, param6:int = 1, param7:Boolean = false, param8:Boolean = false, param9:Object = undefined) : void
      {
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc10_:int = 0;
         while(_loc10_ < param6)
         {
            _loc10_++;
            _loc11_ = _loc10_;
            _loc12_ = 0;
            while(_loc12_ < param5)
            {
               _loc12_++;
               _loc13_ = _loc12_;
               method_993(param1 + _loc13_ * param3,param2 + _loc11_ * param4,param3,param4,param7,param8,param9);
            }
         }
      }
      
      public function method_369(param1:int = 0, param2:int = 0) : void
      {
         var_736 = param1;
         var_737 = param2;
      }
      
      public function method_994() : TextField
      {
         var _loc1_:TextField = new TextField();
         var _loc2_:TextFormat = _loc1_.getTextFormat();
         _loc2_.color = 16777215;
         _loc2_.font = "04b03";
         _loc2_.size = 8;
         _loc1_.defaultTextFormat = _loc2_;
         _loc1_.filters = [new GlowFilter(0,1,2,2,10)];
         return _loc1_;
      }
      
      public function method_271() : int
      {
         return int(var_1313.length);
      }
      
      public function method_289(param1:Array) : Array
      {
         var _loc4_:String = null;
         var _loc5_:class_878 = null;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         while(_loc3_ < int(param1.length))
         {
            _loc4_ = param1[_loc3_];
            _loc3_++;
            _loc5_ = new class_878(this);
            _loc5_.var_745 = var_1312.method_98(_loc4_);
            _loc2_.push(_loc5_);
         }
         return _loc2_;
      }
      
      public function method_995(param1:int = 200) : Sprite
      {
         var _loc8_:String = null;
         var _loc9_:class_878 = null;
         var _loc10_:class_656 = null;
         var _loc11_:Number = NaN;
         var _loc12_:TextField = null;
         var _loc13_:Number = NaN;
         var _loc2_:Sprite = new Sprite();
         var _loc3_:Number = 0;
         var _loc4_:Number = 0;
         var _loc5_:Number = 0;
         var _loc6_:* = var_1312.method_215();
         var _loc7_:* = _loc6_;
         while(_loc7_.method_74())
         {
            _loc8_ = _loc7_.name_1();
            _loc9_ = method_136(_loc8_);
            _loc10_ = new class_656();
            _loc2_.addChild(_loc10_);
            _loc10_.method_483(0,0);
            _loc10_.method_137(_loc9_);
            _loc10_.x = _loc3_;
            _loc10_.y = _loc4_;
            _loc11_ = _loc10_.width;
            _loc12_ = method_994();
            _loc2_.addChild(_loc12_);
            _loc12_.x = _loc3_;
            _loc12_.y = Number(_loc4_ + _loc10_.height);
            _loc12_.text = _loc8_;
            _loc12_.width = Number(_loc12_.textWidth + 3);
            _loc13_ = _loc12_.width - _loc11_;
            _loc10_.x = Number(_loc10_.x + _loc13_ * 0.5);
            _loc11_ = Number(Math.max(_loc11_,_loc12_.width));
            _loc3_ = Number(_loc3_ + _loc11_);
            _loc5_ = Number(Math.max(_loc5_,Number(_loc10_.height + 10)));
            if(_loc3_ > param1)
            {
               _loc3_ = 0;
               _loc4_ = Number(_loc4_ + _loc5_);
               _loc5_ = 0;
            }
         }
         return _loc2_;
      }
      
      public function method_136(param1:String) : class_878
      {
         var _loc2_:class_878 = new class_878(this);
         _loc2_.var_745 = var_1312.method_98(param1);
         return _loc2_;
      }
      
      public function method_98(param1:Object = undefined, param2:String = undefined) : class_750
      {
         if(param1 == null)
         {
            param1 = 0;
         }
         if(param2 != null)
         {
            param1 = param1 + var_295.method_98(param2);
         }
         return var_1313[param1];
      }
      
      public function method_365(param1:String) : void
      {
         var_295.method_216(param1,int(var_1313.length));
         var_1314 = int(var_1313.length);
      }
      
      public function method_993(param1:Object, param2:Object, param3:Object, param4:Object, param5:Boolean = false, param6:Boolean = false, param7:Object = undefined) : class_750
      {
         var _loc8_:class_750 = new class_750(var_741,param1,param2,param3,param4,param5,param6,param7);
         _loc8_.var_736 = var_736;
         _loc8_.var_737 = var_737;
         var_1313.push(_loc8_);
         return _loc8_;
      }
      
      public function method_367(param1:String, param2:Array, param3:Array = undefined, param4:int = 1) : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:Array = null;
         var _loc5_:Array = [];
         var _loc6_:int = 0;
         _loc7_ = 0;
         while(_loc7_ < int(param2.length))
         {
            _loc8_ = int(param2[_loc7_]);
            _loc7_++;
            _loc9_ = 1;
            if(param3 != null)
            {
               if(_loc6_ < int(param3.length))
               {
                  _loc9_ = int(param3[_loc6_]);
               }
               else
               {
                  _loc9_ = int(param3[int(param3.length) - 1]);
               }
            }
            _loc10_ = 0;
            while(_loc10_ < _loc9_)
            {
               _loc10_++;
               _loc11_ = _loc10_;
               _loc5_.push(_loc8_ + var_1314);
            }
            _loc6_++;
         }
         if(param4 > 1)
         {
            _loc7_ = 0;
            while(_loc7_ < param4)
            {
               _loc7_++;
               _loc8_ = _loc7_;
               _loc12_ = [];
               _loc9_ = _loc8_ * int(_loc5_.length);
               _loc10_ = 0;
               while(_loc10_ < int(_loc5_.length))
               {
                  _loc11_ = int(_loc5_[_loc10_]);
                  _loc10_++;
                  _loc12_.push(_loc11_ + _loc9_);
               }
               var_1312.method_216(param1 + "_" + _loc8_,_loc5_);
            }
         }
         else
         {
            var_1312.method_216(param1,_loc5_);
         }
      }
   }
}
