package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_877 extends class_645
   {
      
      public static var var_493:int = 22;
       
      
      public var var_751:Number;
      
      public var var_351:Number;
      
      public var var_531:MovieClip;
      
      public var var_1327:Array;
      
      public var var_363:MovieClip;
      
      public var var_1326:class_892;
      
      public var iUkn:Array;
      
      public var var_488:Array;
      
      public var var_948:Number;
      
      public var var_1328:Array;
      
      public var var_470:Array;
      
      public var var_248:Number;
      
      public function class_877()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var_751 = var_751 + 1;
         var_1326.var_233 = Math.min(var_751 / 40,1) * 0.92;
         var_351 = var_351 * 1.001;
         var_948 = method_100().var_244 / class_742.var_217;
         method_1000();
         method_1001();
         method_408();
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_1001() : void
      {
         var _loc8_:MovieClip = null;
         var _loc9_:* = null;
         if(name_3 != 2)
         {
            var_248 = Number(var_248 + Number(class_663.method_99(-0.5,(var_948 * 2 - 1) * 0.1,0.5)));
         }
         var _loc2_:Number = var_351 * (1 + var_1326.var_245 / class_742.var_219 * 0.5);
         var_1326.var_243.rotation = var_248 / 0.0174;
         var_1326.var_278 = Math.cos(var_248) * _loc2_;
         var_1326.var_279 = Math.sin(var_248) * _loc2_;
         var _loc3_:Number = Number(var_1326.var_244 + Math.cos(Number(var_248 + 3.14)) * 40);
         var _loc4_:Number = Number(var_1326.var_245 + Math.sin(Number(var_248 + 3.14)) * 40);
         var_363.graphics.clear();
         var_363.graphics.lineStyle(1,14540202,50);
         var_363.graphics.moveTo(_loc3_,_loc4_);
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:Array = var_488;
         while(_loc6_ < int(_loc7_.length))
         {
            _loc8_ = _loc7_[_loc6_];
            _loc6_++;
            _loc9_ = var_1327[int(var_1327.length) - (3 + _loc5_ * 4)];
            _loc8_.x = Number(_loc9_.var_244);
            _loc8_.y = Number(_loc9_.var_245);
            _loc8_.rotation = Number(Number(_loc9_.var_93) + 90);
            var_363.graphics.lineTo(Number(_loc9_.var_244),Number(_loc9_.var_245));
            _loc5_++;
         }
         var_1327.push({
            "L\x01":_loc3_,
            "\x03\x01":_loc4_,
            "z\x01":var_1326.var_243.rotation
         });
         while(int(var_1327.length) > 50)
         {
            var_1327.shift();
         }
         var_531.x = var_1326.var_244;
         _loc6_ = 30;
         if(var_1326.var_244 > class_742.var_217 + _loc6_ || var_1326.var_244 < -_loc6_ || var_1326.var_245 < -_loc6_ || var_1326.var_245 > class_742.var_219 - 10)
         {
            method_81(false,5);
            name_3 = 2;
            if(var_1326.var_245 > class_742.var_217 - 10)
            {
               var_1326.var_279 = var_1326.var_279 * -0.5;
               var_248 = Number(Math.atan2(var_1326.var_279,var_1326.var_278));
            }
         }
      }
      
      public function method_1000() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc4_:int = 0;
         var _loc5_:* = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:MovieClip = null;
         var _loc10_:Number = NaN;
         var _loc11_:* = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = iUkn[_loc2_];
            _loc4_ = 8;
            _loc5_ = {
               "L\x01":Number(Number(_loc4_ + var_948 * (class_742.var_217 - 2 * _loc4_)) + (_loc2_ * 2 - 1) * 35),
               "\x03\x01":class_742.var_219 + 20 - (1 - var_1326.var_245 / class_742.var_219) * 50
            };
            _loc3_.method_358(_loc5_,0.1,0.5);
            _loc6_ = Number(Number(_loc3_.method_231(var_1326)) + 1.57);
            _loc7_ = 17 + int(class_663.method_99(-1,_loc6_,1) * 16);
            _loc3_.var_243.gotoAndStop(_loc7_);
            _loc9_ = Reflect.field(_loc3_.var_243,class_899.__unprotect__("X\x01"));
            _loc9_ = Reflect.field(_loc3_.var_243,class_899.__unprotect__("X\x01"));
            _loc8_ = {
               "L\x01":Number(_loc3_.var_244 + _loc9_.x),
               "\x03\x01":Number(_loc3_.var_245 + _loc9_.y)
            };
            _loc10_ = Number(var_248 + 1.57 * (_loc2_ * 2 - 1));
            _loc11_ = {
               "L\x01":Number(var_1326.var_244 + Math.cos(_loc10_) * class_877.var_493),
               "\x03\x01":Number(var_1326.var_245 + Math.sin(_loc10_) * class_877.var_493)
            };
            _loc9_ = var_470[_loc2_];
            _loc9_.x = Number(_loc8_.var_244);
            _loc9_.y = Number(_loc8_.var_245);
            _loc12_ = _loc11_.var_244 - _loc8_.var_244;
            _loc13_ = _loc11_.var_245 - _loc8_.var_245;
            _loc14_ = Number(Math.atan2(_loc13_,_loc12_));
            _loc15_ = Number(Math.sqrt(Number(_loc12_ * _loc12_ + _loc13_ * _loc13_)));
            _loc9_.rotation = _loc14_ / 0.0174;
            _loc9_.scaleX = _loc15_ * 0.01;
         }
      }
      
      public function method_408() : void
      {
         var _loc4_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1328;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.x = Number(_loc4_.x + (0.3 - _loc1_ * 0.1));
            _loc1_++;
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [360];
         super.method_95(param1);
         var_248 = -1.57;
         var_351 = Number(2 + param1 * 3);
         method_117();
         method_72();
         var_751 = 0;
      }
      
      public function method_117() : void
      {
         var _loc1_:class_892 = null;
         var _loc3_:int = 0;
         var _loc4_:MovieClip = null;
         var _loc6_:* = null;
         var _loc7_:class_892 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1b\x0b\x0f\x06\x03"),0);
         var_531 = var_232.method_84(class_899.__unprotect__("H{\tS\x03"),class_645.var_209);
         var_531.x = class_742.var_217 * 0.5;
         var_531.y = class_742.var_219;
         var_363 = var_232.method_92(class_645.var_209);
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("vA]-\x02"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_1326 = _loc1_;
         var_1326.var_244 = class_742.var_217 * 0.5;
         var_1326.var_245 = class_742.var_219 - 30;
         var_1326.method_118();
         var_1326.var_233 = 0.92;
         var_488 = [];
         var _loc2_:int = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = var_232.method_84(class_899.__unprotect__("Y&\bV\x03"),class_645.var_209);
            _loc4_.gotoAndStop(_loc3_ + 1);
            var_488.push(_loc4_);
         }
         var_1327 = [];
         var _loc5_:* = {
            "L\x01":var_1326.var_244,
            "\x03\x01":Number(var_1326.var_245 + var_351),
            "z\x01":-90
         };
         while(int(var_1327.length) < 50)
         {
            _loc6_ = {
               "L\x01":Number(_loc5_.var_244),
               "\x03\x01":Number(Number(_loc5_.var_245) + var_351),
               "z\x01":-90
            };
            var_1327.push(_loc6_);
            _loc5_ = _loc6_;
         }
         iUkn = [];
         _loc2_ = 0;
         while(_loc2_ < 2)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc7_ = new class_892(var_232.method_84(class_899.__unprotect__("f\x18df\x01"),class_645.var_209));
            _loc7_.var_233 = 0.99;
            _loc1_ = _loc7_;
            _loc1_.var_244 = class_742.var_217 * 0.5;
            _loc1_.var_245 = class_742.var_219;
            _loc1_.var_233 = 0.92;
            _loc1_.method_118();
            _loc1_.var_243.stop();
            iUkn.push(_loc1_);
         }
         var_470 = [];
         _loc2_ = 0;
         while(_loc2_ < 2)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = var_232.method_84(class_899.__unprotect__("B\x018\x13\x03"),class_645.var_209);
            var_470.push(_loc4_);
         }
         var_1328 = [];
         _loc2_ = 0;
         while(_loc2_ < 3)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = Reflect.field(class_319,"$c" + _loc3_);
            var_1328.push(_loc4_);
         }
      }
   }
}
