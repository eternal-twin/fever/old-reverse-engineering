package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_655 extends class_645
   {
      
      public static var var_264:Array = [{
         "L\x01":1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":1
      },{
         "L\x01":-1,
         "\x03\x01":0
      },{
         "L\x01":0,
         "\x03\x01":-1
      }];
      
      public static var var_332:int = 6;
      
      public static var var_333:int = 12;
      
      public static var var_212:int = 6;
       
      
      public var var_251:Number;
      
      public var var_342:int;
      
      public var var_335:MovieClip;
      
      public var var_338:Array;
      
      public var var_339:Array;
      
      public var var_198:Array;
      
      public function class_655()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               method_218();
               if(!name_5)
               {
                  name_3 = 3;
                  break;
               }
               break;
            case 3:
               method_218();
               if(name_5)
               {
                  method_219();
                  break;
               }
               break;
            case 4:
               _loc1_ = 0;
               _loc2_ = var_198;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  if(Number(_loc3_.name_20) > 0)
                  {
                     _loc3_.name_20 = Number(_loc3_.name_20) - 1;
                  }
                  else
                  {
                     _loc3_.name_21 = (Number(_loc3_.name_21) + 30) % 628;
                     _loc3_.y = Number(Number(_loc3_.var_334) + (Math.cos(_loc3_.name_21 / 100) - 1) * 3);
                  }
               }
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true,20);
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_220(param1:Array) : Object
      {
         var _loc2_:int = int(class_691.method_114(int(param1.length)));
         var _loc3_:* = param1[_loc2_];
         param1.splice(_loc2_,1);
         return _loc3_;
      }
      
      public function method_223(param1:int, param2:int, param3:int) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         if(name_3 != 1)
         {
            return;
         }
         var_335 = method_221(param1);
         var_335.x = Number(var_335.x + (int(var_335.var_336) + param2) * class_655.var_333);
         var_335.y = Number(var_335.y + (int(var_335.var_337) + param3) * class_655.var_333);
         var_232.method_110(var_335);
         var _loc4_:int = 0;
         while(_loc4_ < 20)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = 0;
            while(_loc6_ < 20)
            {
               _loc6_++;
               _loc7_ = _loc6_;
               if(method_222(_loc5_,_loc7_))
               {
                  _loc8_ = var_338[_loc5_][_loc7_];
                  if(_loc8_ != null && int(_loc8_.var_215) == param1)
                  {
                     _loc8_.parent.removeChild(_loc8_);
                     var_338[_loc5_][_loc7_] = null;
                  }
               }
            }
         }
         name_3 = 2;
      }
      
      public function method_221(param1:int) : MovieClip
      {
         var _loc6_:* = null;
         var _loc8_:MovieClip = null;
         var _loc2_:Array = var_339[param1];
         var _loc3_:MovieClip = var_232.method_92(class_645.var_209);
         _loc3_.var_232 = new class_755(_loc3_);
         _loc3_.var_215 = param1;
         var _loc4_:* = {
            "O6EU\x01":99.9,
            "O\x15;\x03\x02":99.9,
            "^\'\x14^\x01":-99.9,
            "~tj[\x01":-99.9
         };
         var _loc5_:int = 0;
         while(_loc5_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc5_];
            _loc5_++;
            _loc4_.name_22 = Number(Math.min(Number(_loc4_.name_22),int(_loc6_.var_244)));
            _loc4_.name_23 = Number(Math.min(Number(_loc4_.name_23),int(_loc6_.var_245)));
            _loc4_.var_340 = Number(Math.max(Number(_loc4_.var_340),int(_loc6_.var_244)));
            _loc4_.var_341 = Number(Math.max(Number(_loc4_.var_341),int(_loc6_.var_245)));
         }
         _loc3_.var_336 = int((Number(_loc4_.name_22) + Number(_loc4_.var_340)) * 0.5);
         _loc3_.var_337 = int((Number(_loc4_.name_23) + Number(_loc4_.var_341)) * 0.5);
         var _loc7_:Function = function(param1:Object, param2:Object):int
         {
            if(int(param1.var_245) > int(param2.var_245))
            {
               return 1;
            }
            if(int(param1.var_245) < int(param2.var_245))
            {
               return -1;
            }
            return 0;
         };
         _loc2_.sort(_loc7_);
         _loc5_ = 0;
         while(_loc5_ < int(_loc2_.length))
         {
            _loc6_ = _loc2_[_loc5_];
            _loc5_++;
            _loc8_ = _loc3_.var_232.method_84(class_899.__unprotect__("rKH\x01\x02"),1);
            _loc8_.gotoAndStop(param1 + 1);
            _loc8_.x = (int(_loc6_.var_244) - int(_loc3_.var_336)) * class_655.var_333;
            _loc8_.y = (int(_loc6_.var_245) - int(_loc3_.var_337)) * class_655.var_333;
         }
         return _loc3_;
      }
      
      public function method_218() : void
      {
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - var_335.x - class_655.var_333 * 0.5;
         var _loc3_:Number = _loc1_.var_245 - var_335.y - class_655.var_333 * 0.5;
         var_335.x = Number(var_335.x + Number(class_663.method_99(-18,_loc2_ * 0.5,18)));
         var_335.y = Number(var_335.y + Number(class_663.method_99(-18,_loc3_ * 0.5,18)));
      }
      
      public function method_224(param1:int, param2:int, param3:int = 0) : Boolean
      {
         return param1 >= param3 && param1 < class_655.var_332 - param3 && param2 >= param3 && param2 < class_655.var_332 - param3;
      }
      
      public function method_222(param1:int, param2:int, param3:int = 1) : Boolean
      {
         return param1 >= param3 && param1 < 20 - param3 && param2 >= param3 && param2 < 20 - param3;
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc3_:int = 0;
         var_227 = [600 - param1 * 160];
         super.method_95(param1);
         var_342 = 2 + int(param1 * 4);
         var_338 = [];
         var _loc2_:int = 0;
         while(_loc2_ < 20)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            var_338[_loc3_] = [];
         }
         method_225();
         method_117();
         method_72();
      }
      
      public function method_225() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var _loc10_:Array = null;
         var _loc11_:Array = null;
         var _loc12_:* = null;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:* = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x01XHj"),0);
         var _loc1_:Array = [];
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         _loc4_ = class_655.var_332;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_[_loc5_] = [];
            _loc6_ = 0;
            _loc7_ = class_655.var_332;
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               _loc9_ = {
                  "\x1d\x0b\x01":-1,
                  "L\x01":_loc5_,
                  "\x03\x01":_loc8_
               };
               _loc2_[_loc5_][_loc8_] = _loc9_;
               _loc1_.push(_loc9_);
            }
         }
         var_339 = [];
         _loc3_ = 0;
         _loc4_ = var_342;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc9_ = method_220(_loc1_);
            _loc9_.var_215 = _loc5_;
            var_339[_loc5_] = [_loc9_];
         }
         _loc3_ = 0;
         while(int(_loc1_.length) > 0 && _loc3_ < 200)
         {
            _loc4_ = 0;
            _loc5_ = var_342;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc10_ = [];
               _loc7_ = 0;
               while(_loc7_ < int(_loc1_.length))
               {
                  _loc9_ = _loc1_[_loc7_];
                  _loc7_++;
                  _loc8_ = 0;
                  _loc11_ = class_655.var_264;
                  while(_loc8_ < int(_loc11_.length))
                  {
                     _loc12_ = _loc11_[_loc8_];
                     _loc8_++;
                     _loc13_ = int(_loc9_.var_244) + int(_loc12_.var_244);
                     _loc14_ = int(_loc9_.var_245) + int(_loc12_.var_245);
                     if(method_224(_loc13_,_loc14_,0))
                     {
                        _loc15_ = _loc2_[_loc13_][_loc14_];
                        if(int(_loc15_.var_215) == _loc6_)
                        {
                           _loc10_.push(_loc9_);
                           break;
                        }
                     }
                  }
               }
               if(int(_loc10_.length) > 0)
               {
                  _loc9_ = _loc10_[int(class_691.method_114(int(_loc10_.length) - 1))];
                  _loc9_.var_215 = _loc6_;
                  _loc1_.method_116(_loc9_);
                  var_339[_loc6_].push(_loc9_);
               }
            }
         }
      }
      
      public function method_226(param1:int, param2:int, param3:int) : Boolean
      {
         var _loc6_:* = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc4_:Array = var_339[param1];
         var _loc5_:int = 0;
         while(_loc5_ < int(_loc4_.length))
         {
            _loc6_ = _loc4_[_loc5_];
            _loc5_++;
            _loc7_ = int(_loc6_.var_244) + param2;
            _loc8_ = int(_loc6_.var_245) + param3;
            if(!method_222(_loc7_,_loc8_) || var_338[_loc7_][_loc8_] != null)
            {
               return false;
            }
         }
         return true;
      }
      
      public function method_219() : void
      {
         var _loc6_:* = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc1_:* = method_100();
         var _loc2_:int = int(Math.round(_loc1_.var_244 / class_655.var_333 - 0.5)) - int(var_335.var_336);
         var _loc3_:int = int(Math.round(_loc1_.var_245 / class_655.var_333 - 0.5)) - int(var_335.var_337);
         var _loc4_:Array = class_655.var_264.method_78();
         _loc4_.unshift({
            "L\x01":0,
            "\x03\x01":0
         });
         var _loc5_:int = 0;
         while(_loc5_ < int(_loc4_.length))
         {
            _loc6_ = _loc4_[_loc5_];
            _loc5_++;
            _loc7_ = _loc2_ + int(_loc6_.var_244);
            _loc8_ = _loc3_ + int(_loc6_.var_245);
            if(method_226(int(var_335.var_215),_loc7_,_loc8_))
            {
               method_227(int(var_335.var_215),_loc7_,_loc8_);
               var_335.parent.removeChild(var_335);
               name_3 = 1;
               method_228();
               return;
            }
         }
      }
      
      public function method_227(param1:int, param2:int, param3:int) : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:MovieClip = null;
         var var_343:int = param1;
         var var_336:int = param2;
         var var_337:int = param3;
         var _loc4_:Array = var_339[var_343];
         var _loc5_:int = 0;
         _loc6_ = int(_loc4_.length);
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc8_ = _loc4_[_loc7_];
            _loc9_ = int(_loc8_.var_244) + var_336;
            _loc10_ = int(_loc8_.var_245) + var_337;
            _loc11_ = var_232.method_84(class_899.__unprotect__("rKH\x01\x02"),class_645.var_208);
            _loc11_.gotoAndStop(var_343 + 1);
            _loc11_.x = _loc9_ * class_655.var_333;
            _loc11_.y = _loc10_ * class_655.var_333;
            var_338[_loc9_][_loc10_] = _loc11_;
            _loc11_.var_215 = var_343;
            var var_214:Array = [this];
            _loc11_.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:Array):Function
            {
               var var_214:Array = param1;
               return function(param1:*):void
               {
                  var_214[0].method_223(var_343,var_336,var_337);
               };
            }(var_214));
         }
         _loc5_ = 0;
         while(_loc5_ < 20)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = 0;
            while(_loc7_ < 20)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               _loc11_ = var_338[_loc6_][_loc9_];
               if(_loc11_ != null)
               {
                  var_232.method_110(_loc11_);
               }
            }
         }
      }
      
      public function method_228() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var_198 = [];
         var _loc1_:int = 7;
         while(_loc1_ < 13)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = 7;
            while(_loc3_ < 13)
            {
               _loc3_++;
               _loc4_ = _loc3_;
               _loc5_ = var_338[_loc2_][_loc4_];
               if(_loc5_ == null)
               {
                  return;
               }
               _loc5_.name_21 = 0;
               _loc5_.var_334 = _loc5_.y;
               _loc5_.name_20 = (_loc2_ + _loc4_ - 14) * 1.5;
               var_198.push(_loc5_);
            }
         }
         var_229 = [true];
         var_251 = 30;
         name_3 = 4;
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc1_:int = 0;
         var _loc2_:int = var_342;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = var_339[_loc3_];
            _loc5_ = 0;
            _loc6_ = 0;
            do
            {
               _loc7_ = 1;
               _loc5_ = _loc7_ + int(class_691.method_114(20 - 2 * _loc7_));
               _loc6_ = _loc7_ + int(class_691.method_114(20 - 2 * _loc7_));
            }
            while(!method_226(_loc3_,_loc5_,_loc6_));
            
            method_227(_loc3_,_loc5_,_loc6_);
         }
      }
   }
}
