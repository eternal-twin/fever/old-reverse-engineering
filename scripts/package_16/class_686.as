package package_16
{
   import package_11.class_844;
   import package_11.package_12.class_659;
   import package_32.class_899;
   
   public class class_686
   {
       
      
      public var var_445:Array;
      
      public var name_51:class_685;
      
      public var var_509:Number;
      
      public var var_503:class_844;
      
      public var var_443:Array;
      
      public var var_489:Array;
      
      public var var_215:int;
      
      public var var_510:Array;
      
      public function class_686(param1:class_685 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         param1.var_455 = this;
         name_51 = param1;
         var_443 = [];
         var_489 = [];
         var_510 = [];
         var_445 = [];
      }
      
      public function method_326(param1:class_686) : Boolean
      {
         var _loc4_:class_686 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_489;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_ == param1)
            {
               return true;
            }
         }
         return false;
      }
      
      public function method_328() : class_685
      {
         var _loc1_:int = int(var_509 * (int(var_445.length) - 1));
         var _loc3_:class_844 = var_503;
         var _loc4_:Number = _loc3_.var_503 * 16807 % 2147483647;
         _loc3_.var_503 = _loc4_;
         var _loc2_:int = _loc1_ + int((int(_loc4_) & 1073741823) % (int(var_445.length) - _loc1_));
         var _loc5_:class_685 = var_445[_loc2_];
         var_445.splice(_loc2_,1);
         if(_loc5_.var_455 == null)
         {
            method_329(_loc5_);
         }
         return _loc5_;
      }
      
      public function method_329(param1:class_685) : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_685 = null;
         param1.var_508 = 1;
         param1.var_455 = this;
         var_443.push(param1);
         var _loc2_:Array = [0,1,2,3];
         class_659.method_241(_loc2_,var_503);
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = int(_loc2_[_loc3_]);
            _loc3_++;
            _loc5_ = param1.var_507[_loc4_];
            if(_loc5_ != null)
            {
               if(_loc5_.var_508 == 0)
               {
                  var_445.push(_loc5_);
               }
               else if(_loc5_.var_455 != this)
               {
                  var_510.push(_loc5_);
               }
            }
         }
      }
   }
}
