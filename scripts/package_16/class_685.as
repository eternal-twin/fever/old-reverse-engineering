package package_16
{
   import package_32.class_899;
   
   public class class_685
   {
       
      
      public var var_455:class_686;
      
      public var var_245:int;
      
      public var var_244:int;
      
      public var var_480:Array;
      
      public var var_508:int;
      
      public var var_507:Array;
      
      public var var_489:Array;
      
      public var var_237:int;
      
      public function class_685(param1:int = 0, param2:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_244 = param1;
         var_245 = param2;
         var_508 = 0;
         var_237 = 0;
         var_480 = [class_683.var_497,class_683.var_497];
         var_507 = [];
         var_489 = [];
      }
      
      public function method_327(param1:int) : void
      {
         var _loc2_:class_685 = var_507[param1];
         var_237 = _loc2_.var_237 + 1;
         var_489.push(_loc2_);
         _loc2_.var_489.push(this);
         var _loc3_:class_683 = class_683.var_444;
         if(param1 < 2)
         {
            var_480[param1] = _loc3_;
         }
         else
         {
            _loc2_.var_480[param1 - 2] = _loc3_;
         }
         if(_loc2_.var_455 != var_455)
         {
            if(!var_455.method_326(_loc2_.var_455))
            {
               var_455.var_489.push(_loc2_.var_455);
               _loc2_.var_455.var_489.push(var_455);
            }
         }
      }
      
      public function method_286(param1:int) : class_683
      {
         if(param1 < 2)
         {
            return var_480[param1];
         }
         if(var_507[param1] == null)
         {
            return class_683.var_497;
         }
         return var_507[param1].var_480[param1 - 2];
      }
   }
}
