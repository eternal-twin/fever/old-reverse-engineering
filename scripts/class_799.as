package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_32.class_899;
   
   public class class_799 extends class_645
   {
      
      public static var var_998:int = 1170;
      
      public static var var_726:int = 26;
       
      
      public var var_251:Number;
      
      public var var_699:Array;
      
      public function class_799()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Boolean = false;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = true;
               _loc2_ = 0;
               _loc3_ = var_699;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  if(_loc4_.var_10)
                  {
                     _loc4_.var_37 = _loc4_.var_37 - 1;
                     if(_loc4_.var_37 < 0)
                     {
                        _loc4_.var_37 = null;
                        method_579(_loc4_);
                     }
                     _loc4_.nextFrame();
                  }
                  else
                  {
                     _loc4_.prevFrame();
                  }
                  if(_loc4_.currentFrame < 5)
                  {
                     _loc1_ = false;
                  }
               }
               if(_loc1_)
               {
                  var_229 = [true];
                  name_3 = 2;
                  var_251 = 18;
                  break;
               }
               break;
            case 2:
               _loc2_ = 0;
               _loc3_ = var_699;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  _loc4_.nextFrame();
                  _loc4_.var_37 = _loc4_.var_37 + 6;
                  if(_loc4_.var_37 > class_799.var_998)
                  {
                     _loc4_.scaleX = _loc4_.scaleX * 0.7;
                     _loc4_.scaleY = _loc4_.scaleX;
                  }
               }
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true,20);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400 - param1 * 100];
         super.method_95(param1);
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__(";\x1b\'\x0e\x02"),0);
         var _loc1_:int = 3 + int(Math.floor(var_237[0] * 4));
         if(_loc1_ > 8)
         {
            _loc1_ = 8;
         }
         var _loc2_:Number = (class_742.var_217 - (_loc1_ - 1) * class_799.var_726) * 0.5;
         var_699 = [];
         var _loc3_:int = 0;
         while(_loc3_ < _loc1_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = 0;
            while(_loc5_ < _loc1_)
            {
               _loc5_++;
               _loc6_ = _loc5_;
               var var_631:Array = [var_232.method_84(class_899.__unprotect__("WW/w\x02"),class_645.var_209)];
               var_631[0].x = Number(_loc2_ + _loc4_ * class_799.var_726);
               var_631[0].y = Number(_loc2_ + _loc6_ * class_799.var_726);
               var_631[0].stop();
               var_631[0].var_10 = false;
               var_699.push(var_631[0]);
               var var_214:Array = [this];
               var_631[0].addEventListener(MouseEvent.ROLL_OVER,function(param1:Array, param2:Array):Function
               {
                  var var_214:Array = param1;
                  var var_631:Array = param2;
                  return function(param1:*):void
                  {
                     var_214[0].method_579(var_631[0]);
                  };
               }(var_214,var_631));
            }
         }
      }
      
      public function method_579(param1:MovieClip) : void
      {
         param1.var_10 = !param1.var_10;
         if(param1.var_10)
         {
            param1.var_37 = class_799.var_998;
         }
      }
   }
}
