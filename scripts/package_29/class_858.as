package package_29
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.filters.GlowFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   import package_11.class_755;
   import package_11.class_844;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_13.class_765;
   import package_24.class_656;
   import package_24.class_738;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_660;
   import package_8.package_9.class_814;
   import package_8.package_9.class_873;
   import package_9.class_879;
   import package_9.class_924;
   import package_9.class_932;
   
   public class class_858 extends class_756
   {
      
      public static var var_1010:int = 18;
      
      public static var var_1252:int = 97;
       
      
      public var var_220:Boolean;
      
      public var name_89:Function;
      
      public var var_1253:Array;
      
      public var var_1254:Boolean;
      
      public var var_251:int;
      
      public var var_1257:Array;
      
      public var var_792:TextField;
      
      public var name_3:int;
      
      public var var_1258:Bitmap;
      
      public var var_447:Object;
      
      public var var_1255:Boolean;
      
      public var var_1259:class_738;
      
      public var var_288:Object;
      
      public var var_1262:Array;
      
      public var var_328:int;
      
      public var package_31:Object;
      
      public var var_1260:Array;
      
      public var var_1261:class_738;
      
      public var var_795:int;
      
      public var var_1256:Array;
      
      public var var_303:Function;
      
      public function class_858(param1:Object = undefined, param2:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_1254 = false;
         package_31 = param1;
         var_1262 = class_858.method_923(param1,param2);
         var_795 = -1;
         var_328 = 0;
         var_1255 = false;
         method_362();
         var_1253 = [];
         method_312();
         class_908.method_95();
         class_908.var_1263[67] = function(param1:Function, param2:_GameBonus):Function
         {
            var var_27:Function = param1;
            var var_18:_GameBonus = param2;
            return function():void
            {
               return var_27(var_18);
            };
         }(method_930,_GameBonus.var_403);
         class_908.var_1263[86] = method_927;
         class_908.var_1263[66] = function(param1:Function, param2:_GameBonus):Function
         {
            var var_27:Function = param1;
            var var_18:_GameBonus = param2;
            return function():void
            {
               return var_27(var_18);
            };
         }(method_930,_GameBonus.var_402);
      }
      
      public static function method_923(param1:Object, param2:int, param3:int = 32) : Array
      {
         var _loc8_:String = null;
         var _loc9_:* = null;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:* = null;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:Number = NaN;
         var _loc17_:int = 0;
         var _loc18_:int = 0;
         var _loc19_:int = 0;
         var _loc4_:Array = [];
         var _loc5_:class_844 = new class_844(param2);
         var _loc6_:Array = param1._gameFam.split(",");
         var _loc7_:int = 0;
         while(_loc7_ < int(_loc6_.length))
         {
            _loc8_ = _loc6_[_loc7_];
            _loc7_++;
            _loc9_ = class_691.method_352(_loc8_);
            _loc10_ = 0;
            _loc11_ = class_885.var_308._games;
            while(_loc10_ < int(_loc11_.length))
            {
               _loc12_ = _loc11_[_loc10_];
               _loc10_++;
               if(int(_loc12_._acc) == _loc9_)
               {
                  _loc4_.push(_loc12_);
               }
            }
         }
         _loc11_ = param1._gameSpecial.split(",");
         _loc7_ = 0;
         while(_loc7_ < int(_loc11_.length))
         {
            _loc8_ = _loc11_[_loc7_];
            _loc7_++;
            _loc4_.push(class_885.var_308._games[class_691.method_352(_loc8_)]);
         }
         _loc7_ = 0;
         _loc10_ = 0;
         while(_loc10_ < int(_loc4_.length))
         {
            _loc9_ = _loc4_[_loc10_];
            _loc10_++;
            _loc7_ = _loc7_ + int(_loc9_._weight);
         }
         var _loc13_:Array = [];
         _loc10_ = 0;
         while(_loc10_ < 48)
         {
            _loc10_++;
            _loc14_ = _loc10_;
            _loc16_ = _loc5_.var_503 * 16807 % 2147483647;
            _loc5_.var_503 = _loc16_;
            _loc15_ = int((int(_loc16_) & 1073741823) % _loc7_);
            _loc17_ = 0;
            _loc18_ = 0;
            _loc19_ = 0;
            while(_loc19_ < int(_loc4_.length))
            {
               _loc9_ = _loc4_[_loc19_];
               _loc19_++;
               _loc17_ = _loc17_ + int(_loc9_._weight);
               if(_loc17_ > _loc15_)
               {
                  _loc13_.push(int(_loc9_._id));
                  break;
               }
            }
         }
         var _loc20_:Array = [];
         _loc10_ = -1;
         _loc14_ = 0;
         while(_loc14_ < int(_loc13_.length))
         {
            _loc15_ = int(_loc13_[_loc14_]);
            _loc14_++;
            if(_loc10_ != _loc15_)
            {
               _loc20_.push(_loc15_);
            }
            _loc10_ = _loc15_;
         }
         return _loc20_;
      }
      
      public function method_927() : void
      {
         if(!var_1255 || var_536.var_220 != null)
         {
            return;
         }
         if(method_588(_Item.var_406) && !var_1254)
         {
            var_1254 = true;
            method_924("voodoo_mask");
         }
         else if(method_925(_GameBonus.var_401))
         {
            method_926(_GameBonus.var_401);
            method_924();
         }
      }
      
      public function method_930(param1:_GameBonus) : void
      {
         if(var_1255 || !method_925(param1) || name_89 != null)
         {
            return;
         }
         if(param1 == _GameBonus.var_403 && int(var_288.var_683) == int(var_288.name_121))
         {
            return;
         }
         method_926(param1);
         name_89 = var_303;
         name_3 = 0;
         var_251 = 0;
         switch(int(param1.var_295))
         {
            case 0:
               var_303 = method_928;
               break;
            default:
               break;
            case 2:
               var_303 = method_929;
         }
      }
      
      public function method_170() : void
      {
         var_536.method_79();
         method_931(var_536.var_227[0] / var_536.var_238[0]);
      }
      
      public function method_929() : void
      {
         var _loc1_:class_738 = null;
         var _loc2_:class_660 = null;
         var_251 = var_251 + 1;
         switch(name_3)
         {
            case 0:
               var_288.var_282.method_290([class_702.var_288.method_136("hero_knife"),class_702.var_288.method_136("hero_front")]);
               name_3 = name_3 + 1;
               break;
            case 1:
               if(var_251 == 20)
               {
                  _loc1_ = new class_738();
                  _loc1_.method_128(class_702.package_9.method_98(null,"knife"));
                  var_1256[1].var_232.method_124(_loc1_,5);
                  _loc1_.x = Number(var_288.var_282.x + 8);
                  _loc1_.y = var_288.var_282.y;
                  _loc2_ = new class_660(_loc1_,var_447.var_282.x - 4,var_447.var_282.y,0.05);
                  _loc2_.var_258 = function(param1:Function, param2:class_738, param3:Boolean):Function
                  {
                     var var_27:Function = param1;
                     var var_18:class_738 = param2;
                     var var_940:Boolean = param3;
                     return function():void
                     {
                        return var_27(var_18,var_940);
                     };
                  }(method_932,_loc1_,false);
                  name_3 = name_3 + 1;
                  break;
               }
         }
      }
      
      public function method_928() : void
      {
         var _loc1_:int = 0;
         var_251 = var_251 + 1;
         switch(name_3)
         {
            case 0:
               var_288.var_282.method_290([class_702.var_288.method_136("hero_cheese"),class_702.var_288.method_136("hero_front")]);
               name_3 = name_3 + 1;
               break;
            case 1:
               if(var_251 > 40)
               {
                  name_3 = name_3 + 1;
                  var_251 = 0;
                  var_288.var_683 = int(var_288.name_121);
                  method_933(var_288);
                  break;
               }
               break;
            case 2:
               _loc1_ = var_251;
               var_251 = var_251 + 1;
               if(_loc1_ > 10)
               {
                  var_303 = name_89;
                  name_89 = null;
                  break;
               }
         }
      }
      
      override public function method_79(param1:* = undefined) : void
      {
         super.method_79(param1);
         if(var_303 != null)
         {
            var_303();
         }
      }
      
      public function method_932(param1:class_738, param2:Boolean) : void
      {
         var _loc3_:class_660 = null;
         if(!param2 && int(package_31._id) == 11)
         {
            _loc3_ = new class_660(param1,Number(var_288.var_282.x + 4),var_288.var_282.y,0.05);
            _loc3_.var_258 = function(param1:Function, param2:class_738, param3:Boolean):Function
            {
               var var_27:Function = param1;
               var var_18:class_738 = param2;
               var var_940:Boolean = param3;
               return function():void
               {
                  return var_27(var_18,var_940);
               };
            }(method_932,param1,true);
            param1.scaleX = -1;
            var_447.var_282.method_290(class_702.var_242.method_289([package_31._anim + "_repel",package_31._anim]));
            return;
         }
         var _loc4_:* = var_447;
         if(param2)
         {
            _loc4_ = var_288;
         }
         param1.method_128(class_702.package_9.method_98(1,"knife"));
         if(param1.parent != null)
         {
            param1.parent.removeChild(param1);
         }
         _loc4_.var_282.addChild(param1);
         param1.x = param1.x - _loc4_.var_282.x;
         param1.y = param1.y - _loc4_.var_282.y;
         if(param2)
         {
            method_934(-1);
         }
         else
         {
            method_935(-1);
         }
         new class_814(param1,15);
         if(int(var_447.var_683) <= 0)
         {
            method_936(true);
         }
         else if(int(var_288.var_683) <= 0)
         {
            method_936(false);
         }
         else
         {
            var_303 = name_89;
            name_89 = null;
         }
      }
      
      public function method_926(param1:_GameBonus) : void
      {
         if(class_765.var_214 == null)
         {
            return;
         }
         var_1253.push(param1);
         class_765.var_214.method_593(param1,-1);
         method_937();
      }
      
      public function method_924(param1:String = undefined) : void
      {
         var _loc6_:class_738 = null;
         var _loc7_:Matrix = null;
         var_1255 = false;
         var_303 = null;
         var _loc2_:int = 0;
         var _loc3_:Number = 0.1;
         if(param1 != null)
         {
            _loc2_ = class_742.var_216;
            _loc3_ = 0.025;
         }
         var_1258 = new Bitmap();
         var_1258.bitmapData = new BitmapData(class_742.var_216 * 2 + _loc2_,class_742.var_218,false,0);
         var _loc4_:Rectangle = new Rectangle(0,0,class_742.var_216,class_742.var_218);
         var_1258.bitmapData.draw(var_536,null,null,null,_loc4_);
         var_1258.y = class_858.var_1010;
         var_536.method_89();
         method_525(int(method_938()));
         var _loc5_:Matrix = new Matrix();
         _loc5_.translate(class_742.var_216 + _loc2_,0);
         _loc4_.x = Number(_loc4_.x + (class_742.var_216 + _loc2_));
         var_1258.bitmapData.draw(var_536,_loc5_,null,null,_loc4_);
         if(param1 != null)
         {
            _loc6_ = new class_738();
            _loc6_.method_128(class_702.var_572.method_98(null,param1),0,0);
            _loc7_ = new Matrix();
            _loc7_.scale(8,8);
            _loc7_.translate(class_742.var_216,0);
            var_1258.bitmapData.draw(_loc6_,_loc7_);
         }
         var _loc8_:class_660 = new class_660(var_1258,-(class_742.var_216 + _loc2_),class_858.var_1010,_loc3_);
         _loc8_.method_122();
         _loc8_.var_258 = method_939;
         var_232.method_124(var_1258,class_756.var_786);
      }
      
      public function method_931(param1:Number) : void
      {
         param1 = Number(class_663.method_99(0,param1,1));
         var _loc2_:Number = class_858.var_1252 * param1;
         var _loc3_:class_738 = var_1257[1];
         _loc3_.width = _loc2_;
         var _loc4_:class_738 = var_1257[2];
         _loc4_.x = Number(30 + _loc2_);
      }
      
      public function method_940(param1:Number) : void
      {
         if(param1 > int(package_31._tempMax) * 0.01)
         {
            param1 = int(package_31._tempMax) * 0.01;
         }
         class_756.var_214.var_237 = param1;
         var_792.text = int(class_756.var_214.var_237 * 100) + "°";
         var_1259.x = Number(Number(var_792.x + var_792.textWidth) + 3);
      }
      
      override public function name_4(param1:Boolean) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Boolean = false;
         var _loc4_:class_878 = null;
         var _loc5_:class_878 = null;
         var_220 = param1;
         if(param1)
         {
            _loc2_ = 1;
            _loc3_ = method_588(_Item.var_421) && int(class_691.method_114(10)) == 0;
            if(_loc3_)
            {
               _loc2_++;
            }
            method_935(-_loc2_);
            _loc4_ = class_702.var_288.method_136("hero_front");
            if(_loc3_)
            {
               _loc5_ = class_702.var_288.method_136("hero_fork");
               var_288.var_282.method_290([_loc5_,_loc4_]);
            }
            else
            {
               _loc5_ = class_702.var_288.method_136("hero_happy_jump");
               var_288.var_282.method_290([_loc5_,_loc5_,_loc5_,_loc4_]);
            }
         }
         else
         {
            _loc2_ = int(package_31._atk);
            if(method_588(_Item.var_418) && _loc2_ > 1)
            {
               _loc2_--;
            }
            if(int(package_31._id) == 9)
            {
               var_328 = Boolean(class_765.var_214.method_588(_Item.class_848))?2:1;
            }
            else
            {
               method_934(-_loc2_);
            }
         }
      }
      
      override public function method_525(param1:int) : void
      {
         super.method_525(param1);
         var_536.y = class_858.var_1010;
      }
      
      public function method_941() : void
      {
         var _loc1_:int = var_251;
         var_251 = var_251 + 1;
         if(_loc1_ == 12)
         {
            method_924();
         }
      }
      
      public function method_933(param1:Object) : void
      {
         var _loc5_:class_738 = null;
         var _loc6_:int = 0;
         var _loc7_:class_656 = null;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = param1.name_122;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc6_ = _loc2_ < int(param1.var_683)?0:1;
            if(_loc5_.tabIndex == 0 && _loc6_ == 1)
            {
               _loc7_ = new class_656();
               _loc7_.method_137(class_702.var_304.method_136("heart_explode"));
               var_1256[1].var_232.method_124(_loc7_,2);
               _loc7_.x = _loc5_.x;
               _loc7_.y = _loc5_.y;
               _loc7_.var_198.var_258 = _loc7_.method_89;
            }
            _loc5_.tabIndex = _loc6_;
            _loc5_.method_128(class_702.var_304.method_98(_loc6_,"heart"));
            _loc2_++;
         }
      }
      
      public function method_937() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:String = null;
         if(class_765.var_214 == null)
         {
            return;
         }
         var _loc1_:* = class_765.var_214.package_31._inv;
         var _loc2_:int = 0;
         while(_loc2_ < 3)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = int([int(_loc1_._knife),int(_loc1_._cheese),int(_loc1_._leaf)][_loc3_]);
            if(_loc4_ > 99)
            {
               _loc4_ = 99;
            }
            _loc5_ = class_691.method_97(_loc4_);
            while(_loc5_.length == 1)
            {
               _loc5_ = "0" + _loc5_;
            }
            var_1260[_loc3_].text = _loc5_;
         }
      }
      
      public function method_312() : void
      {
         var _loc1_:Number = int(package_31._tempStart) * 0.01;
         if(method_588(_Item.var_422))
         {
            _loc1_ = _loc1_ - 0.1;
         }
         if(_loc1_ < 0)
         {
            _loc1_ = 0;
         }
         method_940(_loc1_);
         method_942();
      }
      
      public function method_942() : void
      {
         var _loc1_:class_645 = var_536;
         method_525(int(method_938()));
         method_943(_loc1_);
         if(_loc1_ != null)
         {
            _loc1_.method_89();
         }
      }
      
      public function method_944() : void
      {
         var _loc1_:class_360 = new class_360();
         var_536.addChild(_loc1_);
         _loc1_.x = class_742.var_216 * 0.5;
         _loc1_.y = class_742.var_218 * 0.5;
         var_251 = 0;
         var_303 = method_941;
      }
      
      public function method_943(param1:class_645 = undefined) : void
      {
         var _loc2_:int = int(class_691.method_114(class_645.var_213));
         if(param1 != null)
         {
            _loc2_ = param1.var_215;
         }
         var _loc3_:class_840 = new class_840(_loc2_,var_536.var_215,this);
         if(param1 == null)
         {
            _loc3_.var_254 = 0.5;
         }
         else
         {
            class_840.method_832(param1);
         }
         class_840.method_832(var_536);
         var_303 = _loc3_.method_79;
         _loc3_.method_79();
      }
      
      public function method_362() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_738 = null;
         var _loc4_:Number = NaN;
         var _loc5_:class_738 = null;
         var _loc6_:int = 0;
         var _loc7_:TextField = null;
         var_1256 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = new class_738();
            _loc3_.y = _loc2_ * (class_742.var_323 - class_858.var_1010);
            _loc3_.method_128(class_702.var_304.method_98(_loc2_,"bar"),0,0);
            _loc4_ = 2;
            _loc3_.scaleY = _loc4_;
            _loc3_.scaleX = _loc4_;
            var_232.method_124(_loc3_,class_756.var_297);
            var_1256.push({
               "U\\\x01":_loc3_,
               "\'~\x01":new class_755(_loc3_)
            });
            _loc3_.mouseEnabled = false;
            _loc3_.mouseChildren = false;
         }
         var_1257 = [];
         _loc3_ = new class_738();
         var_1256[0].var_232.method_124(_loc3_,1);
         _loc3_.method_128(class_702.var_304.method_98(null,"bg_timebar"),0,0);
         _loc3_.x = 30;
         _loc3_.width = class_858.var_1252;
         _loc1_ = 0;
         while(_loc1_ < 3)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc5_ = new class_738();
            var_1256[0].var_232.method_124(_loc5_,1);
            _loc5_.method_128(class_702.var_304.method_98(int([0,1,0][_loc2_]),"timebar"),0,0);
            _loc5_.x = 30 + _loc2_;
            _loc5_.y = 1;
            var_1257.push(_loc5_);
         }
         method_931(1);
         var_1260 = [];
         _loc1_ = 0;
         _loc2_ = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc6_ = _loc2_;
            _loc5_ = new class_738();
            _loc5_.method_128(class_702.var_304.method_98(_loc6_,"adv_icons"),0,0);
            var_1256[0].var_232.method_124(_loc5_,1);
            _loc5_.x = _loc1_;
            _loc1_ = _loc1_ + 9;
            _loc7_ = class_742.method_308(16777215,8,-1,"nokia");
            var_1256[0].var_232.method_124(_loc7_,1);
            _loc7_.x = _loc1_;
            _loc7_.y = -2;
            _loc7_.text = "0";
            _loc7_.filters = [new GlowFilter(5570560,1,4,4,100)];
            _loc7_.height = 10;
            _loc7_.width = 16;
            _loc7_.selectable = false;
            _loc1_ = _loc1_ + 15;
            if(_loc6_ == 0)
            {
               var_792 = _loc7_;
               var_792.width = 20;
               var_1259 = new class_738();
               var_1259.method_128(class_702.var_304.method_98(null,"icon_temp"),0,0);
               var_1256[0].var_232.method_124(var_1259,1);
               var_1259.y = 0;
               _loc1_ = _loc1_ + 105;
            }
            else
            {
               var_1260.push(_loc7_);
            }
         }
         method_937();
         _loc2_ = int(package_31._life);
         if(method_588(_Item.var_407))
         {
            _loc2_--;
         }
         _loc6_ = 18 - _loc2_;
         var _loc8_:int = int(method_945());
         if(_loc8_ > _loc6_)
         {
            _loc8_ = _loc6_;
         }
         var_288 = method_946(-1,_loc8_);
         var_447 = method_946(1,_loc2_);
         var_288.var_282.method_137(class_702.var_288.method_136("hero_front"));
         var_447.var_282.method_137(class_702.var_242.method_136(package_31._anim));
         var_447.var_282.y = Number(var_447.var_282.y + (int(package_31._oy) + 4));
         method_935(0);
         method_934(0);
      }
      
      public function method_835() : void
      {
         var_232.method_124(var_536,class_756.var_786);
         var_303 = method_170;
         var_1255 = true;
         var_536.var_234 = true;
         if(method_588(_Item.class_920) && var_536.var_215 == int(class_652.var_214.var_296.name_123))
         {
            method_944();
         }
         if(method_588(_Item.var_420) && int(class_885.var_308._games[var_536.var_215]._type) == 1)
         {
            var_536.var_227 = [int(var_536.var_227[0] * 1.25)];
            var_536.var_238 = [var_536.var_227[0]];
            method_947(class_702.var_304.method_98(16,"items"));
         }
      }
      
      public function method_935(param1:int) : void
      {
         var_447.var_683 = int(var_447.var_683) + param1;
         if(int(var_447.var_683) < 0)
         {
            var_447.var_683 = 0;
         }
         method_933(var_447);
         if(param1 < 0)
         {
            method_948();
         }
      }
      
      public function method_934(param1:int) : void
      {
         var_288.var_683 = int(var_288.var_683) + param1;
         if(int(var_288.var_683) < 0)
         {
            var_288.var_683 = 0;
         }
         method_933(var_288);
         if(param1 < 0)
         {
            var_288.var_282.method_290([class_702.var_288.method_136("hero_hurt"),class_702.var_288.method_136("hero_front")]);
            new class_873(var_288.var_282,4,0,0.8);
         }
      }
      
      public function method_949() : void
      {
         if(var_1261 == null)
         {
            return;
         }
         var_1261.visible = false;
      }
      
      public function method_925(param1:_GameBonus) : Boolean
      {
         if(class_652.var_214 == null)
         {
            return true;
         }
         var _loc2_:* = class_765.var_214.package_31._inv;
         switch(int(param1.var_295))
         {
            case 0:
               return int(_loc2_._cheese) > 0;
            case 1:
               return int(_loc2_._leaf) > 0;
            case 2:
               return int(_loc2_._knife) > 0;
         }
      }
      
      public function method_588(param1:_Item) : Boolean
      {
         if(class_652.var_214 == null)
         {
            return false;
         }
         return Boolean(class_765.var_214.method_588(param1));
      }
      
      public function method_938() : int
      {
         var_795 = int((var_795 + 1) % int(var_1262.length));
         return int(var_1262[var_795]);
      }
      
      public function method_945() : int
      {
         if(class_652.var_214 == null)
         {
            return 12;
         }
         return int(class_765.var_214.method_597());
      }
      
      public function method_950() : class_738
      {
         var _loc1_:class_738 = new class_738();
         _loc1_.method_128(class_702.var_304.method_98(1,"heart"));
         _loc1_.y = 4;
         _loc1_.tabIndex = 1;
         var_1256[1].var_232.method_124(_loc1_,2);
         return _loc1_;
      }
      
      public function method_946(param1:int, param2:int) : Object
      {
         var _loc6_:int = 0;
         var _loc7_:class_738 = null;
         var _loc3_:class_656 = new class_656();
         var_1256[1].var_232.method_124(_loc3_,1);
         _loc3_.x = 9;
         _loc3_.y = 1;
         if(param1 == 1)
         {
            _loc3_.x = class_742.var_216 * 0.5 - _loc3_.x;
         }
         var _loc4_:* = {
            "\bOX!\x03":_loc3_,
            "A_U{\x01":param2,
            "C\x13T;":[],
            "Y\\-z":param2
         };
         var _loc5_:int = 0;
         while(_loc5_ < param2)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = method_950();
            _loc7_.x = 21 + _loc6_ * 9;
            _loc4_.name_122.push(_loc7_);
            if(param1 == 1)
            {
               _loc7_.x = class_742.var_216 * 0.5 - _loc7_.x;
            }
         }
         return _loc4_;
      }
      
      public function method_948() : void
      {
         var_447.var_282.method_290([class_702.var_242.method_136(package_31._anim + "_hurt"),class_702.var_242.method_136(package_31._anim)]);
         new class_873(var_447.var_282,4,0,0.8);
      }
      
      public function method_936(param1:Boolean) : void
      {
         class_908.method_266();
         var_303 = null;
         class_652.var_214.method_186(_PlayerAction._GameResult(param1,var_1253));
         class_652.var_214.name_14 = param1;
         var _loc2_:class_647 = null;
         switch(var_328)
         {
            case 0:
               if(param1)
               {
                  _loc2_ = new class_879(this);
                  break;
               }
               _loc2_ = new class_924(this);
               break;
            case 1:
            case 2:
               _loc2_ = new class_932(this);
         }
      }
      
      public function method_939() : void
      {
         var_1258.parent.removeChild(var_1258);
         var_1258.bitmapData.dispose();
         var_1258 = null;
         method_835();
      }
      
      override public function method_524() : void
      {
         var_1255 = false;
         method_949();
         if(int(var_447.var_683) <= 0 || var_328 == 2)
         {
            method_936(true);
         }
         else if(int(var_288.var_683) <= 0 || var_328 == 1)
         {
            method_936(false);
         }
         if(var_220 || !method_588(_Item.var_404))
         {
            method_940(Number(var_237 + int(package_31._tempInc) * 0.01));
         }
         method_942();
      }
      
      public function method_947(param1:class_750) : void
      {
         var _loc2_:Number = NaN;
         if(var_1261 == null)
         {
            var_1261 = new class_738();
            var_1261.y = class_858.var_1010;
            _loc2_ = 2;
            var_1261.scaleY = _loc2_;
            var_1261.scaleX = _loc2_;
            class_658.var_146(var_1261,4,200,0);
            var_232.method_124(var_1261,class_756.var_297);
         }
         var_1261.visible = true;
         var_1261.method_128(param1,0,0);
      }
   }
}
