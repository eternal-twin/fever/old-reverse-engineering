package package_14.package_15
{
   import flash.display.BlendMode;
   import flash.display.Graphics;
   import flash.display.Sprite;
   import flash.filters.GlowFilter;
   import flash.text.TextField;
   import package_13.class_672;
   import package_13.class_719;
   import package_13.class_765;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_747;
   
   public class class_680 extends class_672
   {
      
      public static var var_477:int = 34816;
      
      public static var var_478:int = 17408;
       
      
      public var var_251:int;
      
      public var var_105:class_738;
      
      public var var_479:Sprite;
      
      public var name_49:Sprite;
      
      public var var_267:class_719;
      
      public function class_680()
      {
         var _loc10_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:* = null;
         var _loc17_:_IslandStatus = null;
         var _loc18_:* = null;
         var _loc19_:* = null;
         var _loc20_:int = 0;
         var _loc21_:int = 0;
         var _loc22_:int = 0;
         var _loc23_:int = 0;
         var _loc24_:Array = null;
         var _loc25_:* = null;
         var _loc26_:_IslandStatus = null;
         var _loc27_:int = 0;
         var _loc28_:int = 0;
         var _loc29_:Array = null;
         var _loc30_:_Reward = null;
         var _loc31_:int = 0;
         var _loc32_:int = 0;
         var _loc33_:Sprite = null;
         if(class_899.var_239)
         {
            return;
         }
         var_431 = 128;
         var_432 = 128;
         super();
         var_251 = 0;
         var_267 = class_652.var_214.var_288.var_267;
         y = y - 27;
         var_105 = new class_738();
         var_105.method_128(class_702.package_15.method_98(null,"radar"),0,0);
         var_232.method_124(var_105,0);
         var _loc1_:Sprite = new Sprite();
         var_232.method_124(_loc1_,0);
         _loc1_.graphics.beginFill(class_680.var_478);
         _loc1_.graphics.drawRect(0,0,var_431,var_432);
         var _loc2_:Sprite = new Sprite();
         _loc2_.graphics.beginFill(16711680);
         _loc2_.graphics.drawCircle(0,0,52);
         _loc2_.x = var_431 * 0.5;
         _loc2_.y = var_432 * 0.5;
         var_232.method_124(_loc2_,0);
         _loc1_.mask = _loc2_;
         var _loc3_:Sprite = new Sprite();
         _loc3_.blendMode = BlendMode.OVERLAY;
         var _loc5_:int = int(var_431 / 4);
         var _loc6_:int = int(var_432 / 4);
         _loc3_.graphics.lineStyle(1,16777215,0.2);
         var _loc9_:int = 0;
         while(_loc9_ < _loc5_)
         {
            _loc9_++;
            _loc10_ = _loc9_;
            _loc3_.graphics.moveTo(2 + _loc10_ * 4,0);
            _loc3_.graphics.lineTo(2 + _loc10_ * 4,var_432);
         }
         _loc9_ = 0;
         while(_loc9_ < _loc6_)
         {
            _loc9_++;
            _loc10_ = _loc9_;
            _loc3_.graphics.moveTo(0,2 + _loc10_ * 4);
            _loc3_.graphics.lineTo(var_431,2 + _loc10_ * 4);
         }
         _loc1_.addChild(_loc3_);
         var_479 = new Sprite();
         _loc1_.addChild(var_479);
         var _loc11_:Graphics = _loc1_.graphics;
         _loc9_ = 4;
         _loc10_ = 1 + 2 * _loc9_;
         var _loc12_:int = 0;
         while(_loc12_ < _loc10_)
         {
            _loc12_++;
            _loc13_ = _loc12_;
            _loc14_ = 0;
            while(_loc14_ < _loc10_)
            {
               _loc14_++;
               _loc15_ = _loc14_;
               _loc16_ = class_911.method_127(var_267.var_319 + _loc13_ - _loc9_,var_267.var_318 + _loc15_ - _loc9_);
               _loc17_ = class_765.var_214.method_307(int(_loc16_.var_244),int(_loc16_.var_245));
               _loc18_ = class_911.var_214.method_175(int(_loc16_.var_244),int(_loc16_.var_245));
               _loc19_ = method_294(_loc13_ - _loc9_,_loc15_ - _loc9_);
               _loc20_ = 5;
               _loc21_ = 12;
               _loc22_ = 0;
               while(_loc22_ < 2)
               {
                  _loc22_++;
                  _loc23_ = _loc22_;
                  _loc24_ = class_742.var_264[_loc23_];
                  _loc25_ = class_911.method_127(int(_loc16_.var_244) + int(_loc24_[0]),int(_loc16_.var_245) + int(_loc24_[1]));
                  _loc26_ = class_765.var_214.method_307(int(_loc25_.var_244),int(_loc25_.var_245));
                  if(!_loc18_.var_480[_loc23_] && (_loc17_ != _IslandStatus.var_392 || _loc26_ != _IslandStatus.var_392))
                  {
                     _loc27_ = class_680.var_477;
                     _loc11_.lineStyle(4,_loc27_,1);
                     _loc28_ = 2;
                     _loc11_.moveTo(Number(Number(_loc19_.var_244) + int(_loc24_[0]) * (_loc20_ - _loc28_)),Number(Number(_loc19_.var_245) + int(_loc24_[1]) * (_loc20_ - _loc28_)));
                     _loc11_.lineTo(Number(Number(_loc19_.var_244) + int(_loc24_[0]) * (_loc20_ * 2 - _loc28_)),Number(Number(_loc19_.var_245) + int(_loc24_[1]) * (_loc20_ * 2 - _loc28_)));
                  }
               }
               _loc22_ = int(_loc18_.name_15.var_242.length);
               if(_loc18_.var_309 != null)
               {
                  _loc22_++;
               }
               _loc23_ = 0;
               _loc24_ = _loc17_.var_296;
               switch(int(_loc17_.var_295))
               {
                  case 0:
                     _loc23_ = 0;
                     break;
                  case 1:
                     _loc29_ = _loc24_[0];
                     _loc30_ = _loc24_[1];
                     _loc23_ = class_680.var_477;
                     _loc22_ = _loc22_ - int(_loc29_.length);
                     _loc27_ = 1 + int(Number(Math.sqrt(_loc22_)));
                     _loc28_ = -_loc27_;
                     _loc31_ = 0;
                     while(_loc31_ < _loc22_)
                     {
                        _loc31_++;
                        _loc32_ = _loc31_;
                        _loc33_ = new Sprite();
                        _loc33_.graphics.beginFill(65280);
                        _loc33_.graphics.drawRect(0,0,1,1);
                        _loc33_.x = Number(Number(_loc28_ + Number(_loc19_.var_244)) + int(_loc32_ % _loc27_) * 2);
                        _loc33_.y = Number(Number(_loc28_ + Number(_loc19_.var_245)) + int(_loc32_ / _loc27_) * 2);
                        var_479.addChild(_loc33_);
                     }
                     break;
                  case 2:
                     _loc23_ = class_680.var_477;
               }
               if(_loc23_ > 0)
               {
                  _loc11_.lineStyle();
                  _loc11_.beginFill(_loc23_);
                  _loc11_.drawRect(_loc19_.var_244 - _loc20_,_loc19_.var_245 - _loc20_,_loc20_ * 2,_loc20_ * 2);
               }
            }
         }
         _loc16_ = method_294(0,0);
         var _loc34_:class_738 = new class_738();
         _loc34_.method_128(class_702.package_9.method_98(null,"scan_hero"));
         _loc34_.x = Number(_loc16_.var_244);
         _loc34_.y = Number(_loc16_.var_245);
         _loc1_.addChild(_loc34_);
         new class_747(_loc34_,-1,8,4);
         var _loc35_:TextField = class_742.method_308(65280,8,-1);
         _loc12_ = int(class_911.var_214.var_307 * 0.5);
         _loc35_.text = "[" + (var_267.var_319 - _loc12_) + "][" + (var_267.var_318 - _loc12_) + "]";
         _loc35_.x = int((var_431 - _loc35_.textWidth) * 0.5) - 1;
         _loc35_.y = var_432 - 22;
         addChild(_loc35_);
         _loc35_.filters = [new GlowFilter(17408,1,2,2,40)];
      }
      
      override public function method_79(param1:*) : void
      {
         super.method_79(param1);
      }
      
      public function method_294(param1:int, param2:int) : Object
      {
         return {
            "L\x01":Number(var_431 * 0.5 + param1 * 12),
            "\x03\x01":Number(var_432 * 0.5 + param2 * 12)
         };
      }
      
      override public function name_5(param1:*) : void
      {
         super.name_5(param1);
         method_89();
      }
   }
}
