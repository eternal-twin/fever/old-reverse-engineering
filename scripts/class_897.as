package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_897 extends class_645
   {
      
      public static var var_350:int = 171;
      
      public static var var_1393:int = 44;
      
      public static var var_332:Array = [54,90];
      
      public static var var_467:int = 20;
       
      
      public var var_1394:Number;
      
      public var var_751:Number;
      
      public var var_351:Number;
      
      public var var_288:class_892;
      
      public var var_1396:int;
      
      public var var_783:Number;
      
      public var var_1397:Boolean;
      
      public var var_1395:Boolean;
      
      public var var_682:class_892;
      
      public var var_470:Array;
      
      public function class_897()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:class_892 = null;
         if(!var_1395 && !name_5)
         {
            var_1395 = true;
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_751 < 0)
               {
                  _loc1_ = null;
                  if(int(var_470.length) > 0)
                  {
                     _loc1_ = var_470[int(var_470.length) - 1];
                  }
                  if(_loc1_ == null || _loc1_.var_244 < class_742.var_217 - 60 && int(class_691.method_114(int(var_783))) == 0)
                  {
                     method_1048();
                  }
               }
               else
               {
                  var_751 = var_751 - 1;
               }
               method_1049();
               method_707();
         }
         super.method_79();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_707() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:class_892 = null;
         switch(var_1396)
         {
            case 0:
               if(var_288.var_244 < class_897.var_1393)
               {
                  var_288.var_250 = 0.5;
                  var_288.var_278 = var_288.var_278 - 1;
                  var_1396 = 1;
               }
               _loc1_ = method_100().var_244 - var_288.var_244;
               _loc2_ = 0.8;
               _loc3_ = Number(class_663.method_99(-_loc2_,_loc1_ * 0.05,_loc2_));
               var_288.var_278 = Number(var_288.var_278 + _loc3_);
               var_288.var_278 = var_288.var_278 * 0.8;
               _loc4_ = Number(Math.abs(_loc3_));
               if(_loc4_ < 0.1)
               {
                  var_1394 = 0;
                  var_288.var_243.gotoAndStop("1");
               }
               else
               {
                  var_1394 = (var_1394 + _loc4_ * 1.2) % 11;
                  var_288.var_243.gotoAndStop(5 + int(var_1394));
                  if(var_288.var_278 * var_288.var_243.scaleX < 0)
                  {
                     var_288.var_243.scaleX = var_288.var_243.scaleX * -1;
                  }
               }
               if(var_682 != null)
               {
                  _loc5_ = var_682.var_244 - var_288.var_244;
                  if(_loc5_ < 0 || _loc5_ > Number(var_682.var_4))
                  {
                     var_288.var_243.gotoAndStop("10");
                     var_1397 = true;
                     var_288.var_250 = 0.5;
                     var_1396 = 1;
                     var_682 = null;
                  }
               }
               if(var_682 != null)
               {
                  var_288.var_244 = Number(var_288.var_244 + var_682.var_278);
                  var_288.var_245 = Number(var_288.var_245 + var_682.var_279);
               }
               if(name_5)
               {
                  var_1395 = false;
                  var_288.var_243.gotoAndStop("10");
                  var_1397 = true;
                  var_288.var_250 = 0.5;
                  var_288.var_279 = -(10 - var_237[0] * 3);
                  var_1396 = 1;
                  var_682 = null;
                  break;
               }
               break;
            case 1:
               if(var_288.var_279 > 0 && var_288.var_244 >= class_897.var_1393)
               {
                  if(!!var_1397 && var_288.var_245 > class_897.var_350 - class_897.var_467)
                  {
                     var_1397 = false;
                     _loc6_ = 0;
                     _loc7_ = var_470;
                     while(_loc6_ < int(_loc7_.length))
                     {
                        _loc8_ = _loc7_[_loc6_];
                        _loc6_++;
                        if(_loc8_.var_250 == null)
                        {
                           _loc1_ = _loc8_.var_244 - var_288.var_244;
                           if(_loc1_ > 0 && _loc1_ < Number(_loc8_.var_4))
                           {
                              var_1396 = 0;
                              var_288.var_250 = 0;
                              var_288.var_278 = 0;
                              var_288.var_279 = 0;
                              var_288.var_243.gotoAndPlay("land");
                              var_288.var_245 = class_897.var_350 - class_897.var_467;
                              var_682 = _loc8_;
                              break;
                           }
                        }
                     }
                  }
                  if(var_288.var_245 > class_897.var_350)
                  {
                     var_288.var_245 = class_897.var_350;
                     var_1396 = 0;
                     var_288.var_250 = null;
                     var_288.var_278 = 0;
                     var_288.var_279 = 0;
                     var_288.var_243.gotoAndPlay("land");
                     break;
                  }
                  break;
               }
               break;
            case 2:
               if(var_288.var_245 > class_897.var_350 - 6 && var_288.var_244 > class_897.var_1393)
               {
                  var_288.var_245 = class_897.var_350 - 6;
                  var_288.var_279 = var_288.var_279 * -1;
                  break;
               }
         }
         if(var_288.var_245 > class_742.var_219 || var_288.var_244 < -10)
         {
            var_229 = [false];
            method_81(false);
         }
      }
      
      public function method_1049() : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:class_892 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_470;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc1_ != null)
            {
               _loc5_ = Number(Math.abs(_loc4_.var_244 - _loc1_.var_244));
               if(_loc5_ < Number(_loc4_.var_4))
               {
                  _loc6_ = _loc4_.var_4 - _loc5_;
                  _loc4_.var_244 = Number(_loc4_.var_244 + _loc6_ * 0.5);
                  _loc1_.var_244 = _loc1_.var_244 - _loc6_ * 0.5;
               }
            }
            if(_loc4_.var_244 - _loc4_.var_4 * 0.5 < class_897.var_1393)
            {
               if(_loc4_.var_250 == null)
               {
                  _loc4_.var_250 = 0.75;
                  _loc4_.var_580 = -4;
               }
            }
            else
            {
               _loc1_ = _loc4_;
            }
            _loc5_ = _loc4_.var_244 - _loc4_.var_4 - var_288.var_244;
            if(!_loc4_.var_615 && _loc5_ < 0 || _loc4_.var_615 && _loc5_ > 0)
            {
               if(var_1397)
               {
                  _loc4_.var_615 = !_loc4_.var_615;
               }
               else if(var_1396 != 2)
               {
                  var_1396 = 2;
                  var_288.var_250 = 0.5;
                  var_288.var_278 = _loc4_.var_278 * 2;
                  var_288.var_279 = -(3 + Math.random() * 3);
                  var_288.var_580 = 12;
                  var_288.var_243.gotoAndStop("ouch");
                  var_288.var_245 = var_288.var_245 - 6;
                  _loc4_.var_278 = _loc4_.var_278 * 0.5;
                  var_229 = [true];
               }
            }
            if(_loc4_.var_244 < 0)
            {
               _loc4_.method_89();
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200];
         super.method_95(param1);
         var_1395 = true;
         var_1396 = 0;
         var_1394 = 0;
         var_751 = 20;
         var_783 = 20 - param1 * 17;
         var_351 = Number(2 + param1 * 4);
         var_1397 = false;
         var_470 = [];
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("E{YR\x02"),0);
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("hZa\x06\x03"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_244 = 74;
         _loc1_.var_245 = class_897.var_350;
         _loc1_.method_118();
         var_288 = _loc1_;
      }
      
      public function method_1048() : void
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\nQdC\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_278 = -(var_351 + Math.random() * var_351);
         _loc1_.var_4 = int(class_897.var_332[0]);
         _loc1_.var_244 = Number(Number(class_742.var_217 + Number(_loc1_.var_4)) + 10);
         _loc1_.var_245 = class_897.var_350;
         _loc1_.var_233 = 1;
         _loc1_.var_615 = false;
         _loc1_.method_118();
         _loc1_.var_243.gotoAndStop(int(class_691.method_114(_loc1_.var_243.totalFrames)) + 1);
         var_470.push(_loc1_);
      }
   }
}
