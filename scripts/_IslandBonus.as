package
{
   import package_32.class_899;
   
   public final class _IslandBonus
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = [
         "+~E\x10\x01",
         "\x1bl@7\x02",
         "*#r\x1c\x02",
      ];
      
      public static var var_398:_IslandBonus;
      
      public static var var_399:_IslandBonus;
      
      public static var var_400:_IslandBonus;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function _IslandBonus(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
