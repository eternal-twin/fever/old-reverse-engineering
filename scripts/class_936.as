package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_32.class_899;
   
   public class class_936 extends class_645
   {
       
      
      public var var_341:int;
      
      public var _Iz8:Number;
      
      public var var_340:int;
      
      public var var_382:Number;
      
      public var var_1525:Object;
      
      public var var_1550:Array;
      
      public var var_307:int;
      
      public var var_1551:Array;
      
      public var var_1552:int;
      
      public var var_1553:int;
      
      public var var_470:Array;
      
      public var var_41:MovieClip;
      
      public function class_936()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1161();
            case 2:
               method_1161();
         }
         method_1162();
         super.method_79();
      }
      
      public function method_400(param1:int, param2:int) : void
      {
         var _loc5_:* = null;
         var _loc3_:int = 0;
         var _loc4_:Array = var_470;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(int(_loc5_.var_244) == param1 && int(_loc5_.var_245) == param2)
            {
               method_1163();
               return;
            }
         }
         _loc3_ = int(var_1551[int(var_470.length)].var_215);
         _loc5_ = var_1550[param1][param2];
         if(int(_loc5_.var_215) == _loc3_)
         {
            var_470.push({
               "L\x01":param1,
               "\x03\x01":param2
            });
            class_657.gfof(_loc5_.var_631,0.8,16777215);
            if(int(var_470.length) == int(var_1551.length))
            {
               method_81(true,20);
            }
         }
         else
         {
            method_1163();
         }
      }
      
      public function method_1165(param1:int, param2:int) : void
      {
         if(var_220)
         {
            return;
         }
         if(int(var_470.length) == 0)
         {
            method_400(param1,param2);
            return;
         }
         var _loc3_:* = var_470[int(var_470.length) - 1];
         var _loc4_:int = int(_loc3_.var_244) - param1;
         var _loc5_:int = int(_loc3_.var_245) - param2;
         var _loc6_:Number = Number(Number(Math.abs(_loc4_)) + Number(Math.abs(_loc5_)));
         if(_loc6_ == 0)
         {
            method_1164(param1,param2);
         }
         else if(_loc6_ == 1)
         {
            method_400(param1,param2);
         }
         else
         {
            method_1163();
         }
      }
      
      public function method_1166(param1:int, param2:int) : void
      {
         var var_244:int = param1;
         var var_245:int = param2;
         var _loc3_:int = int(class_691.method_114(var_1552));
         var _loc4_:MovieClip = var_232.method_84(class_899.__unprotect__("\x01N&x\x03"),class_645.var_209);
         _loc4_.x = Number(var_382 * 0.5 + (var_244 + 0.5) * var_307);
         _loc4_.y = Number(_Iz8 * 0.66 + (var_245 + 1.5) * var_307);
         _loc4_.scaleX = var_307 * 0.01;
         _loc4_.scaleY = var_307 * 0.01;
         _loc4_.gotoAndStop(_loc3_ + 1);
         var _loc5_:MovieClip = Reflect.field(_loc4_,class_899.__unprotect__("U\\\x01"));
         _loc5_.gotoAndStop("1");
         var var_214:class_936 = this;
         _loc4_.addEventListener(MouseEvent.CLICK,function(param1:*):void
         {
            var_214.method_1165(var_244,var_245);
         });
         _loc4_.mouseEnabled = true;
         _loc4_.useHandCursor = true;
         var_1550[var_244][var_245] = {
            "gB\x01":_loc4_,
            "\x1d\x0b\x01":_loc3_
         };
      }
      
      public function method_1167(param1:int) : void
      {
         var _loc2_:int = int(var_1551[param1].var_215);
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__("\x01N&x\x03"),class_645.var_209);
         _loc3_.x = Number(var_382 * 0.5 + (param1 + 0.5) * var_307);
         _loc3_.y = Number(_Iz8 * 0.33 + 0.5 * var_307);
         _loc3_.scaleX = var_307 * 0.01;
         _loc3_.scaleY = var_307 * 0.01;
         _loc3_.gotoAndStop(_loc2_ + 1);
         var _loc4_:MovieClip = Reflect.field(_loc3_,class_899.__unprotect__("U\\\x01"));
         _loc4_.gotoAndStop("5");
         var_1551[param1].var_631 = _loc3_;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400 - param1];
         super.method_95(param1);
         var_1553 = 8;
         var_1552 = 5;
         var_307 = 50 - int(Math.round(param1 * 20));
         if(var_307 < 16)
         {
            var_307 = 16;
         }
         method_117();
         var_470 = [];
         method_72();
      }
      
      public function method_1162() : void
      {
         if(var_1525 == null)
         {
            var_1525 = 0;
         }
         var_1525 = int((int(var_1525 + 32)) % 628);
         var _loc1_:* = var_1551[int(var_470.length)];
         var _loc2_:Number = Number(0.5 + Math.cos(var_1525 * 0.01) * 0.5);
         if(_loc1_ == null)
         {
            return;
         }
         _loc1_.var_631.filters = [];
         class_658.var_146(_loc1_.var_631,3,_loc2_ * 4,16777215);
         class_658.var_146(_loc1_.var_631,20 * _loc2_,1.5,16776960);
         var_232.method_110(_loc1_.var_631);
      }
      
      public function method_1163() : void
      {
         var _loc1_:* = null;
         var _loc2_:MovieClip = null;
         while(int(var_470.length) > 0)
         {
            _loc1_ = var_470.pop();
            _loc2_ = var_1550[int(_loc1_.var_244)][int(_loc1_.var_245)].var_631;
            class_657.gfof(_loc2_,0,16777215);
         }
      }
      
      public function method_1161() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:MovieClip = null;
         var _loc6_:Number = NaN;
         var _loc1_:int = 0;
         var _loc2_:int = int(var_1551.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = var_307 * 0.01;
            if(_loc3_ < int(var_470.length))
            {
               _loc4_ = 0;
            }
            _loc5_ = var_1551[_loc3_].var_631;
            _loc6_ = _loc4_ - _loc5_.scaleX;
            _loc5_.scaleX = Number(_loc5_.scaleX + _loc6_ * 0.5);
            _loc5_.scaleY = _loc5_.scaleX;
         }
      }
      
      public function method_1164(param1:int, param2:int) : void
      {
         var _loc3_:MovieClip = var_1550[param1][param2].var_631;
         class_657.gfof(_loc3_,0,16777215);
         var_470.pop();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc9_:* = null;
         var _loc10_:* = null;
         var _loc11_:Boolean = false;
         var _loc12_:* = null;
         class_319 = var_232.method_84(class_899.__unprotect__("g{ST"),0);
         var_1550 = [];
         var_340 = int(Math.floor((class_742.var_217 - 8) / var_307));
         var_382 = class_742.var_217 - var_340 * var_307;
         var_341 = int(Math.floor((class_742.var_219 - (60 + var_307)) / var_307));
         _Iz8 = class_742.var_219 - (var_341 + 1) * var_307;
         var _loc1_:int = 0;
         _loc2_ = var_340;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_1550[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = var_341;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               method_1166(_loc3_,_loc6_);
            }
         }
         var_1551 = [];
         var _loc7_:Array = [{
            "L\x01":-1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":1
         },{
            "L\x01":1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":-1
         }];
         var _loc8_:Array = [{
            "L\x01":int(class_691.method_114(var_340)),
            "\x03\x01":int(class_691.method_114(var_341))
         }];
         var_1551.push({
            "\x1d\x0b\x01":int(var_1550[int(_loc8_[0].var_244)][int(_loc8_[0].var_245)].var_215),
            "gB\x01":null
         });
         _loc1_ = 0;
         while(int(_loc8_.length) < var_340)
         {
            _loc9_ = _loc7_[int(class_691.method_114(int(_loc7_.length)))];
            _loc10_ = _loc8_[int(_loc8_.length) - 1];
            _loc2_ = int(_loc10_.var_244) + int(_loc9_.var_244);
            _loc3_ = int(_loc10_.var_245) + int(_loc9_.var_245);
            _loc11_ = _loc2_ >= 0 && _loc2_ < var_340 && _loc3_ >= 0 && _loc3_ < var_341;
            _loc4_ = 0;
            while(_loc4_ < int(_loc8_.length))
            {
               _loc12_ = _loc8_[_loc4_];
               _loc4_++;
               if(_loc2_ == int(_loc12_.var_244) && _loc3_ == int(_loc12_.var_245))
               {
                  _loc11_ = false;
                  break;
               }
            }
            if(_loc11_)
            {
               _loc8_.push({
                  "L\x01":_loc2_,
                  "\x03\x01":_loc3_
               });
               var_1551.push({
                  "\x1d\x0b\x01":int(var_1550[_loc2_][_loc3_].var_215),
                  "gB\x01":null
               });
            }
            _loc1_++;
            if(_loc1_ > 20)
            {
               _loc1_ = 0;
               while(int(_loc8_.length) > 1)
               {
                  _loc8_.pop();
                  var_1551.pop();
               }

            }
         }
         _loc2_ = 0;
         _loc3_ = int(var_1551.length);
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            method_1167(_loc4_);
         }
      }
   }
}
