package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_727 extends class_645
   {
       
      
      public var var_703:int;
      
      public var var_704:int;
      
      public var var_705:Number;
      
      public var var_100:MovieClip;
      
      public var var_706:String;
      
      public var var_609:Number;
      
      public var var_702:MovieClip;
      
      public var var_707:int;
      
      public var var_709:class_892;
      
      public var var_714:Number;
      
      public var var_712:Number;
      
      public var var_715:Boolean;
      
      public var var_710:Number;
      
      public var var_488:Array;
      
      public var var_708:Array;
      
      public var var_711:Number;
      
      public var var_713:class_15;
      
      public function class_727()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Array = null;
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_440();
               method_441();
               method_442();
               _loc1_ = var_488.method_78();
               _loc2_ = 0;
               while(_loc2_ < int(_loc1_.length))
               {
                  _loc3_ = _loc1_[_loc2_];
                  _loc2_++;
                  _loc3_.var_279 = Number(Number(_loc3_.var_279) + 0.4);
                  _loc3_.var_279 = _loc3_.var_279 * 0.98;
                  _loc3_.y = Number(_loc3_.y + Number(_loc3_.var_279));
                  if(_loc3_.y > Number(_loc3_.name_64))
                  {
                     _loc3_.y = Number(_loc3_.name_64);
                     _loc3_.x = Number(_loc3_.var_587);
                     var_488.method_116(_loc3_);
                  }
               }
               if(var_702.rotation > 0)
               {
                  var_702.var_278 = var_702.var_278 * 0.9;
               }
               else
               {
                  _loc2_ = 5;
                  if(var_702.x < _loc2_)
                  {
                     var_702.x = _loc2_;
                     name_48();
                  }
                  if(var_702.x > class_742.var_217 - (_loc2_ + 28))
                  {
                     var_702.x = class_742.var_217 - (_loc2_ + 28);
                     name_48();
                  }
                  if(int(class_691.method_114(60)) == 0)
                  {
                     name_48();
                  }
               }
               var_702.x = Number(var_702.x + Number(var_702.var_278));
            case 2:
               method_440();
               method_441();
               method_442();
               _loc1_ = var_488.method_78();
               _loc2_ = 0;
               while(_loc2_ < int(_loc1_.length))
               {
                  _loc3_ = _loc1_[_loc2_];
                  _loc2_++;
                  _loc3_.var_279 = Number(Number(_loc3_.var_279) + 0.4);
                  _loc3_.var_279 = _loc3_.var_279 * 0.98;
                  _loc3_.y = Number(_loc3_.y + Number(_loc3_.var_279));
                  if(_loc3_.y > Number(_loc3_.name_64))
                  {
                     _loc3_.y = Number(_loc3_.name_64);
                     _loc3_.x = Number(_loc3_.var_587);
                     var_488.method_116(_loc3_);
                  }
               }
               if(var_702.rotation > 0)
               {
                  var_702.var_278 = var_702.var_278 * 0.9;
               }
               else
               {
                  _loc2_ = 5;
                  if(var_702.x < _loc2_)
                  {
                     var_702.x = _loc2_;
                     name_48();
                  }
                  if(var_702.x > class_742.var_217 - (_loc2_ + 28))
                  {
                     var_702.x = class_742.var_217 - (_loc2_ + 28);
                     name_48();
                  }
                  if(int(class_691.method_114(60)) == 0)
                  {
                     name_48();
                  }
               }
               var_702.x = Number(var_702.x + Number(var_702.var_278));
         }
      }
      
      public function name_48() : void
      {
         var_702.var_278 = var_702.var_278 * -1;
         var_702.scaleX = var_702.scaleX * -1;
      }
      
      public function method_443() : class_892
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("T\x1fpb\x01"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_244 = Number(class_742.var_217 * 0.5 + (Math.random() * 2 - 1) * 10);
         _loc1_.var_245 = class_742.var_219 + Math.random() * 5 - 55;
         var _loc3_:Number = (Math.random() * 2 - 1) * 0.4 - 1.57;
         var _loc4_:Number = Number(5 + Math.random() * 10);
         _loc1_.var_278 = Math.cos(_loc3_) * _loc4_;
         _loc1_.var_279 = Math.sin(_loc3_) * _loc4_;
         _loc1_.name_65 = (Math.random() * 2 - 1) * _loc4_;
         _loc1_.var_250 = 0.2;
         _loc1_.var_66 = 4;
         _loc1_.name_66 = 0;
         _loc1_.name_67 = 2000;
         _loc1_.var_580 = Math.random() * 5;
         _loc1_.var_243.rotation = Math.random() * 360;
         _loc1_.var_243.gotoAndStop(int(class_691.method_114(2)) + 1);
         _loc1_.method_118();
         var_708.push(_loc1_);
         return _loc1_;
      }
      
      public function method_442() : void
      {
         var _loc3_:class_892 = null;
         var _loc1_:* = method_100();
         _loc1_.var_245 = Number(Number(_loc1_.var_245) + 10);
         var_709.method_229(_loc1_,0.4,null);
         var _loc2_:Number = -var_709.var_243.rotation;
         if(name_5)
         {
            _loc2_ = 90 - var_709.var_243.rotation;
            if(_loc2_ < 5 && var_709.var_243.currentFrame > 1)
            {
               var_710 = var_710 - 1;
               if(var_710 < 0)
               {
                  _loc3_ = method_443();
                  _loc3_.var_244 = Number(var_709.var_244 + var_711);
                  _loc3_.var_245 = Number(var_709.var_245 + (var_712 - var_711 * 2));
                  _loc3_.var_278 = Number(0.1 + Math.random() * 0.5);
                  _loc3_.var_279 = 0;
                  _loc3_.name_65 = 0;
                  method_444(_loc3_);
                  _loc3_.method_118();
                  var_710 = 8;
                  var_709.var_243.prevFrame();
               }
            }
         }
         else
         {
            var_710 = 0;
         }
         var_709.var_243.rotation = Number(var_709.var_243.rotation + _loc2_ * 0.3);
      }
      
      public function method_441() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         var _loc5_:Boolean = false;
         var _loc6_:Number = NaN;
         var _loc7_:MovieClip = null;
         var _loc1_:Array = var_708.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.name_65 = _loc3_.name_65 * 0.96;
            _loc3_.name_66 = Number(Number(_loc3_.name_66) + Number(_loc3_.name_65));
            _loc4_ = Number(Math.min(Number(Math.max(0,Number(20 + _loc3_.name_66 * 0.4))),80));
            class_657.gfof(_loc3_.var_243,_loc4_ * 0.01,4693579);
            if(var_709.var_243.rotation < 5 && _loc3_.var_279 > 0 && _loc3_.name_68 != null)
            {
               _loc5_ = _loc3_.var_245 < var_709.var_245;
               if(!_loc5_ && _loc3_.name_68)
               {
                  _loc6_ = Number(Math.abs(var_709.var_244 - _loc3_.var_244));
                  if(_loc6_ < var_712 - var_711)
                  {
                     if(var_709.var_243.currentFrame == 5)
                     {
                        _loc3_.var_279 = _loc3_.var_279 * -0.8;
                        _loc3_.var_245 = var_709.var_245;
                     }
                     else
                     {
                        var_709.var_279 = Number(var_709.var_279 + 6);
                        var_709.var_243.nextFrame();
                        _loc3_.name_67 = 0;
                     }
                  }
                  else if(_loc6_ < Number(var_712 + var_711))
                  {
                  }
               }
               _loc3_.name_68 = _loc5_;
            }
            if(Number(_loc3_.name_67) > 0 && _loc3_.var_279 > 0 && _loc3_.name_69 != null)
            {
               _loc5_ = _loc3_.var_245 < var_100.y - var_705;
               if(!_loc5_ && _loc3_.name_69)
               {
                  _loc6_ = Number(Math.abs(var_100.x - _loc3_.var_244));
                  if(_loc6_ < (var_609 - var_711) * 1.2)
                  {
                     if(var_704 < var_703)
                     {
                        var_704 = var_704 + 1;
                        _loc7_ = var_100.var_232.method_84(class_899.__unprotect__("T\x1fpb\x01"),1);
                        _loc7_.y = -var_705;
                        _loc7_.var_279 = _loc3_.var_279;
                        _loc7_.name_64 = var_711 - var_704 * (var_711 * 2 * 0.85);
                        _loc7_.var_587 = (int(var_704 % 2) * 2 - 1) * (var_609 - var_711) * 0.7;
                        _loc7_.gotoAndStop(3);
                        var_488.push(_loc7_);
                        _loc3_.name_67 = 0;
                        if(var_704 == var_703)
                        {
                           method_81(true,10);
                        }
                     }
                     var_713.visible = false;
                  }
                  else if(_loc6_ < Number(var_609 + var_711))
                  {
                     if(_loc3_.var_279 > 2)
                     {
                        _loc3_.var_279 = _loc3_.var_279 * -0.8;
                     }
                  }
               }
               _loc3_.name_69 = _loc5_;
            }
            if(_loc3_.name_68 != null)
            {
               if(_loc3_.var_279 > 2 && var_702.rotation == 0 && _loc3_.var_245 > var_714 - (Number(var_711 + 20)) && Number(Math.abs(var_702.x - _loc3_.var_244)) < var_711 * 2)
               {
                  var_702.gotoAndPlay("dead");
                  var_702.rotation = 0.5;
                  _loc3_.var_245 = var_714 - (Number(var_711 + 20));
                  _loc3_.var_279 = _loc3_.var_279 * -0.5;
                  _loc3_.var_580 = (Math.random() * 2 - 1) * 20;
               }
               if(_loc3_.var_245 > var_714 - var_711)
               {
                  _loc3_.var_245 = var_714 - var_711;
                  _loc3_.var_279 = _loc3_.var_279 * -0.5;
                  _loc3_.var_580 = (Math.random() * 2 - 1) * 20;
                  if(Number(_loc3_.name_67) > 20)
                  {
                     _loc3_.name_67 = Number(10 + Math.random() * 10);
                  }
               }
            }
            _loc3_.name_67 = Number(_loc3_.name_67) - 1;
            if(Number(_loc3_.name_67) < 10)
            {
               _loc3_.var_243.scaleX = _loc3_.name_67 * 0.1;
               _loc3_.var_243.scaleY = _loc3_.var_243.scaleX;
            }
            if(_loc3_.var_244 < int(_loc3_.var_66) || _loc3_.var_244 > class_742.var_217 + int(_loc3_.var_66) || _loc3_.var_245 > class_742.var_219 + int(_loc3_.var_66) || Number(_loc3_.name_67) < 0)
            {
               var_708.method_116(_loc3_);
            }
         }
      }
      
      public function method_444(param1:class_892) : void
      {
         param1.var_243.gotoAndStop("3");
         param1.name_68 = false;
         param1.name_69 = false;
         param1.var_250 = 0.4;
         param1.name_65 = 0;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [520];
         super.method_95(param1);
         var_712 = 20;
         var_711 = 4;
         var_609 = 9;
         var_707 = 0;
         var_714 = class_742.var_219 - 8;
         var_704 = 0;
         var_703 = 3 + int(Math.round(param1 * 8));
         var_705 = var_703 * (var_711 * 2 * 0.85);
         var_708 = [];
         var_488 = [];
         method_117();
         method_72();
      }
      
      public function method_440() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_892 = null;
         var _loc1_:int = 0;
         while(_loc1_ < 1)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = method_443();
            if(int(class_691.method_114(16)) == 0 || var_707 > 16)
            {
               method_444(_loc3_);
               var_707 = 0;
            }
            else
            {
               var_707 = var_707 + 1;
            }
            _loc3_.method_118();
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("TE-s\x03"),0);
         var_702 = var_232.method_84(class_899.__unprotect__("&sW^"),class_645.var_209);
         var_702.x = class_742.var_217 * 0.5;
         var_702.y = var_714;
         var_702.var_278 = -4;
         var_100 = var_232.method_92(class_645.var_209);
         var_100.x = class_742.var_217 - (Number(var_609 + 4));
         var_100.y = class_742.var_219;
         var_100.var_232 = new class_755(var_100);
         var _loc1_:MovieClip = var_100.var_232.method_84(class_899.__unprotect__("Sl\f"),2);
         _loc1_.scaleY = var_705 * 0.01;
         var_713 = new class_15();
         var_713.x = var_100.x;
         var_713.y = var_100.y - (Number(var_100.height + 10));
         var_232.method_124(var_713,class_645.var_209);
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x0b}4a\x03"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var_709 = _loc2_;
         var_709.var_244 = class_742.var_217 * 0.5;
         var_709.var_245 = class_742.var_219 * 0.5;
         var_709.var_243.stop();
         var_709.method_118();
      }
   }
}
