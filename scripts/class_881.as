package
{
   import package_10.class_870;
   import package_32.class_899;
   
   public class class_881 extends class_645
   {
       
      
      public var var_1331:Number;
      
      public var var_1334:Number;
      
      public var var_650:class_622;
      
      public var name_26:Boolean;
      
      public var name_41:Boolean;
      
      public var var_1333:Number;
      
      public var var_1332:class_623;
      
      public var var_1335:class_620;
      
      public function class_881()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         if(var_220 == false)
         {
            return;
         }
         if(!!name_5 && var_1332.currentFrame == 1)
         {
            name_26 = true;
         }
         if(name_26)
         {
            var_1333 = Number(var_1333 + var_1331 * 0.2);
         }
         if(var_1333 >= 15)
         {
            var_1333 = 0;
            name_26 = false;
         }
         var_1332.gotoAndStop(int(var_1333) + 1);
         var_1334 = (var_1334 + var_1331) % 100;
         var_650.gotoAndStop(int(var_1334) + 1);
         var_1331 = Number(var_1331 + var_237[0] / 70);
         if((var_650.currentFrame >= 90 || var_650.currentFrame <= 10) && (var_1332.currentFrame <= 2 || var_1332.currentFrame > 12))
         {
            var_1332.gotoAndPlay("fall");
            method_81(false,20);
         }
      }
      
      override public function method_80() : void
      {
         method_81(true,10);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [220];
         super.method_95(param1);
         var_1335 = new class_620();
         addChild(var_1335);
         var_650 = new class_622();
         addChild(var_650);
         var_1332 = new class_623();
         addChild(var_1332);
         class_870.name_18(16777215);
         var_650.stop();
         var_1334 = 11;
         var_1333 = 0;
         var_1331 = Number(param1 * 2 + 3);
      }
   }
}
