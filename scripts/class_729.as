package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_10.class_769;
   import package_32.class_899;
   
   public class class_729 extends class_645
   {
       
      
      public var var_4:Number;
      
      public var name_30:Number;
      
      public var var_344:Number;
      
      public var name_71:int;
      
      public var y693:int;
      
      public var var_718:int;
      
      public var var_719:int;
      
      public var var_717:Boolean;
      
      public var var_716:Array;
      
      public function class_729()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:MovieClip = class_934.method_463(var_716[0].y693)[0];
         if(var_227[0] == 1)
         {
            if(Number(Math.abs(Number(_loc1_.x + 41))) > 2)
            {
               var_227 = [var_227[0] + 1];
            }
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_464();
         }
         super.method_79();
      }
      
      public function method_465(param1:MovieClip) : void
      {
         param1.gotoAndStop(param1.currentFrame == 1?2:1);
      }
      
      public function method_464() : void
      {
         var _loc3_:* = null;
         var _loc4_:MovieClip = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:* = null;
         var _loc8_:MovieClip = null;
         var _loc9_:Boolean = false;
         var _loc10_:int = 0;
         var _loc1_:int = 0;
         var _loc2_:Array = var_716;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(!_loc3_.method_134)
            {
               _loc4_ = _loc3_.y693.name_13();
               _loc5_ = _loc4_.x;
               _loc6_ = _loc3_.y693.name_36().x;
               _loc7_ = _loc3_.y693.method_73();
               while(_loc7_.method_74())
               {
                  _loc8_ = _loc7_.name_1();
                  _loc8_.x = Number(_loc8_.x + Number(_loc3_.var_86));
                  _loc8_.x = _loc8_.x;
                  if(Number(_loc3_.var_86) > 0 && _loc8_.x >= class_742.var_216)
                  {
                     if(int(_loc3_.name_70) > 0)
                     {
                        _loc8_.gotoAndStop(int(_loc3_.name_70));
                        _loc3_.name_70 = -1;
                        _loc3_.name_36 = _loc8_.currentFrame == 1?2:1;
                     }
                     else
                     {
                        _loc9_ = int(class_691.method_114(10)) < name_71;
                        if(_loc9_)
                        {
                           _loc10_ = int(_loc3_.name_36) == 1?2:1;
                           _loc8_.gotoAndStop(_loc10_);
                           _loc3_.name_70 = _loc10_;
                        }
                        else
                        {
                           _loc8_.gotoAndStop(int(_loc3_.name_36) == 1?2:1);
                           _loc3_.name_36 = _loc8_.currentFrame;
                        }
                     }
                     _loc3_.y693.method_116(_loc8_);
                     _loc8_.x = _loc5_ + Number(_loc3_.var_86) - var_4;
                     _loc3_.y693.method_103(_loc8_);
                  }
                  else if(Number(_loc3_.var_86) < 0 && Number(_loc8_.x + var_4) <= 0)
                  {
                     if(int(_loc3_.name_70) > 0)
                     {
                        _loc8_.gotoAndStop(int(_loc3_.name_70));
                        _loc3_.name_70 = -1;
                        _loc3_.name_36 = _loc8_.currentFrame == 1?2:1;
                     }
                     else
                     {
                        _loc9_ = int(class_691.method_114(10)) < name_71;
                        if(_loc9_)
                        {
                           _loc10_ = int(_loc3_.name_36) == 1?2:1;
                           _loc8_.gotoAndStop(_loc10_);
                           _loc3_.name_70 = _loc10_;
                        }
                        else
                        {
                           _loc8_.gotoAndStop(int(_loc3_.name_36) == 1?2:1);
                           _loc3_.name_36 = _loc8_.currentFrame;
                        }
                     }
                     _loc3_.y693.method_116(_loc8_);
                     _loc8_.x = Number(_loc6_ + var_4);
                     _loc3_.y693.method_124(_loc8_);
                  }
               }
            }
         }
      }
      
      override public function method_80() : void
      {
         var _loc3_:* = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:MovieClip = null;
         var _loc11_:Number = NaN;
         var _loc12_:* = null;
         var _loc13_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_716;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(!_loc3_.method_134)
            {
               _loc3_.method_134 = true;
               _loc4_ = _loc3_.y693.name_37 - 1;
               _loc5_ = class_934.method_463(_loc3_.y693);
               _loc6_ = false;
               _loc7_ = 0;
               _loc8_ = int(_loc5_.length);
               while(_loc7_ < _loc8_)
               {
                  _loc7_++;
                  _loc9_ = _loc7_;
                  _loc10_ = _loc5_[_loc9_];
                  _loc10_.mouseEnabled = false;
                  if(_loc10_.x < 0 || Number(_loc10_.x + var_4) > class_742.var_216 || _loc10_.x > class_742.var_216)
                  {
                     _loc10_.parent.removeChild(_loc10_);
                     _loc10_ = null;
                     _loc5_[_loc9_] = null;
                  }
               }
               _loc11_ = 0;
               _loc12_ = null;
               _loc7_ = 0;
               _loc8_ = int(_loc5_.length);
               while(_loc7_ < _loc8_)
               {
                  _loc7_++;
                  _loc9_ = _loc7_;
                  _loc10_ = _loc5_[_loc9_];
                  if(_loc10_ != null)
                  {
                     _loc10_.x = Number(_loc10_.x + _loc11_);
                     if(_loc12_ != null && _loc10_.currentFrame == _loc12_ && !_loc6_)
                     {
                        _loc13_ = var_232.method_84(class_899.__unprotect__("\x18\x03\x11\x15\x03"),2);
                        _loc13_.x = 0;
                        _loc13_.y = _loc10_.y;
                        _loc13_.scaleX = _loc10_.scaleX;
                        _loc13_.scaleY = _loc10_.scaleY;
                        _loc6_ = true;
                        var_717 = false;
                     }
                     else
                     {
                        _loc12_ = _loc10_.currentFrame;
                     }
                  }
               }
            }
         }
         var var_214:class_729 = this;
         if(!var_717)
         {
            class_769.name_47(function():void
            {
               var_214.method_81(false);
            },200);
            return;
         }
         class_769.name_47(function():void
         {
            var_214.method_81(true);
         },1000);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200 + int(class_691.method_114(100))];
         super.method_95(param1);
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("\x05S\x1a\'"),3);
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__("Y\rx5\x03"),3);
         _loc3_.x = class_742.var_216;
         var_716 = [];
         name_30 = Number(1.2 + param1);
         var_718 = 5;
         var _loc4_:Number = class_742.var_216 / 40;
         var_344 = _loc4_ / var_718 * 100;
         y693 = 5;
         name_71 = int(Math.ceil(param1 * 5)) + 1;
         var_4 = 40 * var_344 / 100;
         var_717 = true;
         method_117();
      }
      
      public function name_58(param1:Number, param2:Number, param3:int) : MovieClip
      {
         var var_8:MovieClip = var_232.method_84(class_899.__unprotect__("7g(c\x03"),2);
         var_8.gotoAndStop(param3);
         var _loc4_:Number = var_344 * 0.01;
         var_8.scaleY = _loc4_;
         var_8.scaleX = _loc4_;
         var_8.x = param1;
         var_8.y = param2;
         var_8.x = var_8.x;
         var_8.y = var_8.y;
         var var_214:class_729 = this;
         var_8.addEventListener(MouseEvent.CLICK,function(param1:*):void
         {
            var_214.method_465(var_8);
         });
         return var_8;
      }
      
      public function method_117() : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_795 = null;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("J\x06;\n\x01"),0);
         var _loc1_:Boolean = int(class_691.method_114(2)) == 0?true:false;
         var _loc2_:Boolean = _loc1_;
         var _loc3_:Boolean = true;
         var _loc4_:int = 0;
         var _loc5_:int = var_718;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 3 + int(class_691.method_114(3));
            _loc8_ = new class_795();
            if(_loc3_)
            {
               _loc9_ = !!_loc2_?1:2;
               _loc8_.method_124(name_58(-var_4,_loc6_ * var_4,_loc9_));
               _loc2_ = !_loc2_;
               _loc10_ = -1;
               _loc11_ = 0;
               _loc12_ = y693;
               while(_loc11_ < _loc12_)
               {
                  _loc11_++;
                  _loc13_ = _loc11_;
                  _loc9_ = !!_loc2_?1:2;
                  if(_loc13_ == _loc7_)
                  {
                     _loc9_ = _loc9_ == 1?2:1;
                  }
                  _loc14_ = name_58(_loc13_ * var_4,_loc6_ * var_4,_loc9_);
                  _loc8_.method_124(_loc14_);
                  _loc2_ = !_loc2_;
               }
               _loc11_ = _loc9_ == 1?2:1;
               var_716.push({
                  "X\x01":name_30,
                  "y693":_loc8_,
                  "\x06z v\x01":_loc11_,
                  "+/\x15\x02\x02":false,
                  " C\x18\x1e\x01":-1
               });
            }
            else
            {
               _loc2_ = !_loc2_;
               _loc9_ = 0;
               _loc10_ = 0;
               _loc11_ = y693;
               while(_loc10_ < _loc11_)
               {
                  _loc10_++;
                  _loc12_ = _loc10_;
                  _loc9_ = !!_loc2_?1:2;
                  _loc8_.method_124(name_58(_loc12_ * var_4,_loc6_ * var_4,_loc9_));
                  _loc2_ = !_loc2_;
               }
               _loc9_ = !!_loc2_?1:2;
               _loc8_.method_124(name_58(y693 * var_4,_loc6_ * var_4,_loc9_));
               var_716.push({
                  "X\x01":-name_30,
                  "y693":_loc8_,
                  "\x06z v\x01":_loc9_,
                  "+/\x15\x02\x02":false,
                  " C\x18\x1e\x01":-1
               });
            }
            _loc3_ = !_loc3_;
         }
      }
   }
}
