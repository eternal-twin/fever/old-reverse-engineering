package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_693 extends class_645
   {
      
      public static var var_556:int = 208;
      
      public static var var_557:int = 14;
      
      public static var var_558:int = 80;
       
      
      public var var_559:Number;
      
      public var var_560:Number;
      
      public var var_251:Number;
      
      public var var_561:MovieClip;
      
      public var var_562:Number;
      
      public var var_563:class_892;
      
      public var var_471:Boolean;
      
      public var var_566:Boolean;
      
      public var var_565:class_892;
      
      public var var_564:Number;
      
      public var var_41:MovieClip;
      
      public function class_693()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         var_560 = (var_560 + 15) % 628;
         var_559 = (var_560 + 1.5) % 628;
         var_561.x = (var_561.x + (Number(Number(Math.cos(var_560 / 100)) + 0.5))) % 24;
         var_561.y = Number(class_693.var_556 + Math.sin(var_559 / 100) * 4);
         method_356();
         method_357();
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true,20);
                  break;
               }
         }
      }
      
      public function method_357() : void
      {
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         if(var_563 == null)
         {
            return;
         }
         var_563.method_358(var_563.var_280,0.2,0.5);
         var _loc2_:int = 21 - int(Number(class_663.method_99(-20,var_563.var_278,20)));
         var_563.var_243.gotoAndStop(_loc2_);
         if(Number(var_563.method_115(var_563.var_280)) < 16 || Number(Math.random()) < 0.02)
         {
            method_359();
         }
         if(var_564 < 0 && var_565.var_250 == null)
         {
            return;
         }
         var _loc3_:Number = Number(var_563.method_115(var_565));
         if(_loc3_ < class_693.var_558)
         {
            _loc4_ = class_693.var_558 - _loc3_;
            _loc5_ = Number(var_565.method_231(var_563));
            var_563.var_244 = Number(var_563.var_244 + Math.cos(_loc5_) * var_564 * _loc4_);
            var_563.var_245 = Number(var_563.var_245 + Math.sin(_loc5_) * var_564 * _loc4_);
         }
      }
      
      public function method_356() : void
      {
         var _loc3_:Number = NaN;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:MovieClip = null;
         var _loc1_:Number = var_565.var_278 / Math.abs(var_565.var_278);
         var _loc2_:Number = 0;
         if(var_565.var_250 == null)
         {
            _loc3_ = 0.9;
            var_565.var_278 = var_565.var_278 * _loc3_;
            var_565.var_279 = var_565.var_279 * _loc3_;
            _loc4_ = {
               "L\x01":Number(class_742.var_217 * 0.5 + Math.cos(var_560 / 100) * 5),
               "\x03\x01":Number(var_561.y + 6)
            };
            var_565.method_358(_loc4_,0.3,0.5);
            var_565.var_243.rotation = var_565.var_243.rotation * 0.5;
            if(!var_471)
            {
               var_565.var_243.scaleX = _loc1_;
               if(Number(Math.abs(var_565.var_244 - class_742.var_217 * 0.5)) < 8)
               {
                  var_565.var_243.gotoAndPlay("profil");
                  var_471 = true;
               }
            }
            if(var_566)
            {
               var_562 = Number(Math.min(Number(var_562 + 0.4),class_693.var_557));
               _loc2_ = var_562 / class_693.var_557;
               if(!name_5)
               {
                  name_26();
               }
            }
            else if(name_5)
            {
               var_566 = true;
               var_562 = 0;
            }
         }
         else
         {
            if(var_565.var_245 > var_561.y && var_565.var_279 > 0)
            {
               var_565.var_250 = null;
               var_471 = false;
               var_565.var_243.gotoAndStop("profil");
               _loc5_ = int(Number(6 + var_565.var_279 * 1.5));
               _loc6_ = 0;
               while(_loc6_ < _loc5_)
               {
                  _loc6_++;
                  _loc7_ = _loc6_;
                  _loc9_ = new class_892(var_232.method_84(class_899.__unprotect__("?}ga\x03"),class_645.var_209));
                  _loc9_.var_233 = 0.99;
                  _loc8_ = _loc9_;
                  _loc3_ = -Math.random() * 3.14;
                  _loc10_ = Number(Math.cos(_loc3_));
                  _loc11_ = Number(Math.sin(_loc3_));
                  _loc12_ = Number(1 + var_565.var_279 * 0.1);
                  _loc13_ = Number(_loc12_ + Math.random() * _loc12_);
                  _loc8_.var_244 = Number(var_565.var_244 + _loc10_ * 7);
                  _loc8_.var_245 = Number(var_561.y + 10);
                  _loc8_.var_278 = _loc10_ * _loc13_;
                  _loc8_.var_279 = _loc11_ * _loc13_ * 3;
                  _loc8_.var_272 = 0;
                  _loc8_.var_250 = Number(0.3 + Math.random() * 0.2);
                  _loc8_.method_119(Number(1 + Math.random() * 1.5));
                  _loc8_.var_251 = 10 + int(class_691.method_114(30));
                  _loc8_.method_118();
               }
            }
            if(var_563 != null && Number(var_565.method_115(var_563)) < 18)
            {
               _loc14_ = var_232.method_84(class_899.__unprotect__("\r5nR\x02"),class_645.var_209);
               _loc14_.x = var_563.var_244;
               _loc14_.y = var_563.var_245;
               var_563.method_89();
               var_563 = null;
               var_565.var_243.gotoAndPlay("gnac");
               name_3 = 2;
               var_251 = 6;
            }
            var_565.var_243.rotation = Number(Math.atan2(var_565.var_279,var_565.var_278) / 0.0174 + (-_loc1_ + 1) * 0.5 * 180);
            var_565.var_243.scaleX = _loc1_;
         }
         _loc3_ = _loc2_ - var_41.scaleX;
         var_41.scaleX = Number(var_41.scaleX + _loc3_ * 0.5);
      }
      
      public function name_26() : void
      {
         var_566 = false;
         var_565.var_250 = 0.7;
         var_565.var_278 = (method_100().var_244 - var_565.var_244) * 0.13;
         var_565.var_279 = -var_562 * 1.7;
         var_565.var_243.gotoAndPlay("jump");
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - param1 * 200];
         super.method_95(param1);
         var_560 = 0;
         var_559 = 0;
         var_566 = false;
         var_564 = param1 * 0.2 - 0.1;
         method_117();
         method_72();
      }
      
      public function method_359() : void
      {
         var_563.var_280 = {
            "L\x01":Number(10 + Math.random() * (class_742.var_217 - 2 * 10)),
            "\x03\x01":Number(10 + Math.random() * (class_742.var_219 - (Number(50 + var_237[0] * 100))))
         };
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("lh\x1a8\x01"),0);
         var_561 = var_232.method_84(class_899.__unprotect__("\x06h~\x0b\x01"),class_645.var_209);
         var_561.y = class_693.var_556;
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("}\x19\'Q\x02"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_565 = _loc1_;
         var_565.var_244 = 0;
         var_565.var_245 = class_693.var_556;
         var_565.var_243.gotoAndStop("profil");
         var_565.method_118();
         var_565.var_233 = 0.94;
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("FG{#\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_563 = _loc1_;
         var_563.var_244 = Math.random() * class_742.var_217;
         var_563.var_245 = Math.random() * class_742.var_219;
         var_563.name_21 = 0;
         var_563.method_118();
         var_563.var_233 = 0.94;
         method_359();
         var_41 = var_232.method_84(class_899.__unprotect__("=\x13\x07\x18"),class_645.var_209 + 1);
         var_41.x = class_742.var_217 * 0.5;
         var_41.y = class_742.var_219 - 6;
         var_41.scaleX = 0;
      }
   }
}
