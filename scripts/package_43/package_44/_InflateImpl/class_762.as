package package_43.package_44._InflateImpl
{
   import package_22.package_23.class_822;
   import package_27.package_28.Bytes;
   import package_32.class_899;
   
   public class class_762
   {
      
      public static var var_332:int = 32768;
      
      public static var var_1423:int = 65536;
       
      
      public var var_460:int;
      
      public var var_1424:class_822;
      
      public var var_1425:Bytes;
      
      public function class_762(param1:Boolean = false)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1425 = Bytes.alloc(65536);
         var_460 = 0;
         if(param1)
         {
            var_1424 = new class_822();
         }
      }
      
      public function name_112() : void
      {
         if(var_1424 != null)
         {
            var_1424.method_79(var_1425,0,32768);
         }
         var _loc1_:Bytes = Bytes.alloc(65536);
         var_460 = var_460 - 32768;
         _loc1_.method_504(0,var_1425,32768,var_460);
         var_1425 = _loc1_;
      }
      
      public function method_1065() : int
      {
         return int(var_1425.b[var_460 - 1]);
      }
      
      public function method_1066() : class_822
      {
         if(var_1424 != null)
         {
            var_1424.method_79(var_1425,0,var_460);
         }
         return var_1424;
      }
      
      public function method_1067() : int
      {
         return var_460;
      }
      
      public function method_1068(param1:Bytes, param2:int, param3:int) : void
      {
         if(var_460 + param3 > 65536)
         {
            name_112();
         }
         var_1425.method_504(var_460,param1,param2,param3);
         var_460 = var_460 + param3;
      }
      
      public function method_1069(param1:int) : void
      {
         if(var_460 == 65536)
         {
            name_112();
         }
         var_1425.b[var_460] = param1;
         var_460 = var_460 + 1;
      }
   }
}
