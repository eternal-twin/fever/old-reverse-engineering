package package_39.package_40
{
   import package_22.package_23.class_846;
   import package_22.package_23.class_907;
   import package_27.package_28.Bytes;
   import package_27.package_28.class_768;
   import package_27.package_28.class_780;
   import package_27.package_28.class_855;
   import package_27.package_28.class_916;
   import package_32.class_899;
   
   public class class_861
   {
       
      
      public var var_343:class_855;
      
      public function class_861(param1:class_855 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_343 = param1;
      }
      
      public function method_955() : Date
      {
         var _loc1_:int = int(var_343.method_904());
         var _loc2_:int = _loc1_ >> 11 & 31;
         var _loc3_:int = _loc1_ >> 5 & 63;
         var _loc4_:int = _loc1_ & 31;
         var _loc5_:int = int(var_343.method_904());
         var _loc6_:int = _loc5_ >> 9;
         var _loc7_:int = _loc5_ >> 5 & 15;
         var _loc8_:int = _loc5_ & 31;
         return new Date(_loc6_ + 1980,_loc7_ - 1,_loc8_,_loc2_,_loc3_,_loc4_ << 1);
      }
      
      public function method_956(param1:int) : class_795
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:class_916 = null;
         var _loc8_:int = 0;
         var _loc9_:String = null;
         var _loc2_:class_795 = new class_795();
         while(param1 > 0)
         {
            if(param1 < 4)
            {
               class_899.var_317 = new Error();
               throw "Invalid extra fields data";
            }
            _loc3_ = int(var_343.method_904());
            _loc4_ = int(var_343.method_904());
            if(param1 < _loc4_)
            {
               class_899.var_317 = new Error();
               throw "Invalid extra fields data";
            }
            _loc5_ = _loc3_;
            if(_loc5_ == 28789)
            {
               _loc6_ = int(var_343.method_774());
               if(_loc6_ != 1)
               {
                  _loc7_ = new class_916();
                  _loc7_.var_12.writeByte(_loc6_);
                  _loc7_.var_12.writeBytes(var_343.method_653(_loc4_ - 1).b);
                  _loc2_.method_124(class_829.method_803(_loc3_,_loc7_.method_914()));
               }
               else
               {
                  _loc8_ = int(var_343.method_908());
                  _loc9_ = var_343.method_653(_loc4_ - 5).toString();
                  _loc2_.method_124(class_829.method_804(_loc9_,_loc8_));
               }
            }
            else
            {
               _loc2_.method_124(class_829.method_803(_loc3_,var_343.method_653(_loc4_)));
            }
            param1 = param1 - (4 + _loc4_);
         }
         return _loc2_;
      }
      
      public function method_957() : Object
      {
         var _loc1_:class_855 = var_343;
         var _loc2_:int = int(_loc1_.method_909());
         if(_loc2_ == 33639248 || _loc2_ == 101010256)
         {
            return null;
         }
         if(_loc2_ != 67324752)
         {
            class_899.var_317 = new Error();
            throw "Invalid Zip Data";
         }
         var _loc3_:int = int(_loc1_.method_904());
         var _loc4_:int = int(_loc1_.method_904());
         var _loc5_:Boolean = (_loc4_ & 2048) != 0;
         if((_loc4_ & 63479) != 0)
         {
            class_899.var_317 = new Error();
            throw "Unsupported flags " + _loc4_;
         }
         var _loc6_:int = int(_loc1_.method_904());
         var _loc7_:Boolean = _loc6_ != 0;
         if(!!_loc7_ && _loc6_ != 8)
         {
            class_899.var_317 = new Error();
            throw "Unsupported compression " + _loc6_;
         }
         var _loc8_:Date = method_955();
         var _loc9_:int = int(_loc1_.method_908());
         var _loc10_:int = int(_loc1_.method_902());
         var _loc11_:int = int(_loc1_.method_902());
         var _loc12_:int = int(_loc1_.method_911());
         var _loc13_:int = int(_loc1_.method_911());
         var _loc14_:String = _loc1_.method_503(_loc12_);
         var _loc15_:class_795 = method_956(_loc13_);
         if(_loc5_)
         {
            _loc15_.method_103(class_829.var_1099);
         }
         if((_loc4_ & 8) != 0)
         {
            _loc10_ = -1;
         }
         return {
            "\x06\fi2\x03":_loc14_,
            "\x10aoe\x02":_loc11_,
            "(-5\n\x03":_loc8_,
            "\x17\x15f;":_loc7_,
            "\rydP":_loc10_,
            "aLZ\x1f\x01":null,
            "\x01pr\t\x02":_loc9_,
            ")^\\`\x01":_loc15_
         };
      }
      
      public function method_958(param1:Object, param2:Bytes, param3:class_768) : void
      {
         class_846.method_78(var_343,param3,param2,int(param1.name_124));
      }
      
      public function method_653() : class_795
      {
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc6_:class_916 = null;
         var _loc7_:class_907 = null;
         var _loc8_:int = 0;
         var _loc1_:class_795 = new class_795();
         var _loc3_:Bytes = null;
         while(true)
         {
            _loc4_ = method_957();
            if(_loc4_ == null)
            {
               break;
            }
            if(int(_loc4_.name_124) < 0)
            {
               _loc5_ = 65536;
               if(_loc3_ == null)
               {
                  _loc3_ = Bytes.alloc(_loc5_);
               }
               _loc6_ = new class_916();
               _loc7_ = new class_907(var_343,false,false);
               while(true)
               {
                  _loc8_ = int(_loc7_.method_615(_loc3_,0,_loc5_));
                  if(_loc8_ < 0 || _loc8_ > _loc3_.length)
                  {
                     break;
                  }
                  _loc6_.var_12.writeBytes(_loc3_.b,0,_loc8_);
                  if(_loc8_ < _loc5_)
                  {
                     _loc4_.package_31 = _loc6_.method_914();
                     _loc4_.name_125 = int(var_343.method_908());
                     _loc8_ = _loc4_.name_125;
                     if(_loc8_ - 134695760 == 0)
                     {
                        _loc4_.name_125 = int(var_343.method_908());
                     }
                     _loc4_.name_124 = int(var_343.method_902());
                     _loc4_.name_126 = int(var_343.method_902());
                     _loc4_.name_124 = int(_loc4_.name_126);
                     _loc4_.name_87 = false;
                  }
                  else
                  {

                  }
               }
               class_899.var_317 = new Error();
               throw class_780.var_768;
            }
            _loc4_.package_31 = var_343.method_653(int(_loc4_.name_124));
            _loc1_.method_124(_loc4_);
         }
         return _loc1_;
      }
   }
}
