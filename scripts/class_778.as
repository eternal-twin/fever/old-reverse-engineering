package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_778 extends class_645
   {
      
      public static var var_900:int = 190;
      
      public static var var_901:int = 210;
      
      public static var var_902:int = 400;
      
      public static var var_903:int = 300;
      
      public static var var_904:int = 200;
       
      
      public var var_520:Boolean;
      
      public var var_181:MovieClip;
      
      public var var_351:Number;
      
      public var var_70:Number;
      
      public var var_204:class_653;
      
      public var var_905:MovieClip;
      
      public var var_908:Number;
      
      public var var_909:Number;
      
      public var var_907:MovieClip;
      
      public var var_906:MovieClip;
      
      public var var_910:MovieClip;
      
      public function class_778()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc5_:MovieClip = null;
         var _loc6_:class_892 = null;
         var _loc1_:int = 0;
         var _loc2_:Array = class_656.var_226;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_79();
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_905.x = var_905.x - var_351;
               var_905.x = var_905.x;
               var_905.var_318 = Number(var_204.method_213(var_905.x,var_905.y,-var_351).var_245);
               if(Number(var_905.var_318) > 0)
               {
                  var_905.y = Number(var_905.var_318);
               }
               var_906.x = Number(var_906.x + 0.3);
               var_907.x = Number(var_907.x + 1.5);
               var_70 = Number(var_70 + 1);
               var_906.scaleY = Number(0.5 + 0.5 * class_649.name_9(var_70));
               var_907.scaleY = 0.5 - 0.5 * class_649.name_9(var_70);
               if(var_905.x < class_778.var_902)
               {
                  if(var_181.QC0C.currentFrame < 20)
                  {
                     var_181.QC0C.gotoAndStop(var_181.QC0C.currentFrame + 1);
                  }
                  var_181.rotation = Number(var_181.rotation + 0.1);
                  if(var_905.x < class_778.var_903)
                  {
                     if(var_905.QC0C.currentFrame < 20)
                     {
                        var_905.QC0C.gotoAndStop(var_905.QC0C.currentFrame + 1);
                     }
                     var_905.rotation = var_905.rotation - 0.1;
                  }
                  if(var_905.x > class_778.var_904)
                  {
                     class_319.alpha = class_319.alpha - var_351 * 0.002;
                     _loc4_ = Number(var_905.var_146.scaleY + 0.3 * var_351 / 2 * 0.01);
                     var_905.var_146.scaleY = _loc4_;
                     var_905.var_146.scaleX = _loc4_;
                     _loc4_ = Number(var_181.var_146.scaleY + 0.4 * var_351 / 2 * 0.01);
                     var_181.var_146.scaleY = _loc4_;
                     var_181.var_146.scaleX = _loc4_;
                     var_905.var_146.alpha = 0.7;
                  }
                  else if(!var_520)
                  {
                     var_181.QC0C.gotoAndStop(var_181.QC0C.currentFrame + 1);
                     var_905.QC0C.gotoAndStop(var_905.QC0C.currentFrame + 1);
                     class_319.alpha = Number(class_319.alpha + var_351 * 0.002);
                     _loc4_ = var_905.var_146.scaleY - 0.3 * var_351 / 2 * 0.01;
                     var_905.var_146.scaleY = _loc4_;
                     var_905.var_146.scaleX = _loc4_;
                     var_181.rotation = var_181.rotation - 0.1;
                     var_905.rotation = Number(var_905.rotation + 0.1);
                  }
                  if(int(class_691.method_114(10)) == 1 && var_905.x > 100)
                  {
                     _loc5_ = var_232.method_84(class_899.__unprotect__("S+\b/\x03"),0);
                     _loc5_.x = int(class_691.method_114(400));
                     _loc5_.y = int(class_691.method_114(400));
                     _loc5_.rotation = int(class_691.method_114(360));
                     _loc4_ = 0.1;
                     _loc5_.scaleY = _loc4_;
                     _loc5_.scaleX = _loc4_;
                     _loc6_ = new class_892(_loc5_);
                     _loc6_.method_685(20 + int(class_691.method_114(50)));
                     _loc6_.var_251 = 40;
                     _loc6_.var_817 = 1.04;
                  }
               }
               if(!var_520 && var_905.x < class_778.var_900)
               {
                  method_81(false,10);
                  var_181.QC0C.gotoAndStop(35);
                  var_905.QC0C.gotoAndStop(35);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         if(var_905.x > class_778.var_901 + 100)
         {
            return;
         }
         if(var_155())
         {
            var_520 = true;
            method_81(true);
            var_181.QC0C.gotoAndStop(var_181.QC0C.totalFrames);
            var_905.QC0C.gotoAndStop(var_905.QC0C.totalFrames);
            return;
         }
         var_181.QC0C.gotoAndStop(35);
         var_905.QC0C.gotoAndStop(35);
         method_81(false);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [250];
         super.method_95(param1);
         var_351 = Number(2 + param1 * 18);
         var_908 = 50;
         var_909 = 105;
         name_3 = 1;
         var_70 = 1;
         var_204 = new class_653();
         var_204.method_214(-200,250);
         var_204.method_214(0,240);
         var_204.method_214(200,200);
         var_204.method_214(400,240);
         var_204.method_214(600,250);
         method_117();
      }
      
      public function var_155() : Boolean
      {
         if(var_905.x <= class_778.var_901 && var_905.x >= class_778.var_900)
         {
            return true;
         }
         return false;
      }
      
      public function method_117() : void
      {
         var_910 = var_232.method_84(class_899.__unprotect__("J\x06;\n\x01"),0);
         class_319 = var_232.method_84(class_899.__unprotect__("\x03o\x10\x1f\x01"),1);
         var_906 = var_232.method_84(class_899.__unprotect__(";\x1eD\f\x01"),1);
         var_906.x = 200;
         var_906.y = int(class_691.method_114(200)) + 100;
         var_906.alpha = 0.2;
         var_907 = var_232.method_84(class_899.__unprotect__(";\x1eD\f\x01"),1);
         var_907.x = 200;
         var_907.y = var_906.y - 50;
         var_907.alpha = 0.3;
         var_181 = var_232.method_84(class_899.__unprotect__("g;mf\x03"),2);
         var_181.x = 200;
         var_181.y = 200;
         var_181.QC0C.gotoAndStop(1);
         var_905 = var_232.method_84(class_899.__unprotect__("R\x1e}n"),3);
         var _loc1_:Number = 600;
         var_905.x = _loc1_;
         var_905.var_319 = _loc1_;
         _loc1_ = 200;
         var_905.y = _loc1_;
         var_905.var_318 = _loc1_;
         var_905.QC0C.gotoAndStop(1);
      }
   }
}
