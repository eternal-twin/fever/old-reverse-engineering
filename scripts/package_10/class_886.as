package package_10
{
   import flash.events.HTTPStatusEvent;
   import flash.events.IOErrorEvent;
   import flash.events.SecurityErrorEvent;
   import flash.net.URLLoader;
   import flash.net.URLRequest;
   import flash.net.URLRequestHeader;
   import flash.net.URLVariables;
   import package_32.class_899;
   
   public class class_886
   {
       
      
      public var var_1361:String;
      
      public var var_296:class_720;
      
      public var var_1363:Function;
      
      public var name_95:Function;
      
      public var var_1060:Function;
      
      public var var_1362:class_720;
      
      public function class_886(param1:String = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(!var_1060)
         {
            var_1060 = function(param1:String):void
            {
            };
         }
         if(!name_95)
         {
            name_95 = function(param1:String):void
            {
            };
         }
         if(!var_1363)
         {
            var_1363 = function(param1:int):void
            {
            };
         }
         var_1361 = param1;
         var_1362 = new class_720();
         var_296 = new class_720();
      }
      
      public function method_746(param1:String, param2:String) : void
      {
         var_296.method_216(param1,param2);
      }
      
      public function method_1021(param1:String, param2:String) : void
      {
         var_1362.method_216(param1,param2);
      }
      
      public function method_748(param1:Boolean) : void
      {
         var _loc6_:String = null;
         var _loc7_:Array = null;
         var _loc9_:String = null;
         var var_214:class_886 = this;
         var var_301:URLLoader = new URLLoader();
         var_301.addEventListener("complete",function(param1:*):void
         {
            var_214.var_1060(var_301.data);
         });
         var_301.addEventListener("httpStatus",function(param1:HTTPStatusEvent):void
         {
            if(param1.status != 0)
            {
               var_214.var_1363(param1.status);
            }
         });
         var_301.addEventListener("ioError",function(param1:IOErrorEvent):void
         {
            var_214.name_95(param1.text);
         });
         var_301.addEventListener("securityError",function(param1:SecurityErrorEvent):void
         {
            var_214.name_95(param1.text);
         });
         var _loc3_:Boolean = false;
         var _loc4_:URLVariables = new URLVariables();
         while(_loc5_.method_74())
         {
            _loc6_ = _loc5_.name_1();
            _loc3_ = true;
            _loc4_[_loc6_] = var_296.method_98(_loc6_);
         }
         _loc6_ = var_1361;
         if(!!_loc3_ && !param1)
         {
            _loc7_ = var_1361.split("?");
            if(int(_loc7_.length) > 1)
            {
               _loc6_ = _loc7_.shift();
               _loc4_.decode(_loc7_.join("?"));
            }
         }
         _loc7_ = _loc6_.split("xxx");
         var _loc8_:URLRequest = new URLRequest(_loc6_);
         while(_loc5_.method_74())
         {
            _loc9_ = _loc5_.name_1();
            _loc8_.requestHeaders.push(new URLRequestHeader(_loc9_,var_1362.method_98(_loc9_)));
         }
         _loc8_.data = _loc4_;
         _loc8_.method = !!param1?"POST":"GET";
         try
         {
            var_301.load(_loc8_);

         }
         catch(_loc5_:*)
         {
            if(_loc5_ as Error)
            {
               class_899.var_317 = _loc5_;
            }
            name_95("Exception: " + class_691.method_97(_loc5_));

         }
      }
   }
}
