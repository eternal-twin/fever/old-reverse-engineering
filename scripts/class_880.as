package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_880 extends class_645
   {
       
      
      public var var_251:int;
      
      public var var_351:Number;
      
      public var var_1330:MovieClip;
      
      public var var_1329:class_795;
      
      public var name_47:Number;
      
      public function class_880()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_1003() : void
      {
         var _loc2_:MovieClip = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:* = var_1329.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc2_.var_68 = Number(Math.min(Number(Number(_loc2_.var_68) + var_351 * _loc2_.name_92),1));
            _loc3_ = Number(Math.pow(Number(_loc2_.var_68),2));
            _loc4_ = Number(0.6 + _loc3_ * 0.8);
            _loc5_ = 40 * _loc4_;
            _loc2_.name_65 = Number(Number(_loc2_.name_65) + 0.5);
            _loc2_.name_132 = Number(Number(_loc2_.name_132) + Number(_loc2_.name_65));
            if(Number(_loc2_.name_132) > -_loc5_)
            {
               _loc2_.name_132 = -_loc5_;
               _loc2_.name_65 = _loc2_.name_65 * -1;
            }
            _loc2_.x = Number(-40 + 540 * _loc3_);
            _loc2_.y = Number(280 + 160 * _loc3_);
            _loc6_ = _loc4_;
            _loc2_.scaleY = _loc6_;
            _loc2_.scaleX = _loc6_;
            _loc2_.var_531.x = _loc2_.x;
            _loc2_.var_531.y = _loc2_.y;
            _loc6_ = _loc4_;
            _loc2_.var_531.scaleY = _loc6_;
            _loc2_.var_531.scaleX = _loc6_;
            _loc2_.y = Number(_loc2_.y + _loc2_.name_132 * _loc4_);
            if(Number(_loc2_.var_68) == 1)
            {
               _loc2_.parent.removeChild(_loc2_);
               var_1329.method_116(_loc2_);
               method_81(false);
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = var_251;
         var_251 = var_251 - 1;
         if(_loc1_ < 0 && var_227[0] > 100 - var_237[0] * 70)
         {
            var_251 = int(Number(name_47 + Math.random() * name_47));
            method_1004();
         }
         method_1003();
         super.method_79();
         if(var_227[0] <= 0 && var_1329.name_37 == 0)
         {
            method_81(true);
         }
      }
      
      override public function method_80() : void
      {
      }
      
      override public function method_83() : void
      {
         method_1005();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [340 - 100 * param1];
         super.method_95(param1);
         var_1329 = new class_795();
         var_251 = 30;
         var_351 = Number(0.005 + param1 * 0.025);
         name_47 = 50 - param1 * 34;
         method_117();
      }
      
      public function method_1004() : void
      {
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("q\x02h\n\x01"),2);
         _loc1_.var_68 = 0;
         _loc1_.name_132 = -(60 + Math.random() * 80);
         _loc1_.name_65 = 0;
         _loc1_.x = -100;
         _loc1_.name_92 = Number(0.9 + Math.random() * 0.2);
         _loc1_.var_531 = var_232.method_84(class_899.__unprotect__("{\x0bf(\x01"),0);
         _loc1_.var_531.x = -100;
         var_1329.method_103(_loc1_);
      }
      
      public function method_1005() : void
      {
         var _loc2_:MovieClip = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:class_892 = null;
         if(var_1330.currentFrame > 1)
         {
            return;
         }
         var_1330.gotoAndPlay(2);
         method_102(8);
         var _loc1_:* = var_1329.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(Number(_loc2_.var_68) > 0.7 && Number(_loc2_.var_68) < 0.8)
            {
               _loc3_ = 0;
               while(_loc3_ < 2)
               {
                  _loc3_++;
                  _loc4_ = _loc3_;
                  _loc5_ = _loc4_ * 2 - 1;
                  _loc6_ = new class_892(var_232.method_84(class_899.__unprotect__("\x0eQ\ni\x01"),1 + _loc4_));
                  _loc6_.var_244 = _loc2_.x;
                  _loc6_.var_245 = _loc2_.y;
                  _loc6_.var_278 = _loc5_ * (0.5 + Math.random() * 3);
                  _loc6_.var_279 = -(3 + Math.random() * 6);
                  _loc6_.var_580 = (Math.random() * 2 - 1) * 4;
                  _loc6_.var_233 = 0.97;
                  _loc6_.var_687 = 0.95;
                  _loc6_.var_243.gotoAndStop(2 - _loc4_);
                  _loc6_.var_250 = 0.5;
                  _loc6_.method_118();
                  _loc6_.method_119(_loc2_.scaleX);
               }
               _loc2_.var_531.parent.removeChild(_loc2_.var_531);
               _loc2_.parent.removeChild(_loc2_);
               var_1329.method_116(_loc2_);
               var_232.method_110(var_1330);
            }
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("!\x11\x04}\x03"),0);
         var_1330 = var_232.method_84(class_899.__unprotect__(" p;G\x02"),1);
         var_1330.x = 16;
         var_1330.stop();
      }
   }
}
