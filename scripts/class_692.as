package
{
   import flash.display.MovieClip;
   import flash.filters.GlowFilter;
   import flash.geom.Point;
   import package_32.class_899;
   
   public class class_692 extends class_645
   {
      
      public static var var_542:int = 20;
      
      public static var var_543:int = 4;
      
      public static var var_544:Number = 0;
       
      
      public var var_520:Boolean;
      
      public var var_351:Number;
      
      public var var_548:Array;
      
      public var var_546:MovieClip;
      
      public var var_550:Boolean;
      
      public var var_547:int;
      
      public var var_553:int;
      
      public var var_554:MovieClip;
      
      public var var_549:Array;
      
      public var var_552:Number;
      
      public var var_551:int;
      
      public var var_545:MovieClip;
      
      public function class_692()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc2_:int = 0;
         var _loc1_:* = method_100();
         var_545.x = Number(_loc1_.var_244);
         var_545.y = Number(_loc1_.var_245);
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_354();
               name_56();
               method_355();
               break;
            case 2:
               var_546 = var_232.method_84(class_899.__unprotect__("\x1c\x06D6"),5);
               var_546.x = 0;
               var_546.y = class_742.var_218 / 2;
               var_546.gotoAndStop(!!var_520?2:3);
               name_3 = 3;
               var_547 = class_692.var_542;
               break;
            case 3:
               _loc2_ = var_547;
               var_547 = var_547 - 1;
               if(_loc2_ <= 0)
               {
                  if(var_546.currentFrame == 1)
                  {
                     var_546.gotoAndStop(!!var_520?2:3);
                  }
                  else
                  {
                     var_546.gotoAndStop(1);
                  }
                  var_547 = class_692.var_542;
                  break;
               }
         }
         super.method_79();
         var_545.gotoAndStop(!!name_5?2:1);
      }
      
      override public function method_80() : void
      {
         name_3 = 2;
         var_520 = true;
         method_81(true);
      }
      
      override public function method_83() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:class_892 = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_549;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.hitTestObject(var_545))
            {
               _loc4_ = new class_892(_loc3_);
               _loc4_.var_251 = 3;
               _loc4_.var_272 = 3;
               var_549.method_116(_loc3_);
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300];
         super.method_95(param1);
         var_351 = 4;
         class_692.var_544 = 20;
         var_551 = 1 + int(Math.floor(4 * param1));
         var_552 = class_692.var_544;
         var_547 = class_692.var_542;
         var_553 = class_692.var_543;
         var_549 = [];
         var_548 = [];
         method_117();
      }
      
      public function method_355() : void
      {
         var _loc1_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:Boolean = false;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:GlowFilter = null;
         var _loc12_:class_654 = null;
         var _loc13_:int = 0;
         _loc1_ = 0;
         var _loc2_:Array = var_549;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.y <= _loc3_.var_555.y)
            {
               if(!_loc3_.name_57)
               {
                  _loc3_.name_57 = true;
                  _loc4_ = true;
                  _loc5_ = 0;
                  _loc6_ = int(var_548.length);
                  while(_loc5_ < _loc6_)
                  {
                     _loc5_++;
                     _loc7_ = _loc5_;
                     _loc8_ = var_548[_loc7_];
                     if(_loc8_.currentFrame >= 2)
                     {
                        continue;
                     }
                     if(_loc7_ < int(var_548.length) - 1)
                     {
                        _loc4_ = false;
                     }
                     _loc8_.gotoAndStop(2);
                     method_102(2);
                     _loc9_ = new class_892(_loc3_);
                     _loc9_.var_251 = 10;
                     var_549.method_116(_loc3_);
                     break;
                  }
                  if(_loc4_)
                  {
                     name_3 = 2;
                     method_81(false,20);
                     method_102(18);
                  }
               }
            }
            else
            {
               _loc10_ = _loc3_.width / 2;
               _loc3_.graphics.lineStyle(2,16711680,70);
               _loc3_.graphics.moveTo(_loc10_,_loc3_.height);
               _loc3_.graphics.lineTo(_loc10_,Number(Math.abs(_loc3_.y - var_554.y)));
               _loc3_.y = _loc3_.y - _loc3_.var_351;
               _loc11_ = new GlowFilter(16777215,50,4,4,1,3);
               _loc3_.filters = [_loc11_];
            }
         }
         _loc10_ = var_552;
         var_552 = var_552 - 1;
         if(_loc10_ <= 0)
         {
            _loc12_ = new class_654();
            _loc1_ = 0;
            _loc5_ = var_551;
            while(_loc1_ < _loc5_)
            {
               _loc1_++;
               _loc6_ = _loc1_;
               var_550 = true;
               _loc7_ = int(class_691.method_114(int(var_548.length)));
               if(!_loc12_.method_217(_loc7_))
               {
                  _loc12_.method_216(_loc7_,_loc7_);
                  _loc3_ = var_548[_loc7_];
                  _loc8_ = var_232.method_92(3);
                  _loc13_ = 10;
                  class_649.method_154(_loc8_,_loc13_,16711680,50,16711680,100,1);
                  _loc8_.y = Number(var_554.y + _loc8_.height);
                  _loc8_.x = _loc3_.x + _loc3_.width / 2 - _loc13_ / 2;
                  _loc8_.var_555 = new Point(Number(_loc3_.x + _loc3_.width / 2),108 - _loc8_.height);
                  _loc8_.var_351 = Number(var_351 / 2 + int(class_691.method_114(int(var_351))));
                  var_549.push(_loc8_);
               }
            }
            var_552 = Number(int(Math.floor(class_692.var_544)) / 3 + int(class_691.method_114(int(class_692.var_544))));
         }
      }
      
      public function name_56() : void
      {
         var _loc1_:int = !!var_550?2:3;
         var _loc2_:int = var_547;
         var_547 = var_547 - 1;
         if(_loc2_ <= 0)
         {
            if(var_554.currentFrame != _loc1_)
            {
               var_554.gotoAndStop(_loc1_);
            }
            else
            {
               var_554.gotoAndStop(1);
            }
            var_547 = class_692.var_542;
            var_550 = false;
         }
      }
      
      public function method_354() : void
      {
         var _loc1_:MovieClip = var_232.method_92(2);
         _loc1_.x = 25 + int(class_691.method_114(int(var_554.width)));
         _loc1_.y = class_742.var_218;
         _loc1_.graphics.lineStyle(int(class_691.method_114(3)) + 1,16711680);
         _loc1_.graphics.lineTo(0,var_554.y + var_554.height - class_742.var_218 - 1);
         var _loc2_:class_892 = new class_892(_loc1_);
         _loc2_.var_251 = 3 + int(class_691.method_114(5));
         _loc2_.var_272 = 4;
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__("tA*?\x02"),3);
         _loc3_.x = _loc1_.x;
         _loc3_.y = Number(var_554.y + var_554.height);
         var _loc4_:Number = Number(0.5 + Math.random() * 0.5);
         _loc3_.scaleY = _loc4_;
         _loc3_.scaleX = _loc4_;
         var _loc5_:class_892 = new class_892(_loc3_);
         _loc5_.var_251 = 10;
         _loc5_.var_272 = 4;
         _loc5_.name_20 = _loc5_.var_251 - 2;
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc6_:MovieClip = null;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         var _loc9_:MovieClip = null;
         var _loc10_:int = 0;
         class_319 = var_232.method_92(0);
         class_649.method_153(class_319,400,400,0,100,0,100);
         var_554 = var_232.method_84(class_899.__unprotect__("G`u\x13\x03"),2);
         var_554.x = 25;
         var_554.y = 340;
         var_554.gotoAndStop(1);
         var_545 = var_232.method_84(class_899.__unprotect__("\x15\x02\x0fv"),4);
         var_545.x = class_742.var_216 / 2;
         var_545.y = class_742.var_218 / 2;
         var_545.gotoAndStop(1);
         var _loc3_:int = 0;
         while(_loc3_ < 5)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = var_232.method_84(class_899.__unprotect__("\tyt\x01\x01"),2);
            _loc5_.y = 10;
            _loc5_.x = Number(Number(var_554.x + 12.5) + _loc4_ * _loc5_.width);
            _loc5_.gotoAndStop(1);
            var_548.push(_loc5_);
            _loc6_ = var_232.method_92(2);
            _loc6_.x = Number(_loc5_.x + _loc5_.width / 2);
            _loc6_.y = var_554.y - 4;
            _loc6_.graphics.lineStyle(1,1926599);
            _loc7_ = 94;
            _loc6_.graphics.lineTo(0,-(var_554.y - _loc7_ - 4));
            _loc8_ = var_232.method_92(2);
            _loc8_.x = _loc6_.x - 2;
            _loc8_.y = _loc6_.y;
            class_649.method_153(_loc8_,4,4,null,1,1926599,100,1);
            _loc9_ = var_232.method_92(2);
            _loc9_.x = _loc6_.x - 2;
            _loc9_.y = _loc7_;
            class_649.method_153(_loc9_,4,4,null,1,1926599,100,1);
         }
         _loc3_ = 60 + int(class_691.method_114(50));
         _loc4_ = 0;
         _loc7_ = _loc3_ - 1;
         while(_loc4_ < _loc7_)
         {
            _loc4_++;
            _loc10_ = _loc4_;
            _loc5_ = var_232.method_92(2);
            _loc5_.x = Number(Number(var_554.x + 12.5) + int(class_691.method_114(int(var_554.width - 25))));
            _loc5_.y = 80;
            _loc5_.graphics.lineStyle(1,1926599);
            _loc5_.graphics.lineTo(0,15);
         }
         _loc4_ = 0;
         while(_loc4_ < 2)
         {
            _loc4_++;
            _loc7_ = _loc4_;
            _loc5_ = var_232.method_92(2);
            _loc5_.x = var_554.x - 10;
            _loc5_.y = 90 + 4 * (_loc7_ + 1);
            _loc5_.graphics.lineStyle(1,1926599);
            _loc5_.graphics.lineTo(Number(var_554.width + 20),0);
         }
      }
   }
}
