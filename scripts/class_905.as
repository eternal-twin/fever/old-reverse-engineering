package {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.text.TextField;

import package_10.Serializer;

import package_11.package_12.class_657;

import package_32.App;

import package_8.package_9.class_660;

public class class_905 {

  public static var var_243:MovieClip;

  public static var var_316:String;

  public static var var_1310:String;

  public static var var_329:int;

  public static var var_573:int;


  public function class_905() {
  }

  public static function var_99():void {
    MtCodec.codecSalt = class_885.var_1351;
    Serializer.USE_ENUM_INDEX = true;
    class_905.var_243 = App.rootMc;
    class_808.method_95();
    class_885.method_95();
    var _loc1_:* = MtCodec.method_70("data");
    MtCodec.var_1059 = class_905.method_187;
    var _loc2_:* = App.rootMc.stage.loaderInfo.parameters;
    class_905.var_316 = Reflect.field(_loc2_, "dom");
    class_905.var_1310 = Reflect.field(_loc2_, "noplay");
    class_905.var_329 = class_691.method_352(Reflect.field(_loc2_, "wid"));
    class_905.var_573 = int(class_657.method_237((0.3 + class_905.var_329 * 0.3) % 1));
    class_905.var_573 = int(class_657.method_238(class_905.var_573, 8947848, 0.5));
    class_702.method_95();
    new class_652();
  }

  public static function method_187(param1:String):void {
    var _loc2_:Sprite = new Sprite();
    var _loc3_:Number = class_742.var_216 * 0.5;
    var _loc4_:Number = class_742.var_323 * 0.5;
    var _loc5_:TextField = class_742.method_308(16711680, 8, -1, "nokia");
    _loc5_.width = _loc3_;
    _loc5_.height = _loc4_;
    _loc5_.wordWrap = Boolean(true);
    _loc5_.multiline = true;
    _loc5_.selectable = true;
    _loc5_.text = param1;
    _loc2_.addChild(_loc5_);
    _loc2_.graphics.beginFill(0);
    _loc2_.graphics.drawRect(0, 0, _loc3_, _loc4_);
    _loc2_.scaleY = Number(2);
    _loc2_.scaleX = 2;
    App.rootMc.addChild(_loc2_);
    _loc2_.y = class_742.var_323;
    new class_660(_loc2_, 0, 0).method_122();
  }
}
}
