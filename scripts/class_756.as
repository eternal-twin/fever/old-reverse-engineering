package
{
   import flash.display.Sprite;
   import flash.events.Event;
   import package_11.class_755;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_938;
   
   public class class_756 extends Sprite
   {
      
      public static var var_211:int = 400;
      
      public static var var_212:int = 436;
      
      public static var var_297:int = 2;
      
      public static var var_786:int = 1;
      
      public static var var_214:class_756;
       
      
      public var var_787:class_898;
      
      public var var_536:class_645;
      
      public var var_305:class_938;
      
      public var name_2:Function;
      
      public var var_232:class_755;
      
      public var var_237:Number;
      
      public function class_756()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         class_756.var_214 = this;
         var_232 = new class_755(this);
         addEventListener(Event.ENTER_FRAME,method_188);
         if(class_647.var_253 == null)
         {
            var_305 = new class_938();
         }
         var_237 = 0;
      }
      
      public function method_79(param1:* = undefined) : void
      {
         var _loc4_:* = null as package_24.class_656;
         var _loc2_:Array = package_24.class_656.var_299.method_78();
         var _loc3_:int = 0;
         while(_loc3_ < int(_loc2_.length))
         {
            _loc4_ = _loc2_[_loc3_];
            _loc3_++;
            _loc4_.method_79();
         }
         if(var_305 != null)
         {
            var_305.method_79();
         }
      }
      
      public function method_188(param1:*) : void
      {
         var _loc3_:Error = null;
         try
         {
            method_79();
            return;
         }
         catch(:Error;)
         {
            _loc3_ = ;
            if(_loc3_ as Error)
            {
               class_899.var_317 = _loc3_;
            }
            class_905.method_187(class_691.method_97(_loc3_));

         }
      }
      
      public function name_4(param1:Boolean) : void
      {
      }
      
      public function method_525(param1:int) : void
      {
         var_536 = class_645.method_71(param1);
         var_536.method_95(var_237);
         var_536.name_4 = name_4;
         var_536.name_2 = method_524;
      }
      
      public function method_89() : void
      {
         removeEventListener(Event.ENTER_FRAME,method_188);
         class_905.var_243.removeChild(this);
      }
      
      public function method_524() : void
      {
      }
   }
}
