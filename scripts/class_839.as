package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_739;
   import package_8.package_9.class_931;
   
   public class class_839 extends class_645
   {
      
      public static var var_1152:int = 20;
      
      public static var var_1153:Number = 0.1;
      
      public static var var_1154:Number = 0.2;
      
      public static var var_1150:class_839;
       
      
      public var name_48:Number;
      
      public var var_1155:Number;
      
      public var name_55:Array;
      
      public var var_1157:Sprite;
      
      public var var_1158:BitmapData;
      
      public var name_21:Number;
      
      public var var_254:Number;
      
      public var var_1156:Number;
      
      public var var_1103:Array;
      
      public var z2Eo:int;
      
      public function class_839()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_838 = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc9_:Array = null;
         var _loc10_:class_838 = null;
         name_21 = (name_21 + name_48) % 628;
         if(int(name_55.length) == 2)
         {
            var_254 = Number(Math.min(Number(var_254 + var_1155),1));
            _loc1_ = 0.5 - Math.cos(var_254 * 3.14) * 0.5;
            _loc2_ = 1;
            _loc3_ = 0;
            _loc4_ = name_55;
            while(_loc3_ < int(_loc4_.length))
            {
               _loc5_ = _loc4_[_loc3_];
               _loc3_++;
               _loc6_ = 3 / var_1155;
               _loc7_ = Number(class_663.method_112(_loc5_.var_629 - _loc5_.var_1151,3.14));
               _loc5_.var_65 = Number(_loc5_.var_1151 + _loc7_ * _loc1_);
               _loc5_.var_17 = Math.sin(_loc1_ * 3.14) * _loc2_ * _loc6_;
               _loc2_ = -_loc2_;
            }
            if(var_254 == 1)
            {
               _loc3_ = 0;
               _loc4_ = name_55;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  _loc5_.name_55 = false;
                  _loc5_.method_824();
               }
               name_55 = [];
               method_825();
            }
         }
         if(var_220)
         {
            name_48 = Number(name_48 + 0.025);
            name_48 = name_48 * 1.15;
            var_1156 = Number(var_1156 + 2);
            var_1156 = var_1156 * 1.03;
         }
         var_1157.graphics.clear();
         _loc2_ = 0;
         _loc4_ = var_1103;
         while(_loc2_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc2_];
            _loc2_++;
            _loc5_.method_129(Number(_loc5_.var_65 + name_21 * 0.01),var_1156,var_1157.graphics);
         }
         var _loc8_:Matrix = new Matrix();
         _loc8_.scale(class_839.var_1154,class_839.var_1154);
         var_1158.draw(var_1157,_loc8_);
         var_1158.colorTransform(var_1158.rect,new ColorTransform(1,1,1,1,0,0,0,-10));
         _loc2_ = 0;
         _loc4_ = var_1103;
         while(_loc2_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc2_];
            _loc2_++;
            _loc3_ = 0;
            _loc9_ = var_1103;
            while(_loc3_ < int(_loc9_.length))
            {
               _loc10_ = _loc9_[_loc3_];
               _loc3_++;
               if(!(Number(Math.abs(Number(class_663.method_112(_loc10_.var_795 - _loc5_.var_795,int(var_1103.length) * 0.5)))) != 1 && var_220 == null))
               {
                  _loc1_ = _loc5_.x - _loc10_.x;
                  _loc6_ = _loc5_.y - _loc10_.y;
                  if(Number(Math.sqrt(Number(_loc1_ * _loc1_ + _loc6_ * _loc6_))) < 64)
                  {
                     name_61(_loc5_,_loc10_);
                  }
               }
            }
         }
         super.method_79();
      }
      
      public function name_61(param1:class_838, param2:class_838) : void
      {
         var _loc3_:class_739 = new class_739(new class_350());
         var_232.method_124(_loc3_.var_243,0);
         _loc3_.method_129(param1.x,param1.y);
         _loc3_.var_251 = 10;
         var _loc4_:int = int((param1.var_369 + param2.var_369) * 0.5);
         class_657.method_139(_loc3_.var_243,param1.var_369);
         _loc3_.var_243.mouseChildren = false;
         _loc3_.var_243.mouseEnabled = false;
         _loc3_.var_244 = Number(_loc3_.var_244 + (Math.random() * 2 - 1) * 9);
         _loc3_.var_245 = Number(_loc3_.var_245 + (Math.random() * 2 - 1) * 9);
         var _loc6_:Number = param2.x - param1.x;
         var _loc7_:Number = param2.y - param1.y;
         _loc3_.var_278 = _loc6_ / _loc3_.var_251;
         _loc3_.var_279 = _loc7_ / _loc3_.var_251;
      }
      
      public function method_826(param1:class_838, param2:class_838) : int
      {
         if(param1.var_65 < param2.var_65)
         {
            return -1;
         }
         return 1;
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc8_:int = 0;
         var _loc12_:class_838 = null;
         var_227 = [400];
         class_839.var_1150 = this;
         super.method_95(param1);
         z2Eo = 5 + int(Math.round(Math.pow(param1,1.5) * 8));
         var_1156 = 120;
         name_48 = 0.2;
         var _loc2_:int = 38 + class_839.var_1152;
         var _loc3_:int = _loc2_ * z2Eo;
         var_1156 = _loc3_ / 6.28;
         var _loc4_:class_378 = new class_378();
         var_232.method_124(_loc4_,0);
         var_1103 = [];
         var _loc5_:Array = [];
         var _loc6_:int = 0;
         var _loc7_:int = z2Eo;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc5_.push(_loc8_ / z2Eo * 6.28);
         }
         class_659.method_241(_loc5_);
         _loc6_ = 0;
         _loc7_ = z2Eo;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            var var_631:Array = [new class_838(_loc8_)];
            var_232.method_124(var_631[0],1);
            var_631[0].var_65 = Number(_loc5_[_loc8_]);
            var_1103.push(var_631[0]);
            var var_214:Array = [this];
            var_631[0].mouseChildren = false;
            var_631[0].mouseEnabled = true;
            var_631[0].addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_631:Array = param2;
               return function(param1:*):void
               {
                  var_214[0].method_827(var_631[0]);
               };
            }(var_214,var_631));
         }
         var_1158 = new BitmapData(int(class_742.var_216 * class_839.var_1154),int(class_742.var_218 * class_839.var_1154),true,0);
         var _loc9_:Bitmap = new Bitmap(var_1158);
         var _loc10_:Number = 1 / class_839.var_1154;
         _loc9_.scaleY = _loc10_;
         _loc9_.scaleX = _loc10_;
         var_232.method_124(_loc9_,0);
         var_1157 = new Sprite();
         name_21 = 0;
         name_55 = [];
         var_1103.sort(method_826);
         _loc6_ = 0;
         var _loc11_:Array = var_1103;
         while(_loc6_ < int(_loc11_.length))
         {
            _loc12_ = _loc11_[_loc6_];
            _loc6_++;
            _loc12_.method_129(_loc12_.var_65,var_1156);
         }
      }
      
      public function method_827(param1:class_838) : void
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:class_838 = null;
         var _loc5_:Number = NaN;
         if(param1.name_55 || int(name_55.length) == 2 || var_220 != null)
         {
            return;
         }
         name_55.push(param1);
         param1.var_10();
         if(int(name_55.length) == 2)
         {
            var_254 = 0;
            name_55[0].var_629 = name_55[1].var_65;
            name_55[1].var_629 = name_55[0].var_65;
            name_55[0].var_1151 = name_55[0].var_65;
            name_55[1].var_1151 = name_55[1].var_65;
            _loc2_ = 0;
            _loc3_ = name_55;
            while(_loc2_ < int(_loc3_.length))
            {
               _loc4_ = _loc3_[_loc2_];
               _loc2_++;
               _loc4_.name_55 = true;
            }
            _loc5_ = Number(class_663.method_112(name_55[0].var_65 - name_55[1].var_65,3.14));
            var_1155 = class_839.var_1153 * (1 + var_237[0] * 0.5) / Math.abs(_loc5_);
            var_1155 = Number(Math.min(0.15,var_1155));
         }
      }
      
      public function method_825() : void
      {
         var _loc2_:Array = null;
         var _loc3_:class_838 = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc1_:int = 0;
         _loc2_ = var_1103;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_65 = Number(class_663.method_112(_loc3_.var_65,Math.PI));
         }
         var_1103.sort(method_826);
         _loc1_ = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc4_ = _loc1_;
            if(method_828(_loc4_ * 2 - 1))
            {
               _loc5_ = 0;
               _loc2_ = var_1103;
               while(_loc5_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc5_];
                  _loc5_++;
                  new class_931(_loc3_);
               }
               method_81(true,32);
            }
         }
      }
      
      public function method_828(param1:int) : Boolean
      {
         var _loc5_:class_838 = null;
         var _loc6_:int = 0;
         var _loc2_:int = var_1103[int(var_1103.length) - 1].var_795;
         var _loc3_:int = 0;
         var _loc4_:Array = var_1103;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc6_ = _loc5_.var_795 - _loc2_;
            if(_loc6_ != param1 && Number(class_663.method_113(_loc6_,z2Eo - 1)) != 0)
            {
               return false;
            }
            _loc2_ = _loc5_.var_795;
         }
         return true;
      }
   }
}
