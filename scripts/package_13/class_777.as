package package_13
{
   import flash.display.Sprite;
   import flash.ui.Mouse;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_18.package_19.class_872;
   import package_24.class_656;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   
   public class class_777
   {
      
      public static var var_892:int = 8;
      
      public static var var_434:int = 12;
      
      public static var var_893:Number = 0.12;
      
      public static var var_894:int = 16;
       
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_896:class_826;
      
      public var var_280:class_826;
      
      public var name_3:class_776;
      
      public var var_289:class_826;
      
      public var var_282:class_656;
      
      public var var_243:Sprite;
      
      public var var_898:Function;
      
      public var var_267:class_719;
      
      public var var_265:int;
      
      public var var_254:Number;
      
      public var var_897:Array;
      
      public var var_326:Object;
      
      public var var_713:Sprite;
      
      public function class_777(param1:class_719 = undefined, param2:class_826 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_267 = param1;
         var_267.var_281 = param2;
         var_243 = new class_656();
         var_267.var_232.method_124(var_243,class_719.var_451);
         var_282 = new class_656();
         var_282.y = -5;
         var_243.addChild(var_282);
         method_107();
         method_130(param2);
         name_89();
      }
      
      public function name_89() : void
      {
         name_3 = class_776.var_890;
         var_267.method_179(var_289);
         var_282.var_198.method_133(0);
         var_282.var_198.name_8(0);
      }
      
      public function method_79() : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         switch(int(name_3.var_295))
         {
            case 0:
               break;
            case 1:
               _loc2_ = Boolean(class_765.var_214.method_588(_Item.var_408))?Number(1):0.5;
               var_254 = Number(Math.min(Number(var_254 + class_777.var_893 * _loc2_),1));
               var_282.var_198.name_8(_loc2_);
               _loc3_ = var_280.var_244 - var_289.var_244;
               _loc4_ = var_280.var_245 - var_289.var_245;
               _loc5_ = Number(var_289.var_244 + _loc3_ * var_254);
               _loc6_ = Number(var_289.var_245 + _loc4_ * var_254);
               method_129(_loc5_,_loc6_);
               if(var_254 == 1)
               {
                  var_289 = var_280;
                  var_254 = 0;
                  if(var_280.var_895 == 0)
                  {
                     name_89();
                     class_652.var_214.method_135(true);
                     break;
                  }
                  method_681();
                  break;
               }
         }
      }
      
      public function method_143(param1:class_719) : void
      {
         var_267 = param1;
         var_267.var_232.method_124(var_243,class_719.var_451);
      }
      
      public function method_130(param1:class_826) : void
      {
         var_289 = param1;
         var_267.method_179(param1);
         method_129(param1.var_244,param1.var_245);
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_243.x = int(Number(param1 * 16 + class_777.var_892));
         var_243.y = int(Number(param2 * 16 + class_777.var_434));
      }
      
      public function method_145(param1:int, param2:Boolean = false) : void
      {
         if(var_265 == param1 && !param2)
         {
            return;
         }
         var_265 = param1;
         var _loc3_:Array = ["hero_right","hero_front","hero_left","hero_back"];
         var_282.method_137(class_702.var_288.method_136(_loc3_[param1]));
         var_282.var_198.method_133(2);
      }
      
      public function method_181() : void
      {
         var _loc3_:class_656 = null;
         if(var_713 == null)
         {
            return;
         }
         var _loc1_:int = 0;
         var _loc2_:Array = var_897;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_89();
         }
         var_243.removeChild(var_713);
         var_713 = null;
         var_326 = null;
      }
      
      public function method_682() : void
      {
         var _loc1_:Number = Number(Number(Math.abs(var_243.mouseX)) + Number(Math.abs(var_243.mouseY)));
         if(_loc1_ < 32)
         {
            Mouse.hide();
         }
         else
         {
            Mouse.show();
         }
      }
      
      public function method_683() : Boolean
      {
         return name_3 == class_776.var_890;
      }
      
      public function method_385(param1:int) : void
      {
         var _loc4_:int = 0;
         var _loc5_:class_656 = null;
         var_326 = param1;
         var_713 = new Sprite();
         var_243.addChild(var_713);
         var _loc2_:Array = class_742.var_264[param1];
         var_713.x = int(_loc2_[0]) * 16;
         var_713.y = int(_loc2_[1]) * 16 - 8;
         class_658.var_146(var_713,2,40,16777215);
         var_897 = [];
         var _loc3_:int = 0;
         while(_loc3_ < 3)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new class_656();
            var_713.addChild(_loc5_);
            if(var_267.method_384())
            {
               _loc5_.method_128(class_702.var_304.method_98(null,"classic_arrow"));
            }
            else
            {
               _loc5_.method_137(class_702.var_304.method_136("rainbow_arrow"));
            }
            if(_loc4_ != 2)
            {
               class_657.name_18(_loc5_,0,-100);
               _loc5_.y = Number(_loc5_.y + (2 - _loc4_));
            }
            _loc5_.rotation = param1 * 90;
            var_897.push(_loc5_);
         }
      }
      
      public function method_133(param1:class_826) : void
      {
         method_181();
         var_267.method_179(param1);
         name_3 = class_776.var_891;
         var_254 = 0;
         method_681();
         if(name_3 == class_776.var_891)
         {
            class_868.var_214.method_684();
            class_652.var_214.method_135(false);
         }
      }
      
      public function method_297(param1:Number = 999.9) : class_872
      {
         var _loc7_:class_872 = null;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc2_:Array = class_742.var_264[var_265];
         if(var_265 == -1)
         {
            _loc2_ = class_742.var_264[0];
         }
         var _loc4_:class_872 = null;
         var _loc5_:int = 0;
         var _loc6_:Array = var_267.var_242;
         while(_loc5_ < int(_loc6_.length))
         {
            _loc7_ = _loc6_[_loc5_];
            _loc5_++;
            _loc8_ = var_289.var_244 + int(_loc2_[0]) * 0.1 - _loc7_.var_289.var_244;
            _loc9_ = var_289.var_245 + int(_loc2_[1]) * 0.1 - _loc7_.var_289.var_245;
            _loc10_ = Number(Math.sqrt(Number(_loc8_ * _loc8_ + _loc9_ * _loc9_)));
            if(_loc10_ < param1)
            {
               param1 = _loc10_;
               _loc4_ = _loc7_;
            }
         }
         return _loc4_;
      }
      
      public function method_205() : int
      {
         var _loc1_:Number = Number(Number(Math.atan2(var_243.mouseY,var_243.mouseX)) + 0.77);
         _loc1_ = Number(class_663.method_113(_loc1_,Math.PI * 2)) / (Math.PI * 2);
         return int(_loc1_ * 4);
      }
      
      public function method_107() : void
      {
         method_145(1,true);
         var_282.var_198.method_133(0);
         var_282.var_198.name_8(0);
      }
      
      public function method_681() : void
      {
         var _loc4_:class_826 = null;
         var_280 = var_289.var_541[0];
         var _loc1_:int = 999;
         var _loc2_:int = 0;
         var _loc3_:Array = var_289.var_541;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_895 > -1 && _loc4_.var_895 < _loc1_)
            {
               var_280 = _loc4_;
               _loc1_ = _loc4_.var_895;
            }
         }
         _loc2_ = 0;
         var _loc5_:int = 0;
         _loc3_ = var_289.var_899;
         while(_loc5_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc5_];
            _loc5_++;
            if(_loc4_ == var_280)
            {
               break;
            }
            _loc2_++;
         }
         method_145(_loc2_);
         var_282.var_198.name_8(1);
         if(var_280.method_380())
         {
            name_89();
         }
      }
   }
}
