package package_13
{
   import flash.display.Sprite;
   import flash.text.TextField;
   import package_11.class_844;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_18.package_19.class_872;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   
   public class class_826
   {
       
      
      public var var_245:int;
      
      public var var_244:int;
      
      public var var_220:Boolean;
      
      public var var_252:int;
      
      public var var_1084:Number;
      
      public var var_1086:Number;
      
      public var var_1085:Number;
      
      public var var_895:int;
      
      public var var_541:Array;
      
      public var var_1088:Boolean;
      
      public var var_1087:Array;
      
      public var var_507:Array;
      
      public var var_267:class_719;
      
      public var var_322:Array;
      
      public var var_215:int;
      
      public var var_1089:int;
      
      public var GLWA:TextField;
      
      public var var_757:class_744;
      
      public var package_19:class_710;
      
      public var name_78:Array;
      
      public var var_899:Array;
      
      public var var_951:Sprite;
      
      public var var_65:int;
      
      public function class_826(param1:int = 0, param2:int = 0, param3:class_719 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_267 = param3;
         var_244 = param1;
         var_245 = param2;
         var_215 = 0;
         var_1089 = 0;
         var_507 = [];
         var_899 = [];
         var_541 = [];
         var_322 = [];
         name_78 = [];
         var_252 = 0;
         var_895 = 0;
         var_65 = 0;
         var_1085 = 0;
         var_1084 = 0;
         var_1086 = Math.random() * 2 - 1;
         var_1088 = false;
         var_220 = false;
         var_267.var_496[param1][param2] = this;
         var_267.var_831.push(this);
      }
      
      public function method_783() : void
      {
         if(var_1084 == var_1085)
         {
            return;
         }
         var _loc1_:Number = var_1084 - var_1085;
         var_1085 = Number(var_1085 + Number(class_663.method_99(-0.005,_loc1_ * 0.1,0.005)));
         if(Number(Math.abs(_loc1_)) < 0.02)
         {
            var_1085 = var_1084;
         }
         method_782();
      }
      
      public function method_380() : Boolean
      {
         if(package_19 == null)
         {
            return false;
         }
         return Boolean(package_19.method_380());
      }
      
      public function method_784(param1:Number, param2:Boolean = false) : void
      {
         var_1084 = param1;
         if(param2)
         {
            var_1085 = param1;
            method_782();
         }
      }
      
      public function method_785(param1:class_738) : void
      {
         if(var_1087 == null)
         {
            var_1087 = [];
         }
         var_1087.push(param1);
      }
      
      public function method_782() : void
      {
         var _loc4_:class_738 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc1_:Number = 1 - var_1085;
         if(_loc1_ < 1)
         {
            _loc1_ = Number(class_663.method_99(0,Number(_loc1_ + var_1086 * 0.02),1));
         }
         var _loc2_:int = 0;
         var _loc3_:Array = var_1087;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = 11167300;
            _loc6_ = int(class_657.method_238(class_905.var_573,_loc5_,_loc1_));
            class_657.method_139(_loc4_,_loc6_);
         }
      }
      
      public function method_786() : void
      {
         if(GLWA != null)
         {
            GLWA.parent.removeChild(GLWA);
         }
         var _loc1_:TextField = class_742.method_308(16777215,8,-1,"nokia");
         _loc1_.x = var_244 * 16;
         _loc1_.y = var_245 * 16;
         var_267.var_232.method_124(_loc1_,8);
         _loc1_.text = class_691.method_97(var_895);
         GLWA = _loc1_;
      }
      
      public function method_788() : void
      {
         var _loc2_:int = 0;
         var _loc3_:class_826 = null;
         var_541 = [];
         if(!method_787())
         {
            return;
         }
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_899[_loc2_];
            if(!(_loc3_ == null || !_loc3_.method_787()))
            {
               if(!(var_252 == 1 && _loc3_.var_252 == 1 && var_1089 != _loc3_.var_1089))
               {
                  if(!((var_252 == 3 || _loc3_.var_252 == 3) && int(_loc2_ % 2) == 0))
                  {
                     var_541.push(_loc3_);
                  }
               }
            }
         }
      }
      
      public function method_789() : Boolean
      {
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_826 = null;
         var _loc9_:Boolean = false;
         var _loc1_:* = null;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         while(_loc3_ < 9)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = class_742.var_747[int(_loc4_ % 8)];
            _loc6_ = var_244 + int(_loc5_[0]);
            _loc7_ = var_245 + int(_loc5_[1]);
            _loc8_ = var_267.var_496[_loc6_][_loc7_];
            _loc9_ = Boolean(_loc8_.method_787());
            if(_loc1_ != null && _loc1_ != _loc9_)
            {
               _loc2_++;
            }
            _loc1_ = _loc9_;
         }
         return _loc2_ <= 2;
      }
      
      public function method_787() : Boolean
      {
         return var_252 == 1 || var_252 == 3;
      }
      
      public function method_382() : Boolean
      {
         if(package_19 == null)
         {
            return false;
         }
         return Boolean(package_19.method_382());
      }
      
      public function method_790() : Boolean
      {
         return var_895 > 0;
      }
      
      public function method_791() : Boolean
      {
         return package_19 != null && package_19.name_58;
      }
      
      public function method_180() : void
      {
         if(package_19 == null)
         {
            return;
         }
         package_19.method_180();
      }
      
      public function method_194() : Object
      {
         var _loc1_:class_872 = package_19;
         return _loc1_.package_31;
      }
      
      public function method_792() : Object
      {
         return {
            "L\x01":Number(var_267.x + var_244 * 16),
            "\x03\x01":Number(var_267.y + var_245 * 16)
         };
      }
      
      public function method_294() : Object
      {
         return {
            "L\x01":(var_244 + 0.5) * 16,
            "\x03\x01":(var_245 + 0.5) * 16
         };
      }
      
      public function method_296(param1:class_826) : Number
      {
         var _loc2_:int = var_244 - param1.var_244;
         var _loc3_:int = var_245 - param1.var_245;
         return Number(Math.sqrt(_loc2_ * _loc2_ + _loc3_ * _loc3_));
      }
      
      public function method_793() : void
      {
         var_220 = true;
      }
      
      public function method_482(param1:int, param2:String, param3:int = 0, param4:int = 0) : void
      {
         var _loc5_:class_738 = new class_738();
         _loc5_.visible = false;
         _loc5_.x = (var_244 + 0.5) * 16;
         _loc5_.y = (var_245 + 0.5) * 16;
         var _loc6_:class_844 = var_267.var_503;
         var _loc7_:Number = _loc6_.var_503 * 16807 % 2147483647;
         _loc6_.var_503 = _loc7_;
         _loc5_.x = Number(_loc5_.x + (int((int(_loc7_) & 1073741823) % (param4 * 2)) - param4));
         _loc6_ = var_267.var_503;
         _loc7_ = _loc6_.var_503 * 16807 % 2147483647;
         _loc6_.var_503 = _loc7_;
         _loc5_.y = Number(_loc5_.y + (int((int(_loc7_) & 1073741823) % (param4 * 2)) - param4));
         _loc5_.method_128(class_702.package_13.method_98(param3,param2));
         name_78.push({
            "\x05\x10\x01":_loc5_,
            "cHX@\x01":param1
         });
         var_267.var_232.method_124(_loc5_,class_719.var_451);
      }
   }
}
