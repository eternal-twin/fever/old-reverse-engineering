package package_13
{
   import flash.display.Sprite;
   import package_32.class_899;
   
   public class class_710 extends Sprite
   {
       
      
      public var var_252:class_669;
      
      public var var_289:class_826;
      
      public var var_267:class_719;
      
      public var var_378:Object;
      
      public var name_58:Boolean;
      
      public function class_710(param1:class_719 = undefined, param2:class_826 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_267 = param1;
         var_289 = param2;
         name_58 = false;
         param2.package_19 = this;
         if(var_378 == null)
         {
            var_378 = class_719.var_451;
         }
         param1.var_232.method_124(this,var_378);
         x = (param2.var_244 + 0.5) * 16;
         y = (param2.var_245 + 0.5) * 16;
      }
      
      public function method_380() : Boolean
      {
         return false;
      }
      
      public function method_381() : void
      {
      }
      
      public function method_89() : void
      {
         parent.removeChild(this);
         var_289.package_19 = null;
      }
      
      public function method_382() : Boolean
      {
         return false;
      }
      
      public function method_180() : void
      {
      }
      
      public function method_383() : int
      {
         return 0;
      }
      
      public function method_207(param1:String = undefined) : void
      {
         method_89();
      }
   }
}
