package
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_791 extends class_645
   {
      
      public static var var_952:Number = 0.7;
       
      
      public var var_953:MovieClip;
      
      public var var_251:Object;
      
      public var var_955:MovieClip;
      
      public var var_961:Number;
      
      public var var_959:Array;
      
      public var var_956:Array;
      
      public var var_954:MovieClip;
      
      public var var_957:Boolean;
      
      public var var_700:Number;
      
      public var var_958:Array;
      
      public var var_960:Object;
      
      public var var_187:MovieClip;
      
      public function class_791()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_712() : void
      {
         var _loc1_:Number = Number(8 + var_237[0] * 16);
         var_700 = (var_700 + _loc1_) % 628;
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:MovieClip = null;
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         method_105();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_712();
               var_954.y = (Number(Math.cos(var_700 / 100)) + 1) * class_742.var_219 * 0.5;
               var_954.GLWA.text = "y:" + method_713(var_954.y);
               break;
            case 2:
               method_712();
               var_953.x = (Number(Math.cos(var_700 / 100)) + 1) * class_742.var_217 * 0.5;
               var_953.GLWA.text = "x:" + method_713(var_953.x);
               var_955.x = var_953.x;
               var_955.y = var_954.y;
               break;
            case 3:
               _loc1_ = 0;
               _loc2_ = var_956;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc3_.var_37 = Number(_loc3_.var_37) - 1;
                  if(Number(_loc3_.var_37) < 0)
                  {
                     _loc3_.scaleX = _loc3_.scaleX * 0.8;
                     _loc3_.scaleY = _loc3_.scaleX;
                     if(_loc3_.scaleX < 0.02 && !var_957)
                     {
                        var_155();
                     }
                  }
               }
               if(var_251 != null)
               {
                  var_251 = var_251 - 1;
                  if(var_251 < 0)
                  {
                     method_81(true,20);
                     var_251 = null;
                  }
               }
               _loc1_ = 0;
               _loc2_ = var_958;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc4_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc4_.name_66 = _loc4_.name_66 * _loc4_.name_92;
                  _loc4_.var_243.scaleX = _loc4_.var_344 * _loc4_.name_66;
                  _loc4_.var_243.scaleY = _loc4_.var_344 * _loc4_.name_66;
                  _loc5_ = Number(class_663.method_99(0,100 - Math.pow(Number(_loc4_.name_66),1) * 100,100));
                  class_657.gfof(_loc4_.var_243,_loc5_ * 0.01,3355545);
               }
               _loc2_ = var_959.method_78();
               _loc1_ = 0;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc3_.rotation = Number(_loc3_.rotation + Number(_loc3_.var_580));
                  _loc3_.scaleY = _loc3_.scaleY * 0.75;
                  if(_loc3_.scaleY < 0.05)
                  {
                     _loc3_.parent.removeChild(_loc3_);
                     var_959.method_116(_loc3_);
                  }
               }
               if(var_960 != null)
               {
                  class_657.gfof(class_319,var_960 * 0.01,0);
                  var_960 = var_960 * 0.5;
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_714(param1:Number, param2:Number) : Boolean
      {
         var _loc3_:Number = param1 - var_187.x;
         var _loc4_:Number = param2 - var_187.y;
         var _loc5_:Number = var_187.scaleX * 30;
         var _loc6_:Number = Number(Math.sqrt(Number(_loc3_ * _loc3_ + _loc4_ * _loc4_)));
         return _loc6_ < _loc5_;
      }
      
      override public function method_83() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               name_3 = 2;
               var_953 = var_232.method_84(class_899.__unprotect__("*vm?\x02"),class_645.var_209);
               var_955 = var_232.method_84(class_899.__unprotect__("\x07k}"),class_645.var_209);
               var_953.alpha = class_791.var_952;
               var_955.alpha = class_791.var_952;
               break;
            case 2:
               name_3 = 3;
               var_956 = [];
               _loc1_ = 0;
               while(_loc1_ < 12)
               {
                  _loc1_++;
                  _loc2_ = _loc1_;
                  _loc3_ = Number(var_953.x + (Math.random() * 2 - 1) * 3);
                  _loc4_ = Number(var_954.y + (Math.random() * 2 - 1) * 3);
                  _loc5_ = Math.random() * 360;
                  _loc6_ = 0;
                  while(_loc6_ < 3)
                  {
                     _loc6_++;
                     _loc7_ = _loc6_;
                     _loc8_ = var_232.method_84(class_899.__unprotect__("\r\x1b\bB\x01"),class_645.var_208);
                     _loc8_.x = _loc3_;
                     _loc8_.y = _loc4_;
                     _loc8_.rotation = _loc5_;
                     _loc8_.var_37 = _loc7_ * 6;
                     var_956.push(_loc8_);
                  }
               }
               var_229 = [true];
         }
      }
      
      public function method_105() : void
      {
         var_961 = (var_961 + 20) % 628;
         var_187.y = Number(var_187.y + Math.cos(var_961 / 100) * var_187.scaleX);
         var_187.scaleX = var_187.scaleX * 1.003;
         var_187.scaleY = var_187.scaleX;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [200];
         super.method_95(param1);
         var_957 = false;
         var_961 = 0;
         var_700 = 0;
         method_117();
         var_958 = [];
         var_959 = [];
         method_72();
      }
      
      public function var_155() : void
      {
         var_957 = true;
         if(method_714(var_953.x,var_954.y))
         {
            method_715();
            var_251 = 20;
         }
         else
         {
            method_81(false,20);
         }
      }
      
      public function method_713(param1:Number) : String
      {
         var _loc2_:String = class_691.method_97(int(param1));
         while(_loc2_.length < 3)
         {
            _loc2_ = "0" + _loc2_;
         }
         return _loc2_ + "." + int(class_691.method_114(10));
      }
      
      public function method_715() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:class_892 = null;
         var _loc6_:class_892 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:MovieClip = null;
         var _loc1_:int = 0;
         while(_loc1_ < 10)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_232.method_84(class_899.__unprotect__(",&PN"),class_645.var_208);
            _loc3_.x = var_187.x;
            _loc3_.y = var_187.y;
            _loc3_.rotation = Math.random() * 360;
            _loc3_.scaleY = Number(0.8 + Math.random() * 3);
            _loc3_.var_580 = (Math.random() * 2 - 1) * 1.5;
            var_959.push(_loc3_);
         }
         _loc1_ = 28;
         _loc2_ = 0;
         while(_loc2_ < _loc1_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc6_ = new class_892(var_232.method_84(class_899.__unprotect__("@\x05\x05q\x02"),class_645.var_209));
            _loc6_.var_233 = 0.99;
            _loc5_ = _loc6_;
            _loc5_.var_244 = var_187.x;
            _loc5_.var_245 = var_187.y;
            _loc5_.name_66 = 1;
            _loc5_.var_344 = var_187.scaleX * (1 + (Math.random() * 2 - 1) * 0.2);
            _loc7_ = Math.random() * 6.28;
            _loc8_ = Number(1 + Math.random() * 4);
            _loc5_.var_278 = Math.cos(_loc7_) * _loc8_;
            _loc5_.var_279 = Math.sin(_loc7_) * _loc8_;
            _loc5_.name_92 = Number(1 + (_loc4_ / _loc1_ * 2 - 1) * 0.05);
            _loc5_.var_250 = 0;
            _loc5_.method_118();
            _loc5_.var_243.rotation = Math.random() * 500;
            _loc9_ = Reflect.field(_loc5_.var_243,class_899.__unprotect__("a\x01"));
            _loc3_ = _loc9_;
            if(_loc3_ != null)
            {
               _loc3_.gotoAndPlay(int(class_691.method_114(2)) + 1);
            }
            var_958.push(_loc5_);
         }
         _loc3_ = var_232.method_84(class_899.__unprotect__("\x19k\'^\x02"),class_645.var_208);
         _loc3_.x = var_187.x;
         _loc3_.y = var_187.y;
         var_187.gotoAndPlay("death");
         var_960 = 100;
         var_954.parent.removeChild(var_954);
         var_953.parent.removeChild(var_953);
         var_955.parent.removeChild(var_955);
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\';[4\x02"),0);
         var_187 = var_232.method_84(class_899.__unprotect__(";B\x1ds\x02"),class_645.var_208);
         var_187.x = Number(50 + Math.random() * (class_742.var_217 - 2 * 50));
         var_187.y = Number(50 + Math.random() * (class_742.var_219 - 2 * 50));
         var_187.scaleX = 0.8 - var_237[0] * 0.5;
         var_187.scaleY = var_187.scaleX;
         var_954 = var_232.method_84(class_899.__unprotect__("\x0f|\x1cP\x02"),class_645.var_209);
         var_954.alpha = class_791.var_952;
      }
   }
}
