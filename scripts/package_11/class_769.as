package package_11
{
   import flash.utils.getTimer;
   
   public class class_769
   {
      
      public static var var_370:Boolean;
      
      public static var var_1159:int = 32;
      
      public static var var_1160:Number = 0.5;
      
      public static var var_1161:int;
      
      public static var var_1162:Number = 0.95;
      
      public static var var_1163:Number = 1;
      
      public static var var_918:Number = 1;
      
      public static var var_1164:Number = 1;
      
      public static var var_1165:int = 0;
       
      
      public function class_769()
      {
      }
      
      public static function method_79() : void
      {
         class_769.var_1165 = class_769.var_1165 + 1;
         var _loc1_:int = getTimer();
         class_769.var_1164 = (_loc1_ - class_769.var_1161) / 1000;
         class_769.var_1161 = _loc1_;
         if(class_769.var_1164 < class_769.var_1160)
         {
            class_769.var_1163 = Number(class_769.var_1163 * class_769.var_1162 + (1 - class_769.var_1162) * class_769.var_1164 * class_769.var_1159);
         }
         else
         {
            class_769.var_1164 = 1 / class_769.var_1159;
         }
         class_769.var_918 = class_769.var_1163;
      }
      
      public static function method_829() : Number
      {
         return class_769.var_1159 / class_769.var_918;
      }
   }
}
