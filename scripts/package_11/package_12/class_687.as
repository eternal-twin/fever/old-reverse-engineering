package package_11.package_12
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.filters.ColorMatrixFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import package_32.class_899;
   
   public class class_687 extends BitmapData
   {
       
      
      public var var_243:MovieClip;
      
      public var var_527:Number;
      
      public var var_526:BitmapData;
      
      public var var_230:Bitmap;
      
      public function class_687(param1:MovieClip = undefined, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined, param5:Object = undefined, param6:Object = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(param2 == null)
         {
            param2 = 100;
         }
         if(param3 == null)
         {
            param3 = 100;
         }
         if(param4 == null)
         {
            param4 = true;
         }
         if(param5 == null)
         {
            param5 = 0;
         }
         if(param6 == null)
         {
            param6 = 1;
         }
         var_527 = param6;
         super(int(param2 * var_527),int(param3 * var_527),param4,param5);
         var_243 = param1;
         var_230 = new Bitmap();
         var_230.bitmapData = this;
         var_243.addChild(var_230);
         var_243.scaleX = 1 / var_527;
         var_243.scaleY = 1 / var_527;
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_243.x = param1;
         var_243.y = param2;
      }
      
      public function method_332() : void
      {
         draw(var_526,new Matrix());
      }
      
      public function method_89() : void
      {
         dispose();
         if(var_526 != null)
         {
            var_526.dispose();
         }
         var_243.parent.removeChild(var_243);
      }
      
      public function method_333() : void
      {
         if(var_526 == null)
         {
            var_526 = clone();
         }
      }
      
      public function method_334(param1:Number, param2:Object = undefined) : void
      {
         var _loc11_:int = 0;
         method_333();
         var _loc3_:Array = [1,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0];
         if(param2 == null)
         {
            param2 = 30;
         }
         var _loc7_:Array = [0.4,0.5,0.1,0,param2,0.4,0.5,0.1,0,param2,0.4,0.5,0.1,0,param2,0,0,0,1,0];
         var _loc8_:Array = [];
         var _loc9_:int = 0;
         var _loc10_:int = int(_loc3_.length);
         while(_loc9_ < _loc10_)
         {
            _loc9_++;
            _loc11_ = _loc9_;
            _loc8_.push(Number(int(_loc3_[_loc11_]) * (1 - param1) + _loc7_[_loc11_] * param1));
         }
         var _loc12_:ColorMatrixFilter = new ColorMatrixFilter();
         _loc12_.matrix = _loc8_;
         applyFilter(this,rect,new Point(0,0),_loc12_);
      }
      
      public function method_335(param1:Sprite, param2:Object = undefined, param3:Object = undefined, param4:ColorTransform = undefined) : void
      {
         if(param2 == null)
         {
            param2 = 0;
         }
         if(param3 == null)
         {
            param3 = 0;
         }
         var _loc5_:Matrix = new Matrix();
         _loc5_.scale(param1.scaleX * var_527,param1.scaleY * var_527);
         _loc5_.rotate(param1.rotation * 0.0174);
         _loc5_.translate(Number(param1.x * var_527 + param2),Number(param1.y * var_527 + param3));
         if(param4 == null)
         {
            param4 = new ColorTransform(1,1,1,1,0,0,0,Number(-255 + param1.alpha * 255));
         }
         var _loc6_:String = param1.blendMode;
         draw(param1,_loc5_,param4,_loc6_,null,false);
      }
   }
}
