package package_11.package_12
{
   import flash.display.MovieClip;
   import flash.filters.BitmapFilter;
   import flash.geom.ColorTransform;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import package_10.class_870;
   import package_11.class_769;
   import package_32.class_899;
   
   public class class_783 extends class_687
   {
      
      public static var var_370:Boolean;
      
      public static var var_916:Boolean = false;
      
      public static var name_40:Array;
       
      
      public var var_251:Object;
      
      public var var_463:Array;
      
      public var var_273:Number;
      
      public var var_259:Boolean;
      
      public var var_917:ColorTransform;
      
      public function class_783(param1:MovieClip = undefined, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined)
      {
         var _loc5_:int = 0;
         if(class_899.var_239)
         {
            return;
         }
         super(param1,param2,param3,true,0,param4);
         var_259 = false;
         class_783.name_40.push(this);
         var_463 = [];
         var_273 = 10;
         if(class_783.var_916)
         {
            _loc5_ = 2;
            fillRect(rect,-65536);
            fillRect(new Rectangle(_loc5_,_loc5_,param2 - 2 * _loc5_,param3 - 2 * _loc5_),0);
         }
      }
      
      public function method_690() : void
      {
         var _loc3_:class_783 = null;
         var _loc1_:int = 0;
         var _loc2_:Array = class_783.name_40;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.method_79();
         }
      }
      
      public function method_79() : void
      {
         var _loc3_:BitmapFilter = null;
         var _loc4_:Number = NaN;
         if(var_259)
         {
            return;
         }
         if(var_917 != null)
         {
            colorTransform(rect,var_917);
         }
         var _loc1_:int = 0;
         var _loc2_:Array = var_463;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            applyFilter(this,rect,new Point(0,0),_loc3_);
         }
         if(var_251 != null)
         {
            var_251 = var_251 - class_769.var_918;
            if(var_251 < var_273)
            {
               _loc4_ = var_251 / var_273;
               var_243.alpha = _loc4_;
               if(var_251 <= 0)
               {
                  method_89();
               }
            }
         }
         if(var_243 == null)
         {
            class_870.name_17("plasma suicide!",{
               "\x06\fi2\x03":"Plasma.hx",
               "d\x0f^S\x03":49,
               "v2z\x01":"mt.bumdum9.Plasma",
               "d\x19o\f\x03":"update"
            });
            method_89();
         }
      }
      
      override public function method_89() : void
      {
         var_259 = true;
         class_783.name_40.method_116(this);
         dispose();
      }
   }
}
