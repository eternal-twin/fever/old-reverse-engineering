package package_11.package_12
{
   public class class_662
   {
       
      
      public function class_662()
      {
      }
      
      public static function method_98(param1:Class, param2:int) : *
      {
         var _loc3_:Array = Type.method_253(param1);
         return Type.method_254(param1,_loc3_[param2]);
      }
      
      public static function name_1(param1:Object) : Object
      {
         return class_662.name_21(param1,1);
      }
      
      public static function name_25(param1:Object) : Object
      {
         return class_662.name_21(param1,-1);
      }
      
      public static function name_21(param1:Object, param2:int) : Object
      {
         var _loc3_:int = int(param1.var_295) + param2;
         var _loc4_:Class = Type.method_255(param1);
         var _loc5_:Array = Type.method_253(_loc4_);
         var _loc6_:int = int(_loc5_.length);
         while(_loc3_ >= _loc6_)
         {
            _loc3_ = _loc3_ - _loc6_;
         }
         while(_loc3_ < 0)
         {
            _loc3_ = _loc3_ + _loc6_;
         }
         return Type.method_254(_loc4_,_loc5_[_loc3_]);
      }
   }
}
