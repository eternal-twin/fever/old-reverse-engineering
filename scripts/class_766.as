package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import package_11.package_12.class_659;
   import package_24.class_738;
   import package_24.class_739;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_747;
   
   public class class_766 extends class_645
   {
      
      public static var var_839:int = 30;
       
      
      public var var_251:int;
      
      public var name_80:int;
      
      public var var_351:Number;
      
      public var var_840:Array;
      
      public var var_844:class_738;
      
      public var var_532:BitmapData;
      
      public var var_630:int;
      
      public var var_843:Object;
      
      public var var_842:int;
      
      public var var_841:int;
      
      public function class_766()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Array = null;
         var _loc3_:* = null;
         var _loc4_:Array = null;
         var _loc5_:class_747 = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:int = 0;
         var _loc11_:* = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         super.method_79();
         loop5:
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_598();
               _loc1_ = var_251;
               var_251 = var_251 - 1;
               if(_loc1_ <= 0)
               {
                  method_599();
               }
               _loc2_ = var_840.method_78();
               _loc1_ = 0;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc3_.var_522 = _loc3_.var_522 - var_351;
                  _loc4_ = class_742.var_264[int(_loc3_.var_630)];
                  _loc3_.var_475.x = var_841 - int(_loc4_[0]) * _loc3_.var_522;
                  _loc3_.var_475.y = var_842 - int(_loc4_[1]) * _loc3_.var_522;
                  _loc3_.var_475.method_293();
                  if(Number(_loc3_.var_522) < 18 && name_80 > 0 && var_630 == int((int(_loc3_.var_630) + 2) % 4))
                  {
                     var_840.method_116(_loc3_);
                     _loc3_.var_475.var_198 = null;
                     _loc3_.var_475.method_128(class_702.var_571.method_98(null,"orc_die"));
                     _loc5_ = new class_747(_loc3_.var_475,12,4,4);
                     _loc5_.var_258 = _loc3_.var_475.method_89;
                  }
                  if(Number(_loc3_.var_522) < 8)
                  {
                     var_843 = _loc3_;
                     method_145(var_843,0);
                     method_600();
                     name_3 = name_3 + 1;
                  }
               }
               if(name_3 == 2)
               {
                  _loc1_ = 0;
                  _loc4_ = var_840;
                  while(_loc1_ < int(_loc4_.length))
                  {
                     _loc3_ = _loc4_[_loc1_];
                     _loc1_++;
                     _loc3_.var_630 = -1;
                     _loc3_.var_475.var_198.name_8(0);
                  }
                  break;
               }
               break;
            case 2:
               var_251 = var_251 + 1;
               if(var_251 == 12)
               {
                  var_844.visible = false;
                  var_843.var_630 = -1;
                  method_145(var_843,0);
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 3:
               _loc1_ = 0;
               _loc2_ = var_840;
               while(_loc1_ < int(_loc2_.length))
               {
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc6_ = 0.25;
                  _loc7_ = var_351 * 2;
                  if(_loc3_ == var_843)
                  {
                     _loc7_ = _loc7_ * 0.75;
                  }
                  else
                  {
                     _loc8_ = var_843.var_475.x - _loc3_.var_475.x;
                     _loc9_ = var_843.var_475.y - _loc3_.var_475.y;
                     _loc6_ = Number(Math.atan2(_loc9_,_loc8_));
                  }
                  _loc3_.var_475.x = Number(_loc3_.var_475.x + Math.cos(_loc6_) * _loc7_);
                  _loc3_.var_475.y = Number(_loc3_.var_475.y + Math.sin(_loc6_) * _loc7_);
                  _loc3_.var_475.method_293();
                  method_145(_loc3_,int(class_742.method_489(_loc6_)));
               }
               _loc1_ = 0;
               _loc2_ = var_840;
               while(true)
               {
                  if(_loc1_ >= int(_loc2_.length))
                  {
                     break loop5;
                  }
                  _loc3_ = _loc2_[_loc1_];
                  _loc1_++;
                  _loc10_ = 0;
                  _loc4_ = var_840;
                  while(_loc10_ < int(_loc4_.length))
                  {
                     _loc11_ = _loc4_[_loc10_];
                     _loc10_++;
                     if(_loc3_ != _loc11_)
                     {
                        _loc6_ = _loc3_.var_475.x - _loc11_.var_475.x;
                        _loc7_ = _loc3_.var_475.y - _loc11_.var_475.y;
                        _loc8_ = Number(Math.sqrt(Number(_loc6_ * _loc6_ + _loc7_ * _loc7_)));
                        _loc9_ = 12 - _loc8_;
                        if(_loc9_ > 0)
                        {
                           _loc12_ = Number(Math.atan2(_loc7_,_loc6_));
                           _loc13_ = Math.cos(_loc12_) * _loc9_ * 0.5;
                           _loc14_ = Math.sin(_loc12_) * _loc9_ * 0.5;
                           _loc3_.var_475.x = Number(_loc3_.var_475.x + _loc13_);
                           _loc3_.var_475.y = Number(_loc3_.var_475.y + _loc14_);
                           _loc11_.var_475.x = _loc11_.var_475.x - _loc13_;
                           _loc11_.var_475.y = _loc11_.var_475.y - _loc14_;
                           _loc3_.var_475.method_293();
                           _loc11_.var_475.method_293();
                        }
                     }
                  }
               }
         }
         var_232.method_519(1);
      }
      
      public function method_599(param1:int = 0) : void
      {
         var _loc2_:package_24.class_656 = new package_24.class_656();
         var_232.method_124(_loc2_,1);
         var _loc3_:* = {
            "\x05\x10\x01":_loc2_,
            "{\x10\x01":-1,
            ")}\x1b)\x01":120
         };
         method_145(_loc3_,int(class_691.method_114(4)));
         var_840.push(_loc3_);
         var_251 = int(class_766.var_839 / var_351);
         _loc3_.var_522 = _loc3_.var_522 - param1 * var_251 * var_351;
      }
      
      public function method_145(param1:Object, param2:int) : void
      {
         if(int(param1.var_630) == param2)
         {
            return;
         }
         param1.var_630 = param2;
         var _loc3_:String = ["orc_side","orc_front","orc_side","orc_back"][param2];
         if(param1 == var_843 && !var_844.visible)
         {
            _loc3_ = "orc_carry";
         }
         param1.var_475.method_137(class_702.var_571.method_136(_loc3_));
         param1.var_475.scaleX = int(param1.var_630) == 2?-1:1;
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function method_598() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         if(name_80 == 0)
         {
            _loc1_ = method_100();
            _loc2_ = _loc1_.var_244 - class_742.var_216 * 0.25;
            _loc3_ = _loc1_.var_245 - class_742.var_218 * 0.25;
            var_630 = int(class_742.method_489(Number(Math.atan2(_loc3_,_loc2_))));
            if(name_5)
            {
               name_80 = int(20 / var_351);
            }
         }
         else
         {
            name_80 = name_80 - 1;
         }
         var _loc4_:int = var_630;
         if(name_80 > 0)
         {
            _loc4_ = _loc4_ + 4;
         }
         var_844.method_128(class_702.var_571.method_98(_loc4_,"super_knight"));
      }
      
      override public function method_89() : void
      {
         super.method_89();
         var_532.dispose();
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:* = null;
         var _loc12_:class_750 = null;
         var_227 = [400];
         super.method_95(param1);
         var_201.scaleY = Number(2);
         var_201.scaleX = 2;
         class_319 = new MovieClip();
         class_319.graphics.beginFill(5592405);
         class_319.graphics.drawRect(0,0,class_742.var_216,class_742.var_218);
         var_232.method_124(class_319,0);
         var _loc3_:class_750 = class_702.var_571.method_98(null,"knight_tile");
         var_532 = new BitmapData(int(class_742.var_216 * 0.5),int(class_742.var_218 * 0.5),false,8675633);
         var _loc6_:Array = [];
         var _loc7_:int = 0;
         while(_loc7_ < 13)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc9_ = 0;
            while(_loc9_ < 13)
            {
               _loc9_++;
               _loc10_ = _loc9_;
               if(Number(Math.abs(_loc8_ - 6)) <= 1 || Number(Math.abs(_loc10_ - 6)) <= 1)
               {
                  _loc3_.method_506(var_532,_loc8_ * 16 - 3,_loc10_ * 16 - 2);
               }
               else
               {
                  _loc6_.push({
                     "L\x01":_loc8_,
                     "\x03\x01":_loc10_
                  });
               }
            }
         }
         class_659.method_241(_loc6_);
         _loc7_ = 0;
         while(_loc7_ < 32)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc11_ = _loc6_.pop();
            switch(int(class_691.method_114(2)))
            {
               case 0:
                  _loc12_ = class_702.var_571.method_98(int(class_691.method_114(3)),"knight_holes");
                  _loc12_.method_506(var_532,int(_loc11_.var_244) * 16 + int(class_691.method_114(8)),int(_loc11_.var_245) * 16 + int(class_691.method_114(8)));
                  continue;
               case 1:
                  _loc12_ = class_702.var_571.method_98(4 + int(class_691.method_114(4)),"knight_holes");
                  _loc12_.method_506(var_532,int(_loc11_.var_244) * 16 + int(class_691.method_114(12)),int(_loc11_.var_245) * 16 + int(class_691.method_114(12)));

            }
         }
         var_232.method_124(new Bitmap(var_532),0);
         var_841 = int(class_742.var_216 * 0.25);
         var_842 = int(class_742.var_216 * 0.25);
         var_844 = new class_738();
         var_844.method_128(class_702.var_571.method_98(null,"super_knight"));
         var_232.method_124(var_844,1);
         var_844.x = var_841;
         var_844.y = var_842;
         name_80 = 0;
         var_251 = 0;
         var_351 = Number(0.5 + param1 * 3);
         var_840 = [];
         name_3 = 1;
         _loc7_ = 0;
         while(_loc7_ < 3)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            method_599(_loc8_);
         }
         method_79();
      }
      
      public function method_600() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_739 = null;
         method_81(false,48);
         name_80 = 0;
         var_251 = 0;
         var_844.method_128(class_702.var_571.method_98(null,"knight_naked"));
         var _loc2_:int = 0;
         while(_loc2_ < 7)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = new class_739();
            _loc4_.method_128(class_702.var_571.method_98(_loc3_,"knight_armor"));
            _loc4_.method_129(var_844.x + int(class_691.method_114(5 * 2)) - 5,var_844.y + int(class_691.method_114(5 * 2)) - 5);
            _loc4_.var_250 = Number(0.1 + Math.random() * 0.1);
            _loc4_.var_279 = -(0.5 + Math.random() * 2);
            _loc4_.var_278 = (Math.random() - 0.5) * 2;
            _loc4_.var_233 = 0.95;
            _loc4_.method_488(Number(Number(var_844.y + 4) + int(class_691.method_114(6))),0.8,0.5);
            var_232.method_124(_loc4_,1);
         }
      }
   }
}
