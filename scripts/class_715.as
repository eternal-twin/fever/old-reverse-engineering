package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_41.package_42.qH1g;
   
   public class class_715 extends class_645
   {
      
      public static var var_644:int = 40;
       
      
      public var var_645:MovieClip;
      
      public var var_646:MovieClip;
      
      public var var_647:int;
      
      public var var_250:Number;
      
      public var var_649:Number;
      
      public var var_651:MovieClip;
      
      public var var_650:MovieClip;
      
      public var var_652:class_755;
      
      public var var_648:Number;
      
      public var var_460:Number;
      
      public var name_37:Number;
      
      public function class_715()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_460 = Number(Math.max(var_460 - var_250,0));
               break;
            case 2:
               _loc1_ = method_100();
               var_648 = _loc1_.var_245 - var_649;
               if(!name_5)
               {
                  method_404();
                  break;
               }
            case 3:
               _loc1_ = method_100();
               var_648 = _loc1_.var_245 - var_649;
               if(!name_5)
               {
                  method_404();
                  break;
               }
         }
         var _loc2_:Number = Number(name_37 + 340);
         var _loc3_:Number = Number(class_663.method_99(0,Number(var_460 + var_648),_loc2_));
         if(name_3 < 3 && _loc3_ == _loc2_)
         {
            name_3 = 3;
            method_81(true,20);
         }
         var_650.y = _loc3_ % class_715.var_644 - class_715.var_644;
         if(_loc3_ > name_37)
         {
            var_651.y = name_37 - _loc3_;
            _loc4_ = 0;
            while(_loc4_ < 1)
            {
               _loc4_++;
               _loc5_ = _loc4_;
               _loc6_ = var_652.method_84(class_899.__unprotect__("J&mg\x02"),0);
               _loc6_.x = (Math.random() * 2 - 1) * 60;
               _loc6_.y = Math.random() * 200;
               _loc6_.scaleX = Number(0.5 + Math.random() * 0.5);
               _loc6_.scaleY = _loc6_.scaleX;
               _loc6_.gotoAndPlay(10);
            }
         }
         else
         {
            var_651.y = -(_loc3_ % class_715.var_644);
         }
         _loc6_ = var_646.var_1;
         _loc6_.rotation = _loc3_;
         _loc6_ = var_645.var_1;
         _loc6_.rotation = _loc3_;
         super.method_79();
      }
      
      public function method_404() : void
      {
         if(name_3 == 3)
         {
            return;
         }
         var_460 = Number(var_460 + var_648);
         var_648 = 0;
         name_3 = 1;
      }
      
      public function method_405() : void
      {
         if(name_3 == 3)
         {
            return;
         }
         var _loc1_:* = method_100();
         var_649 = Number(_loc1_.var_245);
         name_3 = 2;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [360];
         super.method_95(param1);
         var_250 = 2;
         var_647 = 0;
         name_37 = 600;
         if(param1 > 0.3)
         {
            var_647 = var_647 + 1;
            var_250 = Number(var_250 + 5);
            name_37 = name_37 - 200;
         }
         if(param1 > 0.8)
         {
            var_647 = var_647 + 1;
            var_250 = Number(var_250 + 3);
            name_37 = name_37 - 500;
         }
         name_37 = Number(name_37 + param1 * 1450);
         var_460 = 0;
         method_117();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x0f9S \x02"),0);
         var_232.method_84(class_899.__unprotect__("N\x03(q\x03"),2);
         var_650 = var_232.method_84(class_899.__unprotect__("eK)d\x02"),1);
         var_650.x = 250;
         var_650.y = -class_715.var_644;
         qH1g.method_406(var_650,method_405);
         var_650.useHandCursor = true;
         var_650.buttonMode = true;
         var_651 = var_232.method_84(class_899.__unprotect__("eK)d\x02"),1);
         var_651.x = 150;
         var_651.y = 0;
         var _loc2_:MovieClip = var_651.var_1;
         _loc2_.gotoAndStop(var_647 + 1);
         _loc2_ = var_651.var_1;
         var_652 = new class_755(_loc2_);
         var_646 = var_232.method_84(class_899.__unprotect__("G6\x01R\x01"),1);
         var_646.x = 200;
         var_646.y = 0;
         var_646.stop();
         var_646.mouseEnabled = false;
         var_645 = var_232.method_84(class_899.__unprotect__("G6\x01R\x01"),0);
         var_645.x = 200;
         var_645.y = 0;
         var_645.gotoAndStop(2);
         var_645.mouseEnabled = false;
      }
   }
}
