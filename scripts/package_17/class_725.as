package package_17
{
   import package_10.class_704;
   import package_10.class_782;
   import package_10.class_806;
   import package_32.class_899;
   
   public class class_725
   {
      
      public static var var_660:int = 0;
       
      
      public var var_245:Number;
      
      public var var_244:Number;
      
      public var var_672:Number;
      
      public var var_4:Number;
      
      public var var_671:class_718;
      
      public var var_86:class_718;
      
      public var var_37:Number;
      
      public var var_676:class_806;
      
      public var var_674:Number;
      
      public var var_673:Number;
      
      public var var_663:class_809;
      
      public var var_539:Function;
      
      public var var_675:Number;
      
      public var var_693:Number;
      
      public var var_267:class_719;
      
      public var var_537:Boolean;
      
      public var var_665:Number;
      
      public var var_667:Number;
      
      public var var_694:Number;
      
      public var var_215:int;
      
      public var var_27:class_718;
      
      public var var_222:class_704;
      
      public var var_65:Number;
      
      public function class_725(param1:Number = 0.0, param2:Number = 0.0, param3:class_809 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(!var_539)
         {
            var_539 = function():void
            {
            };
         }
         var _loc4_:int = class_725.var_660;
         class_725.var_660 = class_725.var_660 + 1;
         var_215 = _loc4_;
         var_663 = param3 == null?Ah8a.var_695:param3;
         var_244 = param1;
         var_245 = param2;
         var_86 = new class_718(0,0);
         var_27 = new class_718(0,0);
         var_671 = new class_718(0,0);
         var _loc5_:Number = 0;
         var_672 = _loc5_;
         _loc5_ = _loc5_;
         var_37 = _loc5_;
         _loc5_ = _loc5_;
         var_4 = _loc5_;
         var_65 = _loc5_;
         var_673 = 1;
         var_674 = 0;
         var_676 = new class_806();
         var_222 = new class_704();
      }
      
      public function method_350() : void
      {
         var _loc4_:class_800 = null;
         var _loc5_:Number = NaN;
         var _loc1_:Number = 0;
         var _loc2_:Number = 0;
         var _loc3_:class_782 = var_676.var_84;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_223;
            _loc3_ = _loc3_.name_1;
            _loc5_ = _loc4_.var_692 * _loc4_.var_538.var_582;
            _loc1_ = Number(_loc1_ + _loc5_);
            _loc2_ = Number(_loc2_ + _loc4_.method_435() * _loc5_);
         }
         if(_loc1_ > 0)
         {
            var_693 = _loc1_;
            var_665 = 1 / _loc1_;
         }
         else
         {
            var_693 = Number(Math.POSITIVE_INFINITY);
            var_665 = 0;
            var_537 = true;
         }
         if(_loc2_ > 0)
         {
            var_694 = _loc2_;
            var_667 = 1 / _loc2_;
         }
         else
         {
            var_694 = Number(Math.POSITIVE_INFINITY);
            var_667 = 0;
         }
      }
      
      public function toString() : String
      {
         return "Body#" + var_215;
      }
      
      public function method_337(param1:Number, param2:Number, param3:Object = undefined) : void
      {
         var_86.var_244 = param1;
         var_86.var_245 = param2;
         if(param3 != null)
         {
            var_4 = param3;
         }
      }
      
      public function method_129(param1:Number, param2:Number, param3:Object = undefined) : void
      {
         var_244 = param1;
         var_245 = param2;
         if(param3 != null)
         {
            method_349(param3);
         }
      }
      
      public function method_349(param1:Number) : void
      {
         var_65 = param1;
         var_673 = Number(Math.cos(param1));
         var_674 = Number(Math.sin(param1));
      }
      
      public function method_216(param1:class_718 = undefined, param2:Object = undefined, param3:class_718 = undefined, param4:Object = undefined) : void
      {
         if(param1 != null)
         {
            var_244 = param1.var_244;
            var_245 = param1.var_245;
         }
         if(param2 != null)
         {
            method_349(param2);
         }
         if(param3 != null)
         {
            var_86.var_244 = param3.var_244;
            var_86.var_245 = param3.var_245;
         }
         if(param4 != null)
         {
            var_4 = param4;
         }
      }
      
      public function method_436(param1:class_800) : void
      {
         var_676.method_116(param1);
         param1.var_21 = null;
      }
      
      public function method_437() : void
      {
         var_694 = Number(Math.POSITIVE_INFINITY);
         var_667 = 0;
      }
      
      public function method_343(param1:class_800) : void
      {
         var _loc2_:class_806 = var_676;
         _loc2_.var_84 = new class_782(param1,_loc2_.var_84);
         param1.var_21 = this;
      }
   }
}
