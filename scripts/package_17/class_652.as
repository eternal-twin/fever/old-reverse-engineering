package package_17
{
   import package_10.class_650;
   import package_10.class_678;
   import package_10.class_704;
   import package_10.class_746;
   import package_10.class_782;
   import package_10.class_803;
   import package_10.class_805;
   import package_10.class_806;
   import package_10.class_821;
   import package_10.class_917;
   import package_20.package_21.class_734;
   import package_20.package_21.class_735;
   import package_20.package_21.class_893;
   import package_25.package_26.class_740;
   import package_32.class_899;
   
   public class class_652 implements class_734
   {
       
      
      public var var_1378:class_650;
      
      public var var_1379:Boolean;
      
      public var var_251:class_769;
      
      public var var_1380:int;
      
      public var var_1383:class_725;
      
      public var name_81:int;
      
      public var var_678:Number;
      
      public var var_663:class_654;
      
      public var var_668:class_746;
      
      public var var_776:class_917;
      
      public var var_661:class_718;
      
      public var LfCE:Boolean;
      
      public var name_118:class_904;
      
      public var var_669:class_735;
      
      public var var_201:class_893;
      
      public var var_1382:int;
      
      public var var_662:class_650;
      
      public var var_222:class_704;
      
      public var var_1109:class_827;
      
      public var var_1381:int;
      
      public function class_652(param1:class_893 = undefined, param2:class_735 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_662 = new class_650();
         var_668 = new class_746();
         var_222 = new class_704();
         var_663 = new class_654();
         var_661 = new class_718(0,0);
         name_81 = 0;
         LfCE = false;
         var_1379 = true;
         var_678 = 0.002;
         var_1382 = 120;
         var_1109 = new class_827();
         name_118 = new class_904();
         var_1383 = new class_725(0,0);
         var_1383.var_267 = new class_719(this);
         var_1383.method_350();
         var_201 = param1;
         var_669 = param2;
         param2.method_95(var_201,this,var_1383);
         var_251 = new class_769();
         var_776 = new class_917();
         var_1378 = new class_650();
      }
      
      public function method_338(param1:class_725) : void
      {
         var _loc3_:class_800 = null;
         var _loc2_:class_782 = param1.var_676.var_84;
         while(_loc2_ != null)
         {
            _loc3_ = _loc2_.var_223;
            _loc2_ = _loc2_.name_1;
            _loc3_.method_79();
            var_669.method_421(_loc3_);
         }
      }
      
      public function name_3(param1:Number, param2:int) : void
      {
         var _loc5_:class_809 = null;
         var _loc6_:class_719 = null;
         var _loc7_:class_827 = null;
         var _loc8_:class_719 = null;
         var _loc9_:class_917 = null;
         var _loc12_:class_833 = null;
         var _loc13_:class_939 = null;
         var _loc14_:class_939 = null;
         var _loc15_:class_725 = null;
         var _loc16_:class_725 = null;
         var _loc17_:class_650 = null;
         var _loc18_:class_803 = null;
         var _loc19_:class_782 = null;
         var _loc20_:class_800 = null;
         if(param1 < 1.0e-99)
         {
            param1 = 0;
         }
         var_251.name_51("all");
         var _loc3_:Number = param1 == 0?Number(0):1 / param1;
         var _loc4_:* = var_663.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc5_.var_664 = Number(Math.pow(_loc5_.var_1041,param1));
            _loc5_.var_666 = Number(Math.pow(_loc5_.var_778,param1));
         }
         var_251.name_51("island");
         if(var_1379)
         {
            method_1028();
         }
         else
         {
            _loc7_ = var_1109;
            _loc8_ = _loc7_.var_1090;
            if(_loc8_ == null)
            {
               §§push(new class_719(this));
            }
            else
            {
               _loc7_.var_1090 = _loc8_.var_680;
               §§push(_loc8_);
            }
            _loc6_ =; §§pop();
            _loc6_.var_662 = var_662;
            _loc6_.var_222 = var_222;
            _loc6_.var_668 = var_668;
            var_678 = 0;
            var_776 = new class_917();
            _loc9_ = var_776;
            _loc9_.var_84 = new class_805(_loc6_,_loc9_.var_84);
         }
         if(LfCE)
         {
            method_1029();
         }
         var_251.method_134();
         var_251.name_51("solve");
         var _loc10_:class_805 = var_776.var_84;
         while(_loc10_ != null)
         {
            _loc6_ = _loc10_.var_223;
            _loc10_ = _loc10_.name_1;
            if(!_loc6_.var_679)
            {
               _loc6_.method_422(param1,_loc3_,param2);
            }
         }
         var_251.method_134();
         var _loc11_:class_821 = var_222.var_84;
         while(_loc11_ != null)
         {
            _loc12_ = _loc11_.var_223;
            _loc11_ = _loc11_.name_1;
            if(name_81 - _loc12_.name_81 > 3)
            {
               _loc7_ = var_1109;
               _loc13_ = _loc12_.var_774;
               while(_loc13_ != null)
               {
                  _loc14_ = _loc13_.name_1;
                  _loc13_.name_1 = _loc7_.var_1091;
                  _loc7_.var_1091 = _loc13_;
                  _loc13_ = _loc14_;
               }
               _loc15_ = _loc12_.var_14.var_21;
               _loc16_ = _loc12_.var_73.var_21;
               _loc15_.var_222.method_116(_loc12_);
               _loc16_.var_222.method_116(_loc12_);
               var_222.method_116(_loc12_);
               null;
               method_1030(_loc15_.var_267);
               method_1030(_loc16_.var_267);
            }
         }
         var_251.name_51("col");
         var_669.method_479();
         var_1380 = 0;
         var_1381 = 0;
         if(!!LfCE && !var_669.method_477())
         {
            class_899.var_317 = new Error();
            throw "INVALID BF DATAS";
         }
         var_669.method_480();
         var_251.method_134();
         name_81 = name_81 + 1;
         if(var_1382 > 0 && int(name_81 % var_1382) == 0)
         {
            _loc17_ = new class_650();
            _loc18_ = var_662.var_84;
            while(_loc18_ != null)
            {
               _loc15_ = _loc18_.var_223;
               _loc18_ = _loc18_.name_1;
               _loc17_.var_84 = new class_803(_loc15_,_loc17_.var_84);
            }
            _loc19_ = var_669.method_478(var_201).var_84;
            while(_loc19_ != null)
            {
               _loc20_ = _loc19_.var_223;
               _loc19_ = _loc19_.name_1;
               _loc17_.method_116(_loc20_.var_21);
            }
            _loc18_ = _loc17_.var_84;
            while(_loc18_ != null)
            {
               _loc15_ = _loc18_.var_223;
               _loc18_ = _loc18_.name_1;
               if(method_340(_loc15_))
               {
                  _loc15_.var_539();
               }
            }
         }
         var_251.method_134();
      }
      
      public function method_1031(param1:class_735) : void
      {
         var _loc3_:class_725 = null;
         var _loc4_:class_782 = null;
         var _loc5_:class_800 = null;
         param1.method_95(var_201,this,var_1383);
         var _loc2_:class_803 = var_662.var_84;
         while(_loc2_ != null)
         {
            _loc3_ = _loc2_.var_223;
            _loc2_ = _loc2_.name_1;
            _loc4_ = _loc3_.var_676.var_84;
            while(_loc4_ != null)
            {
               _loc5_ = _loc4_.var_223;
               _loc4_ = _loc4_.name_1;
               var_669.method_436(_loc5_);
               param1.method_343(_loc5_);
            }
         }
         _loc4_ = var_1383.var_676.var_84;
         while(_loc4_ != null)
         {
            _loc5_ = _loc4_.var_223;
            _loc4_ = _loc4_.name_1;
            var_669.method_436(_loc5_);
            param1.method_343(_loc5_);
         }
         var_669 = param1;
      }
      
      public function method_1032(param1:class_800) : void
      {
         var_1383.method_436(param1);
         var_669.method_436(param1);
      }
      
      public function method_1033(param1:class_740) : void
      {
         var_668.method_116(param1);
         method_1030(param1.var_35.var_267);
         method_1030(param1.var_36.var_267);
      }
      
      public function method_340(param1:class_725) : Boolean
      {
         var _loc3_:class_800 = null;
         var _loc5_:class_833 = null;
         var _loc6_:class_725 = null;
         if(!var_662.method_116(param1))
         {
            return false;
         }
         param1.var_663.name_28 = param1.var_663.name_28 - 1;
         if(param1.var_663.name_28 == 0)
         {
            var_663.method_116(param1.var_663.var_215);
         }
         var _loc2_:class_782 = param1.var_676.var_84;
         while(_loc2_ != null)
         {
            _loc3_ = _loc2_.var_223;
            _loc2_ = _loc2_.name_1;
            var_669.method_436(_loc3_);
         }
         method_1030(param1.var_267);
         var_1378.method_116(param1);
         var _loc4_:class_821 = param1.var_222.var_84;
         while(_loc4_ != null)
         {
            _loc5_ = _loc4_.var_223;
            _loc4_ = _loc4_.name_1;
            _loc6_ = _loc5_.var_14.var_21;
            (_loc6_ == param1?_loc5_.var_73.var_21:_loc6_).var_222.method_116(_loc5_);
         }
         return true;
      }
      
      public function method_476(param1:class_800, param2:class_800) : Boolean
      {
         var _loc5_:class_800 = null;
         var _loc9_:class_833 = null;
         var _loc12_:* = null;
         var _loc13_:* = null;
         var _loc14_:class_704 = null;
         var _loc15_:class_719 = null;
         var _loc3_:class_725 = param1.var_21;
         var _loc4_:class_725 = param2.var_21;
         var_1380 = var_1380 + 1;
         if(_loc3_ == _loc4_ || (param1.var_1005 & param2.var_1005) == 0)
         {
            return false;
         }
         if(param1.var_252 > param2.var_252)
         {
            _loc5_ = param1;
            param1 = param2;
            param2 = _loc5_;
         }
         var _loc6_:Boolean = true;
         var _loc7_:class_833 = null;
         var _loc8_:class_821 = _loc3_.var_222.var_84;
         while(_loc8_ != null)
         {
            _loc9_ = _loc8_.var_223;
            _loc8_ = _loc8_.name_1;
            if(_loc9_.var_14 == param1 && _loc9_.var_73 == param2 || _loc9_.var_14 == param2 && _loc9_.var_73 == param1)
            {
               _loc7_ = _loc9_;
               break;
            }
         }
         if(_loc7_ == null)
         {
            _loc7_ = new class_833(var_1109);
            _loc7_.method_816(param1,param2);
            _loc6_ = false;
         }
         else
         {
            if(_loc7_.var_679)
            {
               _loc7_.name_81 = name_81;
               return true;
            }
            if(_loc7_.name_81 == name_81)
            {
               return true;
            }
         }
         _loc7_.var_679 = false;
         var_1381 = var_1381 + 1;
         var _loc11_:class_904 = name_118;
         if(param1.var_252 == 2 && param2.var_252 == 2)
         {
            §§push(Boolean(_loc11_.method_1034(param1.var_1003,param2.var_1003,_loc7_)));
         }
         else if(param1.var_252 == 0)
         {
            if(param2.var_252 == 2)
            {
               §§push(Boolean(_loc11_.method_1035(param1.var_1006,param2.var_1003,_loc7_)));
            }
            else if(param2.var_252 == 0)
            {
               _loc12_ = param1.var_1006;
               _loc13_ = param2.var_1006;
               §§push(Boolean(_loc11_.method_1036(_loc7_,_loc12_.var_1315,_loc13_.var_1315,Number(_loc12_.var_93),Number(_loc13_.var_93))));
            }
            else
            {
               §§push(Boolean(_loc11_.method_1037(param1.var_1006,param2.var_1002,_loc7_)));
            }
         }
         else
         {
            §§push(param1.var_252 == 1 && param2.var_252 == 2?Boolean(_loc11_.method_1038(param1.var_1002,param2.var_1003,_loc7_)):false);
         }
         var _loc10_:Boolean =; §§pop();
         if(_loc10_)
         {
            _loc7_.name_81 = name_81;
            if(_loc6_)
            {
               _loc7_.var_14 = param1;
               _loc7_.var_73 = param2;
            }
            else
            {
               _loc14_ = var_222;
               _loc14_.var_84 = new class_821(_loc7_,_loc14_.var_84);
               _loc15_ = _loc3_.var_267;
               if(_loc15_ != _loc4_.var_267)
               {
                  method_1030(_loc15_);
                  method_1030(_loc4_.var_267);
               }
               else if(_loc15_ != null)
               {
                  _loc14_ = _loc15_.var_222;
                  _loc14_.var_84 = new class_821(_loc7_,_loc14_.var_84);
                  _loc7_.var_267 = _loc15_;
               }
               _loc14_ = param2.var_21.var_222;
               _loc14_.var_84 = new class_821(_loc7_,_loc14_.var_84);
               _loc14_ = param1.var_21.var_222;
               _loc14_.var_84 = new class_821(_loc7_,_loc14_.var_84);
            }
         }
         else if(!_loc6_)
         {
            null;
         }
         return _loc10_;
      }
      
      public function method_1030(param1:class_719) : void
      {
         var _loc3_:class_725 = null;
         var _loc4_:class_650 = null;
         var _loc6_:class_833 = null;
         var _loc8_:class_740 = null;
         if(param1 == null || !var_1379)
         {
            return;
         }
         if(!var_776.method_116(param1))
         {
            return;
         }
         var _loc2_:class_803 = param1.var_662.var_84;
         while(_loc2_ != null)
         {
            _loc3_ = _loc2_.var_223;
            _loc2_ = _loc2_.name_1;
            _loc3_.var_267 = null;
            _loc4_ = var_1378;
            _loc4_.var_84 = new class_803(_loc3_,_loc4_.var_84);
         }
         var _loc5_:class_821 = param1.var_222.var_84;
         while(_loc5_ != null)
         {
            _loc6_ = _loc5_.var_223;
            _loc5_ = _loc5_.name_1;
            _loc6_.var_679 = false;
            _loc6_.var_267 = null;
         }
         var _loc7_:class_678 = param1.var_668.var_84;
         while(_loc7_ != null)
         {
            _loc8_ = _loc7_.var_223;
            _loc7_ = _loc7_.name_1;
            _loc8_.var_267 = null;
         }
         var _loc9_:class_827 = var_1109;
         param1.var_662.var_84 = null;
         param1.var_222.var_84 = null;
         param1.var_668.var_84 = null;
         param1.var_679 = false;
         param1.var_680 = _loc9_.var_1090;
         _loc9_.var_1090 = param1;
      }
      
      public function method_1029() : void
      {
         var _loc1_:class_803 = null;
         var _loc2_:class_725 = null;
         var _loc4_:class_719 = null;
         _loc1_ = var_1378.var_84;
         while(_loc1_ != null)
         {
            _loc2_ = _loc1_.var_223;
            _loc1_ = _loc1_.name_1;
            method_1039(_loc2_,null);
         }
         var _loc3_:class_805 = var_776.var_84;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_223;
            _loc3_ = _loc3_.name_1;
            _loc1_ = _loc4_.var_662.var_84;
            while(_loc1_ != null)
            {
               _loc2_ = _loc1_.var_223;
               _loc1_ = _loc1_.name_1;
               method_1039(_loc2_,_loc4_);
            }
         }
      }
      
      public function method_1039(param1:class_725, param2:class_719) : void
      {
         var _loc4_:class_833 = null;
         if(param1.var_267 != param2)
         {
            class_899.var_317 = new Error();
            throw "ASSERT";
         }
         var _loc3_:class_821 = param1.var_222.var_84;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_223;
            _loc3_ = _loc3_.name_1;
            if(_loc4_.var_267 != param2)
            {
               class_899.var_317 = new Error();
               throw "ASSERT";
            }
            if(_loc4_.var_14.var_21.var_267 != param2 && !_loc4_.var_14.var_21.var_537)
            {
               class_899.var_317 = new Error();
               throw "ASSERT";
            }
            if(_loc4_.var_73.var_21.var_267 != param2 && !_loc4_.var_73.var_21.var_537)
            {
               class_899.var_317 = new Error();
               throw "ASSERT";
            }
         }
      }
      
      public function method_1028() : void
      {
         var _loc3_:class_725 = null;
         var _loc4_:class_719 = null;
         var _loc5_:class_827 = null;
         var _loc6_:class_719 = null;
         var _loc7_:class_917 = null;
         var _loc8_:class_725 = null;
         var _loc9_:class_803 = null;
         var _loc10_:class_650 = null;
         var _loc11_:class_821 = null;
         var _loc12_:class_833 = null;
         var _loc13_:class_704 = null;
         var _loc14_:class_725 = null;
         var _loc15_:class_725 = null;
         var _loc1_:class_650 = new class_650();
         var _loc2_:class_803 = var_1378.var_84;
         while(_loc2_ != null)
         {
            _loc3_ = _loc2_.var_223;
            _loc2_ = _loc2_.name_1;
            if(!(_loc3_.var_267 != null || _loc3_.var_537))
            {
               _loc5_ = var_1109;
               _loc6_ = _loc5_.var_1090;
               if(_loc6_ == null)
               {
                  §§push(new class_719(this));
               }
               else
               {
                  _loc5_.var_1090 = _loc6_.var_680;
                  §§push(_loc6_);
               }
               _loc4_ =; §§pop();
               _loc7_ = var_776;
               _loc7_.var_84 = new class_805(_loc4_,_loc7_.var_84);
               _loc1_.var_84 = new class_803(_loc3_,_loc1_.var_84);
               _loc3_.var_267 = _loc4_;
               while(true)
               {
                  _loc9_ = _loc1_.var_84;
                  if(_loc9_ == null)
                  {
                     §§push(null);
                  }
                  else
                  {
                     _loc1_.var_84 = _loc9_.name_1;
                     §§push(_loc9_.var_223);
                  }
                  _loc8_ =; §§pop();
                  if(_loc8_ == null)
                  {
                     break;
                  }
                  _loc10_ = _loc4_.var_662;
                  _loc10_.var_84 = new class_803(_loc8_,_loc10_.var_84);
                  _loc11_ = _loc8_.var_222.var_84;
                  while(_loc11_ != null)
                  {
                     _loc12_ = _loc11_.var_223;
                     _loc11_ = _loc11_.name_1;
                     if(_loc12_.var_267 == null)
                     {
                        _loc13_ = _loc4_.var_222;
                        _loc13_.var_84 = new class_821(_loc12_,_loc13_.var_84);
                        _loc12_.var_267 = _loc4_;
                        _loc14_ = _loc12_.var_14.var_21;
                        if(_loc14_.var_267 == null && !_loc14_.var_537)
                        {
                           _loc14_.var_267 = _loc4_;
                           _loc1_.var_84 = new class_803(_loc14_,_loc1_.var_84);
                        }
                        _loc15_ = _loc12_.var_73.var_21;
                        if(_loc15_.var_267 == null && !_loc15_.var_537)
                        {
                           _loc15_.var_267 = _loc4_;
                           _loc1_.var_84 = new class_803(_loc15_,_loc1_.var_84);
                        }
                     }
                  }
               }
            }
         }
         var_1378 = new class_650();
      }
      
      public function method_513(param1:class_800) : class_800
      {
         var_1383.method_343(param1);
         param1.method_79();
         var_669.method_343(param1);
         return param1;
      }
      
      public function method_1040(param1:class_740) : void
      {
         var _loc2_:class_746 = var_668;
         _loc2_.var_84 = new class_678(param1,_loc2_.var_84);
      }
      
      public function method_341(param1:class_725) : void
      {
         var _loc3_:class_782 = null;
         var _loc4_:class_800 = null;
         var _loc2_:class_650 = var_662;
         _loc2_.var_84 = new class_803(param1,_loc2_.var_84);
         _loc2_ = var_1378;
         _loc2_.var_84 = new class_803(param1,_loc2_.var_84);
         param1.var_663.name_28 = param1.var_663.name_28 + 1;
         param1.var_675 = var_678 * 2;
         var_663.method_216(param1.var_663.var_215,param1.var_663);
         if(param1.var_537)
         {
            param1.var_693 = Number(Math.POSITIVE_INFINITY);
            param1.var_665 = 0;
            param1.var_694 = Number(Math.POSITIVE_INFINITY);
            param1.var_667 = 0;
            _loc3_ = param1.var_676.var_84;
            while(_loc3_ != null)
            {
               _loc4_ = _loc3_.var_223;
               _loc3_ = _loc3_.name_1;
               _loc4_.method_79();
            }
         }
         else
         {
            param1.method_350();
         }
         _loc3_ = param1.var_676.var_84;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_223;
            _loc3_ = _loc3_.name_1;
            var_669.method_343(_loc4_);
         }
      }
      
      public function method_579(param1:class_725) : void
      {
         var _loc3_:class_821 = null;
         var _loc4_:class_833 = null;
         var _loc2_:class_719 = param1.var_267;
         param1.var_675 = var_678 * 2;
         if(_loc2_ != null && _loc2_.var_679)
         {
            _loc2_.var_679 = false;
            _loc3_ = _loc2_.var_222.var_84;
            while(_loc3_ != null)
            {
               _loc4_ = _loc3_.var_223;
               _loc3_ = _loc3_.name_1;
               _loc4_.var_679 = false;
            }
         }
      }
   }
}
