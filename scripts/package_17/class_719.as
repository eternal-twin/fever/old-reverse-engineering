package package_17
{
   import package_10.class_650;
   import package_10.class_678;
   import package_10.class_704;
   import package_10.class_746;
   import package_10.class_782;
   import package_10.class_803;
   import package_10.class_821;
   import package_20.package_21.class_735;
   import package_25.package_26.class_740;
   import package_32.class_899;
   
   public class class_719
   {
      
      public static var var_660:int = 0;
       
      
      public var package_13:class_652;
      
      public var var_679:Boolean;
      
      public var var_668:class_746;
      
      public var var_215:int;
      
      public var var_677:Number;
      
      public var var_662:class_650;
      
      public var var_222:class_704;
      
      public var var_680:class_719;
      
      public function class_719(param1:class_652 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var _loc2_:int = class_719.var_660;
         class_719.var_660 = class_719.var_660 + 1;
         var_215 = _loc2_;
         package_13 = param1;
         var_679 = false;
         var_662 = new class_650();
         var_668 = new class_746();
         var_222 = new class_704();
      }
      
      public function method_422(param1:Number, param2:Number, param3:int) : void
      {
         var _loc5_:class_803 = null;
         var _loc6_:class_725 = null;
         var _loc7_:class_718 = null;
         var _loc8_:class_809 = null;
         var _loc9_:class_821 = null;
         var _loc10_:class_833 = null;
         var _loc11_:class_678 = null;
         var _loc12_:class_740 = null;
         var _loc14_:int = 0;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:class_782 = null;
         var _loc21_:class_800 = null;
         var _loc4_:class_718 = package_13.var_661;
         _loc5_ = var_662.var_84;
         while(_loc5_ != null)
         {
            _loc6_ = _loc5_.var_223;
            _loc5_ = _loc5_.name_1;
            _loc7_ = _loc6_.var_86;
            _loc8_ = _loc6_.var_663;
            _loc7_.var_244 = Number(_loc7_.var_244 * _loc8_.var_664 + (_loc4_.var_244 + _loc6_.var_27.var_244 * _loc6_.var_665) * param1);
            _loc7_.var_245 = Number(_loc7_.var_245 * _loc8_.var_664 + (_loc4_.var_245 + _loc6_.var_27.var_245 * _loc6_.var_665) * param1);
            _loc6_.var_4 = Number(_loc6_.var_4 * _loc8_.var_666 + _loc6_.var_37 * _loc6_.var_667 * param1);
         }
         _loc9_ = var_222.var_84;
         while(_loc9_ != null)
         {
            _loc10_ = _loc9_.var_223;
            _loc9_ = _loc9_.name_1;
            _loc10_.method_418(param1);
         }
         _loc11_ = var_668.var_84;
         while(_loc11_ != null)
         {
            _loc12_ = _loc11_.var_223;
            _loc11_ = _loc11_.name_1;
            _loc12_.method_418(param2);
         }
         var _loc13_:int = 0;
         while(_loc13_ < param3)
         {
            _loc13_++;
            _loc14_ = _loc13_;
            _loc9_ = var_222.var_84;
            while(_loc9_ != null)
            {
               _loc10_ = _loc9_.var_223;
               _loc9_ = _loc9_.name_1;
               _loc10_.method_419();
            }
            _loc11_ = var_668.var_84;
            while(_loc11_ != null)
            {
               _loc12_ = _loc11_.var_223;
               _loc11_ = _loc11_.name_1;
               _loc12_.method_420();
            }
         }
         var _loc15_:class_735 = package_13.var_669;
         var _loc16_:Number = 0;
         _loc13_ = 0;
         var _loc17_:* = Math;
         _loc5_ = var_662.var_84;
         while(_loc5_ != null)
         {
            _loc6_ = _loc5_.var_223;
            _loc5_ = _loc5_.name_1;
            _loc18_ = Number(Number(_loc6_.var_86.var_244 * _loc6_.var_86.var_244 + _loc6_.var_86.var_245 * _loc6_.var_86.var_245) + _loc6_.var_4 * _loc6_.var_4 * 30);
            if(_loc18_ > _loc6_.var_663.var_670)
            {
               _loc19_ = Number(Math.sqrt(_loc6_.var_663.var_670 / _loc18_));
               _loc6_.var_86.var_244 = _loc6_.var_86.var_244 * _loc19_;
               _loc6_.var_86.var_245 = _loc6_.var_86.var_245 * _loc19_;
               _loc6_.var_4 = _loc6_.var_4 * _loc19_;
               _loc18_ = _loc18_ * (_loc19_ * _loc19_);
            }
            _loc6_.var_244 = Number(_loc6_.var_244 + (Number(_loc6_.var_86.var_244 * param1 + _loc6_.var_671.var_244)));
            _loc6_.var_245 = Number(_loc6_.var_245 + (Number(_loc6_.var_86.var_245 * param1 + _loc6_.var_671.var_245)));
            _loc6_.var_65 = Number(_loc6_.var_65 + (Number(_loc6_.var_4 * param1 + _loc6_.var_672)));
            _loc6_.var_673 = Number(_loc17_.cos(_loc6_.var_65));
            _loc6_.var_674 = Number(_loc17_.sin(_loc6_.var_65));
            _loc6_.var_675 = Number(_loc6_.var_675 * 0.95 + (1 - 0.95) * _loc18_);
            _loc19_ = 0;
            _loc6_.var_37 = _loc19_;
            _loc19_ = _loc19_;
            _loc6_.var_27.var_245 = _loc19_;
            _loc6_.var_27.var_244 = _loc19_;
            _loc19_ = 0;
            _loc6_.var_672 = _loc19_;
            _loc19_ = _loc19_;
            _loc6_.var_671.var_245 = _loc19_;
            _loc6_.var_671.var_244 = _loc19_;
            _loc16_ = Number(_loc16_ + _loc6_.var_675);
            _loc13_++;
            _loc20_ = _loc6_.var_676.var_84;
            while(_loc20_ != null)
            {
               _loc21_ = _loc20_.var_223;
               _loc20_ = _loc20_.name_1;
               _loc21_.method_79();
               _loc15_.method_421(_loc21_);
            }
         }
         var_677 = _loc16_ / _loc17_.sqrt(_loc13_);
         if(var_677 < package_13.var_678)
         {
            _loc5_ = var_662.var_84;
            while(_loc5_ != null)
            {
               _loc6_ = _loc5_.var_223;
               _loc5_ = _loc5_.name_1;
               _loc6_.var_86.var_244 = 0;
               _loc6_.var_86.var_245 = 0;
               _loc6_.var_4 = 0;
            }
            _loc9_ = var_222.var_84;
            while(_loc9_ != null)
            {
               _loc10_ = _loc9_.var_223;
               _loc9_ = _loc9_.name_1;
               _loc10_.var_679 = true;
            }
            var_679 = true;
         }
      }
   }
}
