package package_17
{
   import package_20.package_21.class_893;
   import package_32.class_899;
   
   public class class_800
   {
      
      public static var var_660:int = 0;
      
      public static var var_999:int = 0;
      
      public static var var_1000:int = 1;
      
      public static var var_1001:int = 2;
       
      
      public var var_252:int;
      
      public var var_1002:class_865;
      
      public var var_1003:class_863;
      
      public var var_1004:class_718;
      
      public var var_538:class_709;
      
      public var var_215:int;
      
      public var var_1005:int;
      
      public var var_1006:class_875;
      
      public var var_21:class_725;
      
      public var var_692:Number;
      
      public var var_1007:class_893;
      
      public function class_800(param1:int = 0, param2:class_709 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var _loc3_:int = class_800.var_660;
         class_800.var_660 = class_800.var_660 + 1;
         var_215 = _loc3_;
         var_1005 = 1;
         var_252 = param1;
         var_538 = param2 == null?Ah8a.var_1008:param2;
         var_692 = 0;
         var_1007 = new class_893(0,0,0,0);
      }
      
      public static function method_346(param1:Number, param2:Number, param3:Object = undefined, param4:Object = undefined, param5:class_709 = undefined) : class_863
      {
         if(param3 == null)
         {
            param3 = -param1 / 2;
         }
         if(param4 == null)
         {
            param4 = -param2 / 2;
         }
         return new class_863([new class_718(0,0),new class_718(0,param2),new class_718(param1,param2),new class_718(param1,0)],new class_718(param3,param4),param5);
      }
      
      public function method_79() : void
      {
      }
      
      public function toString() : String
      {
         return "Shape#" + var_215;
      }
      
      public function method_435() : Number
      {
         return 1;
      }
   }
}
