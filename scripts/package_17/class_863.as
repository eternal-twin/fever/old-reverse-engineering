package package_17
{
   import package_20.package_21.class_893;
   import package_32.class_899;
   
   public class class_863 extends class_800
   {
       
      
      public var var_1269:class_718;
      
      public var var_1270:int;
      
      public var var_1271:class_718;
      
      public var var_1273:class_860;
      
      public var var_1272:class_860;
      
      public function class_863(param1:Array = undefined, param2:class_718 = undefined, param3:class_709 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(2,param3);
         var_1003 = this;
         var_1004 = param2;
         method_963(param1);
      }
      
      override public function method_79() : void
      {
         var _loc8_:class_718 = null;
         var _loc1_:class_718 = var_1269;
         var _loc2_:class_718 = var_1271;
         var _loc3_:class_725 = var_21;
         var _loc4_:class_893 = var_1007;
         var _loc5_:Number = 1.0e99;
         _loc4_.var_37 = _loc5_;
         _loc4_.var_291 = _loc5_;
         _loc5_ = -1.0e99;
         _loc4_.var_12 = _loc5_;
         _loc4_.var_93 = _loc5_;
         while(_loc1_ != null)
         {
            _loc2_.var_244 = Number(_loc3_.var_244 + (_loc1_.var_244 * _loc3_.var_673 - _loc1_.var_245 * _loc3_.var_674));
            _loc2_.var_245 = Number(_loc3_.var_245 + (Number(_loc1_.var_244 * _loc3_.var_674 + _loc1_.var_245 * _loc3_.var_673)));
            if(_loc2_.var_244 < _loc4_.var_291)
            {
               _loc4_.var_291 = _loc2_.var_244;
            }
            if(_loc2_.var_244 > _loc4_.var_93)
            {
               _loc4_.var_93 = _loc2_.var_244;
            }
            if(_loc2_.var_245 < _loc4_.var_37)
            {
               _loc4_.var_37 = _loc2_.var_245;
            }
            if(_loc2_.var_245 > _loc4_.var_12)
            {
               _loc4_.var_12 = _loc2_.var_245;
            }
            _loc1_ = _loc1_.name_1;
            _loc2_ = _loc2_.name_1;
         }
         var _loc6_:class_860 = var_1272;
         var _loc7_:class_860 = var_1273;
         while(_loc6_ != null)
         {
            _loc8_ = _loc6_.var_1264;
            _loc7_.var_1264.var_244 = _loc8_.var_244 * _loc3_.var_673 - _loc8_.var_245 * _loc3_.var_674;
            _loc7_.var_1264.var_245 = Number(_loc8_.var_244 * _loc3_.var_674 + _loc8_.var_245 * _loc3_.var_673);
            _loc7_.var_8 = Number(Number(_loc3_.var_244 * _loc7_.var_1264.var_244 + _loc3_.var_245 * _loc7_.var_1264.var_245) + _loc6_.var_8);
            _loc6_ = _loc6_.name_1;
            _loc7_ = _loc7_.name_1;
         }
      }
      
      public function method_963(param1:Array) : void
      {
         var _loc9_:int = 0;
         var _loc10_:class_718 = null;
         var _loc11_:class_718 = null;
         var _loc12_:class_718 = null;
         var _loc13_:class_718 = null;
         var _loc14_:class_718 = null;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:class_860 = null;
         var _loc20_:class_718 = null;
         var _loc21_:class_860 = null;
         var _loc22_:class_718 = null;
         var _loc2_:* = null;
         var _loc3_:* = null;
         var _loc4_:* = null;
         var _loc5_:* = null;
         var _loc6_:int = int(param1.length);
         var_1270 = _loc6_;
         var_692 = 0;
         var _loc7_:Boolean = var_1004 != null;
         var _loc8_:int = 0;
         while(_loc8_ < _loc6_)
         {
            _loc8_++;
            _loc9_ = _loc8_;
            _loc10_ = param1[_loc9_];
            _loc11_ = param1[int((_loc9_ + 1) % _loc6_)];
            _loc12_ = param1[int((_loc9_ + 2) % _loc6_)];
            var_692 = Number(var_692 + _loc11_.var_244 * (_loc10_.var_245 - _loc12_.var_245));
            if(_loc7_)
            {
               _loc14_ = var_1004;
               §§push(new class_718(Number(_loc10_.var_244 + _loc14_.var_244),Number(_loc10_.var_245 + _loc14_.var_245)));
            }
            else
            {
               §§push(_loc10_);
            }
            _loc13_ =; §§pop();
            _loc15_ = _loc11_.var_244 - _loc10_.var_244;
            _loc16_ = _loc11_.var_245 - _loc10_.var_245;
            _loc17_ = Number(Math.sqrt(Number(_loc15_ * _loc15_ + _loc16_ * _loc16_)));
            _loc18_ = _loc17_ < 1.0e-99?Number(0):1 / _loc17_;
            _loc14_ = new class_718(-_loc16_ * _loc18_,_loc15_ * _loc18_);
            _loc19_ = new class_860(_loc14_,Number(_loc14_.var_244 * _loc13_.var_244 + _loc14_.var_245 * _loc13_.var_245));
            _loc20_ = new class_718(_loc13_.var_244,_loc13_.var_245);
            _loc22_ = _loc19_.var_1264;
            _loc21_ = new class_860(new class_718(_loc22_.var_244,_loc22_.var_245),_loc19_.var_8);
            if(_loc9_ == 0)
            {
               var_1269 = _loc13_;
               var_1271 = _loc20_;
               var_1272 = _loc19_;
               var_1273 = _loc21_;
            }
            else
            {
               _loc2_.name_1 = _loc13_;
               _loc3_.name_1 = _loc20_;
               _loc4_.name_1 = _loc19_;
               _loc5_.name_1 = _loc21_;
            }
            _loc2_ = _loc13_;
            _loc3_ = _loc20_;
            _loc4_ = _loc19_;
            _loc5_ = _loc21_;
         }
         var_692 = var_692 * 0.5;
      }
      
      override public function method_435() : Number
      {
         var _loc7_:int = 0;
         var _loc8_:class_718 = null;
         var _loc9_:class_718 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc1_:Array = [];
         var _loc2_:class_718 = var_1269;
         while(_loc2_ != null)
         {
            _loc1_.push(new class_718(Number(_loc2_.var_244 + var_1004.var_244),Number(_loc2_.var_245 + var_1004.var_245)));
            _loc2_ = _loc2_.name_1;
         }
         var _loc3_:Number = 0;
         var _loc4_:Number = 0;
         var _loc5_:int = 0;
         var _loc6_:int = var_1270;
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc8_ = _loc1_[_loc7_];
            _loc9_ = _loc1_[int((_loc7_ + 1) % var_1270)];
            _loc10_ = _loc9_.var_244 * _loc8_.var_245 - _loc9_.var_245 * _loc8_.var_244;
            _loc11_ = Number(Number(Number(_loc8_.var_244 * _loc8_.var_244 + _loc8_.var_245 * _loc8_.var_245) + (Number(_loc8_.var_244 * _loc9_.var_244 + _loc8_.var_245 * _loc9_.var_245))) + (Number(_loc9_.var_244 * _loc9_.var_244 + _loc9_.var_245 * _loc9_.var_245)));
            _loc3_ = Number(_loc3_ + _loc10_ * _loc11_);
            _loc4_ = Number(_loc4_ + _loc10_);
         }
         return _loc3_ / (6 * _loc4_);
      }
   }
}
