package package_17
{
   import package_32.class_899;
   
   public class class_833
   {
       
      
      public var name_81:int;
      
      public var var_679:Boolean;
      
      public var var_73:class_800;
      
      public var var_14:class_800;
      
      public var var_616:Number;
      
      public var var_1040:Number;
      
      public var var_267:class_719;
      
      public var var_617:Number;
      
      public var var_774:class_939;
      
      public var var_1122:Number;
      
      public var var_1109:class_827;
      
      public function class_833(param1:class_827 = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_1109 = param1;
      }
      
      public function method_418(param1:Number) : void
      {
         var _loc7_:class_939 = null;
         var _loc8_:class_827 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:Number = NaN;
         var _loc2_:class_725 = var_14.var_21;
         var _loc3_:class_725 = var_73.var_21;
         var _loc4_:Number = Number(_loc2_.var_665 + _loc3_.var_665);
         var _loc5_:class_939 = var_774;
         var _loc6_:* = null;
         while(_loc5_ != null)
         {
            if(!_loc5_.var_775)
            {
               _loc7_ = _loc5_;
               _loc5_ = _loc5_.name_1;
               _loc8_ = var_1109;
               _loc7_.name_1 = _loc8_.var_1091;
               _loc8_.var_1091 = _loc7_;
               if(_loc6_ == null)
               {
                  var_774 = _loc5_;
               }
               else
               {
                  _loc6_.name_1 = _loc5_;
               }
            }
            else
            {
               _loc5_.var_775 = false;
               _loc5_.var_1110 = _loc5_.var_319 - _loc2_.var_244;
               _loc5_.var_1111 = _loc5_.var_318 - _loc2_.var_245;
               _loc5_.var_1112 = _loc5_.var_319 - _loc3_.var_244;
               _loc5_.var_1113 = _loc5_.var_318 - _loc3_.var_245;
               _loc5_.var_1114 = -_loc5_.var_1111;
               _loc5_.var_1115 = _loc5_.var_1110;
               _loc5_.var_1116 = -_loc5_.var_1113;
               _loc5_.var_1117 = _loc5_.var_1112;
               _loc9_ = _loc5_.var_1110 * _loc5_.var_1118 - _loc5_.var_1111 * _loc5_.var_1119;
               _loc10_ = _loc5_.var_1112 * _loc5_.var_1118 - _loc5_.var_1113 * _loc5_.var_1119;
               _loc11_ = Number(Number(_loc4_ + _loc2_.var_667 * _loc9_ * _loc9_) + _loc3_.var_667 * _loc10_ * _loc10_);
               _loc5_.var_1120 = 1 / _loc11_;
               _loc12_ = -_loc5_.var_1118;
               _loc13_ = _loc5_.var_1119;
               _loc14_ = _loc5_.var_1110 * _loc13_ - _loc5_.var_1111 * _loc12_;
               _loc15_ = _loc5_.var_1112 * _loc13_ - _loc5_.var_1113 * _loc12_;
               _loc16_ = Number(Number(_loc4_ + _loc2_.var_667 * _loc14_ * _loc14_) + _loc3_.var_667 * _loc15_ * _loc15_);
               _loc5_.var_1121 = 1 / _loc16_;
               _loc5_.var_1122 = -var_1122 * (_loc5_.var_522 + var_1040);
               _loc5_.var_1123 = 0;
               _loc17_ = Number(_loc5_.var_1116 * _loc3_.var_4 + _loc3_.var_86.var_244) - (Number(_loc5_.var_1114 * _loc2_.var_4 + _loc2_.var_86.var_244));
               _loc18_ = Number(_loc5_.var_1117 * _loc3_.var_4 + _loc3_.var_86.var_245) - (Number(_loc5_.var_1115 * _loc2_.var_4 + _loc2_.var_86.var_245));
               _loc5_.var_436 = (_loc5_.var_1119 * _loc17_ + _loc5_.var_1118 * _loc18_) * var_616 * param1;
               _loc19_ = Number(_loc5_.var_1119 * _loc5_.var_1124 + _loc12_ * _loc5_.var_1125);
               _loc20_ = Number(_loc5_.var_1118 * _loc5_.var_1124 + _loc13_ * _loc5_.var_1125);
               _loc2_.var_86.var_244 = _loc2_.var_86.var_244 - _loc19_ * _loc2_.var_665;
               _loc2_.var_86.var_245 = _loc2_.var_86.var_245 - _loc20_ * _loc2_.var_665;
               _loc2_.var_4 = _loc2_.var_4 - _loc2_.var_667 * (_loc5_.var_1110 * _loc20_ - _loc5_.var_1111 * _loc19_);
               _loc3_.var_86.var_244 = Number(_loc3_.var_86.var_244 + _loc19_ * _loc3_.var_665);
               _loc3_.var_86.var_245 = Number(_loc3_.var_86.var_245 + _loc20_ * _loc3_.var_665);
               _loc3_.var_4 = Number(_loc3_.var_4 + _loc3_.var_667 * (_loc5_.var_1112 * _loc20_ - _loc5_.var_1113 * _loc19_));
               _loc6_ = _loc5_;
               _loc5_ = _loc5_.name_1;
            }
         }
      }
      
      public function method_814(param1:class_718, param2:class_718, param3:Number, param4:Number, param5:int) : void
      {
         var _loc7_:class_827 = null;
         var _loc8_:class_939 = null;
         var _loc9_:Number = NaN;
         var _loc6_:class_939 = var_774;
         while(_loc6_ != null)
         {
            if(param5 == _loc6_.var_1126)
            {
               break;
            }
            _loc6_ = _loc6_.name_1;
         }
         if(_loc6_ == null)
         {
            _loc7_ = var_1109;
            _loc8_ = _loc7_.var_1091;
            if(_loc8_ == null)
            {
               §§push(new class_939());
            }
            else
            {
               _loc7_.var_1091 = _loc8_.name_1;
               §§push(_loc8_);
            }
            _loc6_ =; §§pop();
            _loc6_.var_1126 = param5;
            _loc9_ = 0;
            _loc6_.var_1125 = _loc9_;
            _loc6_.var_1124 = _loc9_;
            _loc6_.name_1 = var_774;
            var_774 = _loc6_;
         }
         _loc6_.var_319 = param1.var_244;
         _loc6_.var_318 = param1.var_245;
         _loc6_.var_1119 = param2.var_244 * param3;
         _loc6_.var_1118 = param2.var_245 * param3;
         _loc6_.var_522 = param4;
         _loc6_.var_775 = true;
      }
      
      public function method_815(param1:class_939, param2:class_725, param3:class_725, param4:Number, param5:Number) : void
      {
         param2.var_86.var_244 = param2.var_86.var_244 - param4 * param2.var_665;
         param2.var_86.var_245 = param2.var_86.var_245 - param5 * param2.var_665;
         param2.var_4 = param2.var_4 - param2.var_667 * (param1.var_1110 * param5 - param1.var_1111 * param4);
         param3.var_86.var_244 = Number(param3.var_86.var_244 + param4 * param3.var_665);
         param3.var_86.var_245 = Number(param3.var_86.var_245 + param5 * param3.var_665);
         param3.var_4 = Number(param3.var_4 + param3.var_667 * (param1.var_1112 * param5 - param1.var_1113 * param4));
      }
      
      public function method_816(param1:class_800, param2:class_800) : void
      {
         var_14 = param1;
         var_73 = param2;
         var _loc3_:class_709 = param1.var_538;
         var _loc4_:class_709 = param2.var_538;
         var _loc5_:class_809 = param1.var_21.var_663;
         var _loc6_:class_809 = param2.var_21.var_663;
         var_616 = _loc3_.var_616 > _loc4_.var_616?_loc3_.var_616:_loc4_.var_616;
         var_617 = Number(Math.sqrt(_loc3_.var_617 * _loc4_.var_617));
         var_1122 = _loc5_.var_779 > _loc6_.var_779?_loc5_.var_779:_loc6_.var_779;
         var_1040 = _loc5_.var_1040 > _loc6_.var_1040?_loc6_.var_1040:_loc5_.var_1040;
      }
      
      public function method_419() : void
      {
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc1_:class_725 = var_14.var_21;
         var _loc2_:class_725 = var_73.var_21;
         var _loc3_:class_939 = var_774;
         while(_loc3_ != null)
         {
            _loc4_ = Number((Number(_loc3_.var_1116 * _loc2_.var_672 + _loc2_.var_671.var_244) - (Number(_loc3_.var_1114 * _loc1_.var_672 + _loc1_.var_671.var_244))) * _loc3_.var_1119 + (Number(_loc3_.var_1117 * _loc2_.var_672 + _loc2_.var_671.var_245) - (Number(_loc3_.var_1115 * _loc1_.var_672 + _loc1_.var_671.var_245))) * _loc3_.var_1118);
            _loc5_ = (_loc3_.var_1122 - _loc4_) * _loc3_.var_1120;
            _loc6_ = _loc3_.var_1123;
            _loc3_.var_1123 = Number(_loc6_ + _loc5_);
            if(_loc3_.var_1123 < 0)
            {
               _loc3_.var_1123 = 0;
            }
            _loc5_ = _loc3_.var_1123 - _loc6_;
            _loc7_ = _loc3_.var_1119 * _loc5_;
            _loc8_ = _loc3_.var_1118 * _loc5_;
            _loc1_.var_671.var_244 = _loc1_.var_671.var_244 - _loc7_ * _loc1_.var_665;
            _loc1_.var_671.var_245 = _loc1_.var_671.var_245 - _loc8_ * _loc1_.var_665;
            _loc1_.var_672 = _loc1_.var_672 - _loc1_.var_667 * (_loc3_.var_1110 * _loc8_ - _loc3_.var_1111 * _loc7_);
            _loc2_.var_671.var_244 = Number(_loc2_.var_671.var_244 + _loc7_ * _loc2_.var_665);
            _loc2_.var_671.var_245 = Number(_loc2_.var_671.var_245 + _loc8_ * _loc2_.var_665);
            _loc2_.var_672 = Number(_loc2_.var_672 + _loc2_.var_667 * (_loc3_.var_1112 * _loc8_ - _loc3_.var_1113 * _loc7_));
            _loc9_ = Number(_loc3_.var_1116 * _loc2_.var_4 + _loc2_.var_86.var_244) - (Number(_loc3_.var_1114 * _loc1_.var_4 + _loc1_.var_86.var_244));
            _loc10_ = Number(_loc3_.var_1117 * _loc2_.var_4 + _loc2_.var_86.var_245) - (Number(_loc3_.var_1115 * _loc1_.var_4 + _loc1_.var_86.var_245));
            _loc11_ = (_loc3_.var_436 + (Number(_loc9_ * _loc3_.var_1119 + _loc10_ * _loc3_.var_1118))) * _loc3_.var_1120;
            _loc12_ = _loc3_.var_1124;
            _loc3_.var_1124 = _loc12_ - _loc11_;
            if(_loc3_.var_1124 < 0)
            {
               _loc3_.var_1124 = 0;
            }
            _loc11_ = _loc3_.var_1124 - _loc12_;
            _loc13_ = _loc3_.var_1119 * _loc10_ - _loc3_.var_1118 * _loc9_;
            _loc14_ = var_617 * _loc3_.var_1124;
            _loc15_ = _loc13_ * _loc3_.var_1121;
            _loc16_ = _loc3_.var_1125;
            _loc3_.var_1125 = _loc16_ - _loc15_;
            if(_loc3_.var_1125 < -_loc14_)
            {
               _loc3_.var_1125 = -_loc14_;
            }
            else if(_loc3_.var_1125 > _loc14_)
            {
               _loc3_.var_1125 = _loc14_;
            }
            _loc15_ = _loc3_.var_1125 - _loc16_;
            _loc17_ = _loc3_.var_1119 * _loc11_ - _loc3_.var_1118 * _loc15_;
            _loc18_ = Number(_loc3_.var_1118 * _loc11_ + _loc3_.var_1119 * _loc15_);
            _loc1_.var_86.var_244 = _loc1_.var_86.var_244 - _loc17_ * _loc1_.var_665;
            _loc1_.var_86.var_245 = _loc1_.var_86.var_245 - _loc18_ * _loc1_.var_665;
            _loc1_.var_4 = _loc1_.var_4 - _loc1_.var_667 * (_loc3_.var_1110 * _loc18_ - _loc3_.var_1111 * _loc17_);
            _loc2_.var_86.var_244 = Number(_loc2_.var_86.var_244 + _loc17_ * _loc2_.var_665);
            _loc2_.var_86.var_245 = Number(_loc2_.var_86.var_245 + _loc18_ * _loc2_.var_665);
            _loc2_.var_4 = Number(_loc2_.var_4 + _loc2_.var_667 * (_loc3_.var_1112 * _loc18_ - _loc3_.var_1113 * _loc17_));
            _loc3_ = _loc3_.name_1;
         }
      }
   }
}
