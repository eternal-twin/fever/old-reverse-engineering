package package_17
{
   public class class_904
   {
       
      
      public function class_904()
      {
      }
      
      public function method_1057(param1:class_800, param2:class_800, param3:class_833) : Boolean
      {
         var _loc4_:* = null;
         var _loc5_:* = null;
         if(param1.var_252 == 2 && param2.var_252 == 2)
         {
            §§push(Boolean(method_1034(param1.var_1003,param2.var_1003,param3)));
         }
         else if(param1.var_252 == 0)
         {
            if(param2.var_252 == 2)
            {
               §§push(Boolean(method_1035(param1.var_1006,param2.var_1003,param3)));
            }
            else if(param2.var_252 == 0)
            {
               _loc4_ = param1.var_1006;
               _loc5_ = param2.var_1006;
               §§push(Boolean(method_1036(param3,_loc4_.var_1315,_loc5_.var_1315,Number(_loc4_.var_93),Number(_loc5_.var_93))));
            }
            else
            {
               §§push(Boolean(method_1037(param1.var_1006,param2.var_1002,param3)));
            }
         }
         else
         {
            §§push(param1.var_252 == 1 && param2.var_252 == 2?Boolean(method_1038(param1.var_1002,param2.var_1003,param3)):false);
         }
         return; §§pop();
      }
      
      public function method_1059(param1:class_800, param2:class_718) : Boolean
      {
         var _loc3_:class_875 = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         switch(param1.var_252)
         {
            case 0:
               _loc3_ = param1.var_1006;
               _loc4_ = _loc3_.var_1315.var_244 - param2.var_244;
               _loc5_ = _loc3_.var_1315.var_245 - param2.var_245;
               §§push(Number(_loc4_ * _loc4_ + _loc5_ * _loc5_) <= _loc3_.var_93 * _loc3_.var_93);
               break;
            default:
               §§push(false);
               break;
            case 2:
               §§push(Boolean(method_1058(param1.var_1003,param2)));
         }
         return; §§pop();
      }
      
      public function method_1038(param1:class_865, param2:class_863, param3:class_833) : Boolean
      {
         var _loc5_:class_718 = null;
         var _loc6_:class_718 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         _loc5_ = param1.var_1280;
         _loc6_ = param1.var_1278;
         var _loc4_:Number = Number(_loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245);
         _loc5_ = param1.var_1280;
         _loc6_ = param2.var_1271;
         var _loc8_:Number = 1.0e99;
         while(_loc6_ != null)
         {
            _loc9_ = Number(_loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245);
            if(_loc9_ < _loc8_)
            {
               _loc8_ = _loc9_;
            }
            _loc6_ = _loc6_.name_1;
         }
         var _loc7_:Number = _loc8_ - _loc4_ - param1.var_93;
         _loc5_ = param1.var_1281;
         _loc6_ = param2.var_1271;
         _loc9_ = 1.0e99;
         while(_loc6_ != null)
         {
            _loc10_ = Number(_loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245);
            if(_loc10_ < _loc9_)
            {
               _loc9_ = _loc10_;
            }
            _loc6_ = _loc6_.name_1;
         }
         _loc8_ = _loc9_ - -_loc4_ - param1.var_93;
         if(_loc8_ > 0 || _loc7_ > 0)
         {
            return false;
         }
         var _loc11_:class_860 = param2.var_1273;
         _loc9_ = -1.0e99;
         var _loc12_:class_860 = null;
         while(_loc11_ != null)
         {
            _loc5_ = _loc11_.var_1264;
            _loc13_ = _loc11_.var_8;
            _loc6_ = param1.var_1278;
            _loc14_ = _loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245 - param1.var_93;
            _loc6_ = param1.var_1279;
            _loc15_ = _loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245 - param1.var_93;
            _loc10_ = _loc14_ < _loc15_?_loc14_ - _loc13_:_loc15_ - _loc13_;
            if(_loc10_ > 0)
            {
               return false;
            }
            if(_loc10_ > _loc9_)
            {
               _loc9_ = _loc10_;
               _loc12_ = _loc11_;
            }
            _loc11_ = _loc11_.name_1;
         }
         _loc5_ = _loc12_.var_1264;
         _loc6_ = new class_718(param1.var_1278.var_244 - _loc5_.var_244 * param1.var_93,param1.var_1278.var_245 - _loc5_.var_245 * param1.var_93);
         var _loc16_:class_718 = new class_718(param1.var_1279.var_244 - _loc5_.var_244 * param1.var_93,param1.var_1279.var_245 - _loc5_.var_245 * param1.var_93);
         if(method_1058(param2,_loc6_))
         {
            param3.method_814(_loc6_,_loc5_,-1,_loc9_,0);
         }
         if(method_1058(param2,_loc16_))
         {
            param3.method_814(_loc16_,_loc5_,-1,_loc9_,1);
         }
         if(_loc7_ >= _loc9_ || _loc8_ >= _loc9_)
         {
            if(_loc7_ > _loc8_)
            {
               method_1060(param1,param2,_loc7_,1,param3);
            }
            else
            {
               method_1060(param1,param2,_loc8_,-1,param3);
            }
         }
         return true;
      }
      
      public function method_1061(param1:class_865, param2:class_718, param3:Number) : Number
      {
         var _loc5_:class_718 = param1.var_1278;
         var _loc4_:Number = param2.var_244 * _loc5_.var_244 + param2.var_245 * _loc5_.var_245 - param1.var_93;
         _loc5_ = param1.var_1279;
         var _loc6_:Number = param2.var_244 * _loc5_.var_244 + param2.var_245 * _loc5_.var_245 - param1.var_93;
         return _loc4_ < _loc6_?_loc4_ - param3:_loc6_ - param3;
      }
      
      public function method_1058(param1:class_863, param2:class_718) : Boolean
      {
         var _loc4_:class_718 = null;
         var _loc3_:class_860 = param1.var_1273;
         while(_loc3_ != null)
         {
            _loc4_ = _loc3_.var_1264;
            if(Number(_loc4_.var_244 * param2.var_244 + _loc4_.var_245 * param2.var_245) > _loc3_.var_8)
            {
               return false;
            }
            _loc3_ = _loc3_.name_1;
         }
         return true;
      }
      
      public function method_1062(param1:class_863, param2:class_718, param3:Number) : Number
      {
         var _loc6_:Number = NaN;
         var _loc4_:class_718 = param1.var_1271;
         var _loc5_:Number = 1.0e99;
         while(_loc4_ != null)
         {
            _loc6_ = Number(param2.var_244 * _loc4_.var_244 + param2.var_245 * _loc4_.var_245);
            if(_loc6_ < _loc5_)
            {
               _loc5_ = _loc6_;
            }
            _loc4_ = _loc4_.name_1;
         }
         return _loc5_ - param3;
      }
      
      public function method_1034(param1:class_863, param2:class_863, param3:class_833) : Boolean
      {
         var _loc7_:Number = NaN;
         var _loc8_:class_718 = null;
         var _loc9_:class_718 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc4_:Number = -1.0e99;
         var _loc5_:class_860 = null;
         var _loc6_:class_860 = param1.var_1273;
         while(_loc6_ != null)
         {
            _loc8_ = _loc6_.var_1264;
            _loc9_ = param2.var_1271;
            _loc10_ = 1.0e99;
            while(_loc9_ != null)
            {
               _loc11_ = Number(_loc8_.var_244 * _loc9_.var_244 + _loc8_.var_245 * _loc9_.var_245);
               if(_loc11_ < _loc10_)
               {
                  _loc10_ = _loc11_;
               }
               _loc9_ = _loc9_.name_1;
            }
            _loc7_ = _loc10_ - _loc6_.var_8;
            if(_loc7_ > 0)
            {
               return false;
            }
            if(_loc7_ > _loc4_)
            {
               _loc4_ = _loc7_;
               _loc5_ = _loc6_;
            }
            _loc6_ = _loc6_.name_1;
         }
         _loc7_ = -1.0e99;
         var _loc12_:class_860 = null;
         _loc6_ = param2.var_1273;
         while(_loc6_ != null)
         {
            _loc8_ = _loc6_.var_1264;
            _loc9_ = param1.var_1271;
            _loc11_ = 1.0e99;
            while(_loc9_ != null)
            {
               _loc13_ = Number(_loc8_.var_244 * _loc9_.var_244 + _loc8_.var_245 * _loc9_.var_245);
               if(_loc13_ < _loc11_)
               {
                  _loc11_ = _loc13_;
               }
               _loc9_ = _loc9_.name_1;
            }
            _loc10_ = _loc11_ - _loc6_.var_8;
            if(_loc10_ > 0)
            {
               return false;
            }
            if(_loc10_ > _loc7_)
            {
               _loc7_ = _loc10_;
               _loc12_ = _loc6_;
            }
            _loc6_ = _loc6_.name_1;
         }
         if(_loc4_ > _loc7_)
         {
            method_1063(param3,param1,param2,_loc5_,1,_loc4_);
         }
         else
         {
            method_1063(param3,param1,param2,_loc12_,-1,_loc7_);
         }
         return true;
      }
      
      public function method_1063(param1:class_833, param2:class_863, param3:class_863, param4:class_860, param5:Number, param6:Number) : void
      {
         var _loc7_:int = param2.var_215 > param3.var_215?0:65000;
         var _loc8_:int = 0;
         var _loc9_:class_718 = param2.var_1271;
         while(_loc9_ != null)
         {
            if(method_1058(param3,_loc9_))
            {
               param1.method_814(_loc9_,param4.var_1264,param5,param6,_loc7_);
               _loc8_++;
               if(_loc8_ > 1)
               {
                  return;
               }
            }
            _loc7_++;
            _loc9_ = _loc9_.name_1;
         }
         _loc7_ = param2.var_215 > param3.var_215?65000:0;
         _loc9_ = param3.var_1271;
         while(_loc9_ != null)
         {
            if(method_1058(param2,_loc9_))
            {
               param1.method_814(_loc9_,param4.var_1264,param5,param6,_loc7_);
               _loc8_++;
               if(_loc8_ > 1)
               {
                  return;
               }
            }
            _loc7_++;
            _loc9_ = _loc9_.name_1;
         }
      }
      
      public function method_1060(param1:class_865, param2:class_863, param3:Number, param4:Number, param5:class_833) : void
      {
         var _loc11_:class_718 = null;
         var _loc13_:Number = NaN;
         var _loc7_:class_718 = param1.var_1280;
         var _loc8_:class_718 = param1.var_1278;
         var _loc6_:Number = _loc7_.var_244 * _loc8_.var_245 - _loc7_.var_245 * _loc8_.var_244;
         _loc7_ = param1.var_1280;
         _loc8_ = param1.var_1279;
         var _loc9_:Number = _loc7_.var_244 * _loc8_.var_245 - _loc7_.var_245 * _loc8_.var_244;
         _loc7_ = new class_718(param1.var_1280.var_244 * param4,param1.var_1280.var_245 * param4);
         _loc8_ = param1.var_1280;
         _loc11_ = param1.var_1278;
         var _loc10_:Number = (_loc8_.var_244 * _loc11_.var_244 + _loc8_.var_245 * _loc11_.var_245) * param4;
         _loc8_ = param2.var_1271;
         var _loc12_:int = 2;
         while(_loc8_ != null)
         {
            if(Number(_loc8_.var_244 * _loc7_.var_244 + _loc8_.var_245 * _loc7_.var_245) < Number(_loc10_ + param1.var_93))
            {
               _loc11_ = param1.var_1280;
               _loc13_ = _loc11_.var_244 * _loc8_.var_245 - _loc11_.var_245 * _loc8_.var_244;
               if(_loc6_ >= _loc13_ && _loc13_ >= _loc9_)
               {
                  param5.method_814(_loc8_,_loc7_,1,param3,_loc12_);
               }
            }
            _loc12_++;
            _loc8_ = _loc8_.name_1;
         }
      }
      
      public function method_1037(param1:class_875, param2:class_865, param3:class_833) : Boolean
      {
         var _loc5_:class_718 = null;
         var _loc6_:class_718 = null;
         var _loc11_:Number = NaN;
         _loc5_ = param2.var_1280;
         _loc6_ = param1.var_1315;
         _loc5_ = param2.var_1278;
         _loc6_ = param2.var_1280;
         var _loc4_:Number = Number(_loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245) - (Number(_loc5_.var_244 * _loc6_.var_244 + _loc5_.var_245 * _loc6_.var_245));
         var _loc7_:Number = (_loc4_ < 0?-_loc4_:_loc4_) - param1.var_93 - param2.var_93;
         if(_loc7_ > 0)
         {
            return false;
         }
         _loc5_ = param2.var_1280;
         _loc6_ = param1.var_1315;
         var _loc8_:Number = -(_loc5_.var_244 * _loc6_.var_245 - _loc5_.var_245 * _loc6_.var_244);
         _loc5_ = param2.var_1280;
         _loc6_ = param2.var_1278;
         var _loc9_:Number = -(_loc5_.var_244 * _loc6_.var_245 - _loc5_.var_245 * _loc6_.var_244);
         _loc5_ = param2.var_1280;
         _loc6_ = param2.var_1279;
         var _loc10_:Number = -(_loc5_.var_244 * _loc6_.var_245 - _loc5_.var_245 * _loc6_.var_244);
         if(_loc8_ < _loc9_)
         {
            if(_loc8_ < _loc9_ - param1.var_93)
            {
               return false;
            }
            return Boolean(method_1036(param3,param1.var_1315,param2.var_1278,param1.var_93,param2.var_93));
         }
         if(_loc8_ < _loc10_)
         {
            if(_loc4_ < 0)
            {
               §§push(param2.var_1280);
            }
            else
            {
               _loc6_ = param2.var_1280;
               §§push(new class_718(_loc6_.var_244 * -1,_loc6_.var_245 * -1));
            }
            _loc5_ =; §§pop();
            _loc11_ = Number(param1.var_93 + _loc7_ * 0.5);
            param3.method_814(new class_718(Number(param1.var_1315.var_244 + _loc5_.var_244 * _loc11_),Number(param1.var_1315.var_245 + _loc5_.var_245 * _loc11_)),_loc5_,1,_loc7_,0);
            return true;
         }
         if(_loc8_ < Number(_loc10_ + param1.var_93))
         {
            return Boolean(method_1036(param3,param1.var_1315,param2.var_1279,param1.var_93,param2.var_93));
         }
         return false;
      }
      
      public function method_1035(param1:class_875, param2:class_863, param3:class_833) : Boolean
      {
         var _loc9_:Number = NaN;
         var _loc10_:class_718 = null;
         var _loc11_:class_718 = null;
         var _loc4_:class_860 = null;
         var _loc5_:class_718 = null;
         var _loc6_:class_860 = param2.var_1273;
         var _loc7_:class_718 = param2.var_1271;
         var _loc8_:Number = -1.0e99;
         while(_loc6_ != null)
         {
            _loc10_ = _loc6_.var_1264;
            _loc11_ = param1.var_1315;
            _loc9_ = _loc10_.var_244 * _loc11_.var_244 + _loc10_.var_245 * _loc11_.var_245 - _loc6_.var_8 - param1.var_93;
            if(_loc9_ > 0)
            {
               return false;
            }
            if(_loc9_ > _loc8_)
            {
               _loc8_ = _loc9_;
               _loc4_ = _loc6_;
               _loc5_ = _loc7_;
            }
            _loc6_ = _loc6_.name_1;
            _loc7_ = _loc7_.name_1;
         }
         _loc10_ = _loc4_.var_1264;
         _loc11_ = _loc5_.name_1 == null?param2.var_1271:_loc5_.name_1;
         var _loc12_:class_718 = param1.var_1315;
         _loc9_ = _loc10_.var_244 * _loc12_.var_245 - _loc10_.var_245 * _loc12_.var_244;
         if(_loc9_ < _loc10_.var_244 * _loc11_.var_245 - _loc10_.var_245 * _loc11_.var_244)
         {
            return Boolean(method_1036(param3,param1.var_1315,_loc11_,param1.var_93,0));
         }
         if(_loc9_ >= _loc10_.var_244 * _loc5_.var_245 - _loc10_.var_245 * _loc5_.var_244)
         {
            return Boolean(method_1036(param3,param1.var_1315,_loc5_,param1.var_93,0));
         }
         var _loc13_:Number = _loc10_.var_244 * (param1.var_93 + _loc8_ * 0.5);
         var _loc14_:Number = _loc10_.var_245 * (param1.var_93 + _loc8_ * 0.5);
         param3.method_814(new class_718(param1.var_1315.var_244 - _loc13_,param1.var_1315.var_245 - _loc14_),_loc10_,-1,_loc8_,0);
         return true;
      }
      
      public function method_1036(param1:class_833, param2:class_718, param3:class_718, param4:Number, param5:Number) : Boolean
      {
         var _loc6_:Number = Number(param4 + param5);
         var _loc7_:Number = param3.var_244 - param2.var_244;
         var _loc8_:Number = param3.var_245 - param2.var_245;
         var _loc9_:Number = Number(_loc7_ * _loc7_ + _loc8_ * _loc8_);
         if(_loc9_ >= _loc6_ * _loc6_)
         {
            return false;
         }
         var _loc10_:Number = Number(Math.sqrt(_loc9_));
         var _loc11_:Number = _loc10_ < 1.0e-99?Number(0):1 / _loc10_;
         var _loc12_:Number = Number(0.5 + (param4 - 0.5 * _loc6_) * _loc11_);
         param1.method_814(new class_718(Number(param2.var_244 + _loc7_ * _loc12_),Number(param2.var_245 + _loc8_ * _loc12_)),new class_718(_loc7_ * _loc11_,_loc8_ * _loc11_),1,_loc10_ - _loc6_,0);
         return true;
      }
      
      public function method_1064(param1:Object, param2:Object, param3:class_833) : Boolean
      {
         return Boolean(method_1036(param3,param1.var_1315,param2.var_1315,Number(param1.var_93),Number(param2.var_93)));
      }
   }
}
