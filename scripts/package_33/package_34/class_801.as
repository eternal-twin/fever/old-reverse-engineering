package package_33.package_34
{
   import flash.events.Event;
   import flash.events.EventDispatcher;
   import flash.net.LocalConnection;
   import package_10.class_769;
   import package_32.App;
   
   public class class_801
   {
       
      
      public function class_801()
      {
      }
      
      public static function method_95() : void
      {
         var _loc1_:class_769 = new class_769(1000);
         _loc1_.name_50 = class_801.name_50;
      }
      
      public static function name_50() : void
      {
         try
         {
            new LocalConnection().connect("foo");
            new LocalConnection().connect("foo");

         }
         catch(_loc2_:*)
         {

         }
      }
      
      public static function name_95(param1:Function, param2:Boolean) : Boolean
      {
         var name_94:Function = param1;
         var var_1009:Boolean = param2;
         var _loc4_:EventDispatcher = null;
         try
         {
            _loc4_ = Reflect.field(App.rootMc.loaderInfo,"uncaughtErrorEvents");
         }
         catch(_loc5_:*)
         {
         }
         if(_loc4_ == null)
         {
            return false;
         }
         _loc4_.addEventListener("uncaughtError",function(param1:Event):void
         {
            if(!var_1009)
            {
               param1.preventDefault();
            }
            name_94(class_691.method_97(Reflect.field(param1,"error")));
         });
         return true;
      }
   }
}
