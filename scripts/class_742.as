package
{
   import flash.display.MovieClip;
   import flash.text.TextField;
   import flash.text.TextFormat;
   import flash.text.TextFormatAlign;
   import package_10.class_741;
   import package_11.package_12.class_663;
   
   public class class_742 implements class_741
   {
      
      public static var var_216:int = 400;
      
      public static var var_218:int = 400;
      
      public static var var_323:int = 436;
      
      public static var var_217:int = 240;
      
      public static var var_219:int = 240;
      
      public static var var_264:Array = [[1,0],[0,1],[-1,0],[0,-1]];
      
      public static var var_747:Array = [[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]];
      
      public static var var_748:Array = [[1,1],[-1,1],[-1,-1],[1,-1]];
       
      
      public function class_742()
      {
      }
      
      public static function method_379(param1:int) : Array
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc8_:Number = NaN;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         while(_loc4_ < param1)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = int(class_691.method_114(100));
            _loc2_[_loc5_] = _loc6_;
            _loc3_ = _loc3_ + _loc6_;
         }
         _loc4_ = 0;
         while(_loc4_ < param1)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc8_ = Number(_loc2_[_loc5_]);
            _loc2_[_loc5_] = _loc8_ / _loc3_;
         }
         return _loc2_;
      }
      
      public static function method_109(param1:MovieClip, param2:MovieClip) : int
      {
         if(param1.y < param2.y)
         {
            return -1;
         }
         return 1;
      }
      
      public static function method_308(param1:int = 16777215, param2:int = 10, param3:int = 0, param4:String = undefined) : TextField
      {
         if(param4 == null)
         {
            param4 = "04b03";
         }
         var _loc5_:TextField = new TextField();
         _loc5_.selectable = false;
         _loc5_.embedFonts = true;
         var _loc6_:TextFormat = _loc5_.getTextFormat();
         _loc6_.color = param1;
         _loc6_.font = param4;
         _loc6_.size = param2;
         _loc6_.align = [TextFormatAlign.LEFT,TextFormatAlign.CENTER,TextFormatAlign.RIGHT][param3 + 1];
         _loc5_.defaultTextFormat = _loc6_;
         return _loc5_;
      }
      
      public static function method_199(param1:int, param2:int, param3:int, param4:int) : int
      {
         var _loc10_:Array = null;
         var _loc5_:int = param3 - param1;
         var _loc6_:int = param4 - param2;
         _loc5_ = int(Number(class_663.method_112(_loc5_,class_911.var_214.var_307 >> 1)));
         _loc6_ = int(Number(class_663.method_112(_loc6_,class_911.var_214.var_307 >> 1)));
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:Array = class_742.var_264;
         while(_loc8_ < int(_loc9_.length))
         {
            _loc10_ = _loc9_[_loc8_];
            _loc8_++;
            if(_loc5_ == int(_loc10_[0]) && _loc6_ == int(_loc10_[1]))
            {
               return _loc7_;
            }
            _loc7_++;
         }
         return -1;
      }
      
      public static function method_489(param1:Number) : int
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc2_:int = 0;
         while(_loc2_ < 4)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = Number(class_663.method_112(param1 - _loc3_ * 1.57,3.14));
            if(Number(Math.abs(_loc4_)) < 0.785)
            {
               return _loc3_;
            }
         }
         return -1;
      }
   }
}
