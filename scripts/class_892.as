package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public dynamic class class_892 extends class_656
   {
       
      
      public var var_250:Object;
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_817:Object;
      
      public var var_580:Number;
      
      public var var_251:Object;
      
      public var name_20:Object;
      
      public var var_688:Object;
      
      public var var_689:Function;
      
      public var var_233:Object;
      
      public var var_687:Number;
      
      public var var_272:int;
      
      public var var_273:Number;
      
      public var var_1377:Function;
      
      public var var_1376:Number;
      
      public var var_1012:Number;
      
      public function class_892(param1:MovieClip = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(param1);
         var_278 = 0;
         var_279 = 0;
         var_273 = 10;
         var_1012 = 100;
         var_580 = 0;
         var_687 = 1;
         var_1376 = 0.5;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Number = NaN;
         if(name_20 != null)
         {
            name_20 = name_20 - 1;
            if(name_20 < 0)
            {
               name_20 = null;
               var_243.visible = true;
               var_243.play();
            }
            return;
         }
         if(var_250 != null)
         {
            var_279 = Number(var_279 + var_250);
         }
         if(var_233 != null)
         {
            var_278 = var_278 * var_233;
            var_279 = var_279 * var_233;
         }
         var_244 = Number(var_244 + var_278);
         var_245 = Number(var_245 + var_279);
         var_243.rotation = Number(var_243.rotation + var_580);
         var_580 = var_580 * var_687;
         if(var_817 != null)
         {
            var_243.scaleX = var_243.scaleX * var_817;
            var_243.scaleY = var_243.scaleX;
         }
         if(var_688 != null)
         {
            if(var_245 > var_688)
            {
               var_245 = var_688;
               var_279 = var_279 * -var_1376;
               if(var_689 != null)
               {
                  var_689();
               }
            }
         }
         if(var_251 != null)
         {
            var_251 = var_251 - 1;
            if(var_251 < var_273)
            {
               _loc1_ = var_251 / var_273;
               _loc2_ = var_272;
               if(_loc2_ != -1)
               {
                  if(_loc2_ == 0)
                  {
                     var_243.scaleX = _loc1_ * var_344;
                     var_243.scaleY = _loc1_ * var_344;
                  }
                  else if(_loc2_ == 1)
                  {
                     _loc3_ = var_251;
                     var_243.visible = int(int(_loc3_) % 4) > 1;
                  }
                  else if(_loc2_ == 2)
                  {
                     var_243.play();
                  }
                  else if(_loc2_ == 3)
                  {
                     var_243.scaleY = _loc1_ * var_344 * 0.01;
                  }
                  else if(_loc2_ == 4)
                  {
                     _loc3_ = (1 - _loc1_) * 16;
                     class_658.method_247(var_243,_loc3_,0);
                     var_243.alpha = _loc1_ * var_1012;
                  }
                  else if(_loc2_ == 5)
                  {
                     var_243.scaleX = _loc1_ * var_344 * 0.01;
                  }
                  else
                  {
                     var_243.alpha = _loc1_ * var_1012;
                  }
               }
               if(var_251 <= 0)
               {
                  if(var_1377 != null)
                  {
                     var_1377();
                  }
                  method_89();
               }
            }
         }
         super.method_79();
      }
      
      public function method_358(param1:Object, param2:Number, param3:Object = undefined) : void
      {
         if(param3 == null)
         {
            param3 = 1 / 0;
         }
         var _loc4_:Number = param1.var_244 - var_244;
         var _loc5_:Number = param1.var_245 - var_245;
         var_278 = Number(var_278 + Number(class_663.method_99(-param3,_loc4_ * param2,param3)));
         var_279 = Number(var_279 + Number(class_663.method_99(-param3,_loc5_ * param2,param3)));
      }
      
      public function method_685(param1:Number) : void
      {
         var_1012 = param1;
         var_243.alpha = var_1012;
      }
   }
}
