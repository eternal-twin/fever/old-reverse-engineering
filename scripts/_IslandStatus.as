package
{
   import package_32.class_899;
   
   public final class _IslandStatus
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["D\x01Sb","Hr\x16]\x02","G\x10Y\x0b"];
      
      public static var var_392:_IslandStatus;
      
      public static var var_393:_IslandStatus;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function _IslandStatus(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_272(param1:Array, param2:_Reward) : _IslandStatus
      {
         return new _IslandStatus("Hr\x16]\x02",1,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
