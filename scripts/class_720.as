package
{
   import flash.utils.Dictionary;
   import package_32.class_899;
   
   public class class_720
   {
       
      
      public var var_17:Dictionary;
      
      public function class_720()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_17 = new Dictionary();
      }
      
      public function toString() : String
      {
         var _loc4_:String = null;
         var _loc1_:StringBuf = new StringBuf();
         _loc1_.b = _loc1_.b + "{";
         var _loc2_:* = method_215();
         var _loc3_:* = _loc2_;
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            _loc1_.b = _loc1_.b + _loc4_;
            _loc1_.b = _loc1_.b + " => ";
            _loc1_.b = _loc1_.b + class_691.method_97(method_98(_loc4_));
            if(_loc2_.method_74())
            {
               _loc1_.b = _loc1_.b + ", ";
            }
         }
         _loc1_.b = _loc1_.b + "}";
         return _loc1_.b;
      }
      
      public function method_216(param1:String, param2:Object) : void
      {
         var_17["$" + param1] = param2;
      }
      
      public function method_116(param1:String) : Boolean
      {
         param1 = "$" + param1;
         if(!var_17.hasOwnProperty(param1))
         {
            return false;
         }
         delete var_17[param1];
         return true;
      }
      
      public function method_215() : Object
      {
         var _loc2_:int = 0;
         var _loc1_:Array = [];
         var _loc3_:* = var_17;
         while(§§hasnext(var_17,_loc2_);)
         {
            _loc1_.push((;§§nextname(_loc2_,_loc3_);).substr(1);)
         }
         return _loc1_.method_73();
      }
      
      public function method_73() : Object
      {
         var _loc2_:int = 0;
         var _loc1_:Array = [];
         var _loc3_:* = var_17;
         while(§§hasnext(var_17,_loc2_);)
         {
            _loc1_.push(;§§nextname(_loc2_,_loc3_);)
         }
         return {
            "\x07\x12\x01":var_17,
            "\x04I\x01":_loc1_.method_73(),
            "\'*+x\x03":function():*
            {
               return this.var_331.method_74();
            },
            "?\x05\x10;\x01":function():*
            {
               var _loc1_:* = this.var_331.name_1();
               return this.name_19[_loc1_];
            }
         };
      }
      
      public function method_98(param1:String) : Object
      {
         return var_17["$" + param1];
      }
      
      public function method_217(param1:String) : Boolean
      {
         return Boolean(var_17.hasOwnProperty("$" + param1));
      }
   }
}
