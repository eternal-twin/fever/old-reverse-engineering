package
{
   import flash.display.MovieClip;
   import flash.geom.Point;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_688 extends class_645
   {
       
      
      public var var_533:int;
      
      public var var_529:MovieClip;
      
      public var var_530:MovieClip;
      
      public var var_251:Number;
      
      public var var_534:class_755;
      
      public var var_531:MovieClip;
      
      public var var_528:Number;
      
      public var var_295:int;
      
      public var var_532:MovieClip;
      
      public var var_470:Array;
      
      public function class_688()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Point = null;
         var _loc7_:MovieClip = null;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_528 = Number(Math.min(Number(var_528 + 0.05),1));
               _loc1_ = var_529.mouseX - var_530.x;
               _loc2_ = var_529.mouseY - var_530.y;
               _loc3_ = 0.15;
               var_530.var_278 = Number(Number(var_530.var_278) + class_663.method_99(-_loc3_,_loc1_ * 0.0007,_loc3_) * var_528);
               var_530.var_279 = Number(Number(var_530.var_279) + class_663.method_99(-_loc3_,_loc2_ * 0.0007,_loc3_) * var_528);
               var_530.x = Number(var_530.x + Number(var_530.var_278));
               var_530.y = Number(var_530.y + Number(var_530.var_279));
               var_530.var_94.rotation = Math.random() * 360;
               _loc4_ = var_530.x;
               _loc5_ = var_530.y;
               _loc6_ = var_529.package_21.localToGlobal(new Point(_loc4_,_loc5_));
               if(!var_529.package_21.hitTestPoint(_loc6_.x,_loc6_.y,true))
               {
                  method_336();
               }
               _loc7_ = var_470[var_295];
               if(var_295 < 2)
               {
                  _loc8_ = _loc7_.x - var_530.x;
                  _loc9_ = _loc7_.y - var_530.y;
                  _loc10_ = Number(Math.sqrt(Number(_loc8_ * _loc8_ + _loc9_ * _loc9_)));
                  if(_loc10_ < 100)
                  {
                     var_295 = var_295 + 1;
                  }
               }
               else if(var_530.y < _loc7_.y)
               {
                  method_81(true,10);
               }
               var_531.x = Number(var_530.x + 4);
               var_531.y = Number(var_530.y + 4);
               break;
            case 2:
               _loc1_ = 0.95;
               var_530.var_278 = var_530.var_278 * _loc1_;
               var_530.var_279 = var_530.var_279 * _loc1_;
               var_530.scaleX = var_530.scaleX * _loc1_;
               var_530.scaleY = var_530.scaleX;
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(false,25);
                  break;
               }
         }
         _loc1_ = 0.98;
         var_530.var_278 = var_530.var_278 * _loc1_;
         var_530.var_279 = var_530.var_279 * _loc1_;
         var_530.x = Number(var_530.x + Number(var_530.var_278));
         var_530.y = Number(var_530.y + Number(var_530.var_279));
         var_529.x = class_742.var_217 * 0.5 - var_530.x;
         var_529.y = class_742.var_219 * 0.5 - var_530.y;
         var_532.x = (1000 + var_529.x * 0.5) % 40;
         var_532.y = (1000 + var_529.y * 0.5) % 40;
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - param1 * 100];
         var_533 = int(Math.round(param1 * 3));
         var_227 = [Number(var_227[0] + var_533 * 50)];
         var_295 = 0;
         var_528 = 0;
         super.method_95(param1);
         method_117();
         method_72();
      }
      
      public function method_336() : void
      {
         name_3 = 2;
         var_251 = 5;
         var _loc1_:Number = Number(3.14 + Number(Math.atan2(Number(var_530.var_279),Number(var_530.var_278))));
         var_530.rotation = _loc1_ / 0.0174;
         var_530.gotoAndPlay("fall");
         var_531.parent.removeChild(var_531);
         var_534.method_110(var_529.package_21);
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("o]c\x04\x01"),0);
         var_532 = var_232.method_84(class_899.__unprotect__("\x13\x04&x"),class_645.var_209);
         var_529 = var_232.method_92(class_645.var_209);
         var_529.x = class_742.var_217 * 0.5;
         var_529.y = class_742.var_219 * 0.5;
         var_534 = new class_755(var_529);
         var_529.package_21 = var_534.method_84(class_899.__unprotect__("\x0b=za\x02"),1);
         var_529.package_21.gotoAndStop(var_533 + 1);
         var_531 = var_534.method_84(class_899.__unprotect__("*\x0bq\x01\x01"),1);
         var_530 = var_534.method_84(class_899.__unprotect__("\tyo&"),1);
         var_530.var_278 = 0;
         var_530.var_279 = 0;
         var_470 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 3)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = Reflect.field(var_529.package_21,"$m" + _loc2_);
            _loc3_.visible = false;
            var_470.push(_loc3_);
         }
      }
   }
}
