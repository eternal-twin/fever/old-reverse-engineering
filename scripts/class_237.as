package
{
   import flash.display.MovieClip;
   
   public dynamic class class_237 extends MovieClip
   {
       
      
      public var var_12:MovieClip;
      
      public var var_88:MovieClip;
      
      public var var_89:MovieClip;
      
      public var var_86:MovieClip;
      
      public function class_237()
      {
         super();
         addFrameScript(30,method_41,54,method_25);
      }
      
      public function method_25() : *
      {
         parent.removeChild(this);
         stop();
      }
      
      public function method_41() : *
      {
         gotoAndPlay(1);
      }
   }
}
