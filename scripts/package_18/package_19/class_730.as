package package_18.package_19
{
   import package_11.package_12.class_657;
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_730 extends class_710
   {
       
      
      public var var_215:int;
      
      public var var_94:class_738;
      
      public function class_730(param1:class_719 = undefined, param2:class_826 = undefined, param3:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_215 = param3;
         var_252 = class_669.var_425;
         super(param1,param2);
         name_58 = true;
         var_94 = new class_738();
         var_94.method_128(class_702.package_13.method_98(null,"statue"),0.5,0.85);
         addChild(var_94);
      }
      
      override public function method_380() : Boolean
      {
         class_652.var_214.method_135(true);
         if(!class_652.var_214.method_182())
         {
            return true;
         }
         class_652.var_214.method_186(_PlayerAction._SavePos(var_215));
         new class_931(class_652.var_214.var_109);
         var _loc1_:int = int(class_657.method_237(var_215 / class_885.var_720));
         var _loc2_:String = class_808.method_466(class_808.var_721,class_808.package_21(class_808.var_722[var_215],class_657.method_244(_loc1_)));
         class_868.var_214.method_467(_loc2_,16777215);
         return true;
      }
      
      override public function method_382() : Boolean
      {
         return true;
      }
      
      override public function method_383() : int
      {
         return 0;
      }
   }
}
