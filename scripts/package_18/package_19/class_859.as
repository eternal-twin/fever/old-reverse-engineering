package package_18.package_19
{
   import package_11.package_12.class_658;
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_750;
   import package_32.class_899;
   import package_9.class_851;
   import package_9.class_912;
   
   public class class_859 extends class_710
   {
       
      
      public var var_309:_Reward;
      
      public var var_618:class_738;
      
      public function class_859(param1:class_719 = undefined, param2:class_826 = undefined, param3:_Reward = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_252 = class_669.var_427;
         super(param1,param2);
         var_309 = param3;
         var_618 = new class_738();
         var_618.y = -2;
         addChild(var_618);
         method_877();
      }
      
      public static function method_874(param1:_Reward) : class_750
      {
         var _loc4_:_Item = null;
         var _loc5_:_IslandBonus = null;
         var _loc6_:int = 0;
         var _loc7_:_GameBonus = null;
         var _loc2_:class_750 = null;
         var _loc3_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc4_ = _loc3_[0];
               _loc2_ = class_702.var_304.method_98(int(_loc4_.var_295),"items");
               break;
            case 1:
               _loc5_ = _loc3_[0];
               _loc2_ = class_702.var_304.method_98(int(_loc5_.var_295),"bonus_island");
               break;
            case 2:
               _loc2_ = class_702.var_304.method_98(2,"bonus_ground");
               break;
            case 3:
               _loc2_ = class_702.var_304.method_98(3,"bonus_ground");
               break;
            case 4:
               _loc6_ = _loc3_[0];
               _loc2_ = class_702.var_304.method_98(4,"bonus_ground");
               break;
            case 5:
               _loc2_ = class_702.package_13.method_98(0,"portal");
               break;
            case 6:
               _loc2_ = class_702.var_304.method_98(1,"bonus_ground");
               break;
            case 7:
               _loc2_ = class_702.var_304.method_98(0,"bonus_ground");
               break;
            case 8:
               _loc7_ = _loc3_[0];
               _loc2_ = class_702.var_304.method_98(int(_loc7_.var_295),"bonus_game");
         }
         return _loc2_;
      }
      
      public static function method_951(param1:_Reward, param2:int) : Boolean
      {
         var _loc4_:_Item = null;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:_Item = null;
         var _loc3_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc4_ = _loc3_[0];
               _loc5_ = [_Item.var_418,_Item.var_404,_Item.var_407,_Item.var_406];
               _loc6_ = 0;
               while(_loc6_ < int(_loc5_.length))
               {
                  _loc7_ = _loc5_[_loc6_];
                  _loc6_++;
                  if(_loc4_ == _loc7_)
                  {
                     return true;
                  }
               }
               return false;
            default:
               return false;
            case 2:
            default:
               return int(param2 % 4) == 0;
            case 4:
               _loc6_ = _loc3_[0];
               return int(_loc6_ % 8) == 0;
         }
      }
      
      public static function method_952(param1:_Reward) : Boolean
      {
         if(class_830.method_176(param1))
         {
            return true;
         }
         return false;
      }
      
      override public function method_380() : Boolean
      {
         var _loc2_:class_750 = null;
         if(!class_859.method_952(var_309))
         {
            return false;
         }
         if(!class_652.var_214.method_182())
         {
            class_652.var_214.method_135(true);
            return true;
         }
         var _loc1_:Boolean = false;
         if(class_830.method_176(var_309))
         {
            _loc1_ = int(class_765.var_214.package_31._inv._key) > 0;
            if(!_loc1_)
            {
               class_868.var_214.method_386(class_808.var_1027);
            }
         }
         if(class_859.method_951(var_309,var_289.var_215))
         {
            _loc1_ = Boolean(class_765.var_214.method_588(_Item.var_405));
            if(!_loc1_)
            {
               class_868.var_214.method_386(class_808.var_1028);
            }
         }
         if(!!_loc1_ && !method_953())
         {
            method_954();
            _loc2_ = class_859.method_874(_Reward.var_394);
            new class_851(this,_loc2_);
         }
         else
         {
            class_652.var_214.method_135(true);
         }
         return true;
      }
      
      public function method_877() : void
      {
         var _loc1_:class_750 = class_859.method_874(var_309);
         var_618.filters = [];
         if(class_859.method_951(var_309,var_289.var_215))
         {
            _loc1_ = class_702.package_13.method_98(Boolean(method_953())?3:2,"chest");
            name_58 = true;
         }
         else if(class_830.method_176(var_309))
         {
            _loc1_ = class_702.package_13.method_98(Boolean(method_953())?1:0,"chest");
            name_58 = true;
         }
         else
         {
            class_658.var_146(var_618,2,4,5570560);
         }
         var_618.method_128(_loc1_);
         var_618.visible = !method_953() || name_58;
      }
      
      override public function method_89() : void
      {
         super.method_89();
         var_618.visible = false;
      }
      
      public function method_953() : Boolean
      {
         var _loc3_:Array = null;
         var _loc4_:_Reward = null;
         var _loc1_:_IslandStatus = var_267.method_589();
         var _loc2_:Array = _loc1_.var_296;
         switch(int(_loc1_.var_295))
         {
            case 0:
               return false;
            case 1:
               _loc3_ = _loc2_[0];
               _loc4_ = _loc2_[1];
               return _loc4_ == null;
            case 2:
               return true;
         }
      }
      
      override public function method_382() : Boolean
      {
         return class_859.method_952(var_309) && !method_953();
      }
      
      override public function method_180() : void
      {
         if(!class_652.var_214.method_182() || Boolean(method_953()))
         {
            return;
         }
         method_954();
         new class_912(var_309);
         if(class_859.method_952(var_309))
         {
            method_877();
         }
         else
         {
            method_89();
         }
      }
      
      public function method_954() : void
      {
         class_652.var_214.method_186(_PlayerAction._Grab(var_289.var_215,var_309));
         var_289.method_793();
      }
      
      override public function method_383() : int
      {
         return 5;
      }
   }
}
