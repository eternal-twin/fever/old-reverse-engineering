package package_18.package_19
{
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_765;
   import package_13.class_777;
   import package_13.class_826;
   import package_24.class_656;
   import package_24.class_738;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_9.class_918;
   
   public class class_794 extends class_710
   {
      
      public static var var_968:Array = [2,6,2,9,1,12,16,6,16,9,16,12,9,2];
      
      public static var var_214:class_794;
       
      
      public var var_260:int;
      
      public function class_794(param1:class_719 = undefined, param2:class_826 = undefined)
      {
         var _loc6_:int = 0;
         var _loc7_:class_656 = null;
         if(class_899.var_239)
         {
            return;
         }
         class_794.var_214 = this;
         var_252 = class_669.var_425;
         super(param1,param2);
         name_58 = true;
         var _loc3_:class_738 = new class_738();
         _loc3_.y = _loc3_.y - 4;
         _loc3_.method_128(class_702.package_13.method_98(null,"portal"));
         addChild(_loc3_);
         var_260 = 0;
         var _loc4_:Array = class_765.var_214.method_595();
         var _loc5_:int = 0;
         while(_loc5_ < int(_loc4_.length))
         {
            _loc6_ = int(_loc4_[_loc5_]);
            _loc5_++;
            _loc7_ = new class_656();
            _loc7_.method_483(0,0);
            _loc7_.method_137(class_702.package_13.method_136("portal_stone"));
            addChild(_loc7_);
            _loc7_.x = _loc3_.x + int(class_794.var_968[_loc6_ * 2]) - 10;
            _loc7_.y = _loc3_.y + int(class_794.var_968[_loc6_ * 2 + 1]) - 8;
            _loc7_.var_198.method_140();
            var_260 = var_260 + 1;
         }
         _loc7_ = new class_656();
         _loc7_.method_137(class_702.package_13.method_136("portal_light"));
         _loc7_.x = _loc3_.x;
         _loc7_.y = Number(_loc3_.y + 2);
         _loc7_.alpha = var_260 / 6;
         _loc7_.var_198.var_970 = _loc7_.alpha;
         addChild(_loc7_);
      }
      
      override public function method_380() : Boolean
      {
         if(var_260 < class_885.var_969 || !class_652.var_214.method_182() || class_652.var_214.var_288.var_289.var_245 - var_289.var_245 != 1)
         {
            class_652.var_214.method_135(true);
            return true;
         }
         new class_918();
         return false;
      }
      
      override public function method_382() : Boolean
      {
         return var_260 == class_885.var_969;
      }
      
      override public function method_180() : void
      {
         if(!class_652.var_214.method_182())
         {
            return;
         }
         var _loc1_:class_777 = class_652.var_214.var_288;
         _loc1_.method_133(_loc1_.var_289.var_899[3]);
      }
      
      override public function method_383() : int
      {
         return 10;
      }
   }
}
