package package_30.package_31
{
   import package_32.class_899;
   
   public final class ff7G
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["q\x01","v\x01","&\x01","9=\x02\x02","\'\x15\x10\x01"];
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function ff7G(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_632(param1:class_770) : ff7G
      {
         return new ff7G("&\x01",2,[param1]);
      }
      
      public static function method_633(param1:ff7G) : ff7G
      {
         return new ff7G("9=\x02\x02",3,[param1]);
      }
      
      public static function method_634(param1:String, param2:class_770) : ff7G
      {
         return new ff7G("v\x01",1,[param1,param2]);
      }
      
      public static function method_635(param1:ff7G) : ff7G
      {
         return new ff7G("\'\x15\x10\x01",4,[param1]);
      }
      
      public static function method_636(param1:String, param2:class_770) : ff7G
      {
         return new ff7G("q\x01",0,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
