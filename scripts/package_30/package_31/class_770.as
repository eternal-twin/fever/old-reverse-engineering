package package_30.package_31
{
   import package_32.class_899;
   
   public final class class_770
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["G\\n(\x01","\x19\x14,+\x01","u/kP\x01","+d+8\x02","\x19\'\x1f\x13","\x1a[P;","@^\x14\x1c\x01","X\nZS\x01"," o0&","n\x14~\x12\x02","vl%s\x03","X)5!\x03"];
      
      public static var var_867:class_770;
      
      public static var var_868:class_770;
      
      public static var var_869:class_770;
      
      public static var var_870:class_770;
      
      public static var var_871:class_770;
      
      public static var var_872:class_770;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_770(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_626(param1:Array, param2:Object = undefined) : class_770
      {
         return new class_770("X\nZS\x01",7,[param1,param2]);
      }
      
      public static function method_627(param1:class_722) : class_770
      {
         return new class_770("@^\x14\x1c\x01",6,[param1]);
      }
      
      public static function method_628(param1:Array, param2:Array) : class_770
      {
         return new class_770("n\x14~\x12\x02",9,[param1,param2]);
      }
      
      public static function method_629(param1:Array, param2:Class) : class_770
      {
         return new class_770(" o0&",8,[param1,param2]);
      }
      
      public static function method_630(param1:Function) : class_770
      {
         return new class_770("X)5!\x03",11,[param1]);
      }
      
      public static function method_631(param1:String, param2:class_770) : class_770
      {
         return new class_770("vl%s\x03",10,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
