package package_30.package_31
{
   import package_22.package_23.class_895;
   import package_27.package_28.Bytes;
   import package_27.package_28.class_855;
   import package_32.class_899;
   import package_37.package_38.class_820;
   import package_39.package_40.class_861;
   
   public class class_774
   {
      
      public static var var_370:Boolean;
      
      public static var var_874:class_722;
      
      public static var var_875:class_722;
      
      public static var var_876:class_722;
       
      
      public var var_877:Object;
      
      public var var_878:class_720;
      
      public var var_243:Xml;
      
      public var var_317:class_772;
      
      public var var_885:String;
      
      public function class_774()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_878 = new class_720();
      }
      
      public function method_648(param1:String, param2:class_770) : Object
      {
         var _loc4_:String = null;
         var _loc5_:class_770 = null;
         var _loc6_:int = 0;
         var _loc7_:Array = null;
         var _loc8_:String = null;
         var _loc9_:String = null;
         var _loc3_:Array = param2.var_296;
         loop1:
         switch(int(param2.var_295))
         {
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               break;
            case 10:
               _loc4_ = _loc3_[0];
               _loc5_ = _loc3_[1];
               _loc6_ = 0;
               _loc7_ = param1.split(_loc4_);
               while(true)
               {
                  if(_loc6_ >= int(_loc7_.length))
                  {
                     break loop1;
                  }
                  _loc8_ = _loc7_[_loc6_];
                  _loc6_++;
                  _loc9_ = class_761.method_565(_loc8_);
                  if(method_647(_loc5_,_loc9_) == null)
                  {
                     return {
                        "X\x01":_loc9_,
                        "z\x01":_loc5_
                     };
                  }
               }
         }
         return {
            "X\x01":param1,
            "z\x01":param2
         };
      }
      
      public function method_650() : Object
      {
         var _loc4_:String = null;
         var _loc5_:* = null;
         var _loc6_:Array = null;
         var _loc1_:* = {};
         var _loc2_:int = 0;
         var _loc3_:Array = Reflect.method_649(var_877.name_24);
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = Reflect.field(var_877.name_24,_loc4_);
            if(class_691.method_351(_loc5_,Array))
            {
               _loc6_ = _loc5_;
               _loc1_[_loc4_] = _loc6_.method_78();
            }
            else
            {
               _loc1_[_loc4_] = _loc5_;
            }
         }
         return {
            "\x14Q\x0b-\x01":int(var_877.name_83),
            "\x14b\x1fm\x01":var_877.var_879.method_78(),
            "-Og\x01":var_877.name_84.method_78(),
            "}O\fY\x01":int(var_877.name_85),
            "e-|\x01":_loc1_
         };
      }
      
      public function method_652(param1:class_770) : String
      {
         var _loc3_:class_722 = null;
         var _loc4_:Array = null;
         var _loc5_:String = null;
         var _loc6_:class_770 = null;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               §§push("skip");
               break;
            case 1:
               §§push("blank");
               break;
            case 2:
               §§push("int");
               break;
            case 3:
               §§push("bool");
               break;
            case 4:
               §§push("float");
               break;
            case 5:
               §§push("text");
               break;
            case 6:
               _loc3_ = _loc2_[0];
               §§push("regexp");
               break;
            case 7:
            case 8:
            case 9:
               _loc4_ = _loc2_[0];
               §§push("one of these : (" + class_934.name_49(_loc4_,function(param1:String):String
               {
                  return "\'" + param1 + "\'";
               }).method_651(",") + ")");
               break;
            case 10:
               _loc5_ = _loc2_[0];
               _loc6_ = _loc2_[1];
               §§push("an array of " + method_652(_loc6_) + " separated by \'" + _loc5_ + "\'");
               break;
            case 11:
               §§push("custom data");
         }
         return; §§pop();
      }
      
      public function method_332(param1:Object) : void
      {
         var_877.name_83 = int(param1.name_83);
         var_877.var_879 = param1.var_879;
         var_877.name_84 = param1.name_84;
         var_877.name_85 = int(param1.name_85);
         var_877.name_24 = param1.name_24;
      }
      
      public function method_655(param1:class_855) : void
      {
         var _loc4_:* = null;
         var _loc2_:* = null;
         var _loc3_:* = new class_861(param1).method_653().method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(_loc4_.name_86 == "content.xml")
            {
               _loc2_ = _loc4_;
               break;
            }
         }
         var _loc5_:Bytes = Boolean(_loc2_.name_87)?class_895.name_50(_loc2_.package_31):_loc2_.package_31;
         method_184(Xml.method_654(_loc5_.toString()));
      }
      
      public function method_184(param1:Xml) : void
      {
         var _loc5_:class_820 = null;
         if(param1.var_880 == Xml.class_771)
         {
            param1 = param1.method_656();
         }
         var_243 = param1;
         var _loc2_:class_820 = new class_820(param1);
         var _loc3_:class_820 = _loc2_.var_881.method_657("office:body").var_881.method_657("office:spreadsheet");
         var _loc4_:* = _loc3_.var_882.method_657("table:table").method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            var_878.method_216(_loc5_.var_830.method_657("table:name"),_loc5_);
         }
      }
      
      public function method_658(param1:String) : Boolean
      {
         return Boolean(var_878.method_217(param1));
      }
      
      public function method_659(param1:class_771) : Boolean
      {
         var _loc3_:class_771 = null;
         var _loc4_:Array = null;
         var _loc5_:Boolean = false;
         var _loc6_:int = 0;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc4_ = _loc2_[0];
               §§push(false);
               if(int(_loc4_.length) > 0)
               {
                  §§pop();
                  §§push(Boolean(Boolean(method_659(_loc4_[int(_loc4_.length) - 1]))));
                  break;
               }
               break;
            case 1:
               §§push(true);
               break;
            case 2:
            case 3:
               §§push(false);
               break;
            case 4:
               _loc4_ = _loc2_[0];
               _loc5_ = int(_loc4_.length) > 0;
               _loc6_ = 0;
               while(_loc6_ < int(_loc4_.length))
               {
                  _loc3_ = _loc4_[_loc6_];
                  _loc6_++;
                  if(!method_659(_loc3_))
                  {
                     _loc5_ = false;
                     break;
                  }
               }
               §§push(_loc5_);
               break;
            case 5:
               _loc3_ = _loc2_[1];
               §§push(Boolean(method_659(_loc3_)));
         }
         return; §§pop();
      }
      
      public function method_660() : Array
      {
         return class_934.method_463({"\x1b\x0eN\x0b":var_878.method_215});
      }
      
      public function method_662(param1:String) : Object
      {
         var _loc2_:class_820 = var_878.method_98(param1);
         if(_loc2_ == null)
         {
            class_899.var_317 = new Error();
            throw "Sheet does not exists \'" + param1 + "\'";
         }
         var var_879:class_795 = _loc2_.var_882.method_657("table:table-row");
         var var_883:int = 0;
         var var_363:int = 0;
         var var_214:class_774 = this;
         return {
            "\'*+x\x03":function():Boolean
            {
               return var_879.name_37 > 0 || var_883 > 0;
            },
            "?\x05\x10;\x01":function():Array
            {
               var _loc1_:class_820 = null;
               var _loc2_:String = null;
               var _loc3_:int = 0;
               var _loc6_:class_820 = null;
               var _loc7_:String = null;
               var _loc8_:int = 0;
               var _loc9_:int = 0;
               var_363 = var_363 + 1;
               if(var_883 == 0)
               {
                  _loc1_ = var_879.name_12();
                  _loc2_ = _loc1_.var_244.method_98("table:number-rows-repeated");
                  if(_loc2_ != null)
                  {
                     var_883 = class_691.method_352(_loc2_) - 1;
                     var_879.method_103(_loc1_);
                  }
               }
               else
               {
                  _loc1_ = var_879.name_13();
                  _loc3_ = var_883 - 1;
                  var_883 = _loc3_;
                  if(_loc3_ == 0)
                  {
                     var_879.name_12();
                  }
               }
               var _loc4_:Array = [];
               var _loc5_:* = _loc1_.var_882.method_657("table:table-cell").method_73();
               while(_loc5_.method_74())
               {
                  _loc6_ = _loc5_.name_1();
                  _loc2_ = _loc6_.var_244.method_98("table:number-columns-repeated");
                  if(_loc2_ == null)
                  {
                     _loc4_.push(var_214.method_661(_loc6_,var_363));
                  }
                  else
                  {
                     _loc7_ = var_214.method_661(_loc6_,var_363);
                     _loc3_ = 0;
                     _loc8_ = class_691.method_352(_loc2_);
                     while(_loc3_ < _loc8_)
                     {
                        _loc3_++;
                        _loc9_ = _loc3_;
                        _loc4_.push(_loc7_);
                     }
                  }
               }
               return _loc4_;
            }
         };
      }
      
      public function method_663(param1:class_771) : class_771
      {
         var _loc3_:Array = null;
         var _loc4_:class_771 = null;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc3_ = _loc2_[0];
               §§push(class_771.method_639(class_934.method_463(class_934.name_49(_loc3_,method_663))));
               break;
            case 1:
               §§push(param1);
               break;
            case 2:
            case 3:
               _loc4_ = _loc2_[0];
               §§push(method_663(_loc4_));
               break;
            case 4:
            case 5:
               _loc3_ = _loc2_[0];
               §§push(class_771.method_642(class_934.method_463(class_934.name_49(_loc3_,method_663))));
         }
         return; §§pop();
      }
      
      public function method_661(param1:class_820, param2:int) : String
      {
         var _loc6_:Xml = null;
         var _loc7_:* = null;
         var _loc8_:Xml = null;
         var _loc3_:StringBuf = new StringBuf();
         var _loc4_:Boolean = true;
         var _loc5_:* = param1.var_244.method_73();
         while(_loc5_.method_74())
         {
            _loc6_ = _loc5_.name_1();
            if(_loc6_.method_664() != "office:annotation")
            {
               if(_loc6_.method_664() != "text:p")
               {
                  class_899.var_317 = new Error();
                  throw "assert " + _loc6_.method_664() + " at line " + param2;
               }
               if(_loc4_)
               {
                  _loc4_ = false;
               }
               else
               {
                  _loc3_.b = _loc3_.b + "\n";
               }
               _loc7_ = _loc6_.method_73();
               while(_loc7_.method_74())
               {
                  _loc8_ = _loc7_.name_1();
                  if(_loc8_.var_880 == Xml.class_738 && _loc8_.method_664() == "text:s")
                  {
                     _loc3_.b = _loc3_.b + "\n";
                  }
                  else
                  {
                     _loc3_.b = _loc3_.b + _loc8_.toString();
                  }
               }
            }
         }
         var _loc9_:String = _loc3_.b;
         _loc9_ = _loc9_.split("&apos;").join("\'");
         return _loc9_;
      }
      
      public function method_666(param1:class_772) : String
      {
         var _loc3_:* = null;
         var _loc4_:int = 0;
         var _loc5_:String = null;
         var _loc6_:class_770 = null;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc9_:String = null;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               §§push("No Error");
               break;
            case 1:
               _loc3_ = _loc2_[0];
               _loc4_ = _loc2_[1];
               §§push("line " + _loc4_ + " expected " + (_loc3_.var_884 == null?class_691.method_97(_loc3_.var_461):_loc3_.var_884));
               break;
            case 2:
               _loc5_ = _loc2_[0];
               _loc3_ = _loc2_[1];
               _loc6_ = _loc2_[2];
               _loc4_ = _loc2_[3];
               _loc7_ = _loc2_[4];
               _loc8_ = method_648(_loc5_,_loc6_);
               _loc9_ = method_652(_loc8_.var_93);
               §§push("at " + method_665(_loc7_) + _loc4_ + " \'" + _loc8_.var_86 + "\' should be " + _loc9_ + (_loc3_.var_884 == null?"":" (" + _loc3_.var_884 + ")"));
               break;
            case 3:
               _loc5_ = _loc2_[0];
               _loc3_ = _loc2_[1];
               _loc9_ = _loc2_[2];
               _loc4_ = _loc2_[3];
               _loc7_ = _loc2_[4];
               §§push("at " + method_665(_loc7_) + _loc4_ + " \'" + _loc5_ + "\' " + _loc9_ + (_loc3_.var_884 == null?"":" (" + _loc3_.var_884 + ")"));
         }
         return; §§pop();
      }
      
      public function method_667(param1:class_772) : int
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               §§push(-1);
               break;
            case 1:
               _loc3_ = _loc2_[1];
               §§push(_loc3_ << 16);
               break;
            case 2:
            case 3:
               _loc3_ = _loc2_[3];
               _loc4_ = _loc2_[4];
               §§push((_loc3_ << 16) + _loc4_);
         }
         return; §§pop();
      }
      
      public function method_668(param1:class_772) : void
      {
         if(int(method_667(param1)) > int(method_667(var_317)))
         {
            var_317 = param1;
         }
      }
      
      public function method_665(param1:int) : String
      {
         var _loc2_:String = "";
         do
         {
            _loc2_ = _loc2_ + String.fromCharCode(65 + int(param1 % 26));
            param1 = int(param1 / 26);
         }
         while(param1 > 0);
         
         return _loc2_;
      }
      
      public function method_647(param1:class_770, param2:String) : *
      {
         var _loc5_:class_722 = null;
         var _loc6_:Array = null;
         var _loc7_:* = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Class = null;
         var _loc12_:Array = null;
         var _loc13_:String = null;
         var _loc14_:class_770 = null;
         var _loc15_:String = null;
         var _loc16_:Function = null;
         var _loc17_:class_773 = null;
         var _loc4_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               return null;
            case 1:
               return Boolean(class_774.var_874.method_432(param2))?"":null;
            case 2:
               param2 = param2.split(" ").join("");
               return Boolean(class_774.var_875.method_432(param2))?class_691.method_352(param2):null;
            case 3:
               if(param2 == "true" || param2 == "1")
               {
                  return true;
               }
               if(param2 == "false" || param2 == "0")
               {
                  return false;
               }
               return null;
            case 4:
               return Boolean(class_774.var_876.method_432(param2))?Number(class_691.method_353(param2.split(",").join("."))):null;
            case 5:
               param2 = class_761.method_565(param2);
               return param2 == ""?null:param2;
            case 6:
               _loc5_ = _loc4_[0];
               return Boolean(_loc5_.method_432(param2))?param2:null;
            case 7:
               _loc6_ = _loc4_[0];
               _loc7_ = _loc4_[1];
               param2 = class_761.method_565(param2);
               _loc8_ = 0;
               _loc9_ = int(_loc6_.length);
               while(_loc8_ < _loc9_)
               {
                  _loc8_++;
                  _loc10_ = _loc8_;
                  if(param2 == _loc6_[_loc10_])
                  {
                     if(_loc7_)
                     {
                        return _loc10_;
                     }
                     return param2;
                  }
               }
               return null;
            case 8:
               _loc6_ = _loc4_[0];
               _loc11_ = _loc4_[1];
               param2 = class_761.method_565(param2);
               _loc8_ = 0;
               _loc9_ = int(_loc6_.length);
               while(_loc8_ < _loc9_)
               {
                  _loc8_++;
                  _loc10_ = _loc8_;
                  if(param2 == _loc6_[_loc10_])
                  {
                     return Type.method_669(_loc11_,_loc10_);
                  }
               }
               return null;
            case 9:
               _loc6_ = _loc4_[0];
               _loc12_ = _loc4_[1];
               param2 = class_761.method_565(param2);
               _loc8_ = 0;
               _loc9_ = int(_loc6_.length);
               while(_loc8_ < _loc9_)
               {
                  _loc8_++;
                  _loc10_ = _loc8_;
                  if(param2 == _loc6_[_loc10_])
                  {
                     return _loc12_[_loc10_];
                  }
               }
               return null;
            case 10:
               _loc13_ = _loc4_[0];
               _loc14_ = _loc4_[1];
               _loc6_ = [];
               if(param2 != "")
               {
                  _loc8_ = 0;
                  _loc12_ = param2.split(_loc13_);
                  while(_loc8_ < int(_loc12_.length))
                  {
                     _loc15_ = _loc12_[_loc8_];
                     _loc8_++;
                     _loc7_ = method_647(_loc14_,class_761.method_565(_loc15_));
                     if(_loc7_ == null)
                     {
                        return null;
                     }
                     _loc6_.push(_loc7_);
                  }
               }
               return _loc6_;
            case 11:
               _loc16_ = _loc4_[0];
               try
               {
                  return _loc16_(param2);
               }
               catch(:class_773;)
               {
                  _loc17_ = ;
                  _loc6_ = _loc17_.var_296;
                  if(!int(_loc17_.var_295))
                  {
                     _loc13_ = _loc6_[0];
                     var_885 = _loc13_;
                  }
                  return null;
               }
               return null;
         }
      }
      
      public function method_670(param1:class_771, param2:Boolean) : Boolean
      {
         var _loc4_:Array = null;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         var _loc7_:class_771 = null;
         var _loc8_:* = null;
         var _loc9_:* = null;
         var _loc10_:Xml = null;
         var _loc11_:* = null;
         var _loc12_:Xml = null;
         var _loc3_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc4_ = _loc3_[0];
               _loc5_ = method_650();
               _loc6_ = 0;
               while(_loc6_ < int(_loc4_.length))
               {
                  _loc7_ = _loc4_[_loc6_];
                  _loc6_++;
                  if(!method_670(_loc7_,param2))
                  {
                     method_332(_loc5_);
                     return false;
                  }
               }
               break;
            case 1:
               _loc5_ = _loc3_[0];
               return Boolean(method_671(_loc5_,param2));
            case 2:
               _loc7_ = _loc3_[0];
               while(method_670(_loc7_,true))
               {
               }
               break;
            case 3:
               _loc7_ = _loc3_[0];
               method_670(_loc7_,param2);
               break;
            case 4:
               _loc4_ = _loc3_[0];
               _loc5_ = method_650();
               _loc6_ = 0;
               while(_loc6_ < int(_loc4_.length))
               {
                  _loc7_ = _loc4_[_loc6_];
                  _loc6_++;
                  if(method_670(_loc7_,param2))
                  {
                     return true;
                  }
                  method_332(_loc5_);
                  _loc5_ = method_650();
               }
               return false;
            case 5:
               _loc5_ = _loc3_[0];
               _loc7_ = _loc3_[1];
               _loc8_ = method_650();
               _loc9_ = var_877.name_24;
               var_877.name_84 = [];
               if(!method_671(_loc5_,param2))
               {
                  method_332(_loc8_);
                  return false;
               }
               _loc10_ = var_877.name_84.shift();
               _loc11_ = Reflect.field(var_877.name_24,_loc5_.var_884);
               if(param2)
               {
                  _loc4_ = _loc11_;
                  _loc11_ = _loc4_[int(_loc4_.length) - 1];
               }
               var_877.name_24 = _loc11_;
               if(!method_670(_loc7_,false))
               {
                  method_332(_loc8_);
                  return false;
               }
               _loc6_ = 0;
               _loc4_ = var_877.name_84;
               while(_loc6_ < int(_loc4_.length))
               {
                  _loc12_ = _loc4_[_loc6_];
                  _loc6_++;
                  _loc10_.method_672(_loc12_);
               }
               var_877.name_84 = _loc8_.name_84;
               var_877.name_84.push(_loc10_);
               var_877.name_24 = _loc9_;
               break;
         }
         return true;
      }
      
      public function method_671(param1:Object, param2:Boolean) : Boolean
      {
         var _loc12_:String = null;
         var _loc13_:ff7G = null;
         var _loc14_:class_770 = null;
         var _loc15_:Array = null;
         var _loc3_:class_820 = var_877.var_879[0];
         if(_loc3_ == null)
         {
            method_668(class_772.method_644(param1,int(var_877.name_83)));
            return false;
         }
         var _loc4_:class_795 = _loc3_.var_882.method_657("table:table-cell");
         var _loc5_:int = 0;
         var _loc6_:class_820 = null;
         var _loc7_:int = -1;
         var _loc8_:Xml = param1.var_884 == null?Xml.method_673():Xml.method_674(param1.var_884);
         var _loc9_:int = 0;
         var _loc10_:ff7G = ff7G.method_632(class_770.var_872);
         var _loc11_:* = {};
         while(true)
         {
            _loc5_--;
            _loc7_++;
            if(_loc5_ <= 0)
            {
               _loc6_ = _loc4_.name_12();
               if(_loc6_ == null)
               {
                  _loc5_ = 10000;
               }
               else
               {
                  _loc12_ = _loc6_.var_244.method_98("table:number-columns-repeated");
                  _loc5_ = _loc12_ == null?1:class_691.method_352(_loc12_);
               }
            }
            if(_loc6_ == null)
            {
               _loc12_ = "";
            }
            else
            {
               _loc12_ = method_661(_loc6_,int(var_877.name_83));
            }
            _loc13_ = param1.var_461[_loc9_];
            if(_loc13_ == null)
            {
               if(_loc6_ != null)
               {
                  _loc13_ = _loc10_;
                  _loc7_ = _loc7_ + (_loc5_ - 1);
                  _loc5_ = 1;
               }
               _loc12_ = _loc3_.var_244.method_98("table:number-rows-repeated");
               if(_loc12_ != null)
               {
                  if(int(var_877.name_85) == 0)
                  {
                     var_877.name_85 = class_691.method_352(_loc12_);
                  }
                  else
                  {
                     var_877.name_85 = int(var_877.name_85) - 1;
                     if(int(var_877.name_85) == 1 || param1.var_884 == null)
                     {
                        var_877.var_879.shift();
                        var_877.name_85 = 0;
                     }
                  }
               }
               else
               {
                  var_877.var_879.shift();
               }
               if(param1.var_884 != null)
               {
                  var_877.name_84.push(_loc8_);
                  if(param2)
                  {
                     _loc15_ = Reflect.field(var_877.name_24,param1.var_884);
                     if(_loc15_ == null)
                     {
                        _loc15_ = [];
                        var_877.name_24[param1.var_884] = _loc15_;
                     }
                     _loc15_.push(_loc11_);
                  }
                  else
                  {
                     var_877.name_24[param1.var_884] = _loc11_;
                  }
               }
               var_877.name_83 = int(var_877.name_83) + 1;
               return true;
            }
            _loc14_ = method_675(_loc13_,_loc12_,_loc8_,_loc11_);
            if(_loc14_ != null)
            {
               break;
            }
            switch(int(_loc13_.var_295))
            {
               default:
               default:
               default:
               default:
                  _loc9_++;
                  continue;
               case 4:
                  if(_loc6_ == null)
                  {
                     §§goto(addr244);
                  }
                  else
                  {
                     _loc7_ = _loc7_ + (_loc5_ - 1);
                     _loc5_ = 1;

                  }
            }
         }
         if(var_885 != null)
         {
            method_668(class_772.method_645(_loc12_,param1,var_885,int(var_877.name_83),_loc7_));
            var_885 = null;
         }
         else
         {
            method_668(class_772.method_643(_loc12_,param1,_loc14_,int(var_877.name_83),_loc7_));
         }
         return false;
      }
      
      public function method_675(param1:ff7G, param2:String, param3:Xml, param4:*) : class_770
      {
         var _loc6_:class_770 = null;
         var _loc7_:ff7G = null;
         var _loc8_:String = null;
         var _loc9_:* = null;
         var _loc10_:String = null;
         var _loc11_:Xml = null;
         var _loc5_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               _loc8_ = _loc5_[0];
               _loc6_ = _loc5_[1];
               _loc9_ = method_647(_loc6_,param2);
               if(_loc9_ == null)
               {
                  return _loc6_;
               }
               _loc10_ = param3.method_98(_loc8_);
               if(_loc10_ == null)
               {
                  _loc10_ = "";
               }
               else
               {
                  _loc10_ = _loc10_ + ";";
               }
               param3.method_216(_loc8_,_loc10_ + class_691.method_97(_loc9_));
               method_676(param4,_loc8_,_loc9_);
               break;
            case 1:
               _loc8_ = _loc5_[0];
               _loc6_ = _loc5_[1];
               _loc9_ = method_647(_loc6_,param2);
               if(_loc9_ == null)
               {
                  return _loc6_;
               }
               _loc11_ = Xml.method_674(_loc8_);
               _loc11_.method_672(Xml.method_677(class_761.method_558(class_691.method_97(_loc9_))));
               param3.method_672(_loc11_);
               method_676(param4,_loc8_,_loc9_);
               break;
            case 2:
               _loc6_ = _loc5_[0];
               if(_loc6_ != class_770.var_868 && method_647(_loc6_,param2) == null)
               {
                  return _loc6_;
               }
               break;
            case 3:
               _loc7_ = _loc5_[0];
               _loc6_ = method_675(_loc7_,param2,param3,param4);
               if(_loc6_ != null && param2 != "")
               {
                  return _loc6_;
               }
               break;
            case 4:
               _loc7_ = _loc5_[0];
               return method_675(_loc7_,param2,param3,param4);
         }
         return null;
      }
      
      public function method_678(param1:String, param2:class_771) : Object
      {
         var _loc7_:Xml = null;
         var _loc3_:class_820 = var_878.method_98(param1);
         if(_loc3_ == null)
         {
            class_899.var_317 = new Error();
            throw "Sheet does not exists \'" + param1 + "\'";
         }
         var_877 = {
            "\x14Q\x0b-\x01":1,
            "\x14b\x1fm\x01":class_934.method_463(_loc3_.var_882.method_657("table:table-row")),
            "}O\fY\x01":0,
            "-Og\x01":[],
            "e-|\x01":{}
         };
         var_317 = class_772.var_873;
         if(!method_670(param2,false))
         {
            class_899.var_317 = new Error();
            throw "In \'" + param1 + "\' " + method_666(var_317);
         }
         if(int(var_877.var_879.length) > 0 && !method_659(param2))
         {
            class_899.var_317 = new Error();
            throw "In \'" + param1 + "\' " + method_666(var_317) + " (maybe extra data ?)";
         }
         var _loc4_:Xml = Xml.method_673();
         var _loc5_:int = 0;
         var _loc6_:Array = var_877.name_84;
         while(_loc5_ < int(_loc6_.length))
         {
            _loc7_ = _loc6_[_loc5_];
            _loc5_++;
            _loc4_.method_672(_loc7_);
         }
         return {
            "L\x01":_loc4_,
            "}\x01":var_877.name_24
         };
      }
      
      public function method_676(param1:*, param2:String, param3:*) : void
      {
         var _loc4_:* = null;
         var _loc5_:Array = null;
         if(Reflect.method_679(param1,param2))
         {
            _loc4_ = Reflect.field(param1,param2);
            if(!class_691.method_351(_loc4_,Array))
            {
               _loc4_ = [_loc4_,param3];
               param1[param2] = _loc4_;
            }
            else
            {
               _loc5_ = _loc4_;
               _loc5_.push(param3);
            }
         }
         else
         {
            param1[param2] = param3;
         }
      }
   }
}
