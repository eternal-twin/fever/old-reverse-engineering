package package_30.package_31
{
   import package_10.class_769;
   import package_10.Unserializer;
   import package_27.package_28.Bytes;
   import package_32.class_899;
   
   public class class_871
   {
      
      public static var var_1056:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
       
      
      public function class_871()
      {
      }
      
      public static function method_986(param1:String) : Bytes
      {
         var _loc4_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc2_:Bytes = Bytes.alloc(256);
         var _loc3_:int = 0;
         while(_loc3_ < 256)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc2_.b[_loc4_] = _loc4_ & 127;
         }
         _loc3_ = 0;
         _loc4_ = param1.length;
         var _loc5_:int = 0;
         while(_loc5_ < 256)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc3_ = int(_loc3_ + int(_loc2_.b[_loc6_]) + param1.charCodeAt(int(_loc6_ % _loc4_))) & 127;
            _loc7_ = int(_loc2_.b[_loc6_]);
            _loc2_.b[_loc6_] = int(_loc2_.b[_loc3_]);
            _loc2_.b[_loc3_] = _loc7_;
         }
         return _loc2_;
      }
      
      public static function method_987(param1:String) : String
      {
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc2_:Number = Number(class_769.name_81());
         var _loc3_:Bytes = class_871.method_986(param1.substr(0,5));
         var _loc4_:String = param1.substr(5,4);
         var _loc5_:Bytes = Unserializer.name_50(param1.substr(9));
         var _loc6_:int = int(_loc3_.b[0]);
         var _loc7_:int = int(_loc3_.b[1]);
         var _loc8_:int = 0;
         var _loc9_:int = _loc5_.length;
         while(_loc8_ < _loc9_)
         {
            _loc8_++;
            _loc10_ = _loc8_;
            _loc11_ = int(_loc5_.b[_loc10_]);
            _loc12_ = _loc11_ ^ int(_loc3_.b[_loc10_ & 255]);
            _loc5_.b[_loc10_] = _loc12_ == 0?_loc11_:_loc12_;
            if(_loc12_ == 0)
            {
               _loc11_ = 0;
            }
            _loc6_ = int((_loc6_ + _loc11_) % 65521);
            _loc7_ = int((_loc7_ + _loc6_) % 65521);
         }
         _loc8_ = _loc6_ ^ _loc7_ << 8;
         if(_loc4_.charCodeAt(0) != class_871.var_1056.charCodeAt(_loc8_ & 63) || _loc4_.charCodeAt(1) != class_871.var_1056.charCodeAt(_loc8_ >> 6 & 63) || _loc4_.charCodeAt(2) != class_871.var_1056.charCodeAt(_loc8_ >> 12 & 63) || _loc4_.charCodeAt(3) != class_871.var_1056.charCodeAt(_loc8_ >> 18 & 63))
         {
            class_899.var_317 = new Error();
            throw "Corrupted data";
         }
         return _loc5_.toString();
      }
   }
}
