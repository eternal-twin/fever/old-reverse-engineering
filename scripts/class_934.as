package
{
   public class class_934
   {
       
      
      public function class_934()
      {
      }
      
      public static function method_463(param1:Object) : Array
      {
         var _loc4_:Object = null;
         var _loc2_:Array = [];
         var _loc3_:* = param1.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            _loc2_.push(_loc4_);
         }
         return _loc2_;
      }
      
      public static function name_40(param1:Object) : class_795
      {
         var _loc4_:Object = null;
         var _loc2_:class_795 = new class_795();
         var _loc3_:* = param1.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            _loc2_.method_124(_loc4_);
         }
         return _loc2_;
      }
      
      public static function name_49(param1:Object, param2:Function) : class_795
      {
         var _loc5_:Object = null;
         var _loc3_:class_795 = new class_795();
         var _loc4_:* = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc3_.method_124(param2(_loc5_));
         }
         return _loc3_;
      }
      
      public static function method_1132(param1:Object, param2:Function) : class_795
      {
         var _loc6_:Object = null;
         var _loc3_:class_795 = new class_795();
         var _loc4_:int = 0;
         var _loc5_:* = param1.method_73();
         while(_loc5_.method_74())
         {
            _loc6_ = _loc5_.name_1();
            _loc4_++;
            _loc3_.method_124(param2(_loc4_,_loc6_));
         }
         return _loc3_;
      }
      
      public static function name_107(param1:Object, param2:Object, param3:Object = undefined) : Boolean
      {
         var _loc4_:* = null;
         var _loc5_:Object = null;
         if(param3 == null)
         {
            _loc4_ = param1.method_73();
            while(true)
            {
               if(_loc4_.method_74())
               {
                  _loc5_ = _loc4_.name_1();
                  if(_loc5_ == param2)
                  {
                     break;
                  }

               }
            }
            return true;
         }
         _loc4_ = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            if(param3(_loc5_,param2))
            {
               return true;
            }
         }
         return false;
      }
      
      public static function method_217(param1:Object, param2:Function) : Boolean
      {
         var _loc4_:Object = null;
         var _loc3_:* = param1.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(param2(_loc4_))
            {
               return true;
            }
         }
         return false;
      }
      
      public static function method_1133(param1:Object, param2:Function) : Boolean
      {
         var _loc4_:Object = null;
         var _loc3_:* = param1.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            if(!param2(_loc4_))
            {
               return false;
            }
         }
         return true;
      }
      
      public static function method_1134(param1:Object, param2:Function) : void
      {
         var _loc4_:Object = null;
         var _loc3_:* = param1.method_73();
         while(_loc3_.method_74())
         {
            _loc4_ = _loc3_.name_1();
            param2(_loc4_);
         }
      }
      
      public static function method_267(param1:Object, param2:Function) : class_795
      {
         var _loc5_:Object = null;
         var _loc3_:class_795 = new class_795();
         var _loc4_:* = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            if(param2(_loc5_))
            {
               _loc3_.method_124(_loc5_);
            }
         }
         return _loc3_;
      }
      
      public static function method_1135(param1:Object, param2:Function, param3:Object) : Object
      {
         var _loc5_:Object = null;
         var _loc4_:* = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            param3 = param2(_loc5_,param3);
         }
         return param3;
      }
      
      public static function name_28(param1:Object, param2:Object = undefined) : int
      {
         var _loc4_:* = null;
         var _loc5_:Object = null;
         var _loc3_:int = 0;
         if(param2 == null)
         {
            _loc4_ = param1.method_73();
            while(true)
            {
               if(_loc4_.method_74())
               {
                  _loc5_ = _loc4_.name_1();
                  _loc3_++;

               }
            }
         }
         else
         {
            _loc4_ = param1.method_73();
            while(_loc4_.method_74())
            {
               _loc5_ = _loc4_.name_1();
               if(param2(_loc5_))
               {
                  _loc3_++;
               }
            }
         }
         return _loc3_;
      }
      
      public static function method_92(param1:Object) : Boolean
      {
         return !param1.method_73().method_74();
      }
      
      public static function method_1136(param1:Object, param2:Object) : int
      {
         var _loc5_:Object = null;
         var _loc3_:int = 0;
         var _loc4_:* = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            if(param2 == _loc5_)
            {
               return _loc3_;
            }
            _loc3_++;
         }
         return -1;
      }
      
      public static function method_1137(param1:Object, param2:Object) : class_795
      {
         var _loc5_:Object = null;
         var _loc3_:class_795 = new class_795();
         var _loc4_:* = param1.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc3_.method_124(_loc5_);
         }
         _loc4_ = param2.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc3_.method_124(_loc5_);
         }
         return _loc3_;
      }
   }
}
