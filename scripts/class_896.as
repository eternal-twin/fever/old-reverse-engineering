package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import package_32.class_899;
   
   public class class_896 extends class_645
   {
       
      
      public var var_1385:Number;
      
      public var var_1386:int;
      
      public var var_1390:Number;
      
      public var var_1384:class_892;
      
      public var var_1392:Number;
      
      public var var_1391:int;
      
      public var var_1389:Number;
      
      public var var_1387:int;
      
      public var var_1388:Sprite;
      
      public var var_859:Boolean;
      
      public var var_11:class_14;
      
      public function class_896()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Boolean = false;
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc5_:Boolean = false;
         var _loc6_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1046();
               var_1384.var_245 = Number(var_1384.var_245 + 0.75);
               _loc1_ = var_1384.var_244 - var_11.x;
               _loc2_ = _loc1_ > 0;
               if(Number(Math.abs(_loc1_)) < 60)
               {
                  _loc3_ = var_1385 * (!!_loc2_?1:-1);
                  var_1384.var_278 = Number(var_1384.var_278 + _loc3_ * 0.02);
                  var_1384.var_580 = var_1384.var_580 - _loc3_ * 0.05;
               }
               _loc4_ = 1;
               var_1384.var_580 = var_1384.var_580 - Math.min(Number(Math.max(-_loc4_,var_1384.var_243.rotation * 0.05)),_loc4_);
               var_1384.var_580 = var_1384.var_580 * 0.95;
               if(var_1384.var_244 < var_1386 || var_1384.var_244 > class_742.var_217 - var_1386)
               {
                  var_1384.var_278 = var_1384.var_278 * -0.5;
                  var_1384.var_244 = Number(Math.min(Number(Math.max(var_1386,var_1384.var_244)),class_742.var_217 - var_1386));
               }
               method_1047();
               _loc3_ = Number(var_1384.var_245 + Math.cos(var_1384.var_243.rotation * 0.0175) * var_1386);
               _loc5_ = _loc3_ < var_1387;
               if(!_loc5_)
               {
                  if(var_859)
                  {
                     _loc6_ = var_1384.var_244 - var_1388.x;
                     if(Number(Math.abs(_loc6_)) < var_1389)
                     {
                        var_1390 = _loc6_;
                        method_498(true);
                     }
                  }
                  if(_loc3_ > var_1387 + 10)
                  {
                     method_498(false);
                  }
               }
               var_859 = _loc5_;
               break;
            case 2:
               method_1046();
               if(var_220)
               {
                  var_1384.var_244 = Number(var_1388.x + var_1390);
               }
               var_11.alpha = var_11.alpha * 0.5;
         }
         super.method_79();
      }
      
      public function method_1046() : void
      {
         var_1388.x = Number(var_1388.x + var_1391 * var_1392);
         if(var_1388.x < var_1389 || var_1388.x > class_742.var_217 - var_1389)
         {
            var_1391 = var_1391 * -1;
            var_1388.x = Number(Math.min(Number(Math.max(var_1389,var_1388.x)),class_742.var_217 - var_1389));
         }
      }
      
      public function method_1047() : void
      {
         var _loc1_:* = method_100();
         var_11.x = Number(var_11.x * 0.5 + _loc1_.var_244 * 0.5);
         var_11.y = Number(var_11.y * 0.5 + _loc1_.var_245 * 0.5);
         var _loc2_:Boolean = var_1384.var_244 - var_11.x > 0;
         if(_loc2_)
         {
            var_11.prevFrame();
         }
         else
         {
            var_11.nextFrame();
         }
         var _loc3_:Number = var_1384.var_245 - var_11.y;
         var_11.rotation = _loc3_ * 0.2 * (!!_loc2_?1:-1);
         if(name_5)
         {
            var_1385 = Number(var_1385 + 1);
         }
         var_1385 = var_1385 * 0.95;
         if(var_11.var_11 != null)
         {
            var_11.var_11.var_11.rotation = Number(var_11.var_11.var_11.rotation + var_1385);
         }
      }
      
      public function method_498(param1:Boolean) : void
      {
         name_3 = 2;
         var_1384.var_243.gotoAndPlay(!!param1?"$landing":"$ploufing");
         method_81(param1,50);
         var_1384.var_243.rotation = 0;
         var_1384.var_278 = 0;
         var_1384.var_279 = 0;
         var_1384.var_580 = 0;
      }
      
      override public function method_94() : void
      {
         method_72();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [140];
         super.method_95(param1);
         var_1389 = 40 - param1 * 10;
         var_1392 = Number(0.5 + param1 * 6);
         var_1391 = int(class_691.method_114(2)) * 2 - 1;
         var_1386 = 25;
         var_1387 = class_742.var_219 - 15;
         method_117();
         var_1385 = 0;
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("d\x0f5G\x01"),0);
         var_1388 = var_232.method_84(class_899.__unprotect__("\x17\x16)\x1d\x02"),class_645.var_209);
         var_1388.x = Number(var_1389 + int(class_691.method_114(int(Math.round(class_742.var_217 - var_1389)))));
         var_1388.y = var_1387;
         var_1388.scaleX = var_1389 * 0.02;
         var_1388.scaleY = var_1389 * 0.02;
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x01=J_\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_1384 = _loc1_;
         var_1384.var_244 = class_742.var_217 * 0.5;
         var_1384.var_245 = class_742.var_219 * 0.5;
         var_1384.var_580 = 0;
         var_1384.var_243.scaleX = var_1386 * 0.02;
         var_1384.var_243.scaleY = var_1386 * 0.02;
         var_1384.var_243.stop();
         var_1384.method_118();
         var_11 = new class_14();
         var_232.method_124(var_11,class_645.var_209);
         var_11.x = class_742.var_217 * 0.5;
         var_11.y = class_742.var_219 * 0.5 - 20;
         var_11.stop();
      }
   }
}
