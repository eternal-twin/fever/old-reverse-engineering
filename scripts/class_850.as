package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.filters.GlowFilter;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_850 extends class_645
   {
      
      public static var var_1209:Array = [[8,9,10,11],[0,1,12,13,14,15],[2,3,8,9,14,15],[0,1,2,3,8,9],[4,5,6,7,12,13,14,15],[0,1,6,7],[2,3,4,5,12,13,14,15],[10,11,12,13],[9,10,16,17,22,23],[18,19,20,21],[10,14,20,21],[9,10,13,14],[8,9,10,11,21,22],[12,13,14,15],[2,3,8,9],[0,1,2,3,17,18],[0,1,4,5,6,7],[0,1,6,7,12,13,14],[2,3,4,5,18,19,20,21],[6,7,12,13,14,9,10,11],[9,10,13,22,23],[21,23,23,16],[10,11,14,15,20,21],[9,10,18,22]];
      
      public static var var_726:int = 90;
      
      public static var var_1210:int = 12;
       
      
      public var var_1213:Array;
      
      public var var_740:int;
      
      public var var_1214:Array;
      
      public var var_1212:Sprite;
      
      public var var_1211:Sprite;
      
      public var var_1217:Array;
      
      public var var_1218:MovieClip;
      
      public var var_1216:class_472;
      
      public var var_1215:int;
      
      public var var_254:Number;
      
      public function class_850()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               var_254 = Number(Math.min(Number(var_254 + 0.1),1));
               _loc1_ = Number(Math.pow(var_254,2));
               var_1211.x = class_742.var_216 * 0.5 - class_850.var_726 * (1 - _loc1_);
               var_1212.x = Number(class_742.var_216 * 0.5 + class_850.var_726 * (1 - _loc1_));
               if(var_254 == 1)
               {
                  new class_931(this);
                  name_3 = 3;
                  var_254 = 0;
                  break;
               }
               break;
            case 3:
               var_254 = Number(Math.min(Number(var_254 + 0.06),1));
               _loc1_ = 1 - var_254;
               var_1212.filters = [new GlowFilter(16777215,1,_loc1_ * 16,_loc1_ * 16,_loc1_ * 8)];
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
               }
               class_657.name_18(var_1212,0,int(_loc1_ * 100));
         }
         super.method_79();
      }
      
      public function method_400(param1:int) : void
      {
         var _loc3_:class_474 = null;
         var _loc4_:Number = NaN;
         var _loc2_:class_474 = method_246(param1);
         if(_loc2_.tabIndex == 0)
         {
            if(int(var_1214.length) == var_1215)
            {
               new class_931(var_1216,0.15,16711680);
               return;
            }
            _loc2_.tabIndex = 1;
            _loc2_.alpha = 0.2;
            _loc3_ = new class_474();
            _loc4_ = 2.2;
            _loc3_.scaleY = _loc4_;
            _loc3_.scaleX = _loc4_;
            _loc3_.gotoAndStop(param1 + 1);
            var_1214.push(_loc3_);
            var_1212.addChild(_loc3_);
         }
         else
         {
            _loc2_.tabIndex = 0;
            _loc2_.alpha = 1;
            _loc3_ = method_874(param1);
            var_1212.removeChild(_loc3_);
            var_1214.method_116(_loc3_);
         }
         method_875();
         method_228();
      }
      
      public function method_875() : void
      {
         var_1216.var_140.text = class_691.method_97(int(var_1214.length));
         var_1216.var_141.text = class_691.method_97(var_1215);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 250 * param1];
         super.method_95(param1);
         var_1215 = 2 + int(Math.round(param1 * 2));
         var_740 = int(class_691.method_114(4)) * 90;
         method_117();
      }
      
      public function method_246(param1:int) : class_474
      {
         var _loc4_:class_474 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1217;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.currentFrame - 1 == param1)
            {
               return _loc4_;
            }
         }
         return null;
      }
      
      public function method_874(param1:int) : class_474
      {
         var _loc4_:class_474 = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1214;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.currentFrame - 1 == param1)
            {
               return _loc4_;
            }
         }
         return null;
      }
      
      public function method_228() : void
      {
         var _loc3_:int = 0;
         var _loc5_:class_474 = null;
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         while(_loc2_ < 24)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc1_.push(0);
         }
         _loc2_ = 0;
         var _loc4_:Array = var_1214;
         while(_loc2_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc2_];
            _loc2_++;
            _loc6_ = class_850.var_1209[_loc5_.currentFrame - 1];
            _loc3_ = 0;
            while(_loc3_ < int(_loc6_.length))
            {
               _loc7_ = int(_loc6_[_loc3_]);
               _loc3_++;
               _loc1_[_loc7_] = 1;
            }
         }
         _loc2_ = 0;
         while(_loc2_ < 24)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            if(int(_loc1_[_loc3_]) != int(var_1213[_loc3_]))
            {
               return;
            }
         }
         method_81(true,30);
         _loc2_ = 0;
         _loc4_ = var_1217;
         while(_loc2_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc2_];
            _loc2_++;
            _loc5_.mouseEnabled = false;
         }
         name_3 = 2;
         var_254 = 0;
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_474 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Array = null;
         var _loc11_:int = 0;
         var_1216 = new class_472();
         addChild(var_1216);
         var _loc3_:Array = [];
         var _loc4_:int = 0;
         _loc5_ = class_850.var_1210;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = _loc6_;
            if(int(class_691.method_114(int(12 - var_237[0] * 10))) == 0)
            {
               _loc7_ = _loc7_ + 12;
            }
            _loc3_.push(_loc7_);
         }
         class_659.method_241(_loc3_);
         var var_214:class_850 = this;
         var_1217 = [];
         _loc4_ = 0;
         _loc5_ = class_850.var_1210;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc8_ = new class_474();
            addChild(_loc8_);
            _loc8_.x = 38 + int(_loc6_ % 6) * 64;
            _loc8_.y = class_742.var_218 - (40 + int(_loc6_ / 6) * 64);
            _loc9_ = 0.85;
            _loc8_.scaleY = _loc9_;
            _loc8_.scaleX = _loc9_;
            var var_215:Array = [int(_loc3_[_loc6_])];
            _loc8_.gotoAndStop(int(var_215[0]) + 1);
            _loc8_.addEventListener(MouseEvent.CLICK,function(param1:Array):Function
            {
               var var_215:Array = param1;
               return function(param1:*):void
               {
                  var_214.method_400(int(var_215[0]));
               };
            }(var_215));
            _loc8_.tabIndex = 0;
            _loc8_.rotation = var_740;
            var_1217.push(_loc8_);
         }
         var_1211 = new Sprite();
         var_1212 = new Sprite();
         addChild(var_1211);
         addChild(var_1212);
         var_1211.x = class_742.var_216 * 0.5 - class_850.var_726;
         var_1211.y = 100;
         var_1212.x = Number(class_742.var_216 * 0.5 + class_850.var_726);
         var_1212.y = 100;
         var_1211.rotation = var_740;
         var_1212.rotation = var_740;
         var_1213 = [];
         _loc4_ = 0;
         while(_loc4_ < 24)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            var_1213.push(0);
         }
         class_659.method_241(_loc3_);
         _loc4_ = 0;
         _loc5_ = var_1215;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc8_ = new class_474();
            var_1211.addChild(_loc8_);
            _loc8_.gotoAndStop(int(_loc3_[_loc6_]) + 1);
            _loc9_ = 2.2;
            _loc8_.scaleY = _loc9_;
            _loc8_.scaleX = _loc9_;
            _loc10_ = class_850.var_1209[int(_loc3_[_loc6_])];
            _loc7_ = 0;
            while(_loc7_ < int(_loc10_.length))
            {
               _loc11_ = int(_loc10_[_loc7_]);
               _loc7_++;
               var_1213[_loc11_] = 1;
            }
         }
         var_1214 = [];
         method_875();
      }
   }
}
