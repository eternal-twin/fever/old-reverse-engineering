package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_884 extends class_645
   {
      
      public static var var_1348:int = 24;
       
      
      public var var_1350:int;
      
      public var var_251:Number;
      
      public var var_1349:class_892;
      
      public var var_488:Array;
      
      public function class_884()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc6_:class_892 = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:int = 0;
         var _loc14_:class_21 = null;
         var _loc1_:* = method_100();
         _loc1_.var_245 = _loc1_.var_245 - 60;
         var _loc2_:Number = var_1349.var_244;
         var_1349.method_229(_loc1_,0.2,null);
         var _loc3_:Number = var_1349.var_244 - _loc2_;
         var_1349.var_243.rotation = -_loc3_ * 1.5;
         var _loc4_:int = 0;
         var _loc5_:Array = var_488;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            if(_loc6_.var_250 == null)
            {
               _loc7_ = Number(_loc6_.method_115(_loc6_.var_280));
               if(_loc7_ < 20 || Number(Math.random()) < 0.04)
               {
                  method_1018(_loc6_);
               }
               _loc6_.method_358(_loc6_.var_280,0.1,0.8);
               _loc8_ = var_237[0];
               _loc9_ = Number(_loc6_.method_115(var_1349));
               if(_loc9_ < _loc8_)
               {
                  _loc10_ = Number(var_1349.method_231(_loc6_));
                  _loc11_ = _loc8_ - _loc9_;
                  _loc12_ = 0.04;
                  _loc6_.var_244 = Number(_loc6_.var_244 + Math.cos(_loc10_) * _loc11_ * _loc12_);
                  _loc6_.var_245 = Number(_loc6_.var_245 + Math.sin(_loc10_) * _loc11_ * _loc12_);
               }
               _loc10_ = Number(_loc6_.method_231(_loc6_.var_280));
               _loc13_ = int(_loc10_ / 6.28 * 40);
               if(_loc13_ < 0)
               {
                  _loc13_ = _loc13_ + 40;
               }
               _loc6_.var_243.gotoAndStop(_loc13_ + 1);
               _loc14_ = _loc6_.var_243;
               _loc14_.var_19.gotoAndPlay(int(_loc14_.x % 2) + 1);
               _loc14_.var_18.gotoAndPlay(int(_loc14_.x % 2) + 1);
            }
         }
         switch(name_3)
         {
            default:
            default:
               break;
            case 2:
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true,10);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         var _loc5_:class_892 = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Boolean = false;
         var _loc3_:int = 0;
         var _loc4_:Array = var_488;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.var_250 == null)
            {
               _loc6_ = _loc5_.var_244 - var_1349.var_244;
               _loc7_ = _loc5_.var_245 - var_1349.var_245;
               if(Number(Math.abs(_loc6_)) < class_884.var_1348 && Number(Math.abs(_loc7_)) < class_884.var_1348)
               {
                  _loc1_++;
                  _loc5_.var_250 = 1;
                  _loc5_.var_278 = 0;
                  _loc5_.var_279 = 0;
                  var_1350 = var_1350 - 1;
                  if(var_1350 == 0)
                  {
                     name_3 = 2;
                     var_251 = 12;
                  }
                  _loc5_.var_243.gotoAndStop("death");
                  _loc5_.var_243.rotation = Math.random() * 360;
                  _loc8_ = var_232.method_84(class_899.__unprotect__("^R\x15\t\x01"),class_645.var_208);
                  _loc8_.x = _loc5_.var_244;
                  _loc8_.y = _loc5_.var_245;
                  _loc8_.rotation = Math.random() * 360;
                  _loc8_.gotoAndStop(int(class_691.method_114(_loc8_.totalFrames)) + 1);
                  _loc2_ = true;
               }
            }
         }
         var_1349.var_243.gotoAndPlay("2");
         var _loc9_:class_22 = var_1349.var_243;
         _loc9_.var_20.visible = _loc2_;
      }
      
      public function method_1018(param1:Object) : void
      {
         param1.var_280 = {
            "L\x01":Number(30 + Math.random() * (class_742.var_217 - 2 * 30)),
            "\x03\x01":Number(30 + Math.random() * (class_742.var_219 - 2 * 30))
         };
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400 - param1 * 100];
         super.method_95(param1);
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1b\rZ*"),0);
         class_657.name_18(class_319,0,-20);
         var_488 = [];
         var_1350 = 1 + int(Math.floor(var_237[0] * 12));
         var _loc1_:int = 0;
         var _loc2_:int = var_1350;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("47\x1bP\x03"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            method_1018(_loc4_);
            _loc4_.var_244 = Number(_loc4_.var_280.var_244);
            _loc4_.var_245 = Number(_loc4_.var_280.var_245);
            _loc4_.var_233 = 0.95;
            _loc4_.method_118();
            var_488.push(_loc4_);
            _loc4_.var_243.stop();
         }
         _loc4_ = new class_892(var_232.method_84(class_899.__unprotect__("s\x116L\x03"),class_645.var_209));
         _loc4_.var_233 = 0.99;
         var_1349 = _loc4_;
         var_1349.var_244 = class_742.var_217 * 0.5;
         var_1349.var_245 = class_742.var_219 * 0.5;
         var_1349.var_233 = 0.95;
         var_1349.method_118();
      }
   }
}
