package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_775 extends class_645
   {
      
      public static var var_493:int = 16;
       
      
      public var var_886:Number;
      
      public var var_887:Number;
      
      public var var_888:Object;
      
      public var var_576:Array;
      
      public var var_889:int;
      
      public var name_1:class_892;
      
      public var var_699:Array;
      
      public function class_775()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:class_892 = null;
         var _loc4_:class_892 = null;
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_100();
               _loc2_ = Number(name_1.method_115(_loc1_));
               if(_loc2_ < class_775.var_493)
               {
                  _loc4_ = new class_892(var_232.method_84(class_899.__unprotect__("{IE\x06\x02"),class_645.var_209));
                  _loc4_.var_233 = 0.99;
                  _loc3_ = _loc4_;
                  _loc3_.var_244 = name_1.var_244;
                  _loc3_.var_245 = name_1.var_245;
                  _loc3_.var_243.rotation = Math.random() * 360;
                  _loc3_.var_251 = 6;
                  _loc3_.var_272 = -1;
                  _loc3_.method_118();
                  name_1.method_89();
                  var_699.pop();
                  if(int(var_699.length) > 0)
                  {
                     method_680();
                  }
                  else
                  {
                     name_1 = null;
                     method_81(true,15);
                     name_3 = 2;
                  }
               }
               _loc5_ = 0;
               _loc6_ = var_699;
               while(_loc5_ < int(_loc6_.length))
               {
                  _loc3_ = _loc6_[_loc5_];
                  _loc5_++;
                  if(_loc3_.var_244 < class_775.var_493 || _loc3_.var_244 > class_742.var_217 - class_775.var_493)
                  {
                     _loc3_.var_244 = Number(class_663.method_99(class_775.var_493,_loc3_.var_244,class_742.var_217 - class_775.var_493));
                     _loc3_.var_278 = _loc3_.var_278 * -1;
                  }
                  if(_loc3_.var_245 < class_775.var_493 || _loc3_.var_245 > class_742.var_219 - class_775.var_493)
                  {
                     _loc3_.var_245 = Number(class_663.method_99(class_775.var_493,_loc3_.var_245,class_742.var_219 - class_775.var_493));
                     _loc3_.var_279 = _loc3_.var_279 * -1;
                  }
               }
               if(var_888 == null)
               {
                  if(int(class_691.method_114(int((30 - var_237[0] * 10) * var_889))) == 0)
                  {
                     var_888 = 0;
                     _loc7_ = Math.random() * 6.28;
                     _loc8_ = Number(0.4 + var_237[0] * 1.5);
                     var_887 = Math.cos(_loc7_) * _loc8_;
                     var_886 = Math.sin(_loc7_) * _loc8_;
                     var_576 = var_699.method_78();
                     var_889 = var_889 + 1;
                     break;
                  }
                  break;
               }
               var_888 = var_888 + 10;
               _loc6_ = var_576.method_78();
               _loc5_ = 0;
               while(_loc5_ < int(_loc6_.length))
               {
                  _loc3_ = _loc6_[_loc5_];
                  _loc5_++;
                  if(Number(_loc3_.var_244 + _loc3_.var_245) < var_888)
                  {
                     _loc3_.var_278 = Number(_loc3_.var_278 + var_887);
                     _loc3_.var_279 = Number(_loc3_.var_279 + var_886);
                     var_576.method_116(_loc3_);
                  }
               }
               if(int(var_576.length) == 0)
               {
                  var_888 = null;
                  break;
               }
               break;
         }
         super.method_79();
      }
      
      public function method_680() : void
      {
         name_1 = var_699[int(var_699.length) - 1];
         name_1.var_243.gotoAndStop(2);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [240];
         param1 = 1;
         super.method_95(param1);
         var_889 = 1;
         method_117();
         method_680();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:class_892 = null;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1c\x19[\x07\x03"),0);
         var _loc2_:int = 30 * 1;
         var _loc3_:Number = Number(0.1 + var_237[0] * 0.3);
         var _loc4_:* = {
            "59\x01":0,
            "@\x01":Math.random() * 6.28,
            "L\x01":class_742.var_217 - 2 * _loc2_,
            "\x03\x01":class_742.var_219 - 2 * _loc2_
         };
         var _loc5_:int = int(Number(8 + var_237[0] * 18));
         var_699 = [];
         var _loc6_:int = 0;
         while(_loc6_ < _loc5_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc9_ = new class_892(var_232.method_84(class_899.__unprotect__("K\x1e\x18\x02\x02"),class_645.var_209));
            _loc9_.var_233 = 0.99;
            _loc8_ = _loc9_;
            _loc10_ = Math.random() * 6.28;
            _loc8_.var_244 = Number(_loc4_.var_244);
            _loc8_.var_245 = Number(_loc4_.var_245);
            _loc8_.var_278 = Number(_loc8_.var_278 + Math.cos(_loc10_) * Math.random() * _loc3_);
            _loc8_.var_279 = Number(_loc8_.var_279 + Math.sin(_loc10_) * Math.random() * _loc3_);
            _loc8_.var_243.stop();
            _loc8_.method_118();
            var_699.push(_loc8_);
            _loc4_.name_88 = Number(Number(_loc4_.name_88) + (int(class_691.method_114(2)) * 2 - 1) * 0.25);
            _loc4_.name_88 = _loc4_.name_88 * 0.9;
            _loc4_.var_65 = Number(Number(_loc4_.var_65) + Number(_loc4_.name_88));
            _loc11_ = Math.cos(Number(_loc4_.var_65)) * _loc2_;
            _loc12_ = Math.sin(Number(_loc4_.var_65)) * _loc2_;
            _loc4_.var_244 = Number(Number(_loc4_.var_244) + _loc11_);
            _loc4_.var_245 = Number(Number(_loc4_.var_245) + _loc12_);
            if(Number(_loc4_.var_244) < _loc2_ || Number(_loc4_.var_244) > class_742.var_217 - _loc2_)
            {
               _loc4_.var_244 = Number(class_663.method_99(_loc2_,Number(_loc4_.var_244),class_742.var_217 - _loc2_));
               _loc11_ = _loc11_ * -1;
               _loc4_.var_65 = Number(Math.atan2(_loc12_,_loc11_));
            }
            if(Number(_loc4_.var_245) < _loc2_ || Number(_loc4_.var_245) > class_742.var_219 - _loc2_)
            {
               _loc4_.var_245 = Number(class_663.method_99(_loc2_,Number(_loc4_.var_245),class_742.var_219 - _loc2_));
               _loc12_ = _loc12_ * -1;
               _loc4_.var_65 = Number(Math.atan2(_loc12_,_loc11_));
            }
         }
      }
   }
}
