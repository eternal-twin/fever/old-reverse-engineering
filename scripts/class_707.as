package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_707 extends class_645
   {
       
      
      public var var_601:int;
      
      public var var_602:MovieClip;
      
      public var var_608:class_656;
      
      public var var_607:int;
      
      public var var_609:int;
      
      public var var_69:MovieClip;
      
      public var var_288:class_892;
      
      public var var_603:int;
      
      public var var_532:MovieClip;
      
      public var var_606:int;
      
      public var var_604:Boolean;
      
      public var var_605:Boolean;
      
      public var var_610:Boolean;
      
      public var var_611:MovieClip;
      
      public function class_707()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = Number(var_288.var_245 + var_603);
               if(!var_604 && _loc1_ < var_601)
               {
                  var_604 = true;
                  var_229 = [true];
                  var_288.var_243.gotoAndStop(6);
               }
               if(!!var_604 && _loc1_ > var_601)
               {
                  var_229 = [false];
                  var_604 = false;
                  method_81(true,20);
                  var_69 = var_232.method_84(class_899.__unprotect__("\x1b\x1ash\x02"),class_645.var_209);
                  var_69.scaleX = class_742.var_217 * 0.01;
                  var_69.scaleY = class_742.var_219 * 0.01;
                  var_69.y = var_601 - class_742.var_219;
                  var_288.var_243.mask = var_69;
               }
               if(var_605)
               {
                  if(_loc1_ > var_606)
                  {
                     var_288.var_245 = var_606 - var_603;
                     var_288.var_279 = var_288.var_279 * -0.5;
                     method_81(false,20);
                     var_288.var_243.gotoAndStop("5");
                  }
               }
               else
               {
                  if(_loc1_ > var_607)
                  {
                     if(Number(Math.abs(var_288.var_244 - var_608.var_244)) > var_609 - var_603)
                     {
                        var_605 = true;
                        var_232.method_110(var_288.var_243);
                     }
                     _loc2_ = var_607 - _loc1_;
                     var_288.var_279 = Number(var_288.var_279 + _loc2_ * 0.1 * (!!name_5?2:1));
                     if(name_5)
                     {
                        _loc3_ = method_100().var_244 - var_288.var_244;
                        var_288.var_278 = Number(var_288.var_278 + _loc3_ * 0.01);
                        var_288.var_580 = Number(var_288.var_580 + _loc3_ * 0.05);
                     }
                     if(!!var_610 && Number(Math.abs(_loc2_)) > var_603 * 1.5)
                     {
                        var_610 = false;
                        var_288.var_243.gotoAndStop(class_691.method_97(int(class_691.method_114(3)) + 2));
                     }
                  }
                  else
                  {
                     if(!var_610)
                     {
                        var_610 = true;
                     }
                     var_288.var_580 = var_288.var_580 - var_288.var_243.rotation * 0.002;
                  }
                  if(!var_604 && !var_220 && Number(Math.abs(var_288.var_244 - var_608.var_244)) > var_609 - var_603 * 2)
                  {
                     var_288.var_243.gotoAndStop(7);
                  }
               }
               var_611.graphics.clear();
               if(!var_605 && _loc1_ > var_607 - 4 && var_220 != true)
               {
                  var_611.graphics.lineStyle(1,0,20);
                  var_611.graphics.beginFill(13553273,100);
                  var_611.graphics.moveTo(var_608.var_244 - var_609,var_608.var_245);
                  var_611.graphics.lineTo(Number(var_608.var_244 + var_609),var_608.var_245);
                  var_611.graphics.curveTo(Number((var_608.var_244 + var_609) * 0.3 + var_288.var_244 * 0.7),_loc1_,var_288.var_244,_loc1_);
                  var_611.graphics.curveTo(Number((var_608.var_244 - var_609) * 0.3 + var_288.var_244 * 0.7),_loc1_,var_608.var_244 - var_609,var_608.var_245);
                  var_611.graphics.endFill();
               }
               _loc2_ = class_742.var_216 / class_742.var_217;
               var_201.y = Number(Math.max(0,class_742.var_218 * 0.5 - var_288.var_245 * _loc2_));
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_607 = class_742.var_219 - 40;
         var_606 = class_742.var_219 - 6;
         var_603 = 24;
         var_609 = 94;
         var_601 = (4 - int(Math.round(param1 * 10))) * 32;
         var_604 = false;
         var_605 = false;
         var_610 = true;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var_232.method_84(class_899.__unprotect__("\x0fl\x05^"),0);
         var_602 = var_232.method_84(class_899.__unprotect__("@Uk\x05\x02"),class_645.var_207);
         var_602.y = var_601;
         var_532 = var_232.method_84(class_899.__unprotect__("@9W1\x02"),class_645.var_207);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("n\x06\x15m\x02"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_288 = _loc1_;
         var_288.var_244 = class_742.var_217 * 0.5;
         var_288.var_245 = class_742.var_219 * 0.5;
         var_288.var_580 = 0;
         var_288.var_250 = 0.5;
         var_288.var_243.scaleX = var_603 * 0.02;
         var_288.var_243.scaleY = var_603 * 0.02;
         var_288.var_243.stop();
         var_288.method_118();
         var_611 = var_232.method_92(class_645.var_209);
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__("D;\'#\x01"),class_645.var_209));
         var_608 = _loc2_;
         var_608.var_244 = class_742.var_217 * 0.5;
         var_608.var_245 = var_607;
         var_608.method_118();
      }
   }
}
