package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_903 extends class_645
   {
       
      
      public var var_1409:Number;
      
      public var var_1410:Number;
      
      public var var_351:Number;
      
      public var var_1411:Object;
      
      public var var_1417:Number;
      
      public var var_1414:int;
      
      public var var_1420:Number;
      
      public var var_759:MovieClip;
      
      public var var_1413:int;
      
      public var var_1412:Number;
      
      public var var_1418:int;
      
      public var var_288:MovieClip;
      
      public var var_859:Boolean;
      
      public var var_101:MovieClip;
      
      public var var_1421:class_755;
      
      public var var_1419:int;
      
      public var var_1422:MovieClip;
      
      public var var_1415:MovieClip;
      
      public var var_248:Number;
      
      public var var_1416:Number;
      
      public function class_903()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:MovieClip = null;
         var _loc7_:Boolean = false;
         var _loc8_:Boolean = false;
         method_1055();
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = method_100();
               _loc2_ = _loc1_.var_244 - var_1411.var_244;
               _loc3_ = _loc1_.var_245 - var_1411.var_245;
               _loc4_ = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc3_ * _loc3_)));
               var_351 = Number(var_351 + _loc4_ * 0.012);
               var_351 = var_351 * 0.97;
               var_288.x = Number(var_288.x + var_351);
               _loc5_ = var_351 * 0.3;
               var_1412 = (var_1412 + _loc5_) % var_1413;
               if(_loc5_ < 4)
               {
                  var_288.gotoAndStop(int(Math.round(Number(var_1412 + 1))));
               }
               else
               {
                  var_288.gotoAndStop(40 + int(class_691.method_114(3)));
               }
               var_1411 = _loc1_;
               if(var_288.x > var_1414)
               {
                  var_288.x = var_1414;
                  method_727(-0.2,var_351);
               }
               if(name_5)
               {
                  var_248 = 0;
                  name_3 = 2;
                  var_1415 = var_232.method_84(class_899.__unprotect__("\x1804\x01\x02"),class_645.var_209);
                  var_1415.x = Number(var_288.x + var_101.x);
                  var_1415.y = Number(var_288.y + var_101.y);
                  break;
               }
               break;
            case 2:
               var_248 = var_248 - 0.05;
               _loc6_ = Reflect.field(var_1415,class_899.__unprotect__("@\x01"));
               _loc6_.rotation = var_248 / 0.0174;
               if(var_248 < -1.3 || !name_5)
               {
                  method_727(var_248,var_351);
                  var_1415.parent.removeChild(var_1415);
                  break;
               }
               break;
            case 3:
               var_1409 = Number(var_1409 + 1);
               var_1410 = var_1410 * var_1416;
               var_1409 = var_1409 * var_1416;
               var_288.x = Number(var_288.x + var_1410);
               var_288.y = Number(var_288.y + var_1409);
               var_288.rotation = Math.atan2(var_1409,var_1410) / 0.0174 * 0.8;
               _loc7_ = var_288.y < 0;
               _loc8_ = var_288.x > var_1414 && var_288.x < Number(var_1414 + var_1417);
               if(!_loc7_)
               {
                  if(var_859)
                  {
                     if(!_loc8_)
                     {
                        if(var_288.x > Number(var_1414 + var_1417))
                        {
                           method_81(true,25);
                           var_288.gotoAndPlay("$win");
                        }
                        else
                        {
                           method_81(false,20);
                           var_288.gotoAndPlay("$tooSoon");
                        }
                        var_288.y = 0;
                        var_288.rotation = 0;
                        name_3 = 4;
                     }
                     else
                     {
                        method_81(false,20);
                     }
                  }
                  else if(!_loc8_)
                  {
                     var_288.x = Number(Math.min(Number(Math.max(var_1414,var_288.x)),Number(var_1414 + var_1417)));
                     var_1410 = var_1410 * -0.9;
                  }
               }
               var_859 = _loc7_;
               break;
            case 4:
               var_1410 = var_1410 * 0.9;
               var_288.x = Number(var_288.x + var_1410);
               if(!var_220 && var_288.x > var_1414 - 10)
               {
                  var_288.x = var_1414 - 10;
                  var_1410 = 0;
                  break;
               }
            case 5:
               var_1410 = var_1410 * 0.9;
               var_288.x = Number(var_288.x + var_1410);
               if(!var_220 && var_288.x > var_1414 - 10)
               {
                  var_288.x = var_1414 - 10;
                  var_1410 = 0;
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_1055() : void
      {
         var _loc1_:Number = var_1418 - var_288.x;
         var _loc2_:Number = _loc1_ - var_101.x;
         var_101.x = Number(var_101.x + _loc2_ * 0.5);
      }
      
      public function method_727(param1:Number, param2:Number) : void
      {
         param2 = Number(param2 + 1.5);
         var_859 = true;
         name_3 = 3;
         var_1410 = Math.cos(param1) * param2;
         var_1409 = Math.sin(param1) * param2;
         var_288.gotoAndStop("$jump");
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [260];
         super.method_95(param1);
         var_1414 = 1000;
         var_1417 = Number(60 + param1 * 200);
         var_1419 = class_742.var_219 - 50;
         var_1416 = 0.99;
         var_1418 = 40;
         var_1412 = 0;
         var_1413 = 36;
         var_1411 = method_100();
         var_351 = 0;
         method_117();
         method_72();
      }
      
      public function method_1056() : void
      {
         var _loc4_:MovieClip = null;
         var_101 = var_232.method_92(class_645.var_209);
         var_101.y = var_1419;
         var_1421 = new class_755(var_101);
         var _loc1_:Number = 0;
         var _loc3_:int = 0;
         while(_loc1_ < var_1414)
         {
            _loc3_++;
            _loc1_ = Number(_loc1_ + (Number(40 * 0.2 + int(class_691.method_114(40)))));
            if(_loc1_ < var_1414)
            {
               _loc4_ = var_1421.method_84(class_899.__unprotect__("J\x07*L\x03"),0);
               _loc4_.x = _loc1_;
               _loc4_.y = 0;
               _loc4_.gotoAndStop(int(class_691.method_114(_loc4_.totalFrames)) + 1);
            }
            else
            {
               var_759 = var_1421.method_84(class_899.__unprotect__("\x06B^u"),0);
               var_759.x = var_1414;
               var_759.y = 0;
               _loc4_ = Reflect.field(var_759,class_899.__unprotect__("o5\x0bF\x01"));
               _loc4_.scaleX = var_1417 * 0.01;
               _loc4_ = Reflect.field(var_759,class_899.__unprotect__("`\x1a\x01"));
               _loc4_.x = var_1417;
            }
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("7rub\x03"),0);
         var_1422 = var_232.method_84(class_899.__unprotect__("E;0 \x03"),class_645.var_209);
         var_1422.x = 0;
         var_1422.y = var_1419;
         method_1056();
         var_288 = var_1421.method_84(class_899.__unprotect__("#t\x04\x01\x02"),3);
         var_288.stop();
         var_288.x = 0;
         var_288.y = 0;
      }
   }
}
