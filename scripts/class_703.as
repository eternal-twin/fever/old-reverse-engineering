package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_703 extends class_645
   {
      
      public static var var_574:int = 3;
      
      public static var var_575:int = 112;
       
      
      public var var_576:Array;
      
      public var var_251:Object;
      
      public var var_104:MovieClip;
      
      public var var_577:Array;
      
      public var var_582:Number;
      
      public var name_21:Array;
      
      public var var_581:Number;
      
      public var var_578:MovieClip;
      
      public var var_248:Number;
      
      public function class_703()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:class_656 = null;
         var _loc8_:class_892 = null;
         method_371();
         method_372();
         if(var_251 != null)
         {
            var_251 = var_251 - 1;
            if(var_251 < 0)
            {
               method_81(true);
            }
            _loc1_ = 0;
            _loc2_ = 0;
            _loc3_ = var_576;
            while(_loc2_ < int(_loc3_.length))
            {
               _loc4_ = _loc3_[_loc2_];
               _loc2_++;
               _loc5_ = 0;
               _loc6_ = 0;
               while(_loc6_ < int(_loc4_.length))
               {
                  _loc7_ = _loc4_[_loc6_];
                  _loc6_++;
                  _loc7_.var_66 = Number(Number(_loc7_.var_66) + (10 - _loc1_ * 3));
                  _loc7_.var_66 = _loc7_.var_66 * 1.1;
                  _loc5_++;
               }
               _loc1_++;
            }
         }
         else
         {
            _loc1_ = 0;
            _loc3_ = var_577;
            while(_loc1_ < int(_loc3_.length))
            {
               _loc8_ = _loc3_[_loc1_];
               _loc1_++;
               if(Number(_loc8_.method_115({
                  "L\x01":var_578.x,
                  "\x03\x01":var_578.y
               })) < 22)
               {
                  var_578.gotoAndPlay("death");
                  var_251 = 16;
                  var_229 = [true];
                  _loc8_.method_89();
               }
            }
         }
         super.method_79();
      }
      
      public function method_371() : void
      {
         var _loc2_:Array = null;
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc5_:class_656 = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:class_892 = null;
         var _loc15_:class_892 = null;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:int = 0;
         var _loc20_:Number = NaN;
         var _loc1_:int = 0;
         while(_loc1_ < int(var_576.length))
         {
            _loc2_ = var_576[_loc1_];
            _loc3_ = Number(name_21[_loc1_]);
            name_21[_loc1_] = (_loc3_ + class_703.var_574 / (Number(1 + _loc1_ * 0.8)) * (_loc1_ * 2 - 1)) % 628;
            _loc4_ = 0;
            while(_loc4_ < int(_loc2_.length))
            {
               _loc5_ = _loc2_[_loc4_];
               _loc6_ = Number(Number(_loc5_.var_65) + _loc3_ / 100);
               _loc5_.var_244 = Number(var_578.x + Math.cos(_loc6_) * _loc5_.var_66);
               _loc5_.var_245 = Number(var_578.y + Math.sin(_loc6_) * _loc5_.var_66);
               _loc7_ = 0;
               while(_loc7_ < int(var_577.length))
               {
                  _loc8_ = var_577[_loc7_];
                  _loc9_ = Number(_loc8_.method_115(_loc5_));
                  if(_loc9_ < 8)
                  {
                     _loc10_ = _loc5_.var_244 - _loc5_.var_579.var_244;
                     _loc11_ = _loc5_.var_245 - _loc5_.var_579.var_245;
                     _loc12_ = 0;
                     while(_loc12_ < 10)
                     {
                        _loc12_++;
                        _loc13_ = _loc12_;
                        _loc15_ = new class_892(var_232.method_84(class_899.__unprotect__("qz\'\x07\x03"),class_645.var_209));
                        _loc15_.var_233 = 0.99;
                        _loc14_ = _loc15_;
                        _loc16_ = Math.random() * 6.28;
                        _loc17_ = Number(Math.cos(_loc16_));
                        _loc18_ = Number(Math.sin(_loc16_));
                        _loc19_ = 3;
                        _loc20_ = Number(0.2 + Math.random() * 1.5);
                        _loc14_.var_244 = Number(_loc5_.var_244 + _loc17_ * _loc19_);
                        _loc14_.var_245 = Number(_loc5_.var_245 + _loc18_ * _loc19_);
                        _loc14_.var_278 = Number(_loc17_ * _loc20_ + _loc10_);
                        _loc14_.var_279 = Number(_loc18_ * _loc20_ + _loc11_);
                        _loc14_.var_580 = (Math.random() * 2 - 1) * 16;
                        _loc14_.var_251 = 12 + int(class_691.method_114(12));
                        _loc14_.var_233 = 0.97;
                        _loc14_.var_272 = 0;
                        _loc14_.method_118();
                        _loc14_.var_243.gotoAndStop(_loc13_ + 1);
                     }
                     _loc7_--;
                     var_577.splice(_loc7_,1);
                     _loc8_.method_89();
                     _loc4_--;
                     var_576[_loc1_].splice(_loc4_,1);
                     _loc5_.method_89();
                     break;
                  }
                  _loc7_++;
               }
               _loc5_.var_579 = {
                  "L\x01":_loc5_.var_244,
                  "\x03\x01":_loc5_.var_245
               };
               _loc4_++;
            }
            _loc1_++;
         }
      }
      
      public function method_372() : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc6_:int = 0;
         var _loc1_:* = method_100();
         var _loc2_:Number = Number(class_663.method_99(0,_loc1_.var_244 / class_742.var_217,1));
         var _loc3_:Number = 1.57 - (_loc2_ * 2 - 1) * 1.2;
         var_104.x = Number(var_578.x + Math.cos(_loc3_) * class_703.var_575);
         var_104.y = Number(var_578.y + Math.sin(_loc3_) * class_703.var_575);
         var_104.rotation = _loc3_ / 0.0174;
         if(var_581 > 0)
         {
            var_581 = var_581 - 1;
         }
         else if(name_5)
         {
            _loc3_ = _loc3_ - 3.14;
            var_581 = 16;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("-qnC"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            _loc4_.var_244 = var_104.x;
            _loc4_.var_245 = var_104.y;
            _loc6_ = 4;
            _loc4_.var_278 = Math.cos(_loc3_) * _loc6_;
            _loc4_.var_279 = Math.sin(_loc3_) * _loc6_;
            _loc4_.method_118();
            _loc4_.var_243.rotation = _loc3_ / 0.0174;
            var_577.push(_loc4_);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [480];
         super.method_95(param1);
         var_577 = [];
         var_581 = 0;
         var_582 = 23 - param1 * 12;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:class_656 = null;
         var _loc8_:class_656 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x15KM&\x03"),0);
         var_578 = var_232.method_84(class_899.__unprotect__("\x0erX|\x03"),class_645.var_209);
         var_578.x = class_742.var_217 * 0.5;
         var_578.y = class_742.var_219 * 0.5;
         var_576 = [];
         name_21 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            var_576[_loc2_] = [];
            name_21[_loc2_] = 0;
            _loc3_ = 44 + _loc2_ * 28;
            _loc4_ = int(6.28 * _loc3_ / var_582);
            _loc5_ = 0;
            while(_loc5_ < _loc4_)
            {
               _loc5_++;
               _loc6_ = _loc5_;
               _loc8_ = new class_656(var_232.method_84(class_899.__unprotect__("\'_NO\x02"),class_645.var_209));
               _loc7_ = _loc8_;
               _loc7_.var_65 = _loc6_ / _loc4_ * 6.28;
               _loc7_.var_244 = Number(var_578.x + Math.cos(Number(_loc7_.var_65)) * _loc3_);
               _loc7_.var_245 = Number(var_578.y + Math.sin(Number(_loc7_.var_65)) * _loc3_);
               _loc7_.var_66 = _loc3_;
               _loc7_.method_118();
               var_576[_loc2_].push(_loc7_);
            }
         }
         var_104 = var_232.method_84(class_899.__unprotect__("fd;6\x03"),class_645.var_209);
         var_104.x = var_578.x;
         var_104.y = Number(var_578.y + class_703.var_575);
      }
   }
}
