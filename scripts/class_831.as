package
{
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.geom.Point;
   import package_10.class_870;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_11.package_12.class_659;
   import package_16.class_675;
   import package_16.class_684;
   import package_17.class_709;
   import package_17.class_718;
   import package_17.class_800;
   import package_17.class_863;
   import package_20.package_21.class_893;
   import package_20.package_21.class_901;
   import package_24.class_738;
   import package_24.class_739;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   
   public class class_831 extends class_645
   {
      
      public static var var_370:Boolean;
      
      public static var var_1101:int = 16;
      
      public static var var_770:class_709;
      
      public static var var_1102:Number = 0.3;
       
      
      public var var_307:int;
      
      public var var_1105:class_755;
      
      public var name_49:Sprite;
      
      public var var_496:Array;
      
      public var var_1104:int;
      
      public var var_1103:Array;
      
      public function class_831()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc10_:class_689 = null;
         var _loc11_:Number = NaN;
         var _loc12_:Point = null;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:class_739 = null;
         var _loc16_:* = null;
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 / class_742.var_216 * 2 - 1;
         name_49.rotation = Number(name_49.rotation + _loc2_ * 10);
         var _loc3_:Number = (90 - name_49.rotation) * 0.0174;
         var _loc4_:Number = Math.cos(_loc3_) * class_831.var_1102;
         var _loc5_:Number = Math.sin(_loc3_) * class_831.var_1102;
         var _loc6_:class_718 = package_13.var_661;
         _loc6_.var_244 = _loc4_;
         _loc6_.var_245 = _loc5_;
         package_13.name_3(1,5);
         var _loc7_:Array = var_1103.method_78();
         var _loc8_:Boolean = true;
         var _loc9_:int = 0;
         while(_loc9_ < int(_loc7_.length))
         {
            _loc10_ = _loc7_[_loc9_];
            _loc9_++;
            _loc10_.var_243.rotation = -name_49.rotation;
            _loc11_ = var_1104 * class_831.var_1101 * 0.5;
            if(Number(Math.abs(_loc10_.var_244)) > _loc11_ || Number(Math.abs(_loc10_.var_245)) > _loc11_ || _loc10_.var_344 < 1 || _loc10_.var_243.tabIndex > 5)
            {
               _loc10_.method_119(_loc10_.var_344 * 0.9);
               class_657.gfof(_loc10_.var_243,1 - _loc10_.var_344 * 0.01,16777215);
               _loc12_ = new Point(_loc10_.var_244,_loc10_.var_245);
               _loc12_ = name_49.localToGlobal(_loc12_);
               _loc13_ = Math.random() * 6.28;
               _loc14_ = Number(0.1 + Number(Math.random()));
               _loc15_ = new class_739();
               _loc15_.method_137(class_702.package_9.method_136("spark_grow"));
               _loc15_.var_262 = _loc12_.x;
               _loc15_.var_263 = _loc12_.y;
               _loc15_.method_118();
               _loc15_.var_250 = -(0.1 + Math.random() * 0.1);
               _loc15_.var_278 = Math.cos(_loc13_) * _loc14_;
               _loc15_.var_279 = Math.sin(_loc13_) * _loc14_;
               _loc15_.var_279 = Number(_loc15_.var_279 + (Number(0.5 + Math.random() * 1.5)));
               var_232.method_124(_loc15_,2);
               _loc15_.var_251 = 10 + int(class_691.method_114(40));
               _loc15_.var_198.method_140();
               _loc15_.var_233 = 0.99;
               if(_loc10_.var_344 < 0.2)
               {
                  _loc10_.method_89();
                  var_1103.method_116(_loc10_);
               }
            }
            else
            {
               _loc8_ = false;
               _loc16_ = {
                  "L\x01":int(Number(_loc10_.var_243.x / class_831.var_1101 + var_1104 * 0.5)),
                  "\x03\x01":int(Number(_loc10_.var_243.y / class_831.var_1101 + var_1104 * 0.5))
               };
               if(method_222(int(_loc16_.var_244),int(_loc16_.var_245)) && Boolean(var_496[int(_loc16_.var_244)][int(_loc16_.var_245)]))
               {
                  _loc10_.var_243.tabIndex = _loc10_.var_243.tabIndex + 1;
               }
            }
         }
         if(_loc8_)
         {
            method_81(true,20);
         }
         super.method_79();
         _loc9_ = 0;
         var _loc17_:Array = var_1103;
         while(_loc9_ < int(_loc17_.length))
         {
            _loc10_ = _loc17_[_loc9_];
            _loc9_++;
            _loc10_.var_243.x = int(_loc10_.var_243.x);
            _loc10_.var_243.y = int(_loc10_.var_243.y);
         }
      }
      
      public function method_222(param1:int, param2:int) : Boolean
      {
         return param1 >= 0 && param1 < var_1104 && param2 >= 0 && param2 < var_1104;
      }
      
      public function method_808(param1:Number, param2:Number) : void
      {
         var _loc3_:class_738 = new class_738();
         _loc3_.method_128(class_702.var_571.method_98(null,"laby_ball"));
         var _loc4_:MovieClip = new MovieClip();
         _loc4_.addChild(_loc3_);
         var_1105.method_124(_loc4_,2);
         var _loc5_:* = {
            "L\x01":(param1 - var_1104 * 0.5) * class_831.var_1101,
            "\x03\x01":(param2 - var_1104 * 0.5) * class_831.var_1101
         };
         _loc4_.x = Number(_loc5_.var_244);
         _loc4_.y = Number(_loc5_.var_245);
         _loc4_.tabIndex = 0;
         var _loc6_:class_689 = new class_689(_loc4_);
         _loc6_.var_536 = this;
         _loc6_.var_538 = class_831.var_770;
         _loc6_.method_129(_loc4_.x,_loc4_.y);
         _loc6_.method_349(0);
         _loc6_.method_345(4);
         _loc6_.var_535 = false;
         var_1103.push(_loc6_);
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:class_738 = null;
         var _loc11_:* = null;
         var _loc12_:class_863 = null;
         var_227 = [600 - param1 * 100];
         super.method_95(param1);
         var_307 = 6;
         class_870.name_18(16777215);
         class_319 = new MovieClip();
         class_319.graphics.beginFill(4456448);
         class_319.graphics.drawRect(0,0,class_742.var_216,class_742.var_218);
         var_232.method_124(class_319,0);
         method_809();
         name_49 = new Sprite();
         name_49.x = class_742.var_216 * 0.5;
         name_49.y = class_742.var_218 * 0.5;
         var_232.method_124(name_49,2);
         var_1105 = new class_755(name_49);
         var _loc2_:int = int((var_1104 + 1) * 16 * 0.5);
         var _loc3_:class_893 = new class_893(-_loc2_,-_loc2_,_loc2_ * 2,_loc2_ * 2);
         package_13 = new package_17.class_652(_loc3_,new class_901());
         package_13.var_678 = 0;
         method_91();
         var_1103 = [];
         var _loc4_:int = 0;
         var _loc5_:int = var_1104;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = var_1104;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               if(!var_496[_loc6_][_loc9_])
               {
                  method_808(Number(_loc6_ + 0.5),Number(_loc9_ + 0.5));
               }
               else
               {
                  _loc10_ = new class_738();
                  _loc10_.method_128(class_702.var_571.method_98(null,"laby_ball_wall"),0,0);
                  _loc11_ = {
                     "L\x01":(_loc6_ - var_1104 * 0.5) * class_831.var_1101,
                     "\x03\x01":(_loc9_ - var_1104 * 0.5) * class_831.var_1101
                  };
                  _loc10_.x = Number(_loc11_.var_244);
                  _loc10_.y = Number(_loc11_.var_245);
                  var_1105.method_124(_loc10_,1);
                  _loc12_ = class_800.method_346(class_831.var_1101,class_831.var_1101,_loc10_.x,_loc10_.y);
                  package_13.method_513(_loc12_);
               }
            }
         }
      }
      
      public function method_127(param1:Number, param2:Number) : Object
      {
         return {
            "L\x01":(param1 - var_1104 * 0.5) * class_831.var_1101,
            "\x03\x01":(param2 - var_1104 * 0.5) * class_831.var_1101
         };
      }
      
      public function method_810(param1:Number, param2:Number) : Object
      {
         return {
            "L\x01":int(Number(param1 / class_831.var_1101 + var_1104 * 0.5)),
            "\x03\x01":int(Number(param2 / class_831.var_1101 + var_1104 * 0.5))
         };
      }
      
      public function method_809() : void
      {
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc11_:* = null;
         var _loc12_:Boolean = false;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:Array = null;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:int = 0;
         var _loc19_:int = 0;
         var _loc20_:Array = null;
         var _loc21_:* = null;
         var _loc1_:class_684 = new class_684(var_307,var_307,int(class_691.method_114(2000)));
         _loc1_.var_502 = 1;
         _loc1_.var_506 = 8;
         var _loc2_:int = int(var_307 * 0.5);
         _loc1_.var_390 = {
            "L\x01":_loc2_,
            "\x03\x01":_loc2_
         };
         _loc1_.var_501 = 100;
         _loc1_.method_312();
         while(!_loc1_.var_302)
         {
            _loc1_.method_79();
         }
         var _loc3_:class_675 = new class_675(_loc1_);
         var_496 = [];
         var_1104 = var_307 * 2 + 1;
         var _loc4_:int = 0;
         var _loc5_:int = var_1104;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            var_496[_loc6_] = [];
            _loc7_ = 0;
            _loc8_ = var_1104;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               var_496[_loc6_][_loc9_] = _loc6_ == 0 || _loc9_ == 0;
            }
         }
         var _loc10_:Array = [[1,0],[0,1],[1,1]];
         _loc4_ = 0;
         _loc5_ = var_307;
         while(_loc4_ < _loc5_)
         {
            _loc4_++;
            _loc6_ = _loc4_;
            _loc7_ = 0;
            _loc8_ = var_307;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               _loc11_ = _loc3_.method_285(_loc6_,_loc9_);
               _loc12_ = false;
               _loc13_ = 0;
               while(_loc13_ < 3)
               {
                  _loc13_++;
                  _loc14_ = _loc13_;
                  _loc15_ = _loc10_[_loc14_];
                  _loc16_ = 1 + _loc6_ * 2 + int(_loc15_[0]);
                  _loc17_ = 1 + _loc9_ * 2 + int(_loc15_[1]);
                  if(_loc14_ < 2 && Boolean(_loc11_.var_480[_loc14_]))
                  {
                     _loc12_ = true;
                     var_496[_loc16_][_loc17_] = true;
                  }
                  if(_loc14_ == 2)
                  {
                     if(!_loc12_)
                     {
                        _loc18_ = 0;
                        while(_loc18_ < 2)
                        {
                           _loc18_++;
                           _loc19_ = _loc18_;
                           _loc20_ = class_742.var_264[_loc19_];
                           _loc21_ = _loc3_.method_285(_loc6_ + int(_loc20_[0]),_loc9_ + int(_loc20_[1]));
                           if(_loc21_ == null || Boolean(_loc21_.var_480[1 - _loc19_]))
                           {
                              _loc12_ = true;
                              break;
                           }
                        }
                     }
                     if(_loc12_)
                     {
                        var_496[_loc16_][_loc17_] = true;
                     }
                  }
               }
            }
         }
         _loc15_ = [];
         _loc4_ = 1;
         _loc5_ = var_1104 - _loc4_ * 2;
         _loc6_ = _loc4_;
         while(_loc6_ < _loc5_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc15_.push({
               "L\x01":_loc7_,
               "\x03\x01":0
            });
            _loc15_.push({
               "L\x01":_loc7_,
               "\x03\x01":var_1104 - 1
            });
         }
         _loc6_ = _loc4_;
         while(_loc6_ < _loc5_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc15_.push({
               "L\x01":0,
               "\x03\x01":_loc7_
            });
            _loc15_.push({
               "L\x01":var_1104 - 1,
               "\x03\x01":_loc7_
            });
         }
         class_659.method_241(_loc15_);
         _loc6_ = int(int(_loc15_.length) * (1 - Math.pow(var_237[0],1.3)));
         _loc7_ = 2;
         if(var_237[0] > 1.1)
         {
            _loc7_ = 1;
         }
         if(_loc6_ < _loc7_)
         {
            _loc6_ = _loc7_;
         }
         _loc8_ = 0;
         _loc9_ = 1 + int(class_691.method_114(var_307)) * 2;
         _loc13_ = 0;
         while(_loc13_ < int(_loc15_.length))
         {
            _loc11_ = _loc15_[_loc13_];
            _loc13_++;
            if(int(_loc11_.var_244) == _loc8_ && int(_loc11_.var_245) == _loc9_)
            {
               _loc15_.method_116(_loc11_);
               break;
            }
         }
         _loc15_.push({
            "L\x01":_loc8_,
            "\x03\x01":_loc9_
         });
         _loc13_ = 0;
         while(_loc13_ < _loc6_)
         {
            _loc13_++;
            _loc14_ = _loc13_;
            _loc11_ = _loc15_.pop();
            var_496[int(_loc11_.var_244)][int(_loc11_.var_245)] = false;
         }
      }
   }
}
