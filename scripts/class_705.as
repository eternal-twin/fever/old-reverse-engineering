package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_705 extends class_645
   {
      
      public static var var_350:int = 220;
      
      public static var var_583:int = 15;
      
      public static var var_493:Array = [16,22,31,50];
       
      
      public var var_587:Number;
      
      public var var_251:Number;
      
      public var var_584:class_892;
      
      public var var_307:int;
      
      public var var_352:Number;
      
      public var var_531:MovieClip;
      
      public var var_586:Number;
      
      public var var_585:MovieClip;
      
      public var var_142:Number;
      
      public var var_430:Number;
      
      public var var_494:Boolean;
      
      public var name_21:Number;
      
      public function class_705()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_373();
               method_374();
               if(var_227[0] < var_142 || var_227[0] < 150 && Number(Math.abs(var_584.var_244 - var_585.x)) + int(class_691.method_114(200)) - int(class_705.var_493[var_307]) < 2)
               {
                  name_3 = 2;
                  var_584.var_278 = 0;
                  var_584.var_279 = var_584.var_279 - 6;
                  break;
               }
               break;
            case 2:
               method_374();
               var_584.var_279 = Number(var_584.var_279 + 6);
               if(var_584.var_245 > class_705.var_350)
               {
                  method_375();
                  break;
               }
               break;
            case 3:
               if(var_585 != null)
               {
                  var_585.y = Number(var_585.y + var_586);
                  var_586 = Number(var_586 + 1);
                  if(var_585.y > class_705.var_350)
                  {
                     var_585.y = class_705.var_350;
                     var_586 = var_586 * -0.4;
                  }
               }
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(var_494,20);
                  break;
               }
         }
         var_584.var_243.x = var_584.var_244;
         var_584.var_243.y = var_584.var_245;
         if(var_352 > 0.1)
         {
            var_352 = var_352 * 0.6;
            var_201.y = (Math.random() * 2 - 1) * var_352;
         }
      }
      
      public function method_375() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:MovieClip = null;
         var _loc5_:MovieClip = null;
         var _loc6_:MovieClip = null;
         var _loc7_:class_892 = null;
         var_584.var_245 = class_705.var_350;
         var_584.var_279 = 0;
         var _loc1_:Number = var_585.x - var_584.var_244;
         var_494 = Number(Math.abs(_loc1_)) > int(class_705.var_493[var_307]);
         if(var_494)
         {
            var_586 = -8;
         }
         else
         {
            _loc2_ = 0;
            while(_loc2_ < 8)
            {
               _loc2_++;
               _loc3_ = _loc2_;
               _loc4_ = var_232.method_84(class_899.__unprotect__("\'z{,"),class_645.var_209);
               _loc6_ = Reflect.field(_loc4_,class_899.__unprotect__("-\x01"));
               _loc5_ = _loc6_;
               _loc5_.gotoAndPlay(int(class_691.method_114(_loc5_.totalFrames + 1)));
               _loc7_ = new class_892(_loc4_);
               _loc7_.var_250 = Number(0.1 + Math.random() * 0.2);
               _loc7_.var_278 = (Math.random() * 2 - 1) * 3;
               _loc7_.var_279 = -Math.random() * 4;
               _loc7_.name_20 = int(class_691.method_114(3));
               _loc7_.var_244 = var_585.x;
               _loc7_.var_245 = var_585.y - 40;
               _loc7_.method_118();
            }
            var_585.parent.removeChild(var_585);
            var_585 = null;
         }
         name_3 = 3;
         var_251 = 8;
         var_352 = 5 * (var_307 + 1);
      }
      
      public function method_373() : void
      {
         name_21 = (name_21 + 10) % 628;
         var_587 = Number(var_585.x + Math.cos(name_21 / 100) * 40);
         var_584.method_358({
            "L\x01":var_587,
            "\x03\x01":var_584.var_245
         },0.2,2);
         var_531.x = var_584.var_244;
         var _loc1_:int = int(class_705.var_493[var_307]);
         var_584.var_244 = Number(class_663.method_99(_loc1_,var_584.var_244,class_742.var_217 - _loc1_));
      }
      
      public function method_374() : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc1_:Number = method_100().var_244 - var_585.x;
         if(Number(Math.abs(_loc1_)) > 5)
         {
            _loc2_ = 6 - var_237[0];
            _loc3_ = Number(class_663.method_99(-_loc2_,_loc1_ * 0.1,_loc2_));
            _loc4_ = 14;
            var_585.x = Number(class_663.method_99(_loc4_,Number(var_585.x + _loc3_),class_742.var_217 - _loc4_));
            var_585.scaleX = _loc1_ < 0?-1:1;
            var_430 = Number(var_430 + _loc3_ * 0.2);
            while(var_430 < 0)
            {
               var_430 = Number(var_430 + class_705.var_583);
            }
            while(var_430 >= class_705.var_583)
            {
               var_430 = var_430 - class_705.var_583;
            }
            var_585.gotoAndStop(int(var_430) + 1);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [180];
         super.method_95(param1);
         var_430 = 0;
         name_21 = 0;
         var_587 = class_742.var_217 * 0.5;
         var_142 = Number(30 + Math.random() * 80);
         var_307 = int(Math.round(param1 * 3));
         if(var_307 > 3)
         {
            var_307 = 3;
         }
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x16`Ib\x01"),0);
         var_531 = var_232.method_84(class_899.__unprotect__("VfH\x01\x01"),class_645.var_209);
         var_531.x = class_742.var_217 * 1.2;
         var_531.y = class_705.var_350;
         var_531.scaleX = int(class_705.var_493[var_307]) * 0.02;
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x05w\x11Y\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_584 = _loc1_;
         var_584.var_244 = class_742.var_217 * 1.2;
         var_584.var_245 = 40;
         var_584.method_118();
         var_584.var_243.gotoAndStop(var_307 + 1);
         var_584.var_233 = 0.92;
         var_585 = var_232.method_84(class_899.__unprotect__("B\x10{{\x01"),class_645.var_209);
         var_585.x = class_742.var_217 * 0.5;
         var_585.y = class_705.var_350;
         var_585.stop();
      }
   }
}
