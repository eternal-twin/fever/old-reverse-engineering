package package_27.package_28
{
   import flash.utils.ByteArray;
   import package_32.class_899;
   
   public class class_768
   {
       
      
      public var var_861:Boolean;
      
      public function class_768()
      {
      }
      
      public function method_605(param1:int) : void
      {
         if(param1 < 0 || param1 >= 1073741824)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         if(var_861)
         {
            method_604(param1 >>> 24);
            method_604(param1 >> 16 & 255);
            method_604(param1 >> 8 & 255);
            method_604(param1 & 255);
         }
         else
         {
            method_604(param1 & 255);
            method_604(param1 >> 8 & 255);
            method_604(param1 >> 16 & 255);
            method_604(param1 >>> 24);
         }
      }
      
      public function method_606(param1:int) : void
      {
         if(param1 < 0 || param1 >= 16777216)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         if(var_861)
         {
            method_604(param1 >> 16);
            method_604(param1 >> 8 & 255);
            method_604(param1 & 255);
         }
         else
         {
            method_604(param1 & 255);
            method_604(param1 >> 8 & 255);
            method_604(param1 >> 16);
         }
      }
      
      public function method_607(param1:int) : void
      {
         if(param1 < 0 || param1 >= 65536)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         if(var_861)
         {
            method_604(param1 >> 8);
            method_604(param1 & 255);
         }
         else
         {
            method_604(param1 & 255);
            method_604(param1 >> 8);
         }
      }
      
      public function method_609(param1:String) : void
      {
         var _loc2_:Bytes = Bytes.ofString(param1);
         method_608(_loc2_,0,_loc2_.length);
      }
      
      public function method_610(param1:int) : void
      {
         if(param1 < -128 || param1 >= 128)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         method_604(param1 & 255);
      }
      
      public function method_611(param1:int) : void
      {
         var _loc2_:int = 0;
         if(var_861)
         {
            _loc2_ = param1 >>> 24;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_);
            _loc2_ = param1 >>> 16;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_ & 255);
            _loc2_ = param1 >>> 8;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_ & 255);
            _loc2_ = param1 & 255;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_);
         }
         else
         {
            _loc2_ = param1 & 255;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_);
            _loc2_ = param1 >>> 8;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_ & 255);
            _loc2_ = param1 >>> 16;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_ & 255);
            _loc2_ = param1 >>> 24;
            §§push();
            if((_loc2_ >> 30 & 1) != _loc2_ >>> 31)
            {
               class_899.var_317 = new Error();
               throw "Overflow " + _loc2_;
            }
            §§pop().method_604(_loc2_);
         }
      }
      
      public function method_612(param1:int) : void
      {
         if(param1 < -1073741824 || param1 >= 1073741824)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         if(var_861)
         {
            method_604(param1 >>> 24);
            method_604(param1 >> 16 & 255);
            method_604(param1 >> 8 & 255);
            method_604(param1 & 255);
         }
         else
         {
            method_604(param1 & 255);
            method_604(param1 >> 8 & 255);
            method_604(param1 >> 16 & 255);
            method_604(param1 >>> 24);
         }
      }
      
      public function method_613(param1:int) : void
      {
         if(param1 < -8388608 || param1 >= 8388608)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         method_606(param1 & 16777215);
      }
      
      public function method_614(param1:int) : void
      {
         if(param1 < -32768 || param1 >= 32768)
         {
            class_899.var_317 = new Error();
            throw class_780.var_860;
         }
         method_607(param1 & 65535);
      }
      
      public function method_617(param1:class_855, param2:Object = undefined) : void
      {
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_781 = null;
         if(param2 == null)
         {
            param2 = 4096;
         }
         var _loc4_:Bytes = Bytes.alloc(param2);
         while(true)
         {
            try
            {
               continue;
               return;
            }
            catch(:class_781;)
            {
               _loc8_ = ;
               return;
            }
            return;
         }
         class_899.var_317 = new Error();
         throw class_780.var_862;
      }
      
      public function method_608(param1:Bytes, param2:int, param3:int) : void
      {
         var _loc4_:int = 0;
         while(param3 > 0)
         {
            _loc4_ = int(method_616(param1,param2,param3));
            param2 = param2 + _loc4_;
            param3 = param3 - _loc4_;
         }
      }
      
      public function method_618(param1:Number) : void
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_619(param1:Number) : void
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_616(param1:Bytes, param2:int, param3:int) : int
      {
         var _loc4_:int = param3;
         var _loc5_:ByteArray = param1.b;
         if(param2 < 0 || param3 < 0 || param2 + param3 > param1.length)
         {
            class_899.var_317 = new Error();
            throw class_780.var_768;
         }
         while(_loc4_ > 0)
         {
            method_604(int(_loc5_[param2]));
            param2++;
            _loc4_--;
         }
         return param3;
      }
      
      public function method_604(param1:int) : void
      {
         class_899.var_317 = new Error();
         throw "Not implemented";
      }
      
      public function method_620(param1:Bytes) : void
      {
         var _loc4_:int = 0;
         var _loc2_:int = param1.length;
         var _loc3_:int = 0;
         while(_loc2_ > 0)
         {
            _loc4_ = int(method_616(param1,_loc3_,_loc2_));
            if(_loc4_ == 0)
            {
               class_899.var_317 = new Error();
               throw class_780.var_862;
            }
            _loc3_ = _loc3_ + _loc4_;
            _loc2_ = _loc2_ - _loc4_;
         }
      }
      
      public function method_621(param1:Boolean) : Boolean
      {
         var_861 = param1;
         return param1;
      }
      
      public function method_622(param1:int) : void
      {
      }
      
      public function method_623() : void
      {
      }
      
      public function method_624() : void
      {
      }
   }
}
