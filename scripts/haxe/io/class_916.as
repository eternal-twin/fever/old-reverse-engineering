package package_27.package_28
{
   import flash.utils.ByteArray;
   import package_32.class_899;
   
   public class class_916
   {
       
      
      public var var_12:ByteArray;
      
      public function class_916()
      {
         if(class_899.var_239)
         {
            return;
         }
         var_12 = new ByteArray();
      }
      
      public function method_914() : Bytes
      {
         var _loc1_:Bytes = new Bytes(var_12.length,var_12);
         var_12.position = 0;
         var_12 = null;
         return _loc1_;
      }
      
      public function method_1068(param1:Bytes, param2:int, param3:int) : void
      {
         if(param2 < 0 || param3 < 0 || param2 + param3 > param1.length)
         {
            class_899.var_317 = new Error();
            throw class_780.var_768;
         }
         var_12.writeBytes(param1.b,param2,param3);
      }
      
      public function method_1069(param1:int) : void
      {
         var_12.writeByte(param1);
      }
      
      public function method_124(param1:Bytes) : void
      {
         var_12.writeBytes(param1.b);
      }
   }
}
