package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_739;
   import package_8.package_9.class_931;
   
   public class class_923 extends class_645
   {
      
      public static var var_493:int = 30;
      
      public static var var_213:int = 4;
       
      
      public var var_251:Number;
      
      public var var_181:class_352;
      
      public var var_819:Number;
      
      public var var_351:Number;
      
      public var var_295:int;
      
      public var iUkn:Array;
      
      public var var_714:Number;
      
      public var var_710:Number;
      
      public var var_1517:Number;
      
      public var var_488:Array;
      
      public function class_923()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc6_:Array = null;
         var _loc7_:* = null;
         var _loc8_:* = null;
         var _loc9_:int = 0;
         var _loc10_:class_739 = null;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1118();
               var_710 = var_710 - 1;
               if(var_710 < 0)
               {
                  if(var_295 < class_923.var_213)
                  {
                     var_295 = var_295 + 1;
                     var_710 = Number(var_1517 + Math.random() * var_1517);
                     _loc1_ = Math.random() * 628;
                     _loc2_ = Number(var_819 + Math.random() * var_819);
                     _loc3_ = 0;
                     while(_loc3_ < 2)
                     {
                        _loc3_++;
                        _loc4_ = _loc3_;
                        _loc5_ = var_232.method_84(class_899.__unprotect__("kneq\x03"),class_645.var_209);
                        _loc5_.gotoAndStop(var_295 + 1 + _loc4_ * 10);
                        _loc5_.y = -20;
                        if(_loc4_ == 1)
                        {
                           var_232.method_521(_loc5_);
                        }
                        var_488.push({
                           "gB\x01":_loc5_,
                           "^%\x12P\x01":_loc1_,
                           "DRO)\x01":true,
                           "P,\x01":_loc2_
                        });
                     }
                  }
               }
               _loc6_ = var_488.method_78();
               _loc3_ = 0;
               while(_loc3_ < int(_loc6_.length))
               {
                  _loc7_ = _loc6_[_loc3_];
                  _loc3_++;
                  _loc5_ = _loc7_.var_631;
                  _loc1_ = class_742.var_217 * 0.5;
                  _loc7_.var_700 = (Number(_loc7_.var_700) + Number(_loc7_.name_149)) % 628;
                  _loc5_.x = Number(class_742.var_217 * 0.5 + Math.cos(_loc7_.var_700 / 100) * ((class_742.var_217 - class_923.var_493 * 2) * 0.42));
                  _loc5_.y = Number(_loc5_.y + var_351);
                  if(_loc7_.var_604 && _loc5_.y > var_714)
                  {
                     _loc5_.y = var_714;
                     _loc7_.var_604 = false;
                     _loc8_ = iUkn[int(iUkn.length) - 2];
                     _loc2_ = _loc8_.var_631.x - _loc5_.x;
                     if(Number(Math.abs(_loc2_)) < class_923.var_493 * 0.8)
                     {
                        iUkn.push({
                           "gB\x01":_loc5_,
                           "4j\x01":_loc8_.var_336 - _loc2_
                        });
                        var_488.method_116(_loc7_);
                     }
                  }
                  if(_loc5_.y > class_742.var_219)
                  {
                     method_81(false,20);
                  }
               }
               if(int(var_488.length) == 0 && var_295 == class_923.var_213)
               {
                  var_251 = 0;
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 2:
               method_282();
               _loc1_ = var_251;
               var_251 = var_251 + 1;
               if(_loc1_ > 6)
               {
                  name_3 = name_3 + 1;
                  method_81(true,24);
                  new class_931(var_201,0.2);
                  var_181 = new class_352();
                  class_319.addChild(var_181);
                  var_181.x = class_742.var_217 * 0.5;
                  var_181.y = class_742.var_219 * 0.5;
                  var_181.stop();
                  _loc3_ = 120;
                  _loc4_ = 0;
                  while(_loc4_ < _loc3_)
                  {
                     _loc4_++;
                     _loc9_ = _loc4_;
                     _loc10_ = new class_739(new class_355());
                     _loc10_.var_244 = Math.random() * class_742.var_217;
                     _loc10_.var_245 = Math.random() * class_742.var_219;
                     class_657.name_18(_loc10_.var_243,int(class_657.method_138(Number(Math.random()))));
                     _loc10_.method_1014(10);
                     _loc10_.var_250 = Number(0.1 + Math.random() * 0.1);
                     _loc10_.var_279 = -Math.random() * 3;
                     _loc10_.var_233 = 0.97;
                     _loc10_.method_118();
                     _loc10_.var_243.gotoAndPlay(int(class_691.method_114(_loc10_.var_243.totalFrames)) + 1);
                     var_232.method_124(_loc10_.var_243,5);
                     _loc10_.method_119(0.7);
                     _loc1_ = _loc10_.var_244 - var_181.y;
                     _loc2_ = _loc10_.var_245 - var_181.y;
                     _loc11_ = Number(Math.atan2(_loc2_,_loc1_));
                     _loc12_ = Number(Math.max(10 - Math.sqrt(Number(_loc1_ * _loc1_ + _loc2_ * _loc2_)) * 0.1,0));
                     _loc10_.var_278 = Number(_loc10_.var_278 + Math.cos(_loc11_) * _loc12_);
                     _loc10_.var_279 = Number(_loc10_.var_279 + Math.sin(_loc11_) * _loc12_);
                  }
                  break;
               }
               break;
            case 3:
               var_181.rotation = Number(var_181.rotation + 3);
         }
         super.method_79();
      }
      
      public function method_1118() : void
      {
         var _loc3_:* = null;
         var _loc1_:int = 0;
         var _loc2_:Array = iUkn;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_631.x = Number(Number(class_663.method_99(class_923.var_493,Number(method_100().var_244),class_742.var_217 - class_923.var_493)) + Number(_loc3_.var_336));
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [650];
         super.method_95(param1);
         var_714 = class_742.var_219 - 30;
         var_351 = Number(1.5 + param1 * 2.5);
         var_819 = Number(3 + param1 * 8);
         var_710 = 0;
         var_1517 = 35 - param1 * 20;
         var_295 = 0;
         var_488 = [];
         method_117();
         method_72();
      }
      
      public function method_282() : void
      {
         var _loc6_:* = null;
         var _loc1_:Number = class_742.var_217 * 0.5;
         var _loc2_:Number = Number(class_742.var_219 * 0.5 + 14);
         var _loc4_:int = 0;
         var _loc5_:Array = iUkn;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc6_.var_631.x = Number(_loc6_.var_631.x + (_loc1_ + Number(_loc6_.var_336) - _loc6_.var_631.x) * 0.5);
            _loc6_.var_631.y = Number(_loc6_.var_631.y + (_loc2_ - _loc6_.var_631.y) * 0.5);
         }
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("\x1bso\x13\x03"),0);
         iUkn = [];
         var _loc1_:int = 0;
         while(_loc1_ < 2)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = var_232.method_84(class_899.__unprotect__("kneq\x03"),class_645.var_209);
            _loc3_.gotoAndStop(11 - _loc2_ * 10);
            _loc3_.y = var_714;
            iUkn.push({
               "gB\x01":_loc3_,
               "4j\x01":0
            });
         }
      }
   }
}
