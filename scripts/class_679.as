package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_10.class_769;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_679 extends class_645
   {
      
      public static var var_465:Array = [[3,2],[4,2],[5,2],[4,3],[4,4],[6,3]];
      
      public static var var_466:int = 48;
      
      public static var var_467:int = 72;
       
      
      public var var_341:int;
      
      public var var_340:int;
      
      public var var_468:int;
      
      public var var_469:Number;
      
      public var var_474:class_656;
      
      public var var_473:class_656;
      
      public var var_295:int;
      
      public var var_472:Object;
      
      public var var_470:Array;
      
      public function class_679()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Boolean = false;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:class_656 = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = true;
               _loc2_ = 0;
               _loc3_ = var_470;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  if(_loc4_.name_46)
                  {
                     _loc1_ = false;
                     _loc4_.var_251 = Number(_loc4_.var_251) - 1;
                     if(Number(_loc4_.var_251) <= 0)
                     {
                        if(!_loc4_.var_471)
                        {
                           _loc4_.var_251 = var_469;
                           _loc4_.var_243.gotoAndPlay("face");
                           _loc4_.var_471 = true;
                        }
                        else
                        {
                           _loc4_.name_46 = false;
                           _loc4_.var_243.gotoAndPlay("back");
                           _loc4_.var_471 = false;
                        }
                     }
                  }
                  else if(_loc4_.var_243.currentFrame > 1)
                  {
                     _loc1_ = false;
                  }
               }
               if(_loc1_)
               {
                  _loc2_ = 0;
                  _loc3_ = var_470;
                  while(_loc2_ < int(_loc3_.length))
                  {
                     _loc4_ = _loc3_[_loc2_];
                     _loc2_++;
                     method_302(_loc4_);
                  }
                  name_3 = 2;
                  break;
               }
               break;
            case 2:
               if(var_472 != null)
               {
                  var_472 = var_472 - 1;
                  if(var_472 < 0)
                  {
                     var_472 = null;
                     var_473.var_243.gotoAndPlay("back");
                     var_474.var_243.gotoAndPlay("back");
                     method_302(var_473);
                     method_302(var_474);
                     var_473 = null;
                     var_474 = null;
                  }
               }
               if(int(int(var_470.length) * 0.5) == var_468)
               {
                  method_81(true,20);
                  name_3 = 3;
                  break;
               }
            case 3:
               if(var_472 != null)
               {
                  var_472 = var_472 - 1;
                  if(var_472 < 0)
                  {
                     var_472 = null;
                     var_473.var_243.gotoAndPlay("back");
                     var_474.var_243.gotoAndPlay("back");
                     method_302(var_473);
                     method_302(var_474);
                     var_473 = null;
                     var_474 = null;
                  }
               }
               if(int(int(var_470.length) * 0.5) == var_468)
               {
                  method_81(true,20);
                  name_3 = 3;
                  break;
               }
         }
         super.method_79();
      }
      
      public function name_48(param1:class_656) : void
      {
         var _loc2_:Array = null;
         var _loc3_:int = 0;
         if(var_474 != null)
         {
            return;
         }
         param1.var_243.removeEventListener(MouseEvent.CLICK,param1.var_27);
         param1.var_243.gotoAndPlay("face");
         if(var_473 == null)
         {
            var_473 = param1;
         }
         else if(int(param1.var_215) == int(var_473.var_215))
         {
            _loc2_ = [var_473,param1];
            _loc3_ = 0;
            while(_loc3_ < int(_loc2_.length))
            {
               var var_475:Array = [_loc2_[_loc3_]];
               _loc3_++;
               class_769.name_47(function(param1:Array):Function
               {
                  var var_475:Array = param1;
                  return function():void
                  {
                     var _loc1_:class_931 = new class_931(var_475[0].var_243);
                     _loc1_.var_146(3,6);
                  };
               }(var_475),280);
            }
            var_468 = var_468 + 1;
            var_473 = null;
         }
         else
         {
            var_474 = param1;
            var_472 = 18;
         }
      }
      
      public function method_241(param1:Array) : *
      {
         var _loc3_:int = 0;
         var _loc2_:Array = [];
         while(int(param1.length) > 0)
         {
            _loc3_ = int(class_691.method_114(int(param1.length)));
            _loc2_.push(param1[_loc3_]);
            param1.splice(_loc3_,1);
         }
         return _loc2_;
      }
      
      public function method_302(param1:class_656) : void
      {
         var var_87:class_656 = param1;
         var var_214:class_679 = this;
         var_87.var_27 = function(param1:*):void
         {
            var_214.name_48(var_87);
         };
         var_87.var_243.addEventListener(MouseEvent.CLICK,var_87.var_27);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - param1 * 200];
         var_295 = int(param1 * 0.8 * int(class_679.var_465.length));
         if(var_295 >= int(class_679.var_465.length))
         {
            var_295 = int(class_679.var_465.length) - 1;
         }
         var_227 = [Number(var_227[0] + var_295 * 50)];
         super.method_95(param1);
         var_469 = 80;
         var_468 = 0;
         method_117();
      }
      
      public function method_117() : void
      {
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:class_656 = null;
         var _loc13_:class_656 = null;
         class_319 = var_232.method_84(class_899.__unprotect__("t9\x11S\x01"),0);
         var _loc1_:int = int(class_679.var_465[var_295][0]);
         var _loc2_:int = int(class_679.var_465[var_295][1]);
         var _loc4_:Number = (class_742.var_216 - (_loc1_ * class_679.var_466 + (_loc1_ - 1) * 5)) * 0.5;
         var _loc5_:Number = (class_742.var_218 - (_loc2_ * class_679.var_467 + (_loc2_ - 1) * 5)) * 0.5;
         var _loc6_:int = _loc1_ * _loc2_;
         var _loc7_:Array = [];
         var _loc8_:int = 0;
         while(_loc8_ < _loc6_)
         {
            _loc8_++;
            _loc9_ = _loc8_;
            _loc7_.push(int(Math.floor(_loc9_ / 2)));
         }
         _loc7_ = method_241(_loc7_);
         var_470 = [];
         _loc8_ = 0;
         while(_loc8_ < _loc1_)
         {
            _loc8_++;
            _loc9_ = _loc8_;
            _loc10_ = 0;
            while(_loc10_ < _loc2_)
            {
               _loc10_++;
               _loc11_ = _loc10_;
               _loc13_ = new class_656(var_232.method_84(class_899.__unprotect__("~42\x06\x02"),class_645.var_209));
               _loc12_ = _loc13_;
               _loc12_.var_244 = Number(_loc4_ + _loc9_ * (5 + class_679.var_466));
               _loc12_.var_245 = Number(_loc5_ + _loc11_ * (5 + class_679.var_467));
               _loc12_.var_215 = _loc7_.pop();
               _loc12_.var_243.tabIndex = int(_loc12_.var_215);
               _loc12_.var_251 = 8 + (_loc9_ + _loc11_) * 8;
               _loc12_.name_46 = true;
               _loc12_.var_471 = false;
               _loc12_.method_118();
               var_470.push(_loc12_);
            }
         }
      }
   }
}
