package
{
   import flash.display.MovieClip;
   import flash.geom.Rectangle;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_933 extends class_645
   {
      
      public static var var_1526:int = 3;
      
      public static var var_515:int = 10;
      
      public static var var_1527:int = 100;
      
      public static var var_1528:int = 30;
      
      public static var var_574:int = 3;
      
      public static var var_1529:int = 12;
      
      public static var var_1530:int = 362;
       
      
      public var var_1533:MovieClip;
      
      public var var_351:Number;
      
      public var var_1540:MovieClip;
      
      public var var_1531:Array;
      
      public var var_1534:Number;
      
      public var var_1535:int;
      
      public var var_25:MovieClip;
      
      public var var_1541:Boolean;
      
      public var var_1539:int;
      
      public var var_1542:int;
      
      public var var_1537:MovieClip;
      
      public var var_907:MovieClip;
      
      public var var_1536:MovieClip;
      
      public var var_1538:int;
      
      public var var_1532:Boolean;
      
      public function class_933()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_1127() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:Number = NaN;
         var _loc1_:int = 0;
         var _loc2_:Array = var_1531;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.x = Number(_loc3_.x + var_351);
            if(_loc3_.x > 100)
            {
               var_1532 = false;
            }
            _loc3_.x = _loc3_.x;
            _loc3_.var_65 = Number(Number(_loc3_.var_65) + 6);
            _loc3_.y = Number(Number(_loc3_.var_94) + Math.sin(_loc3_.var_65 * Math.PI / 180) * class_933.var_1528);
            _loc3_.y = _loc3_.y;
            _loc4_ = int((_loc3_.currentFrame - 1) % 3);
            if(_loc4_ == var_1533.var_159.currentFrame - 1)
            {
               _loc3_.gotoAndStop(_loc4_ + 3 + 1);
            }
            else
            {
               _loc3_.gotoAndStop(_loc4_ + 1);
            }
            if(_loc3_.x > 410)
            {
               _loc3_.parent.removeChild(_loc3_);
               var_1531.method_116(_loc3_);
            }
         }
         if(name_3 > 1)
         {
            return;
         }
         _loc5_ = var_1534;
         var_1534 = var_1534 - 1;
         if(_loc5_ <= 0)
         {
            method_1126();
            if(var_1535 > 2)
            {
               var_1535 = 1;
            }
            else
            {
               var_1535 = var_1535 + 1;
            }
            _loc3_ = var_232.method_84(class_899.__unprotect__("(\x14\x1e(\x03"),1);
            _loc3_.gotoAndStop(var_1535);
            _loc5_ = class_933.var_1527;
            _loc3_.var_94 = _loc5_;
            _loc5_ = _loc5_;
            _loc3_.y = _loc5_;
            _loc3_.y = _loc5_;
            _loc5_ = -20;
            _loc3_.x = _loc5_;
            _loc3_.x = _loc5_;
            _loc3_.var_65 = 0;
            var_1531.push(_loc3_);
         }
      }
      
      public function method_1128() : void
      {
         var _loc3_:Number = NaN;
         if(!var_25.name_26)
         {
            var_25.var_65 = Number(Number(var_25.var_65) + 5);
            var_25.y = Number(Number(var_25.var_94) + Math.sin(var_25.var_65 * Math.PI / 180) * 5);
            var_25.y = var_25.y;
            return;
         }
         var _loc1_:Number = Math.sin(var_25.name_151 * Math.PI / 180) * 300;
         if(Number(var_25.var_351) > 2)
         {
            _loc3_ = Number(var_25.var_351);
            var_25.var_351 = Number(var_25.var_351) - 1;
            §§push(_loc3_);
         }
         else
         {
            §§push(Number(3));
         }
         var _loc2_:Number =; §§pop();
         var_25.name_151 = Number(Number(var_25.name_151) + _loc2_);
         if(Number(var_25.name_151) > 180)
         {
            var_25.name_151 = 90;
            var_25.var_364 = true;
         }
         if(var_25.y <= Number(var_25.var_94) && Boolean(var_25.var_364))
         {
            tCqH();
            return;
         }
         var_25.y = var_25.var_94 - _loc1_;
         var_25.y = var_25.y;
      }
      
      public function method_1129() : void
      {
         var_1533.var_156.var_65 = Number(var_1533.var_156.var_65) + 1;
         var_1533.var_156.y = Number(Number(var_1533.var_156.var_94) + Math.sin(var_1533.var_156.var_65 * Math.PI / 180) * 3);
         var _loc1_:Number = Number(1 + Math.sin(var_1533.var_156.var_65 * 2 * Math.PI / 180) * 0.1);
         var_1533.var_156.scaleY = _loc1_;
         var_1533.var_156.scaleX = _loc1_;
         var_1533.var_158.var_65 = Number(var_1533.var_158.var_65) + 1;
         var_1533.var_158.y = Number(Number(var_1533.var_158.var_94) + Math.sin(var_1533.var_158.var_65 * Math.PI / 180) * 3);
         _loc1_ = Number(1 + Math.sin(var_1533.var_158.var_65 * 2 * Math.PI / 180) * 0.1);
         var_1533.var_158.scaleY = _loc1_;
         var_1533.var_158.scaleX = _loc1_;
         var_1533.var_157.var_65 = Number(var_1533.var_157.var_65) + 1;
         var_1533.var_157.y = Number(Number(var_1533.var_157.var_94) + Math.sin(var_1533.var_157.var_65 * Math.PI / 180) * 3);
         _loc1_ = Number(1 + Math.sin(var_1533.var_157.var_65 * 2 * Math.PI / 180) * 0.1);
         var_1533.var_157.scaleY = _loc1_;
         var_1533.var_157.scaleX = _loc1_;
         var_1533.var_159.var_65 = Number(Number(var_1533.var_159.var_65) + 2);
         var_1533.var_159.y = Number(Number(var_1533.var_159.var_94) + Math.sin(var_1533.var_159.var_65 * Math.PI / 180) * 3);
      }
      
      public function method_1130() : void
      {
         var_1536.var_65 = Number(Number(var_1536.var_65) + Number(var_1536.var_351));
         var_1536.x = Number(Number(var_1536.var_94) + Math.sin(var_1536.var_65 * Math.PI / 180) * 100);
         var_907.var_65 = Number(Number(var_907.var_65) + Number(var_907.var_351));
         var_907.x = Number(Number(var_907.var_94) + Math.sin(var_907.var_65 * Math.PI / 180) * 100);
         var_1537.var_65 = Number(Number(var_1537.var_65) + Number(var_1537.var_351));
         var_1537.x = Number(Number(var_1537.var_94) + Math.sin(var_1537.var_65 * Math.PI / 180) * 100);
      }
      
      public function method_1131() : void
      {
         var _loc2_:MovieClip = null;
         var _loc1_:int = var_1538;
         var_1538 = var_1538 - 1;
         if(_loc1_ <= 0)
         {
            _loc2_ = class_319.var_1;
            _loc2_ = class_319.var_1;
            if(_loc2_.currentFrame < _loc2_.totalFrames)
            {
               _loc2_ = class_319.var_1;
               _loc2_ = class_319.var_1;
               _loc2_.gotoAndStop(_loc2_.currentFrame + 1);
            }
            else
            {
               _loc2_ = class_319.var_1;
               _loc2_.gotoAndStop(1);
            }
            var_1538 = class_933.var_1526;
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:MovieClip = null;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1023();
               method_1131();
               method_1130();
               _loc1_ = !!var_1532?2:1;
               _loc2_ = 0;
               while(_loc2_ < _loc1_)
               {
                  _loc2_++;
                  _loc3_ = _loc2_;
                  method_1127();
               }
               method_1128();
               method_1129();
               break;
            case 2:
               _loc4_ = [var_1533.var_157,var_1533.var_158,var_1533.var_156,var_1533.var_159];
               _loc5_ = _loc4_[var_1539];
               _loc5_.scaleX = _loc5_.scaleX - 0.2;
               _loc5_.scaleY = _loc5_.scaleX;
               if(_loc5_.scaleX <= 0.1)
               {
                  _loc5_.visible = false;
                  var_1539 = var_1539 + 1;
                  if(var_1539 == 4)
                  {
                     name_3 = 3;
                  }
               }
               method_1127();
               method_1128();
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         if(!var_25.name_26)
         {
            var_25.name_26 = true;
         }
      }
      
      public function method_1126() : void
      {
         var_1534 = class_933.var_515;
         var_351 = Number(class_933.var_574 + var_237[0] * 14);
      }
      
      public function tCqH() : void
      {
         var_25.x = 310;
         var _loc1_:Number = class_933.var_1530;
         var_25.y = _loc1_;
         var_25.y = _loc1_;
         var_25.var_65 = 0;
         var_25.var_94 = 352;
         var_25.name_151 = 10;
         var_25.var_351 = class_933.var_1529;
         var_25.var_364 = false;
         var_25.name_26 = false;
         var_1541 = false;
         var_1539 = 0;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_1532 = true;
         var_1538 = class_933.var_1526;
         var_227 = [300];
         super.method_95(param1);
         var_1531 = [];
         var_1535 = 0;
         var_1542 = 1;
         method_1126();
         method_117();
      }
      
      public function method_1023() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:class_892 = null;
         if(var_25.var_364)
         {
            return;
         }
         if(var_1541)
         {
            return;
         }
         var _loc1_:int = 0;
         var _loc2_:Array = var_1531;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(var_155(_loc3_,var_25.var_155))
            {
               var_1541 = true;
               if(_loc3_.currentFrame > 3)
               {
                  var_1533.var_159.gotoAndStop(var_1533.var_159.currentFrame + 1);
                  var_1542 = var_1542 + 1;
                  var_1531.method_116(_loc3_);
                  _loc4_ = new class_892(_loc3_);
                  _loc4_.var_251 = 5;
                  _loc4_.var_279 = 0.1;
                  _loc4_.var_250 = 2;
                  _loc4_.var_272 = 4;
                  new class_931(var_25);
                  if(var_1533.var_159.currentFrame == var_1533.var_159.totalFrames)
                  {
                     method_81(true,30);
                     name_3 = 2;
                     var_25.nextFrame();
                  }
               }
            }
         }
      }
      
      public function var_155(param1:MovieClip, param2:MovieClip) : Boolean
      {
         var _loc3_:Rectangle = method_159(param1);
         var _loc4_:Rectangle = method_159(param2);
         return Boolean(_loc4_.intersects(_loc3_));
      }
      
      public function method_159(param1:MovieClip) : Rectangle
      {
         return param1.getBounds(class_319);
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("y\x1c\x01\x16\x02"),0);
         var _loc1_:MovieClip = class_319.var_1;
         _loc1_.stop();
         var_25 = var_232.method_84(class_899.__unprotect__("}zf^\x01"),3);
         tCqH();
         var_1540 = var_232.method_84(class_899.__unprotect__("\x18I~x\x03"),4);
         var_1540.x = 310;
         var_1540.y = 372;
         var_1536 = var_232.method_84(class_899.__unprotect__("}\x13O}\x01"),2);
         var_1536.x = 200;
         var_1536.y = 200;
         var_1536.var_94 = 200;
         var_1536.var_351 = Number(0.2 + int(class_691.method_114(8)) / 10);
         var_1536.var_65 = int(class_691.method_114(360));
         var_907 = var_232.method_84(class_899.__unprotect__("s(wG\x01"),2);
         var_907.x = 200;
         var_907.y = 200;
         var_907.var_94 = 200;
         var_907.var_351 = Number(0.2 + int(class_691.method_114(8)) / 10);
         var_907.var_65 = int(class_691.method_114(360));
         var_1537 = var_232.method_84(class_899.__unprotect__("\'|\x0e0\x03"),4);
         var_1537.x = 200;
         var_1537.y = 200;
         var_1537.var_94 = 200;
         var_1537.var_351 = Number(0.2 + int(class_691.method_114(8)) / 10);
         var_1537.var_65 = int(class_691.method_114(360));
         var_1533 = var_232.method_84(class_899.__unprotect__("O\b\n,\x01"),1);
         var_1533.x = 110;
         var_1533.y = 53;
         var_1533.var_156.var_94 = var_1533.var_156.y;
         var_1533.var_158.var_94 = var_1533.var_158.y;
         var_1533.var_157.var_94 = var_1533.var_157.y;
         var_1533.var_156.var_65 = int(class_691.method_114(360));
         var_1533.var_158.var_65 = int(class_691.method_114(360));
         var_1533.var_157.var_65 = int(class_691.method_114(360));
         var_1533.var_159.var_94 = var_1533.var_159.y;
         var_1533.var_159.var_65 = int(class_691.method_114(360));
         var_1533.var_159.gotoAndStop(1);
      }
   }
}
