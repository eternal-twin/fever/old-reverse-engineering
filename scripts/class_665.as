package
{
   import flash.display.MovieClip;
   import flash.filters.GlowFilter;
   import package_11.package_12.class_657;
   import package_32.class_899;
   import package_8.package_9.class_814;
   import package_8.package_9.class_931;
   
   public class class_665 extends class_645
   {
       
      
      public var var_353:Number;
      
      public var var_354:Number;
      
      public var var_358:int;
      
      public var var_357:class_667;
      
      public var name_30:int;
      
      public var var_359:Array;
      
      public var var_363:MovieClip;
      
      public var name_27:int;
      
      public var var_361:Number;
      
      public var var_364:MovieClip;
      
      public var var_355:class_668;
      
      public var name_28:int;
      
      public var var_356:int;
      
      public var var_360:Array;
      
      public var var_362:Boolean;
      
      public function class_665()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:* = null;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_258();
               _loc1_ = var_355.method_259();
               _loc2_ = method_100();
               _loc3_ = 0;
               if(_loc1_ != null)
               {
                  _loc3_ = int((_loc2_.var_245 - int(_loc1_.var_245)) / 5);
               }
               if(_loc3_ == 0)
               {
                  var_356 = var_356 + 1;
               }
               _loc4_ = 3;
               var_355.method_260(_loc4_,_loc3_);
               var_357.method_79(1,1,0);
               var_355.method_79(1,0,1,function(param1:Object):void
               {
               });
               break;
            case 2:
               _loc3_ = name_27;
               name_27 = name_27 - 1;
               if(_loc3_ < 0)
               {
                  method_81(false,4);
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_258() : void
      {
         var _loc3_:MovieClip = null;
         var _loc5_:int = 0;
         if(var_356 > 30)
         {
            method_261();
         }
         if(int(var_359.length) <= 0)
         {
            method_261();
         }
         var _loc1_:int = name_28;
         name_28 = name_28 - 1;
         if(_loc1_ < 0)
         {
            name_28 = var_360[0];
            method_261();
         }
         var _loc2_:* = var_355.method_259();
         if(int(_loc2_.var_244) > var_361)
         {
            method_81(true,15);
            if(!var_362)
            {
               new class_931(var_363);
               var_364.gotoAndStop(2);
               var_362 = true;
            }
            while(int(var_359.length) > 0)
            {
               _loc3_ = var_359.pop();
               new class_814(_loc3_,10 + int(class_691.method_114(8)));
            }
            return;
         }
         _loc1_ = 0;
         var _loc4_:int = int(var_359.length);
         while(_loc1_ < _loc4_)
         {
            _loc1_++;
            _loc5_ = _loc1_;
            _loc3_ = var_359[_loc5_];
            if(_loc3_ == null)
            {
               var_359.method_116(_loc3_);
            }
            else
            {
               if(!_loc3_.name_29)
               {
                  _loc3_.x = _loc3_.x - (Number(name_30 + Number(_loc3_.var_365)));
                  _loc3_.x = _loc3_.x;
               }
               if(Number(_loc3_.x + _loc3_.width) < 0)
               {
                  _loc3_.parent.removeChild(_loc3_);
                  var_359.method_116(_loc3_);
               }
               else if(_loc3_.x - int(_loc2_.var_244) <= 1 && int(_loc2_.var_244) <= Number(_loc3_.x + _loc3_.width))
               {
                  if(var_155(_loc3_,int(_loc2_.var_244),int(_loc2_.var_245)))
                  {
                     method_102(3);
                     new class_931(_loc3_);
                     var_355.method_116(int(_loc2_.var_366));
                     name_3 = 2;
                     method_262(int(_loc2_.var_244),int(_loc2_.var_245),16777215);
                     _loc3_.name_29 = true;
                  }
               }
            }
         }
      }
      
      override public function method_80() : void
      {
         method_81(false);
         name_3 = 2;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [240];
         super.method_95(param1);
         var_356 = 0;
         var_357 = new class_667(var_201,class_742.var_216,class_742.var_218);
         var_355 = new class_668(var_232.method_92(1),class_742.var_216,class_742.var_218,0,0,int(class_742.var_218 / 2),5);
         var_357.method_263(class_899.__unprotect__("t;\x1e}"));
         name_30 = int(Math.ceil(Number(param1 * 2 + 5)));
         var_355.method_265(int(method_264(true)));
         var_359 = [];
         var_360 = [8 - int(2 * param1)];
         name_28 = var_360[0];
         var_364 = var_232.method_84(class_899.__unprotect__("o\x16?\x02\x02"),1);
         var_364.gotoAndStop(1);
         var_364.y = class_742.var_216 / 2;
         var_361 = Number(Math.min(class_742.var_216 / 2 + param1 * (class_742.var_216 / 3) - 10,class_742.var_216 - var_364.width));
         var_364.x = var_361;
         var_358 = int(class_691.method_114(int(class_666.var_367.length)));
         var_363 = var_232.method_92(1);
         var_363.graphics.lineStyle(1,int(method_264(true)),70);
         var_363.graphics.moveTo(var_361,0);
         var_363.graphics.lineTo(var_361,class_742.var_218);
         var _loc2_:GlowFilter = new GlowFilter();
         _loc2_.color = 16777215;
         _loc2_.blurX = 2;
         _loc2_.blurY = 2;
         var_363.filters = [_loc2_];
         var_362 = false;
         name_3 = 1;
         name_27 = 10;
      }
      
      public function var_155(param1:MovieClip, param2:Number, param3:Number) : Boolean
      {
         if(param2 < param1.x)
         {
            return false;
         }
         if(param2 > Number(param1.x + param1.width))
         {
            return false;
         }
         return param3 >= param1.y && param3 <= Number(param1.y + param1.height);
      }
      
      public function method_264(param1:Boolean = false) : int
      {
         if(!param1)
         {
            return int(class_666.var_368[var_358][int(class_691.method_114(4))]);
         }
         return int(class_666.var_367[var_358][int(class_691.method_114(4))]);
      }
      
      public function method_262(param1:Number, param2:Number, param3:int, param4:int = 15) : void
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:MovieClip = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:class_892 = null;
         var _loc6_:Number = 360 / param4;
         var _loc7_:int = 0;
         while(_loc7_ < 4)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc9_ = 0;
            while(_loc9_ < param4)
            {
               _loc9_++;
               _loc10_ = _loc9_;
               _loc11_ = var_232.method_92(3);
               class_649.method_149(_loc11_,16777215);
               class_657.name_18(_loc11_,int(method_264()));
               _loc12_ = _loc6_ * _loc10_;
               _loc11_.x = Number(param1 + class_649.method_148(_loc12_) * (2 * _loc8_));
               _loc11_.y = Number(param2 + class_649.name_9(_loc12_) * (2 * _loc8_));
               _loc13_ = Number(1 + Math.random() * 3);
               _loc11_.scaleY = _loc13_;
               _loc11_.scaleX = _loc13_;
               _loc14_ = new class_892(_loc11_);
               _loc14_.var_251 = 20;
               _loc14_.var_278 = class_649.method_148(_loc12_) * (8 / (_loc8_ + 1));
               _loc14_.var_279 = class_649.name_9(_loc12_) * (8 / (_loc8_ + 1));
            }
         }
      }
      
      public function method_261(param1:Number = 0) : void
      {
         var _loc3_:Number = NaN;
         var _loc4_:int = 0;
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("1S\x06C"),1);
         _loc3_ = class_742.var_216 - 3;
         _loc2_.x = _loc3_;
         _loc2_.x = _loc3_;
         _loc2_.var_365 = 0;
         if(var_356 > 30)
         {
            _loc4_ = int(var_355.method_259().var_245);
            _loc3_ = _loc4_ - _loc2_.height / 2;
            _loc2_.y = _loc3_;
            _loc2_.y = _loc3_;
            _loc2_.var_365 = 1.5;
            var_356 = 0;
         }
         else
         {
            _loc4_ = int(class_691.method_114(int(name_30 * 10))) * (int(class_691.method_114(2)) == 0?1:-1);
            _loc3_ = Number(1 + _loc4_ * 0.01);
            _loc2_.scaleY = _loc3_;
            _loc2_.scaleX = _loc3_;
            _loc2_.var_365 = Number(Number(_loc2_.var_365) + _loc4_ / 20);
            _loc3_ = int(class_691.method_114(class_742.var_218));
            _loc2_.y = _loc3_;
            _loc2_.y = _loc3_;
         }
         _loc4_ = int(method_264());
         class_657.name_18(_loc2_,_loc4_);
         _loc2_.var_369 = _loc4_;
         _loc2_.name_29 = false;
         var_359.push(_loc2_);
      }
   }
}
