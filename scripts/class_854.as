package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.geom.ColorTransform;
   import flash.text.TextField;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_854 extends class_645
   {
      
      public static var var_1231:int = 1600;
      
      public static var var_1232:int = 1600;
       
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_780:class_795;
      
      public var var_104:MovieClip;
      
      public var var_895:MovieClip;
      
      public var var_1237:int;
      
      public var var_1233:MovieClip;
      
      public var var_1235:Object;
      
      public var var_318:Number;
      
      public var var_319:Number;
      
      public var var_446:class_795;
      
      public var var_359:class_795;
      
      public var var_1236:int;
      
      public var var_1238:int;
      
      public var var_1234:class_795;
      
      public var name_10:int;
      
      public var GLWA:TextField;
      
      public var var_369:int;
      
      public var var_65:Number;
      
      public function class_854()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_898() : void
      {
         var _loc7_:class_656 = null;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:class_892 = null;
         var _loc1_:ColorTransform = new ColorTransform(1,1,1,1,-50,-10,-50,-20);
         var_1233.var_230.colorTransform(var_1233.var_230.rect,_loc1_);
         var _loc3_:Number = 50 / class_854.var_1231;
         var _loc4_:Number = 50 / class_854.var_1232;
         var _loc5_:BitmapData = new BitmapData(50,50,true,0);
         _loc5_.setPixel32(25,25,-65536);
         var _loc6_:* = var_1234.method_73();
         while(_loc6_.method_74())
         {
            _loc7_ = _loc6_.name_1();
            _loc8_ = int(Number(25 + class_663.method_112(_loc7_.var_244 - var_319,class_854.var_1231 * 0.5) * _loc3_));
            _loc9_ = int(Number(25 + class_663.method_112(_loc7_.var_245 - var_318,class_854.var_1232 * 0.5) * _loc4_));
            _loc5_.setPixel32(_loc8_,_loc9_,var_369);
         }
         _loc6_ = var_446.method_73();
         while(_loc6_.method_74())
         {
            _loc10_ = _loc6_.name_1();
            _loc8_ = int(Number(25 + class_663.method_112(_loc10_.var_244 - var_319,class_854.var_1231 * 0.5) * _loc3_));
            _loc9_ = int(Number(25 + class_663.method_112(_loc10_.var_245 - var_318,class_854.var_1232 * 0.5) * _loc4_));
            _loc5_.setPixel32(_loc8_,_loc9_,1342242560);
         }
         var_1233.var_230.draw(_loc5_);
         _loc5_.dispose();
      }
      
      public function method_118() : void
      {
         var _loc2_:class_656 = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc1_:* = var_359.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc3_ = Number(class_663.method_112(_loc2_.var_244 - var_319,class_854.var_1231 * 0.5));
            _loc4_ = Number(class_663.method_112(_loc2_.var_245 - var_318,class_854.var_1232 * 0.5));
            _loc2_.var_243.x = Number(class_742.var_216 * 0.5 + _loc3_);
            _loc2_.var_243.y = Number(class_742.var_218 * 0.5 + _loc4_);
            if(_loc2_.var_243.visible != true)
            {
               var_359.method_116(_loc2_);
            }
         }
      }
      
      public function method_899() : void
      {
         var _loc2_:class_656 = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:class_892 = null;
         var _loc13_:class_892 = null;
         var _loc14_:class_892 = null;
         var _loc1_:* = var_1234.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc3_ = Number(Number(0.5 + Number(Math.cos(var_1235 * 6.28))) + 0.5);
            _loc2_.var_243.filters = [];
            class_658.var_146(_loc2_.var_243,2,4,16777215);
            class_658.var_146(_loc2_.var_243,Number(4 + _loc3_ * 10),Number(0.5 + _loc3_ * 0.5),16777215);
            _loc4_ = Number(class_663.method_112(_loc2_.var_244 - var_319,class_854.var_1231 * 0.5));
            _loc5_ = Number(class_663.method_112(_loc2_.var_245 - var_318,class_854.var_1232 * 0.5));
            if(Number(Math.sqrt(Number(_loc4_ * _loc4_ + _loc5_ * _loc5_))) < 50)
            {
               _loc6_ = 3;
               _loc7_ = 0;
               _loc8_ = var_1236;
               while(_loc7_ < _loc8_)
               {
                  _loc7_++;
                  _loc9_ = _loc7_;
                  _loc10_ = Number(Number(3 + Math.random() * 12) + var_1236 * 0.2);
                  _loc11_ = _loc9_ / var_1236 * 6.28;
                  _loc13_ = new class_892(var_232.method_84(class_899.__unprotect__("6\\Z]\x03"),class_645.var_209));
                  _loc13_.var_233 = 0.99;
                  _loc12_ = _loc13_;
                  _loc12_.var_278 = Math.cos(_loc11_) * _loc10_;
                  _loc12_.var_279 = Math.sin(_loc11_) * _loc10_;
                  _loc12_.var_244 = Number(_loc2_.var_244 + _loc12_.var_278 * _loc6_);
                  _loc12_.var_245 = Number(_loc2_.var_245 + _loc12_.var_279 * _loc6_);
                  _loc12_.var_233 = 0.98;
                  _loc12_.var_580 = (Math.random() * 2 - 1) * 8;
                  _loc12_.var_243.gotoAndStop(int(class_691.method_114(_loc12_.var_243.totalFrames)) + 1);
                  _loc12_.name_116 = 0;
                  var_359.method_103(_loc12_);
                  var_446.method_103(_loc12_);
               }
               _loc2_.method_89();
               var_359.method_116(_loc2_);
               var_1234.method_116(_loc2_);
            }
         }
         _loc6_ = 2;
         _loc1_ = var_446.method_73();
         while(_loc1_.method_74())
         {
            _loc12_ = _loc1_.name_1();
            _loc3_ = Number(class_663.method_112(var_319 - _loc12_.var_244,class_854.var_1231 * 0.5));
            _loc4_ = Number(class_663.method_112(var_318 - _loc12_.var_245,class_854.var_1232 * 0.5));
            _loc5_ = Number(Math.atan2(_loc4_,_loc3_));
            _loc12_.var_278 = Number(_loc12_.var_278 + Math.cos(_loc5_) * _loc6_);
            _loc12_.var_279 = Number(_loc12_.var_279 + Math.sin(_loc5_) * _loc6_);
            if(Number(Math.sqrt(Number(_loc3_ * _loc3_ + _loc4_ * _loc4_))) < 26)
            {
               if(var_1237 < 100)
               {
                  var_1237 = var_1237 + 1;
               }
               var_895.gotoAndPlay(2);
               if(var_1237 >= 100)
               {
                  method_81(true,20);
               }
               GLWA.text = class_691.method_97(var_1237);
               new class_931(var_104,0.2);
               _loc12_.method_89();
               var_359.method_116(_loc12_);
               var_446.method_116(_loc12_);
            }
            _loc10_ = Number(_loc12_.name_116);
            _loc12_.name_116 = Number(Math.min(Number(_loc10_ + 0.002),1));
            _loc12_.var_244 = Number(_loc12_.var_244 + _loc3_ * _loc10_);
            _loc12_.var_245 = Number(_loc12_.var_245 + _loc4_ * _loc10_);
            _loc7_ = 5;
            if(int(class_691.method_114(3)) == 0 && int(class_656.var_226.length) < 200)
            {
               _loc14_ = new class_892(var_232.method_84(class_899.__unprotect__("T3\f8"),class_645.var_209));
               _loc14_.var_233 = 0.99;
               _loc13_ = _loc14_;
               _loc13_.var_243.gotoAndPlay(int(class_691.method_114(_loc13_.var_243.totalFrames)) + 1);
               _loc13_.var_244 = Number(_loc12_.var_244 + (Math.random() * 2 - 1) * _loc7_);
               _loc13_.var_245 = Number(_loc12_.var_245 + (Math.random() * 2 - 1) * _loc7_);
               _loc13_.var_278 = _loc12_.var_278 * 0.5;
               _loc13_.var_279 = _loc12_.var_279 * 0.5;
               _loc13_.var_251 = 20;
               var_359.method_103(_loc13_);
            }
         }
      }
      
      override public function method_79() : void
      {
         if(var_1235 == null)
         {
            var_1235 = 0;
         }
         var_1235 = Number(class_663.method_113(Number(var_1235 + 0.05),1));
         var _loc1_:int = int(255 * var_1235);
         var_369 = int(class_657.method_235({
            "z\x01":_loc1_,
            "6\x01":255,
            "r\x01":_loc1_,
            "@\x01":255
         }));
         method_900();
         name_30();
         method_899();
         super.method_79();
         method_118();
         method_898();
      }
      
      public function name_30() : void
      {
         var _loc5_:MovieClip = null;
         var_278 = var_278 * 0.99;
         var_279 = var_279 * 0.99;
         var_319 = Number(var_319 + var_278);
         var_318 = Number(var_318 + var_279);
         var _loc3_:int = class_742.var_216 + 2 * 10;
         var _loc4_:* = var_780.method_73();
         while(_loc4_.method_74())
         {
            _loc5_ = _loc4_.name_1();
            _loc5_.x = _loc5_.x - var_278 * _loc5_.var_68;
            _loc5_.y = _loc5_.y - var_279 * _loc5_.var_68;
            if(_loc5_.x < -10)
            {
               _loc5_.x = Number(_loc5_.x + _loc3_);
            }
            if(_loc5_.x > class_742.var_216 + 10)
            {
               _loc5_.x = _loc5_.x - _loc3_;
            }
            if(_loc5_.y < -10)
            {
               _loc5_.y = Number(_loc5_.y + _loc3_);
            }
            if(_loc5_.y > class_742.var_216 + 10)
            {
               _loc5_.y = _loc5_.y - _loc3_;
            }
         }
      }
      
      public function method_900() : void
      {
         var _loc7_:Number = NaN;
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - class_742.var_216 * 0.5;
         var _loc3_:Number = _loc1_.var_245 - class_742.var_218 * 0.5;
         var _loc4_:Number = Number(class_663.method_112(Math.atan2(_loc3_,_loc2_) - var_65,3.14));
         var_65 = Number(var_65 + Number(class_663.method_99(-1,_loc4_ * 0.5,1)));
         var_104.rotation = var_65 / 0.0174;
         var _loc6_:MovieClip = var_104.var_1;
         _loc6_.visible = name_5;
         if(name_5)
         {
            _loc7_ = 0.8;
            var_278 = Number(var_278 + Math.cos(var_65) * _loc7_);
            var_279 = Number(var_279 + Math.sin(var_65) * _loc7_);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 100 * param1];
         super.method_95(param1);
         var_359 = new class_795();
         var_446 = new class_795();
         var_1238 = 1 + int(param1 * 7);
         name_10 = 0;
         var_1236 = int(Math.ceil(100 / var_1238));
         method_117();
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:MovieClip = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:class_656 = null;
         var _loc9_:class_656 = null;
         var_232.method_84(class_899.__unprotect__("r2\fU\x01"),0);
         var_104 = var_232.method_84(class_899.__unprotect__("J\x19\x03U\x03"),1);
         var_104.x = class_742.var_216 * 0.5;
         var_104.y = class_742.var_218 * 0.5;
         var_319 = class_854.var_1231 * 0.5;
         var_318 = class_854.var_1232 * 0.5;
         var_278 = 0;
         var_279 = 0;
         var_65 = 0;
         var_780 = new class_795();
         var _loc2_:int = 0;
         while(_loc2_ < 200)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc4_ = Math.pow(_loc3_ / 200,2) * 0.5;
            _loc5_ = var_232.method_84(class_899.__unprotect__("}G&8"),0);
            _loc5_.x = Math.random() * class_742.var_216;
            _loc5_.y = Math.random() * class_742.var_218;
            _loc6_ = Number(0.5 + _loc4_);
            _loc5_.scaleY = _loc6_;
            _loc5_.scaleX = _loc6_;
            _loc5_.var_68 = _loc4_;
            var_780.method_103(_loc5_);
         }
         var_1234 = new class_795();
         _loc2_ = 0;
         _loc3_ = var_1238;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc7_ = _loc2_;
            _loc9_ = new class_656(var_232.method_84(class_899.__unprotect__("W\x0b[)\x03"),class_645.var_209));
            _loc8_ = _loc9_;
            _loc8_.var_244 = Math.random() * class_854.var_1231;
            _loc8_.var_245 = Math.random() * class_854.var_1232;
            _loc8_.method_119(Number(0.8 + var_1236 * 0.01));
            var_359.method_103(_loc8_);
            var_1234.method_103(_loc8_);
         }
         var_1233 = var_232.method_84(class_899.__unprotect__("\r\x04\x01l\x01"),3);
         var_1233.x = class_742.var_216 - 104;
         var_1233.y = class_742.var_218 - 104;
         var_1233.var_230 = new BitmapData(50,50,true,0);
         var _loc10_:Bitmap = new Bitmap();
         _loc10_.bitmapData = var_1233.var_230;
         var_1233.addChild(_loc10_);
         _loc4_ = 2;
         var_1233.scaleY = _loc4_;
         var_1233.scaleX = _loc4_;
         var_895 = var_232.method_84(class_899.__unprotect__("\bt9s\x02"),3);
         var_895.x = class_742.var_216 * 0.5;
         var_895.y = class_742.var_218 * 0.5 - 30;
         _loc5_ = Reflect.field(var_895,class_899.__unprotect__("GLWA"));
         GLWA = _loc5_.GLWA;
         GLWA.text = "0";
         var_1237 = 0;
      }
   }
}
