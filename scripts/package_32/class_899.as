package package_32 {
import flash.display.MovieClip;
import flash.display.Stage;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.utils.getQualifiedClassName;
import flash.utils.setTimeout;

public class class_899 extends MovieClip {

  public static var var_1058:TextField;

  public static var var_718:Array;

  public static var var_317:Error;

  public static var var_239:Boolean = false;

  public function class_899() {
    if (class_899.var_239) {
      return;
    }
    super();
  }

  public static function method_165(param1:Object):String {
    var _loc5_:* = null;
    if (param1.var_296 == null) {
      return param1.var_294;
    }
    var _loc2_:Array = [];
    var _loc3_:int = 0;
    var _loc4_:Array = param1.var_296;
    while (_loc3_ < int(_loc4_.length)) {
      _loc5_ = _loc4_[_loc3_];
      _loc3_++;
      _loc2_.push(class_899.__string_rec(_loc5_, ""));
    }
    return param1.var_294 + "(" + _loc2_.join(",") + ")";
  }

  public static function __instanceof(param1:*, param2:*):Boolean {
    try {
      if (param2 == class_813) {
        return true;
      }
      return param1 is param2;
    } catch (_loc4_:*) {
    }
    return false;
  }

  public static function __clear_trace():void {
    if (class_899.var_1058 == null) {
      return;
    }
    class_899.var_1058.parent.removeChild(class_899.var_1058);
    class_899.var_1058 = null;
    class_899.var_718 = null;
  }

  public static function __set_trace_color(param1:uint):void {
    class_899.method_1050().textColor = param1;
  }

  public static function method_1050():TextField {
    var _loc2_:TextFormat = null;
    var _loc1_:MovieClip = App.rootMc;
    if (class_899.var_1058 == null) {
      class_899.var_1058 = new TextField();
      _loc2_ = class_899.var_1058.getTextFormat();
      _loc2_.font = "_sans";
      class_899.var_1058.defaultTextFormat = _loc2_;
      class_899.var_1058.selectable = false;
      class_899.var_1058.width = _loc1_.stage == null ? 800 : _loc1_.stage.stageWidth;
      class_899.var_1058.autoSize = TextFieldAutoSize.LEFT;
      class_899.var_1058.mouseEnabled = false;
    }
    if (_loc1_.stage == null) {
      _loc1_.addChild(class_899.var_1058);
    } else {
      _loc1_.stage.addChild(class_899.var_1058);
    }
    return class_899.var_1058;
  }

  public static function __trace(param1:*, param2:Object):void {
    var _loc3_:TextField = class_899.method_1050();
    var _loc4_:String = param2 == null ? "(null)" : param2.name_86 + ":" + int(param2.name_136);
    if (class_899.var_718 == null) {
      class_899.var_718 = [];
    }
    class_899.var_718 = class_899.var_718.concat((_loc4_ + ": " + class_899.__string_rec(param1, "")).split("\n"));
    _loc3_.text = class_899.var_718.join("\n");
    var _loc5_:Stage = App.rootMc.stage;
    if (_loc5_ == null) {
      return;
    }
    while (int(class_899.var_718.length) > 1 && _loc3_.height > _loc5_.stageHeight) {
      class_899.var_718.shift();
      _loc3_.text = class_899.var_718.join("\n");
    }
  }

  public static function __string_rec(param1:*, param2:String):String {
    var _loc5_:String = null;
    var _loc6_:Array = null;
    var _loc7_:Array = null;
    var _loc8_:int = 0;
    var _loc10_:String = null;
    var _loc11_:Boolean = false;
    var _loc12_:int = 0;
    var _loc13_:int = 0;
    var _loc14_:String = null;
    var _loc4_:String = getQualifiedClassName(param1);
    _loc5_ = _loc4_;
    if (_loc5_ == "Object") {
      _loc8_ = 0;
      _loc7_ = [];
      if (§§hasnext(param1, _loc8_);
    )
      {
        _loc7_.push(;§§nextname(_loc8_, _loc9_);
      )

               §§goto(addr59);
      }
      _loc6_ = _loc7_;
      _loc10_ = "{";
      _loc11_ = true;
      _loc8_ = 0;
      _loc12_ = int(_loc6_.length);
      if (_loc8_ < _loc12_) {
        _loc8_++;
        _loc13_ = _loc8_;
        _loc14_ = _loc6_[_loc13_];
        if (_loc14_ == "toString") {
          try {
            return param1.toString();
          } catch (_loc9_:*) {
          }
        }
        if (_loc11_) {
          _loc11_ = false;
          _loc10_ = _loc10_ + (" " + _loc14_ + " : " + class_899.__string_rec(param1[_loc14_], param2));
                §§goto(addr138);
        }
        _loc10_ = _loc10_ + ",";
             §§goto(addr116);
      }
      if (!_loc11_) {
        _loc10_ = _loc10_ + " ";
      }
      _loc10_ = _loc10_ + "}";
      return _loc10_;
    }
    if (_loc5_ == "Array") {
      if (param1 == Array) {
        return "#Array";
      }
      _loc10_ = "[";
      _loc11_ = true;
      _loc6_ = param1;
      _loc8_ = 0;
      _loc12_ = int(_loc6_.length);
      if (_loc8_ < _loc12_) {
        _loc8_++;
        _loc13_ = _loc8_;
        if (_loc11_) {
          _loc11_ = false;
          _loc10_ = _loc10_ + class_899.__string_rec(_loc6_[_loc13_], param2);
                §§goto(addr214);
        }
        _loc10_ = _loc10_ + ",";
             §§goto(addr201);
      }
      return _loc10_ + "]";
    }
    _loc5_ = typeof param1;
    if (_loc5_ == "function") {
      return "<function>";
    }
    return String(param1);
  }

  public static function __unprotect__(param1:String):String {
    return param1;
  }

  public function name_51():void {
    var _loc2_:MovieClip = App.rootMc;
    try {
      if (_loc2_ == this && _loc2_.stage != null && _loc2_.stage.align == "") {
        _loc2_.stage.align = "TOP_LEFT";
      }
    } catch (_loc3_:*) {
    }
    if (_loc2_.stage == null) {
      _loc2_.addEventListener(Event.ADDED_TO_STAGE, method_1051);
    } else if (_loc2_.stage.stageWidth == 0) {
      setTimeout(name_51, 1);
    } else {
      method_95();
    }
  }

  public function method_95():void {
    class_899.var_317 = new Error();
    throw "assert";
  }

  public function method_1051(param1:*):void {
    App.rootMc.removeEventListener(Event.ADDED_TO_STAGE, method_1051);
    name_51();
  }
}
}
