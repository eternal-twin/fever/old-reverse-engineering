package
{
   import flash.display.MovieClip;
   import flash.geom.Point;
   import package_32.class_899;
   
   public class class_888 extends class_645
   {
      
      public static var var_1365:int = 20;
      
      public static var var_1366:Array = [{
         "\n\x01":{
            "L\x01":37,
            "\x03\x01":118
         },
         "t\x01":{
            "L\x01":194,
            "\x03\x01":125
         }
      },{
         "\n\x01":{
            "L\x01":184,
            "\x03\x01":184
         },
         "t\x01":{
            "L\x01":59,
            "\x03\x01":115
         }
      },{
         "\n\x01":{
            "L\x01":172,
            "\x03\x01":73
         },
         "t\x01":{
            "L\x01":35,
            "\x03\x01":58
         }
      },{
         "\n\x01":{
            "L\x01":184,
            "\x03\x01":56
         },
         "t\x01":{
            "L\x01":178,
            "\x03\x01":178
         }
      },{
         "\n\x01":{
            "L\x01":49,
            "\x03\x01":183
         },
         "t\x01":{
            "L\x01":175,
            "\x03\x01":190
         }
      }];
       
      
      public var var_1367:class_892;
      
      public var var_251:Number;
      
      public var var_752:Array;
      
      public var name_1:int;
      
      public var var_69:MovieClip;
      
      public var var_1369:MovieClip;
      
      public var var_494:Boolean;
      
      public var var_1368:MovieClip;
      
      public function class_888()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_811(param1:Boolean, param2:Number) : void
      {
         var_494 = param1;
         var_229 = [true];
         name_3 = 4;
         var_251 = param2;
      }
      
      public function method_1022(param1:Object) : void
      {
         var _loc4_:Number = NaN;
         var _loc2_:Number = Number(var_1367.method_231(param1));
         var _loc3_:Number = Number(var_1367.method_115(param1));
         param1.var_631.x = var_1367.var_244;
         param1.var_94.x = var_1367.var_244;
         param1.var_631.y = var_1367.var_245;
         param1.var_94.y = var_1367.var_245;
         param1.var_631.rotation = _loc2_ / 0.0174;
         param1.var_94.rotation = _loc2_ / 0.0174;
         param1.var_631.scaleX = _loc3_ * 0.01;
         if(param1.var_522 == null)
         {
            _loc4_ = _loc3_ * 0.01;
            var_1367.var_278 = Number(var_1367.var_278 + Math.cos(_loc2_) * _loc4_);
            var_1367.var_279 = Number(var_1367.var_279 + Math.sin(_loc2_) * _loc4_);
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         if(var_752 == null)
         {
            var_752 = [];
            _loc1_ = 0;
            while(_loc1_ < 4)
            {
               _loc1_++;
               _loc2_ = _loc1_;
               _loc3_ = {
                  "gB\x01":var_232.method_84(class_899.__unprotect__(",\x04\nx\x02"),class_645.var_208),
                  "\x1by$\x01\x02":var_232.method_84(class_899.__unprotect__("~@Vk\x03"),class_645.var_208),
                  "L\x01":var_1367.var_244,
                  "\x03\x01":var_1367.var_245,
                  "jvD}\x01":null,
                  ")}\x1b)\x01":null
               };
               _loc3_.var_631.stop();
               _loc4_ = (_loc2_ / 4 + 0.125) * 6.28;
               _loc5_ = Number(Math.cos(_loc4_));
               _loc6_ = Number(Math.sin(_loc4_));
               _loc7_ = 0;
               while(!method_1023(Number(_loc3_.var_244),Number(_loc3_.var_245)))
               {
                  _loc3_.var_244 = Number(Number(_loc3_.var_244) + _loc5_);
                  _loc3_.var_245 = Number(Number(_loc3_.var_245) + _loc6_);
                  if(_loc7_ > 400)
                  {
                     break;
                  }
               }
               method_1022(_loc3_);
               var_752.push(_loc3_);
            }
         }
         _loc1_ = 0;
         var _loc8_:Array = var_752;
         while(_loc1_ < int(_loc8_.length))
         {
            _loc3_ = _loc8_[_loc1_];
            _loc1_++;
            method_1022(_loc3_);
         }
         if(name_3 != 4 && Boolean(method_1023(var_1367.var_244,var_1367.var_245)))
         {
            method_811(false,10);
            method_1024();
            var_1367.var_278 = 0;
            var_1367.var_279 = -4;
            var_1367.var_243.gotoAndStop("death");
         }
         if(name_3 != 4)
         {
            _loc4_ = Number(var_1367.method_115({
               "L\x01":var_1368.x,
               "\x03\x01":var_1368.y
            }));
            if(var_1368.currentFrame == 1 && _loc4_ < 80)
            {
               var_1368.play();
            }
            if(var_1368.currentFrame > 14 && _loc4_ < 14)
            {
               method_811(true,10);
               var_69 = var_232.method_84(class_899.__unprotect__("{\fz7"),class_645.var_209);
               var_69.x = var_1368.x;
               var_69.y = var_1368.y;
               var_1367.var_243.mask = var_69;
               method_1024();
            }
         }
         _loc4_ = Number(var_1367.method_231(method_100()));
         var_1367.var_243.rotation = _loc4_ / 0.0174;
         loop4:
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(name_5)
               {
                  name_3 = 2;
                  _loc3_ = var_752[name_1];
                  _loc3_.var_248 = _loc4_;
                  _loc3_.var_522 = 0;
                  _loc3_.var_244 = var_1367.var_244;
                  _loc3_.var_245 = var_1367.var_245;
                  _loc3_.var_631.gotoAndStop(1);
                  break;
               }
               break;
            case 2:
               _loc3_ = var_752[name_1];
               _loc5_ = Number(Math.cos(_loc3_.var_248));
               _loc6_ = Number(Math.sin(_loc3_.var_248));
               _loc1_ = int(Math.floor(class_888.var_1365));
               _loc2_ = 0;
               while(true)
               {
                  if(_loc2_ >= _loc1_)
                  {
                     break loop4;
                  }
                  _loc2_++;
                  _loc7_ = _loc2_;
                  _loc3_.var_244 = Number(Number(_loc3_.var_244) + _loc5_);
                  _loc3_.var_245 = Number(Number(_loc3_.var_245) + _loc6_);
                  if(method_1023(Number(_loc3_.var_244),Number(_loc3_.var_245)))
                  {
                     _loc3_.var_522 = null;
                     _loc3_.var_248 = null;
                     name_1 = int((name_1 + 1) % 4);
                     name_3 = 3;
                     var_752[name_1].var_631.gotoAndStop(2);
                     break loop4;
                  }
               }
               break;
            case 3:
               if(!name_5)
               {
                  name_3 = 1;
                  break;
               }
               break;
            case 4:
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(var_494,10);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600 - 100 * param1];
         super.method_95(param1);
         name_1 = 0;
         method_117();
         method_72();
      }
      
      public function method_1023(param1:Number, param2:Number) : Boolean
      {
         var _loc3_:Point = var_1369.localToGlobal(new Point(param1,param2));
         return Boolean(var_1369.hitTestPoint(_loc3_.x,_loc3_.y,true));
      }
      
      public function method_1024() : void
      {
         var _loc1_:* = null;
         while(int(var_752.length) > 0)
         {
            _loc1_ = var_752.pop();
            _loc1_.var_631.parent.removeChild(_loc1_.var_631);
            _loc1_.var_94.parent.removeChild(_loc1_.var_94);
         }
      }
      
      public function method_117() : void
      {
         var _loc1_:int = int(Math.round(var_237[0] * 5));
         if(_loc1_ > 4)
         {
            _loc1_ = 4;
         }
         var _loc2_:* = class_888.var_1366[_loc1_];
         class_319 = var_232.method_84(class_899.__unprotect__("eFrl\x03"),0);
         var_1369 = var_232.method_84(class_899.__unprotect__("\x01dm\x03"),class_645.var_209);
         var_1369.gotoAndStop(_loc1_ + 1);
         var_1368 = var_232.method_84(class_899.__unprotect__("E{\'&\x02"),class_645.var_208);
         var_1368.x = int(_loc2_.var_8.var_244);
         var_1368.y = int(_loc2_.var_8.var_245);
         var _loc3_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("aize"),class_645.var_209));
         _loc3_.var_233 = 0.99;
         var_1367 = _loc3_;
         var_1367.var_244 = int(_loc2_.var_64.var_244);
         var_1367.var_245 = int(_loc2_.var_64.var_245);
         var_1367.var_250 = 0.3;
         var_1367.method_118();
         var_1367.var_243.stop();
         var_1367.var_233 = 0.95;
      }
   }
}
