package package_22.package_23
{
   import package_32.class_899;
   
   public class class_733
   {
       
      
      public function class_733()
      {
      }
      
      public function method_473(param1:Array, param2:int, param3:int, param4:int, param5:class_732) : void
      {
         var _loc7_:class_732 = null;
         var _loc8_:class_732 = null;
         var _loc6_:Array = param5.var_296;
         switch(int(param5.var_295))
         {
            default:
               param1[param2] = method_472(param5);
               break;
            case 1:
               _loc7_ = _loc6_[0];
               _loc8_ = _loc6_[1];
               if(param4 > 0)
               {
                  method_473(param1,param2,param3 + 1,param4 - 1,_loc7_);
                  method_473(param1,param2 | 1 << param3,param3 + 1,param4 - 1,_loc8_);
                  break;
               }
               param1[param2] = method_472(param5);
               break;
         }
      }
      
      public function method_474(param1:class_654, param2:int, param3:int, param4:int) : class_732
      {
         if(param4 > param2)
         {
            class_899.var_317 = new Error();
            throw "Invalid huffman";
         }
         var _loc5_:int = param3 << 5 | param4;
         if(param1.method_217(_loc5_))
         {
            return class_732.method_471(param1.method_98(_loc5_));
         }
         param3 = param3 << 1;
         param4 = param4 + 1;
         return class_732.method_470(method_474(param1,param2,param3,param4),method_474(param1,param2,param3 | 1,param4));
      }
      
      public function method_475(param1:class_732) : int
      {
         var _loc3_:class_732 = null;
         var _loc4_:class_732 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc2_:Array = param1.var_296;
         switch(int(param1.var_295))
         {
            case 0:
               §§push(0);
               break;
            case 1:
               _loc3_ = _loc2_[0];
               _loc4_ = _loc2_[1];
               _loc5_ = int(method_475(_loc3_));
               _loc6_ = int(method_475(_loc4_));
               §§push(1 + (_loc5_ < _loc6_?_loc5_:_loc6_));
               break;
            case 2:
               class_899.var_317 = new Error();
               throw "assert";
         }
         return; §§pop();
      }
      
      public function method_472(param1:class_732) : class_732
      {
         var _loc3_:Array = null;
         var _loc4_:class_732 = null;
         var _loc5_:class_732 = null;
         var _loc8_:int = 0;
         var _loc2_:int = int(method_475(param1));
         if(_loc2_ == 0)
         {
            return param1;
         }
         if(_loc2_ == 1)
         {
            _loc3_ = param1.var_296;
            switch(int(param1.var_295))
            {
               default:
                  class_899.var_317 = new Error();
                  throw "assert";
               case 1:
                  _loc4_ = _loc3_[0];
                  _loc5_ = _loc3_[1];
                  return class_732.method_470(method_472(_loc4_),method_472(_loc5_));
            }
         }
         else
         {
            var _loc6_:int = 1 << _loc2_;
            _loc3_ = [];
            var _loc7_:int = 0;
            while(_loc7_ < _loc6_)
            {
               _loc7_++;
               _loc8_ = _loc7_;
               _loc3_.push(class_732.method_471(-1));
            }
            method_473(_loc3_,0,0,_loc2_,param1);
            return class_732.method_469(_loc2_,_loc3_);
         }
      }
      
      public function method_445(param1:Array, param2:int, param3:int, param4:int) : class_732
      {
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc12_:int = 0;
         var _loc5_:Array = [];
         var _loc6_:Array = [];
         if(param4 > 32)
         {
            class_899.var_317 = new Error();
            throw "Invalid huffman";
         }
         var _loc7_:int = 0;
         while(_loc7_ < param4)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc5_.push(0);
            _loc6_.push(0);
         }
         _loc7_ = 0;
         while(_loc7_ < param3)
         {
            _loc7_++;
            _loc8_ = _loc7_;
            _loc9_ = int(param1[_loc8_ + param2]);
            if(_loc9_ >= param4)
            {
               class_899.var_317 = new Error();
               throw "Invalid huffman";
            }
            _loc5_[_loc9_] = _loc5_[_loc9_] + 1;
         }
         _loc7_ = 0;
         _loc8_ = 1;
         _loc9_ = param4 - 1;
         while(_loc8_ < _loc9_)
         {
            _loc8_++;
            _loc10_ = _loc8_;
            _loc7_ = _loc7_ + int(_loc5_[_loc10_]) << 1;
            _loc6_[_loc10_] = _loc7_;
         }
         var _loc11_:class_654 = new class_654();
         _loc8_ = 0;
         while(_loc8_ < param3)
         {
            _loc8_++;
            _loc9_ = _loc8_;
            _loc10_ = int(param1[_loc9_ + param2]);
            if(_loc10_ != 0)
            {
               _loc12_ = int(_loc6_[_loc10_ - 1]);
               _loc6_[_loc10_ - 1] = _loc12_ + 1;
               _loc11_.method_216(_loc12_ << 5 | _loc10_,_loc9_);
            }
         }
         return method_472(class_732.method_470(method_474(_loc11_,param4,0,1),method_474(_loc11_,param4,1,1)));
      }
   }
}
