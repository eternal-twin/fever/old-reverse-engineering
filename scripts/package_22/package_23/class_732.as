package package_22.package_23
{
   import package_32.class_899;
   
   public final class class_732
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["\x1b\x1e^X\x03","\x06\n 7\x01","H7)O\x03"];
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function class_732(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function method_469(param1:int, param2:Array) : class_732
      {
         return new class_732("H7)O\x03",2,[param1,param2]);
      }
      
      public static function method_470(param1:class_732, param2:class_732) : class_732
      {
         return new class_732("\x06\n 7\x01",1,[param1,param2]);
      }
      
      public static function method_471(param1:int) : class_732
      {
         return new class_732("\x1b\x1e^X\x03",0,[param1]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
