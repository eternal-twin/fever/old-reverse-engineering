package package_35.package_36._Fast
{
   import package_32.class_899;
   import package_37.package_38.class_820;
   
   public dynamic class class_815
   {
       
      
      public var __x:Xml;
      
      public function class_815(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         __x = param1;
      }
      
      public function method_657(param1:String) : class_820
      {
         var _loc3_:String = null;
         var _loc2_:Xml = __x.method_768(param1).name_1();
         if(_loc2_ == null)
         {
            _loc3_ = __x.var_880 == Xml.class_771?"Document":__x.method_664();
            class_899.var_317 = new Error();
            throw _loc3_ + " is missing element " + param1;
         }
         return new class_820(_loc2_);
      }
   }
}
