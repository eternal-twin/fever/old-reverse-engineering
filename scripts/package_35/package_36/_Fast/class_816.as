package package_35.package_36._Fast
{
   import package_32.class_899;
   
   public dynamic class class_816
   {
       
      
      public var __x:Xml;
      
      public function class_816(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         __x = param1;
      }
      
      public function method_657(param1:String) : String
      {
         if(__x.var_880 == Xml.class_771)
         {
            class_899.var_317 = new Error();
            throw "Cannot access document attribute " + param1;
         }
         var _loc2_:String = __x.method_98(param1);
         if(_loc2_ == null)
         {
            class_899.var_317 = new Error();
            throw __x.method_664() + " is missing attribute " + param1;
         }
         return _loc2_;
      }
   }
}
