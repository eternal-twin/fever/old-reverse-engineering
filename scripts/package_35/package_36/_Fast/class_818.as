package package_35.package_36._Fast
{
   import package_32.class_899;
   
   public dynamic class class_818
   {
       
      
      public var __x:Xml;
      
      public function class_818(param1:Xml = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         __x = param1;
      }
      
      public function method_657(param1:String) : Boolean
      {
         return Boolean(__x.method_768(param1).method_74());
      }
   }
}
