package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_849 extends class_645
   {
       
      
      public var var_1205:Number;
      
      public var var_183:MovieClip;
      
      public var var_1207:Number;
      
      public var var_1206:Number;
      
      public var var_1208:int;
      
      public var var_1204:Array;
      
      public function class_849()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:Boolean = false;
         var _loc2_:* = null;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_892 = null;
         var _loc6_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = true;
               _loc2_ = {
                  "L\x01":var_183.x,
                  "\x03\x01":var_183.y
               };
               _loc3_ = 0;
               _loc4_ = var_1204;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  if(var_220 == null)
                  {
                     _loc6_ = Number(_loc5_.method_115(_loc2_));
                     if(_loc6_ < var_1205 - var_1206)
                     {
                        method_873(_loc5_,2);
                     }
                     else
                     {
                        _loc1_ = false;
                        method_873(_loc5_,1);
                     }
                  }
                  method_390(_loc5_);
                  method_389(_loc5_);
               }
               if(!!_loc1_ && var_220 == null)
               {
                  _loc3_ = 0;
                  _loc4_ = var_1204;
                  while(_loc3_ < int(_loc4_.length))
                  {
                     _loc5_ = _loc4_[_loc3_];
                     _loc3_++;
                     new class_931(_loc5_.var_243);
                  }
                  method_81(true,20);
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_873(param1:class_656, param2:int) : void
      {
         if(param1.var_243.currentFrame != param2)
         {
            param1.var_243.gotoAndStop(param2);
         }
      }
      
      override public function method_83() : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc1_:* = method_100();
         var _loc2_:int = 0;
         var _loc3_:Array = var_1204;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc5_ = Number(_loc4_.method_115(_loc1_));
            _loc6_ = var_1207 * 2;
            if(_loc5_ < _loc6_)
            {
               _loc7_ = Number(_loc4_.method_231(_loc1_));
               _loc8_ = 10 * (_loc6_ - _loc5_) / _loc6_;
               _loc4_.var_278 = _loc4_.var_278 - Math.cos(_loc7_) * _loc8_;
               _loc4_.var_279 = _loc4_.var_279 - Math.sin(_loc7_) * _loc8_;
            }
         }
         var _loc9_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("EUL\x12"),class_645.var_209));
         _loc9_.var_233 = 0.99;
         _loc4_ = _loc9_;
         _loc4_.var_244 = Number(_loc1_.var_244);
         _loc4_.var_245 = Number(_loc1_.var_245);
         _loc4_.var_233 = 0.95;
         _loc4_.var_243.scaleX = var_1207 * 0.02;
         _loc4_.var_243.scaleY = var_1207 * 0.02;
         _loc4_.method_118();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300];
         super.method_95(param1);
         var_1206 = Number(10 + param1 * 5);
         var_1205 = 70 - param1 * 10;
         var_1208 = 1 + int(Math.floor(param1 * 5));
         var_1207 = 20;
         method_117();
         method_72();
      }
      
      public function method_390(param1:class_892) : void
      {
         var _loc4_:class_892 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:Array = var_1204;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_ != param1)
            {
               _loc5_ = Number(param1.method_115(_loc4_));
               if(_loc5_ < var_1206 * 2)
               {
                  _loc6_ = var_1206 * 2 - _loc5_;
                  _loc7_ = Number(param1.method_231(_loc4_));
                  _loc8_ = Number(Math.sqrt(Number(param1.var_278 * param1.var_278 + param1.var_279 * param1.var_279)));
                  _loc9_ = Number(Math.sqrt(Number(_loc4_.var_278 * _loc4_.var_278 + _loc4_.var_279 * _loc4_.var_279)));
                  _loc10_ = (_loc8_ + _loc9_) * 0.5;
                  param1.var_244 = param1.var_244 - Math.cos(_loc7_) * _loc6_ * 0.5;
                  param1.var_245 = param1.var_245 - Math.sin(_loc7_) * _loc6_ * 0.5;
                  _loc4_.var_244 = Number(_loc4_.var_244 + Math.cos(_loc7_) * _loc6_ * 0.5);
                  _loc4_.var_245 = Number(_loc4_.var_245 + Math.sin(_loc7_) * _loc6_ * 0.5);
                  param1.var_278 = param1.var_278 - Math.cos(_loc7_) * _loc10_;
                  param1.var_279 = param1.var_279 - Math.sin(_loc7_) * _loc10_;
                  _loc4_.var_278 = Number(_loc4_.var_278 + Math.cos(_loc7_) * _loc10_);
                  _loc4_.var_279 = Number(_loc4_.var_279 + Math.sin(_loc7_) * _loc10_);
               }
            }
         }
      }
      
      public function method_389(param1:class_892) : void
      {
         var _loc2_:Number = var_1206;
         if(param1.var_244 < _loc2_ || param1.var_244 > class_742.var_217 - _loc2_)
         {
            param1.var_278 = param1.var_278 * -0.8;
            param1.var_244 = Number(Math.min(Number(Math.max(_loc2_,param1.var_244)),class_742.var_217 - _loc2_));
         }
         if(param1.var_245 < _loc2_ || param1.var_245 > class_742.var_219 - _loc2_)
         {
            param1.var_279 = param1.var_279 * -0.8;
            param1.var_245 = Number(Math.min(Number(Math.max(_loc2_,param1.var_245)),class_742.var_219 - _loc2_));
         }
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:class_892 = null;
         var _loc6_:Number = NaN;
         class_319 = var_232.method_84(class_899.__unprotect__("m_fX\x02"),0);
         var_183 = var_232.method_84(class_899.__unprotect__("\x19\x146I\x01"),class_645.var_209);
         var_183.x = class_742.var_217 * 0.5;
         var_183.y = class_742.var_219 * 0.5;
         var_183.scaleX = var_1205 * 0.02;
         var_183.scaleY = var_1205 * 0.02;
         var_1204 = [];
         var _loc1_:int = 0;
         var _loc2_:int = var_1208;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("\x04\x14OQ\x03"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc4_ = _loc5_;
            do
            {
               _loc4_.var_244 = Number(var_1206 + int(class_691.method_114(int(Math.floor(class_742.var_217 - 2 * var_1206)))));
               _loc4_.var_245 = Number(var_1206 + int(class_691.method_114(int(Math.floor(class_742.var_219 - 2 * var_1206)))));
               _loc6_ = Number(_loc4_.method_115({
                  "L\x01":var_183.x,
                  "\x03\x01":var_183.y
               }));
            }
            while(_loc6_ <= Number(var_1206 + var_1205));
            
            _loc4_.var_243.scaleX = var_1206 * 2 * 0.1;
            _loc4_.var_243.scaleY = var_1206 * 2 * 0.1;
            _loc4_.var_243.stop();
            _loc4_.method_118();
            _loc4_.var_233 = 0.95;
            var_1204.push(_loc4_);
         }
      }
   }
}
