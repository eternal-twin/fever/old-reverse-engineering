package
{
   import flash.display.MovieClip;
   import flash.geom.Point;
   import package_32.class_899;
   
   public class class_843 extends class_645
   {
       
      
      public var var_73:MovieClip;
      
      public var var_14:MovieClip;
      
      public var class_86:MovieClip;
      
      public var package_2:class_892;
      
      public var var_700:Number;
      
      public var var_1187:class_892;
      
      public var var_1188:Number;
      
      public function class_843()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_850();
               method_851();
         }
         super.method_79();
      }
      
      public function method_850() : void
      {
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc1_:* = method_100();
         var _loc2_:Number = package_2.var_244 - _loc1_.var_244;
         var _loc3_:Number = package_2.var_245 - _loc1_.var_245;
         package_2.var_244 = package_2.var_244 - _loc2_ * 0.1;
         package_2.var_245 = package_2.var_245 - _loc3_ * 0.1;
         var _loc4_:Number = Number(package_2.method_115(var_1187));
         var _loc5_:* = null;
         if(_loc4_ < 80)
         {
            _loc5_ = var_1187;
         }
         else
         {
            _loc5_ = {
               "L\x01":Number(_loc1_.var_244),
               "\x03\x01":Number(_loc1_.var_245)
            };
         }
         var _loc6_:int = Number(_loc5_.var_244) < package_2.var_244?-1:1;
         _loc2_ = package_2.var_244 - _loc5_.var_244;
         _loc3_ = package_2.var_245 - _loc5_.var_245;
         package_2.var_243.scaleX = _loc6_;
         package_2.var_243.rotation = Number(Math.atan2(_loc3_,_loc2_) / 0.0174 + (_loc6_ * 0.5 + 0.5) * 180);
         if(name_5)
         {
            package_2.var_243.gotoAndStop("2");
            if(_loc4_ < 80)
            {
               _loc7_ = 1 - _loc4_ / 80;
               _loc8_ = Number(package_2.method_231(var_1187));
               var_1187.var_278 = Number(var_1187.var_278 + Math.cos(_loc8_) * _loc7_ * 0.1);
               var_1187.var_279 = Number(var_1187.var_279 + Math.sin(_loc8_) * _loc7_ * 0.1);
               var_1188 = Number(var_1188 + 0.02 * _loc7_);
            }
         }
         else
         {
            package_2.var_243.gotoAndStop("1");
         }
         var _loc9_:int = 1;
         if(method_222(package_2.var_244,package_2.var_245))
         {
            _loc9_ = 0;
         }
         _loc7_ = _loc9_ - package_2.var_243.alpha;
         package_2.var_243.alpha = Number(Math.min(Number(Math.max(0.2,Number(package_2.var_243.alpha + _loc7_ * 0.15))),1));
      }
      
      public function method_851() : void
      {
         var_700 = (var_700 + (Number(16 + var_1188 * 0))) % 628;
         var_1188 = var_1188 * 0.95;
         var _loc1_:Number = Number(1 + Math.cos(var_700 / 100) * var_1188);
         var_1187.var_243.scaleX = _loc1_;
         var_1187.var_243.scaleY = _loc1_;
         if(var_1187.var_244 < 0)
         {
            method_81(true,10);
         }
         if(method_222(var_1187.var_244,var_1187.var_245))
         {
            var_1187.var_243.play();
            var_1187.var_278 = 0;
            var_1187.var_279 = 0;
            method_81(false,10);
            name_3 = 2;
         }
      }
      
      public function method_222(param1:Number, param2:Number) : Boolean
      {
         var _loc3_:Point = new Point(param1,param2);
         _loc3_ = class_86.localToGlobal(new Point(param1,param2));
         param1 = _loc3_.x;
         param2 = _loc3_.y;
         return !class_86.hitTestPoint(param1,param2,true) || Boolean(var_14.hitTestPoint(param1,param2,true)) || Boolean(var_73.hitTestPoint(param1,param2,true));
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [340];
         super.method_95(param1);
         var_1188 = 0;
         var_700 = 0;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("\x01#Z\x1f\x02"),class_645.var_209);
         var_14 = Reflect.field(class_319,"_s1");
         var_73 = Reflect.field(class_319,"_s2");
         class_86 = Reflect.field(class_319,"_mesh");
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\x16TC8\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         package_2 = _loc1_;
         package_2.var_244 = class_742.var_217 - 10;
         package_2.var_245 = class_742.var_219 * 0.5;
         package_2.var_243.stop();
         package_2.method_118();
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("\'\x0b\x11U\x03"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_1187 = _loc1_;
         var_1187.var_244 = class_742.var_217 - 24;
         var_1187.var_245 = class_742.var_219 * 0.5;
         var_1187.var_250 = 0.004;
         var_1187.method_118();
         var _loc2_:String = class_691.method_97(1 + int(Math.floor(var_237[0] * 10)));
         var_14.gotoAndStop(_loc2_);
         var_73.gotoAndStop(_loc2_);
      }
   }
}
