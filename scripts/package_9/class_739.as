package package_9
{
   import flash.display.DisplayObjectContainer;
   import flash.display.Sprite;
   import package_32.class_899;
   
   public class class_739
   {
       
      
      public var var_245:Number;
      
      public var var_244:Number;
      
      public var var_250:Number;
      
      public var var_279:Number;
      
      public var var_278:Number;
      
      public var var_580:Number;
      
      public var var_251:int;
      
      public var name_20:Object;
      
      public var var_344:Number;
      
      public var var_243:Sprite;
      
      public var var_1518:Number;
      
      public var var_233:Number;
      
      public var var_272:int;
      
      public var var_273:int;
      
      public function class_739(param1:Sprite = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         if(param1 == null)
         {
            param1 = new Sprite();
         }
         var_243 = param1;
         var_244 = 0;
         var_245 = 0;
         var_278 = 0;
         var_279 = 0;
         var_580 = 0;
         var_344 = 1;
         var_233 = 1;
         var_1518 = 1;
         var_273 = 10;
         var_272 = 0;
         var_251 = 10 + int(class_691.method_114(10));
      }
      
      public function method_118() : void
      {
         var_243.x = var_244;
         var_243.y = var_245;
      }
      
      public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:Number = NaN;
         var_279 = Number(var_279 + var_250);
         var_278 = var_278 * var_233;
         var_279 = var_279 * var_233;
         var_244 = Number(var_244 + var_278);
         var_245 = Number(var_245 + var_279);
         var_580 = var_580 * var_1518;
         var_243.rotation = Number(var_243.rotation + var_580);
         var_251 = var_251 - 1;
         if(var_251 < var_273)
         {
            _loc1_ = var_251 / var_273;
            switch(var_272)
            {
               case 0:
                  _loc2_ = _loc1_ * var_344;
                  var_243.scaleY = _loc2_;
                  var_243.scaleX = _loc2_;
               case 1:
                  _loc2_ = _loc1_ * var_344;
                  var_243.scaleY = _loc2_;
                  var_243.scaleX = _loc2_;
            }
         }
         if(var_251 == 0)
         {
            method_89();
         }
         method_118();
      }
      
      public function method_119(param1:Number) : void
      {
         var_344 = param1;
         var _loc2_:Number = var_344;
         var_243.scaleY = _loc2_;
         var_243.scaleX = _loc2_;
      }
      
      public function method_129(param1:Number, param2:Number) : void
      {
         var_244 = param1;
         var_245 = param2;
         method_118();
      }
      
      public function method_89() : void
      {
         if(var_243.parent != null)
         {
            var_243.parent.removeChild(var_243);
         }
      }
   }
}
