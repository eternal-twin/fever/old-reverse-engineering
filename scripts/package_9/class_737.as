package package_9
{
   import package_13.class_719;
   import package_24.class_738;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_747;
   
   public class class_737 extends class_736
   {
       
      
      public var var_730:class_738;
      
      public var name_3:int;
      
      public function class_737()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_730 = new class_738();
         var_730.method_128(class_702.var_288.method_98(1,"hero_hurt"));
         var_730.x = var_17.var_243.x;
         var_730.y = var_17.var_243.y - 5;
         new class_747(var_730,40,4,4);
         class_652.var_214.var_267.var_232.method_124(var_730,class_719.var_285);
         var_17.var_282.var_198 = null;
         var_17.var_282.method_128(class_702.var_288.method_98(0,"hero_hurt"));
         name_3 = 0;
         var_254 = 0;
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:* = null as package_24.class_739;
         super.method_79();
         loop1:
         switch(name_3)
         {
            case 0:
               var_254 = Number(Math.min(Number(var_254 + 0.01),1));
               if(var_254 == 1)
               {
                  name_3 = name_3 + 1;
                  var_254 = 0;
                  break;
               }
               break;
            case 1:
               var_254 = Number(Math.min(Number(var_254 + 0.05),1));
               if(var_254 == 1)
               {
                  var_17.method_107();
                  var_730.parent.removeChild(var_730);
                  method_89();
                  class_652.var_214.method_135(true);
                  _loc1_ = 8;
                  _loc2_ = 0;
                  while(true)
                  {
                     if(_loc2_ >= _loc1_)
                     {
                        break loop1;
                     }
                     _loc2_++;
                     _loc3_ = _loc2_;
                     _loc4_ = _loc3_ / _loc1_ * 6.28;
                     _loc5_ = Math.random() * 0.8;
                     _loc6_ = new package_24.class_739();
                     method_482(_loc6_);
                     _loc6_.var_278 = Math.cos(_loc4_) * _loc5_;
                     _loc6_.var_279 = Math.sin(_loc4_) * _loc5_;
                     _loc6_.var_262 = Number(var_730.x + _loc6_.var_278 * 10);
                     _loc6_.var_263 = Number(var_730.y + _loc6_.var_279 * 10);
                     _loc6_.method_118();
                     _loc6_.var_250 = Number(0.05 + Math.random() * 0.05);
                     _loc6_.var_251 = 10 + int(class_691.method_114(8));
                     _loc6_.var_233 = 0.95;
                     _loc6_.method_128(class_702.package_9.method_98(int(class_691.method_114(7)),"grey_stones"));
                  }
                  break;
               }
         }
      }
   }
}
