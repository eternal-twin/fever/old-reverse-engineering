package package_9
{
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_660;
   import package_13.class_719;
   import package_13.class_777;
   import package_13.class_826;
   import package_13.class_868;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_648 extends class_647
   {
      
      public static var var_261:Number = 0.1;
       
      
      public var var_283:class_660;
      
      public var var_280:class_826;
      
      public var var_277:class_351;
      
      public var var_266:class_719;
      
      public var var_270:Object;
      
      public var var_284:class_719;
      
      public var var_17:class_777;
      
      public var var_265:int;
      
      public var var_286:Array;
      
      public function class_648(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_265 = param1;
         class_652.var_214.method_135(false);
         var_254 = 0;
         var _loc2_:class_719 = class_652.var_214.var_267;
         var _loc3_:Array = class_742.var_264[param1];
         var _loc4_:* = _loc2_.method_141(param1);
         var_284 = new class_719(int(_loc4_.var_244),int(_loc4_.var_245));
         var_284.method_142();
         class_652.var_214.var_232.method_124(var_284,class_652.var_287);
         var _loc5_:int = class_719.var_268;
         var _loc6_:int = class_719.var_269;
         var_284.x = Number(_loc2_.x + int(_loc3_[0]) * _loc5_ * 16);
         var_284.y = Number(_loc2_.y + int(_loc3_[1]) * _loc6_ * 16);
         var_17 = class_652.var_214.var_288;
         var_17.method_143(var_284);
         var_280 = var_17.var_267.method_144(int((param1 + 2) % 4));
         var_17.method_129(var_17.var_289.var_244 - int(_loc3_[0]) * _loc5_,var_17.var_289.var_245 - int(_loc3_[1]) * _loc6_);
         var_17.method_145(param1,true);
         var_17.var_282.var_198.name_8(2);
         var_283 = new class_660(var_17.var_243.x,var_17.var_243.y,(var_280.var_244 + 0.5) * 16,(var_280.var_245 + 1) * 16);
         class_652.var_214.var_267 = var_284;
         var_266 = _loc2_;
         var_270 = method_127(0);
         var_277 = new class_351();
         var_277.scaleY = Number(0.1);
         var_277.scaleX = 0.1;
         var_284.var_232.method_124(var_277,class_719.var_285);
         class_658.var_146(var_277,2,10,16777215);
      }
      
      public function method_126(param1:package_24.class_739) : void
      {
         var _loc2_:package_24.class_739 = method_125();
         _loc2_.var_262 = param1.x + int(class_691.method_114(9)) - 4;
         _loc2_.var_263 = param1.y + int(class_691.method_114(9)) - 4;
         _loc2_.method_118();
      }
      
      override public function method_79() : void
      {
         var _loc3_:Number = NaN;
         var _loc4_:* = null;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Boolean = false;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc13_:* = null as package_24.class_739;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var_254 = Number(Math.min(Number(var_254 + 0.015),1));
         var _loc1_:* = method_127(var_254);
         var_17.var_243.x = int(Number(_loc1_.var_244));
         var_17.var_243.y = int(Number(_loc1_.var_245));
         var _loc2_:Array = class_742.var_264[var_265];
         var_266.x = class_652.var_214.var_267.x - int(_loc2_[0]) * class_719.var_268 * 16;
         var_266.y = class_652.var_214.var_267.y - int(_loc2_[1]) * class_719.var_269 * 16;
         var var_214:class_648 = this;
         if(var_270 != null)
         {
            _loc3_ = Number(var_254 + class_648.var_261);
            _loc4_ = method_127(Number(Math.min(_loc3_,1)));
            _loc5_ = _loc4_.var_244 - var_270.var_244;
            _loc6_ = _loc4_.var_245 - var_270.var_245;
            var var_31:package_24.class_739 = new package_24.class_739();
            var_31.method_128(class_702.package_9.method_98(null,"rainbow"),0,0.5);
            var_31.method_129(Number(var_270.var_244),Number(var_270.var_245));
            var_31.rotation = Math.atan2(_loc6_,_loc5_) / 0.0174;
            var_31.scaleX = (Number(Math.sqrt(Number(_loc5_ * _loc5_ + _loc6_ * _loc6_))) + 2) / 16;
            var_31.var_251 = 50;
            var_31.var_271 = false;
            var_31.var_272 = 4;
            var_31.var_273 = 5;
            var_31.var_274 = function():void
            {
               var_214.method_126(var_31);
            };
            _loc7_ = var_254 < 0.5;
            _loc8_ = var_17.var_265 != 3?class_719.var_275:class_719.var_276;
            class_652.var_214.var_267.var_232.method_124(var_31,_loc8_);
            var_270 = _loc4_;
            var_277.x = Number(_loc4_.var_244);
            var_277.y = Number(_loc4_.var_245);
            var_277.rotation = Number(var_277.rotation + 4);
            if(_loc3_ > 0.99)
            {
               var_270 = null;
               var_277.parent.removeChild(var_277);
               _loc9_ = 32;
               _loc10_ = 3;
               _loc11_ = 0;
               while(_loc11_ < _loc9_)
               {
                  _loc11_++;
                  _loc12_ = _loc11_;
                  _loc13_ = method_125();
                  _loc14_ = _loc12_ / _loc9_ * 6.28;
                  _loc15_ = Math.random() * 3;
                  _loc13_.var_278 = Math.cos(_loc14_) * _loc15_;
                  _loc13_.var_279 = Math.sin(_loc14_) * _loc15_ - 1.5;
                  _loc13_.method_129(Number(var_277.x + _loc13_.var_278 * _loc10_),Number(var_277.y + _loc13_.var_279 * _loc10_));
               }
            }
         }
         if(var_254 == 1)
         {
            var_17.method_130(var_280);
            var_17.var_267.var_281 = var_280;
            var_266.method_89();
            class_868.var_214.method_132(var_17.var_267.method_131());
            var_17.var_282.var_198.method_133(0);
            var_17.var_282.var_198.method_134();
            method_89();
            class_652.var_214.method_135(true);
         }
      }
      
      public function method_127(param1:Object) : Object
      {
         var _loc2_:* = var_283.method_127(param1);
         _loc2_.var_245 = _loc2_.var_245 - Math.sin(param1 * 3.14) * 100;
         return _loc2_;
      }
      
      public function method_125() : package_24.class_739
      {
         var _loc1_:package_24.class_739 = new package_24.class_739();
         _loc1_.method_137(class_702.package_9.method_136("twinkle_gray"));
         class_657.method_139(_loc1_,int(class_657.method_138(Number(Math.random()))));
         _loc1_.var_233 = 0.95;
         _loc1_.var_250 = Number(0.05 + Math.random() * 0.05);
         _loc1_.var_251 = 12 + int(class_691.method_114(12));
         _loc1_.var_198.method_140();
         var_284.var_232.method_124(_loc1_,class_719.var_285);
         return _loc1_;
      }
   }
}
