package package_9
{
   import flash.geom.Point;
   import package_13.class_719;
   import package_13.class_826;
   import package_24.class_738;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_869 extends class_647
   {
       
      
      public var var_251:int;
      
      public var var_107:class_367;
      
      public var var_289:class_826;
      
      public var var_267:class_719;
      
      public var name_78:Array;
      
      public function class_869(param1:class_719 = undefined, param2:class_367 = undefined, param3:class_826 = undefined)
      {
         var _loc6_:class_826 = null;
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         var _loc9_:* = null;
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_289 = param3;
         var_107 = param2;
         var_267 = param1;
         name_78 = [];
         var _loc4_:int = 0;
         var _loc5_:Array = var_267.var_455;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc7_ = 0;
            _loc8_ = _loc6_.name_78;
            while(_loc7_ < int(_loc8_.length))
            {
               _loc9_ = _loc8_[_loc7_];
               _loc7_++;
               name_78.push(_loc9_);
            }
         }
         var_251 = 26;
      }
      
      override public function method_79() : void
      {
         var _loc3_:* = null;
         var _loc4_:Point = null;
         var _loc5_:Boolean = false;
         var _loc6_:Boolean = false;
         var _loc7_:class_786 = null;
         var _loc1_:Array = name_78.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc4_ = _loc3_.var_475.localToGlobal(new Point(0,0));
            _loc5_ = Boolean(var_267.var_1309.hitTestPoint(_loc4_.x,_loc4_.y,true));
            _loc6_ = _loc5_;
            if(int(_loc3_.var_252) == 1)
            {
               _loc6_ = !_loc5_;
            }
            if(_loc3_.var_475.visible != _loc6_)
            {
               _loc7_ = new class_786(_loc3_.var_475,_loc6_);
               name_78.method_116(_loc3_);
               if(var_107 == null)
               {
                  _loc7_.var_254 = 1;
               }
            }
         }
         if(var_107 == null)
         {
            method_89();
            return;
         }
         var_251 = var_251 - 1;
         if(var_251 > 0)
         {
            return;
         }
         var_107.scaleX = var_107.scaleX - 0.01;
         var_107.scaleX = var_107.scaleX * 0.95;
         var_107.scaleY = var_107.scaleX;
         if(var_107.scaleX < 0.05)
         {
            method_89();
         }
      }
   }
}
