package package_9
{
   import flash.display.BlendMode;
   import flash.display.Graphics;
   import flash.display.Sprite;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_13.class_710;
   import package_13.class_719;
   import package_13.class_777;
   import package_13.class_826;
   import package_18.package_19.class_872;
   import package_24.class_656;
   import package_24.class_750;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_873;
   import package_8.package_9.class_931;
   
   public class class_676 extends class_647
   {
       
      
      public var var_445:Array;
      
      public var var_453:class_656;
      
      public var var_251:int;
      
      public var name_3:int;
      
      public var var_446:Array;
      
      public var var_144:Boolean;
      
      public var var_447:class_872;
      
      public var var_267:class_719;
      
      public var var_215:int;
      
      public var var_288:class_777;
      
      public var var_448:class_349;
      
      public var var_450:Array;
      
      public var var_265:int;
      
      public var var_452:class_826;
      
      public var var_454:Array;
      
      public var var_449:Sprite;
      
      public function class_676(param1:int = 0)
      {
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:class_872 = null;
         var _loc5_:Number = NaN;
         var _loc6_:Array = null;
         var _loc7_:Array = null;
         var _loc8_:class_826 = null;
         var _loc9_:class_656 = null;
         var _loc10_:Sprite = null;
         var _loc11_:* = null;
         var _loc12_:int = 0;
         var _loc13_:int = 0;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_215 = param1;
         var_288 = class_652.var_214.var_288;
         var_267 = var_288.var_267;
         name_3 = 0;
         var_144 = false;
         loop6:
         switch(param1)
         {
            case 0:
               var_144 = int(var_267.var_242.length) > 0;
               break;
            case 1:
               var_144 = int(var_267.var_242.length) > 0;
               break;
            case 2:
               _loc2_ = 0;
               _loc3_ = var_267.var_242;
               while(_loc2_ < int(_loc3_.length))
               {
                  _loc4_ = _loc3_[_loc2_];
                  _loc2_++;
                  if(_loc4_.var_289.var_244 == var_288.var_289.var_244 || _loc4_.var_289.var_245 == var_288.var_289.var_245)
                  {
                     var_144 = true;
                     break loop6;
                  }
               }
         }
         if(!var_144)
         {
            method_89();
            return;
         }
         var_288.var_282.method_290(class_702.var_288.method_289(["hero_sorcery","hero_sorcery_loop"]));
         var_446 = [];
         var_251 = 0;
         var_454 = [];
         loop7:
         switch(param1)
         {
            case 0:
               var_447 = var_288.method_297();
               var_449 = new Sprite();
               var_449 = var_267.var_232.method_124(var_449,class_719.var_451);
               var_449.x = var_447.x;
               var_449.y = var_447.y - 1;
               var_448 = new class_349();
               _loc5_ = 0;
               var_448.scaleY = _loc5_;
               var_448.scaleX = _loc5_;
               var_449.addChild(var_448);
               break;
            case 1:
               var_450 = [];
               _loc3_ = [];
               _loc6_ = var_267.var_242.method_78();
               _loc2_ = 0;
               _loc7_ = var_267.var_455;
               while(_loc2_ < int(_loc7_.length))
               {
                  _loc8_ = _loc7_[_loc2_];
                  _loc2_++;
                  if(_loc8_.package_19 == null)
                  {
                     _loc3_.push(_loc8_);
                  }
               }
               class_659.method_241(_loc3_);
               while(int(_loc6_.length) > 0 || int(_loc3_.length) > int(var_267.var_455.length) * 0.5)
               {
                  _loc8_ = null;
                  _loc4_ = null;
                  if(int(_loc6_.length) > 0)
                  {
                     _loc4_ = _loc6_.pop();
                     _loc8_ = _loc4_.var_289;
                  }
                  else
                  {
                     _loc8_ = _loc3_.pop();
                  }
                  _loc9_ = new class_656();
                  _loc9_.method_137(class_702.package_9.method_136("fireball"));
                  _loc2_ = 150 + int(class_691.method_114(200));
                  _loc9_.x = -_loc2_;
                  _loc9_.y = -_loc2_;
                  _loc10_ = new Sprite();
                  _loc10_.addChild(_loc9_);
                  var_267.var_232.method_124(_loc10_,class_719.var_451);
                  _loc11_ = _loc8_.method_294();
                  _loc10_.x = Number(_loc11_.var_244);
                  _loc10_.y = Number(_loc11_.var_245);
                  var_450.push({
                     "h`\x01":_loc9_,
                     "\x1by$\x01\x02":_loc10_,
                     "JG\x01":_loc8_,
                     "\x0e\'\\\x01":_loc4_
                  });
               }
               break;
            case 2:
               var_445 = [];
               var_453 = new class_656();
               var_453.method_137(class_702.package_9.method_136("tornado"));
               var_267.var_232.method_124(var_453,class_719.var_451);
               var_254 = 0;
               var_452 = var_288.var_289;
               _loc2_ = 1;
               while(_loc2_ < 20)
               {
                  _loc2_++;
                  _loc12_ = _loc2_;
                  _loc13_ = 0;
                  while(_loc13_ < 4)
                  {
                     _loc13_++;
                     _loc14_ = _loc13_;
                     _loc3_ = class_742.var_264[_loc14_];
                     _loc15_ = var_288.var_289.var_244 + int(_loc3_[0]) * _loc12_;
                     _loc16_ = var_288.var_289.var_245 + int(_loc3_[1]) * _loc12_;
                     _loc8_ = var_267.method_98(_loc15_,_loc16_);
                     if(_loc8_ == null)
                     {
                        break;
                     }
                     _loc17_ = 0;
                     _loc6_ = var_267.var_242;
                     while(_loc17_ < int(_loc6_.length))
                     {
                        _loc4_ = _loc6_[_loc17_];
                        _loc17_++;
                        if(_loc8_.package_19 == _loc4_)
                        {
                           var_447 = _loc4_;
                           var_265 = _loc14_;
                           break;
                        }
                     }
                     if(var_447 != null)
                     {
                        break;
                     }
                  }
                  if(var_447 != null)
                  {
                     break loop7;
                  }
               }
         }
      }
      
      public function method_287() : void
      {
         var _loc3_:* = null as package_24.class_739;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc1_:Array = var_446.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc4_ = 0.02;
            _loc5_ = var_288.var_243.x - _loc3_.x;
            _loc6_ = var_288.var_243.y - 8 - _loc3_.y;
            _loc3_.var_278 = Number(_loc3_.var_278 + _loc5_ * _loc4_);
            _loc3_.var_279 = Number(_loc3_.var_279 + _loc6_ * _loc4_);
            _loc7_ = Number(Math.sqrt(Number(_loc5_ * _loc5_ + _loc6_ * _loc6_)));
            if(_loc7_ < 4)
            {
               _loc3_.method_89();
               var_446.method_116(_loc3_);
            }
         }
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:Array = null;
         var _loc4_:* = null as package_24.class_739;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc12_:* = null;
         var _loc13_:Number = NaN;
         var _loc14_:Graphics = null;
         var _loc15_:Boolean = false;
         var _loc16_:int = 0;
         var _loc17_:Number = NaN;
         var _loc18_:class_656 = null;
         var _loc19_:* = null;
         var _loc20_:Array = null;
         var _loc21_:class_872 = null;
         var _loc22_:Sprite = null;
         var _loc23_:Array = null;
         super.method_79();
         loop10:
         switch(name_3)
         {
            case 0:
               _loc1_ = 4;
               if(var_251 > 32)
               {
                  _loc1_--;
               }
               if(var_251 > 64)
               {
                  _loc1_--;
               }
               if(int(class_652.var_214.var_251 % _loc1_) == 0)
               {
                  method_288();
               }
               method_287();
               _loc2_ = var_251;
               var_251 = var_251 + 1;
               if(_loc2_ == 80)
               {
                  name_3 = (var_215 + 1) * 10;
                  var_251 = 0;
                  var_288.var_282.method_290(class_702.var_288.method_289(["hero_slash","hero_stand"]));
                  if(var_447 != null && !method_291(var_447))
                  {
                     var_447.var_282.var_198.name_8(0);
                  }
                  _loc2_ = 0;
                  _loc3_ = var_446;
                  while(_loc2_ < int(_loc3_.length))
                  {
                     _loc4_ = _loc3_[_loc2_];
                     _loc2_++;
                     switch(var_215)
                     {
                        case 0:
                        case 1:
                        case 2:
                           _loc4_.var_279 = -(1 + Math.random() * 10);
                           _loc4_.var_278 = _loc4_.var_278 * 0.5;
                           _loc4_.var_251 = 10 + int(class_691.method_114(10));

                     }
                  }
                  break;
               }
               break;
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               break;
            case 10:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               _loc1_ = 16;
               _loc2_ = var_251;
               var_251 = var_251 + 1;
               _loc5_ = _loc2_ / _loc1_;
               _loc6_ = Number(Math.min(Math.sin(_loc5_ * 3.14) * 1.5,1));
               _loc7_ = Number(class_663.method_99(0,Number(Math.sin((_loc5_ - 0.15) * 3.14)),1));
               _loc8_ = _loc7_ * 0.3;
               var_448.scaleY = _loc8_;
               var_448.scaleX = _loc8_;
               _loc2_ = 20;
               _loc9_ = 2;
               _loc3_ = [{
                  "L\x01":0,
                  "\x03\x01":0
               }];
               _loc10_ = 0;
               while(_loc10_ < 10)
               {
                  _loc10_++;
                  _loc11_ = _loc10_;
                  _loc12_ = _loc3_[0];
                  _loc8_ = Number(Number(_loc12_.var_244) + (Math.random() * 2 - 1) * _loc9_);
                  _loc13_ = _loc12_.var_245 - _loc2_;
                  _loc3_.unshift({
                     "L\x01":_loc8_,
                     "\x03\x01":_loc13_
                  });
                  _loc9_++;
               }
               _loc14_ = var_449.graphics;
               _loc14_.clear();
               _loc10_ = 0;
               while(_loc10_ < 2)
               {
                  _loc10_++;
                  _loc11_ = _loc10_;
                  _loc15_ = true;
                  _loc8_ = _loc6_ * 16;
                  if(_loc11_ == 1)
                  {
                     _loc8_ = 2;
                  }
                  _loc16_ = 0;
                  while(_loc16_ < int(_loc3_.length))
                  {
                     _loc12_ = _loc3_[_loc16_];
                     _loc16_++;
                     _loc8_ = _loc8_ * 0.85;
                     _loc14_.lineStyle(_loc8_,16777215);
                     if(_loc15_)
                     {
                        _loc14_.moveTo(Number(_loc12_.var_244),Number(_loc12_.var_245));
                        _loc15_ = false;
                     }
                     else
                     {
                        _loc13_ = Number(_loc12_.var_244);
                        _loc17_ = Number(_loc12_.var_245);
                        if(_loc11_ == 1)
                        {
                           _loc13_ = Number(_loc13_ + (int(class_691.method_114(16)) - 8));
                           _loc17_ = Number(_loc17_ + (int(class_691.method_114(16)) - 8));
                        }
                        _loc14_.lineTo(_loc13_,_loc17_);
                     }
                  }
               }
               var_449.filters = [];
               class_658.var_146(var_449,4 * _loc6_,4 * _loc6_,16776960);
               class_658.var_146(var_449,10 * _loc6_,_loc6_,16776960);
               var_449.blendMode = BlendMode.ADD;
               class_657.gfof(var_447,_loc6_,0);
               if(!!var_447.visible && _loc5_ > 0.5)
               {
                  method_292(var_447);
               }
               if(_loc5_ == 1)
               {
                  _loc14_.clear();
                  var_449.parent.removeChild(var_449);
                  name_2();
                  break;
               }
               break;
            case 20:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
            default:
               _loc3_ = var_450.method_78();
               _loc1_ = 0;
               while(_loc1_ < int(_loc3_.length))
               {
                  _loc12_ = _loc3_[_loc1_];
                  _loc1_++;
                  _loc12_.name_42.y = Number(_loc12_.name_42.y + 5);
                  _loc12_.name_42.x = _loc12_.name_42.y;
                  _loc12_.name_42.method_293();
                  if(_loc12_.name_42.y > 0)
                  {
                     _loc12_.name_42.method_89();
                     _loc12_.var_94.parent.removeChild(_loc12_.var_94);
                     var_450.method_116(_loc12_);
                     if(_loc12_.var_447 != null)
                     {
                        method_292(_loc12_.var_447);
                     }
                     new class_873(class_652.var_214.var_267,0,8,0.5);
                     _loc18_ = new class_656();
                     _loc18_.method_137(class_702.package_9.method_136("square_explosion"));
                     _loc19_ = _loc12_.var_289.method_294();
                     _loc18_.x = Number(_loc19_.var_244);
                     _loc18_.y = Number(_loc19_.var_245);
                     _loc18_.method_293();
                     var_267.var_232.method_124(_loc18_,class_719.var_451);
                     _loc18_.var_198.var_258 = _loc18_.method_89;
                     new class_931(class_652.var_214.var_267,0.2);
                  }
               }
               if(int(_loc3_.length) == 0)
               {
                  name_2();
                  break;
               }
               break;
            case 30:
               var_254 = Number(var_254 + 0.1);
               _loc3_ = class_742.var_264[var_265];
               while(var_254 > 1)
               {
                  var_254 = var_254 - 1;
                  var_452 = var_267.method_98(var_452.var_244 + int(_loc3_[0]),var_452.var_245 + int(_loc3_[1]));
                  _loc1_ = 0;
                  _loc20_ = var_267.var_242;
                  while(_loc1_ < int(_loc20_.length))
                  {
                     _loc21_ = _loc20_[_loc1_];
                     _loc1_++;
                     if(!method_291(_loc21_))
                     {
                        if(_loc21_.var_289 == var_452)
                        {
                           var_453.addChild(_loc21_);
                           _loc21_.x = _loc21_.x - var_453.x;
                           _loc21_.y = _loc21_.y - var_453.y;
                           var_445.push(_loc21_);
                           _loc22_ = new Sprite();
                           _loc12_ = _loc21_.var_289.method_294();
                           var_453.addChild(_loc22_);
                           _loc22_.graphics.beginFill(16711680);
                           _loc22_.graphics.drawRect(-8,-42,16,32);
                           _loc21_.mask = _loc22_;
                           var_454.push(_loc21_.var_289.var_215);
                        }
                     }
                  }
               }
               _loc20_ = var_445.method_78();
               _loc1_ = 0;
               while(_loc1_ < int(_loc20_.length))
               {
                  _loc21_ = _loc20_[_loc1_];
                  _loc1_++;
                  if(!method_291(_loc21_))
                  {
                     if(int(class_652.var_214.var_251 % 2) == 0)
                     {
                        _loc21_.y = _loc21_.y + 1;
                     }
                     if(_loc21_.y > 16)
                     {
                        _loc21_.method_295();
                        _loc21_.mask.parent.removeChild(_loc21_.mask);
                        var_445.method_116(_loc21_);
                     }
                  }
               }
               if(var_452 == null || Number(var_452.method_296(var_288.var_289)) > 6)
               {
                  _loc1_ = 0;
                  _loc23_ = var_445;
                  while(_loc1_ < int(_loc23_.length))
                  {
                     _loc21_ = _loc23_[_loc1_];
                     _loc1_++;
                     _loc21_.method_295();
                  }
                  var_453.method_89();
                  name_2();
                  return;
               }
               _loc12_ = var_452.method_294();
               var_453.x = Number(Number(_loc12_.var_244) + var_254 * int(_loc3_[0]) * 16);
               var_453.y = Number(Number(_loc12_.var_245) + var_254 * int(_loc3_[1]) * 16);
               var_453.method_293();
               _loc1_ = 0;
               _loc23_ = var_267.var_242;
               while(true)
               {
                  if(_loc1_ >= int(_loc23_.length))
                  {
                     break loop10;
                  }
                  _loc21_ = _loc23_[_loc1_];
                  _loc1_++;
                  if(!method_291(_loc21_))
                  {
                     _loc5_ = _loc21_.x - var_453.x;
                     _loc6_ = _loc21_.y - var_453.y;
                     _loc7_ = Number(Math.sqrt(Number(_loc5_ * _loc5_ + _loc6_ * _loc6_)));
                     _loc2_ = 14;
                     if(_loc7_ < _loc2_)
                     {
                        _loc8_ = Number(Math.pow(1 - _loc7_ / _loc2_,0.5));
                        _loc21_.var_282.y = -_loc8_ * 20;
                        _loc21_.var_282.method_293();
                     }
                  }
               }
               break;
         }
      }
      
      public function method_291(param1:class_872) : Boolean
      {
         return int(param1.package_31._id) == 11;
      }
      
      public function method_288() : void
      {
         var _loc1_:Number = Math.random() * 6.28;
         var _loc2_:Number = Number(16 + Math.random() * 16);
         var _loc3_:Number = Number(0.5 + Math.random() * 2);
         var _loc4_:package_24.class_739 = new package_24.class_739();
         _loc4_.method_290(class_702.package_9.method_289(["spark_grow","spark_grow_loop"]));
         var_267.var_232.method_124(_loc4_,class_719.var_285);
         _loc4_.var_262 = Number(var_288.var_243.x + Math.cos(_loc1_) * _loc2_);
         _loc4_.var_263 = var_288.var_243.y + Math.sin(_loc1_) * _loc2_ - 8;
         _loc4_.method_118();
         _loc1_ = Number(_loc1_ + 1.57);
         _loc4_.var_278 = Math.cos(_loc1_) * _loc3_;
         _loc4_.var_279 = Math.sin(_loc1_) * _loc3_;
         _loc4_.var_233 = 0.95;
         var_446.push(_loc4_);
         class_657.method_236(_loc4_,int([16776960,16737792,26367][var_215]),-255);
      }
      
      public function name_2() : void
      {
         var_288.var_265 = -1;
         class_652.var_214.method_186(_PlayerAction._Burn(var_454,[_IslandBonus.var_398,_IslandBonus.var_400,_IslandBonus.var_399][var_215]));
         class_652.var_214.method_135(true);
         var_267.method_179(var_288.var_289);
         method_89();
      }
      
      public function method_292(param1:class_872) : void
      {
         var _loc4_:int = 0;
         var _loc5_:* = null as package_24.class_739;
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         if(method_291(param1))
         {
            return;
         }
         param1.visible = false;
         param1.method_295();
         var_454.push(param1.var_289.var_215);
         var _loc3_:int = 0;
         while(_loc3_ < 18)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new package_24.class_739();
            _loc5_.method_128(class_702.package_9.method_98(0,"spark_twinkle"));
            var_267.var_232.method_124(_loc5_,class_719.var_451);
            _loc5_.var_262 = param1.x + int(class_691.method_114(9)) - 4;
            _loc5_.var_263 = param1.y + int(class_691.method_114(11)) - 11;
            _loc5_.method_118();
            _loc6_ = int(class_691.method_114(2)) + 1;
            _loc5_.var_250 = -(0.015 + Math.random() * 0.75) / _loc6_;
            _loc5_.var_279 = _loc5_.var_250 * 8;
            _loc5_.var_233 = 0.98;
            _loc5_.var_251 = 10 + int(class_691.method_114(8));
            if(_loc4_ == 0)
            {
               _loc5_.var_251 = _loc5_.var_251 + 10;
            }
            _loc7_ = _loc6_;
            _loc5_.scaleY = _loc7_;
            _loc5_.scaleX = _loc7_;
            class_657.gfof(_loc5_,1,0);
         }
      }
   }
}
