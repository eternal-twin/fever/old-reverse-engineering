package package_9
{
   import package_13.class_868;
   import package_24.class_738;
   import package_24.class_878;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_931;
   
   public class class_847 extends class_647
   {
       
      
      public var var_1198:class_738;
      
      public function class_847()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var _loc1_:class_868 = class_868.var_214;
         _loc1_.method_191(true);
         _loc1_.method_869();
         var_1198 = _loc1_.method_870();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:* = null as package_24.class_739;
         var _loc5_:int = 0;
         var_1198.x = Number(var_1198.x + 8);
         _loc1_ = 0;
         while(_loc1_ < 1)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = method_125();
            _loc3_.method_129(var_1198.x - int(class_691.method_114(8)),var_1198.y);
         }
         var _loc4_:Number = class_868.var_214.var_1199 - 16;
         if(var_1198.x > _loc4_)
         {
            new class_931(class_652.var_214.var_109);
            _loc1_ = 16;
            _loc2_ = 0;
            while(_loc2_ < _loc1_)
            {
               _loc2_++;
               _loc5_ = _loc2_;
               _loc3_ = method_125();
               _loc3_.var_262 = Number(_loc4_ + (_loc5_ / _loc1_ * 2 - 1) * 20);
               _loc3_.var_263 = 2 + int(class_691.method_114(12));
               _loc3_.var_279 = 0;
               _loc3_.method_118();
            }
            class_868.var_214.method_195();
            class_868.var_214.method_191(false);
            method_89();
         }
      }
      
      public function method_125() : package_24.class_739
      {
         var _loc1_:package_24.class_739 = new package_24.class_739();
         _loc1_.method_137(class_702.package_9.method_136("spark_twinkle"));
         _loc1_.var_279 = -Math.random() * 2;
         _loc1_.var_233 = 0.95;
         _loc1_.var_250 = Number(0.05 + Math.random() * 0.1);
         _loc1_.var_251 = 12 + int(class_691.method_114(8));
         _loc1_.var_198.method_140();
         class_652.var_214.var_232.method_124(_loc1_,8);
         return _loc1_;
      }
   }
}
