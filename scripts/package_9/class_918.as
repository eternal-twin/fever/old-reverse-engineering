package package_9
{
   import flash.display.BitmapData;
   import flash.display.Graphics;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.filters.DisplacementMapFilter;
   import flash.filters.DisplacementMapFilterMode;
   import flash.geom.ColorTransform;
   import flash.geom.Point;
   import flash.geom.Rectangle;
   import flash.text.TextField;
   import package_11.package_12.class_657;
   import package_13.class_719;
   import package_13.class_777;
   import package_13.class_826;
   import package_24.class_750;
   import package_32.App;
   import package_32.class_899;
   import package_8.package_9.class_647;
   import package_8.package_9.class_814;
   import package_8.package_9.class_931;
   
   public class class_918 extends class_647
   {
       
      
      public var var_251:int;
      
      public var name_3:int;
      
      public var var_289:class_826;
      
      public var var_357:Sprite;
      
      public var var_1503:String;
      
      public var var_1137:Sprite;
      
      public var var_267:class_719;
      
      public var var_288:class_777;
      
      public var var_1504:TextField;
      
      public var var_1501:DisplacementMapFilter;
      
      public var var_1502:String;
      
      public var var_804:Array;
      
      public var var_94:Sprite;
      
      public function class_918()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_288 = class_652.var_214.var_288;
         var_267 = var_288.var_267;
         var_289 = var_288.var_289;
         var_1137 = new Sprite();
         var_1137.graphics.beginFill(16711680);
         var_1137.graphics.drawRect(0,0,16,16 + 8);
         var_267.var_232.method_124(var_1137,class_719.var_451);
         var _loc2_:* = var_288.var_289.method_294();
         var_1137.x = _loc2_.var_244 - 8;
         var_1137.y = Number(_loc2_.var_245) - (8 + 8);
         var_288.var_282.mask = var_1137;
         name_3 = 0;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:* = null as package_24.class_739;
         var _loc6_:* = null;
         var _loc7_:Number = NaN;
         var _loc8_:BitmapData = null;
         var _loc9_:int = 0;
         var _loc10_:BitmapData = null;
         var _loc11_:Sprite = null;
         super.method_79();
         switch(name_3)
         {
            case 0:
               _loc1_ = 8 - Math.abs(var_288.var_243.y - 8 - var_1137.y);
               _loc2_ = 0;
               _loc3_ = int(_loc1_);
               while(_loc2_ < _loc3_)
               {
                  _loc2_++;
                  _loc4_ = _loc2_;
                  _loc5_ = new package_24.class_739();
                  _loc5_.method_128(class_702.package_9.method_98(0,"spark_twinkle"));
                  _loc6_ = var_289.method_294();
                  _loc7_ = Math.random() - 0.5;
                  _loc5_.method_129(Number(Number(_loc6_.var_244) + _loc7_ * 12),_loc6_.var_245 - 16);
                  _loc5_.var_251 = 10 + int(class_691.method_114(10));
                  var_267.var_232.method_124(_loc5_,class_719.var_285);
                  _loc5_.var_278 = _loc7_ * 2;
                  _loc5_.var_279 = -Math.random() * 2;
                  _loc5_.var_233 = 0.96;
               }
               if(var_288.var_243.y - 8 < var_1137.y && _loc1_ == 0)
               {
                  var_251 = 0;
                  name_3 = name_3 + 1;
                  break;
               }
               break;
            case 1:
               _loc2_ = var_251;
               var_251 = var_251 + 1;
               if(_loc2_ >= 12)
               {
                  class_652.var_214.var_303 = null;
                  method_1111();
                  name_3 = name_3 + 1;
                  var_251 = 0;
                  break;
               }
               break;
            case 2:
               var_251 = var_251 + 1;
               _loc1_ = var_251 / 10;
               _loc8_ = class_652.var_214.var_109.bitmapData;
               _loc2_ = 200;
               _loc3_ = 0;
               while(_loc3_ < 4)
               {
                  _loc3_++;
                  _loc4_ = _loc3_;
                  _loc9_ = int(class_657.method_138((var_251 / 20 + _loc4_ * 0.12) % 1));
                  if(var_251 > _loc2_)
                  {
                     _loc9_ = int(class_657.method_241(_loc9_,var_251 - _loc2_));
                  }
                  switch(_loc4_)
                  {
                     case 0:
                        _loc8_.fillRect(new Rectangle(0,0,_loc8_.width,1),_loc9_);
                        continue;
                     case 1:
                        _loc8_.fillRect(new Rectangle(0,0,1,_loc8_.height),_loc9_);
                        continue;
                     case 2:
                        _loc8_.fillRect(new Rectangle(0,_loc8_.height - 1,_loc8_.width,1),_loc9_);
                        continue;
                     case 3:
                        _loc8_.fillRect(new Rectangle(_loc8_.width - 1,0,1,_loc8_.height),_loc9_);

                  }
               }
               _loc10_ = _loc8_.clone();
               _loc8_.applyFilter(_loc10_,_loc8_.rect,new Point(0,0),var_1501);
               var_1501.scaleX = _loc1_;
               var_1501.scaleY = _loc1_;
               _loc10_.dispose();
               _loc7_ = 1.02;
               _loc3_ = 1;
               _loc8_.colorTransform(_loc8_.rect,new ColorTransform(_loc7_,_loc7_,_loc7_,1,_loc3_,_loc3_,_loc3_,0));
               if(var_251 > _loc2_ + 50)
               {
                  method_1112();
                  break;
               }
               break;
            case 3:
               var_251 = var_251 + 1;
               if(int(var_251 % 4) == 0)
               {
                  var_357.y = var_357.y - 1;
               }
               if(Number(var_357.y + var_357.height) < 80)
               {
                  name_3 = name_3 + 1;
                  var_804 = [];
                  _loc2_ = 0;
                  while(_loc2_ < 2)
                  {
                     _loc2_++;
                     _loc3_ = _loc2_;
                     _loc11_ = method_981([class_808.method_466(class_808.var_1032,var_1502),class_808.method_466(class_808.var_1033,var_1503)][_loc3_]);
                     _loc11_.x = 40;
                     _loc11_.y = 100 + _loc3_ * 20;
                     _loc11_.addEventListener(MouseEvent.CLICK,function(param1:Function, param2:int):Function
                     {
                        var var_27:Function = param1;
                        var var_18:int = param2;
                        return function(param1:* = undefined):void
                        {
                           return var_27(var_18,param1);
                        };
                     }(method_700,_loc3_));
                     var_804.push(_loc11_);
                  }
                  break;
               }
               break;
            case 4:
               break;
            case 5:
               _loc2_ = 40;
               _loc3_ = var_251;
               var_251 = var_251 + 1;
               if(_loc3_ > _loc2_)
               {
                  class_657.name_18(var_94,0,-(var_251 - _loc2_));
                  class_657.name_18(var_804[1],0,int((var_251 - _loc2_) * 0.5));
               }
               if(var_251 > 255 + _loc2_)
               {
                  name_3 = name_3 + 1;
                  break;
               }
         }
      }
      
      public function method_1113(param1:Sprite, param2:Number, param3:Number, param4:int, param5:int, param6:* = undefined) : void
      {
         param5 = 2;
         var _loc7_:Graphics = param1.graphics;
         _loc7_.clear();
         _loc7_.beginFill(param4);
         _loc7_.drawRect(-param5,-param5,Number(param2 + 2 * param5),Number(param3 + 2 * param5));
         _loc7_.endFill();
         _loc7_.beginFill(0);
         _loc7_.drawRect(0,0,param2,param3);
         _loc7_.endFill();
      }
      
      public function method_1111() : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:int = 0;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:int = 0;
         var _loc19_:int = 0;
         var _loc1_:BitmapData = class_652.var_214.var_109.bitmapData;
         var _loc2_:BitmapData = new BitmapData(_loc1_.width,_loc1_.height,false,0);
         var _loc3_:Number = _loc2_.width * 0.5;
         var _loc4_:Number = _loc2_.height * 0.5;
         var _loc5_:int = 0;
         var _loc6_:int = _loc2_.width;
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc7_ = _loc5_;
            _loc8_ = 0;
            _loc9_ = _loc2_.height;
            while(_loc8_ < _loc9_)
            {
               _loc8_++;
               _loc10_ = _loc8_;
               _loc11_ = _loc7_ - _loc3_;
               _loc12_ = _loc10_ - _loc4_;
               _loc13_ = Number(Number(Math.atan2(_loc12_,_loc11_)) + 1.57);
               _loc14_ = Number(Math.sqrt(Number(_loc11_ * _loc11_ + _loc12_ * _loc12_)));
               _loc15_ = 128;
               _loc16_ = 128 + int(Math.cos(_loc13_) * _loc15_);
               _loc17_ = 128 + int(Math.sin(_loc13_) * _loc15_);
               _loc18_ = 128;
               _loc19_ = int(class_657.method_233({
                  "z\x01":_loc16_,
                  "6\x01":_loc17_,
                  "r\x01":_loc18_
               }));
               _loc2_.setPixel(_loc7_,_loc10_,_loc19_);
            }
         }
         var_1501 = new DisplacementMapFilter(_loc2_,new Point(0,0),1,2,1,1,DisplacementMapFilterMode.CLAMP);
      }
      
      public function method_981(param1:String) : Sprite
      {
         var _loc2_:Sprite = new Sprite();
         var _loc3_:TextField = class_742.method_308(16777215,8,-1,"nokia");
         _loc3_.htmlText = param1;
         _loc3_.width = Number(_loc3_.textWidth + 3);
         _loc3_.height = Number(_loc3_.textHeight + 3);
         _loc2_.addChild(_loc3_);
         var_94.addChild(_loc2_);
         var _loc4_:Number = _loc3_.width;
         var _loc5_:Number = _loc3_.height;
         method_1113(_loc2_,_loc4_,_loc5_,7829367,2);
         _loc2_.addEventListener(MouseEvent.MOUSE_OVER,function(param1:Function, param2:Sprite, param3:Number, param4:Number, param5:int, param6:int):Function
         {
            var name_145:Function = param1;
            var var_18:Sprite = param2;
            var var_940:Number = param3;
            var name_146:Number = param4;
            var name_147:int = param5;
            var name_148:int = param6;
            return function(param1:* = undefined):void
            {
               return name_145(var_18,var_940,name_146,name_147,name_148,param1);
            };
         }(method_1113,_loc2_,_loc4_,_loc5_,16777215,1));
         _loc2_.addEventListener(MouseEvent.MOUSE_OUT,function(param1:Function, param2:Sprite, param3:Number, param4:Number, param5:int, param6:int):Function
         {
            var name_145:Function = param1;
            var var_18:Sprite = param2;
            var var_940:Number = param3;
            var name_146:Number = param4;
            var name_147:int = param5;
            var name_148:int = param6;
            return function(param1:* = undefined):void
            {
               return name_145(var_18,var_940,name_146,name_147,name_148,param1);
            };
         }(method_1113,_loc2_,_loc4_,_loc5_,7829367,2));
         _loc2_.buttonMode = true;
         return _loc2_;
      }
      
      public function method_1114(param1:*) : void
      {
         if(name_3 == 3)
         {
            var_357.y = var_357.y - 4;
         }
         var_251 = var_251 + 2;
      }
      
      public function method_700(param1:int, param2:* = undefined) : void
      {
         var _loc5_:Sprite = null;
         class_652.var_214.method_186(_PlayerAction._EndGame(param1));
         new class_931(var_804[param1]);
         new class_814(var_804[1 - param1]);
         var _loc3_:int = 0;
         var _loc4_:Array = var_804;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            _loc5_.mouseEnabled = false;
            _loc5_.mouseChildren = false;
            _loc5_.buttonMode = false;
         }
         name_3 = name_3 + 1;
         var_251 = 0;
      }
      
      public function method_1112() : void
      {
         var _loc1_:BitmapData = class_652.var_214.var_109.bitmapData;
         _loc1_.fillRect(_loc1_.rect,0);
         name_3 = name_3 + 1;
         var_94 = new Sprite();
         var_94.addChild(class_652.var_214.var_109);
         var_94.scaleY = Number(2);
         var_94.scaleX = 2;
         var_357 = new Sprite();
         var_357.y = 200;
         var _loc3_:String = class_808.package_21(class_808.var_1034[class_911.var_214.var_329[0]],"#CCFF77");
         var _loc4_:String = class_808.package_21(class_808.var_1034[class_911.var_214.var_329[0] + 1],"#FFCC77");
         var_1503 = _loc4_;
         var_1502 = _loc3_;
         var_1504 = class_742.method_308(16777215,8,-1,"nokia");
         var_1504.wordWrap = true;
         var_1504.multiline = true;
         var_1504.width = 164;
         var_1504.x = (class_742.var_216 * 0.5 - var_1504.width) * 0.5;
         var_1504.htmlText = class_808.var_1030 + "\n\n\n\n\n" + class_808.method_466(class_808.var_1031,_loc3_,_loc4_);
         var_1504.height = Number(var_1504.textHeight + 10);
         var_357.addChild(var_1504);
         var_94.addChild(var_357);
         App.rootMc.addChild(var_94);
         var _loc5_:class_931 = new class_931(var_94,0.02);
         _loc5_.method_123(2);
         _loc5_.method_1016();
      }
   }
}
