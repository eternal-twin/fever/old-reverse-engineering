package package_9
{
   import flash.display.Sprite;
   import package_13.class_719;
   import package_13.class_777;
   import package_32.class_899;
   import package_8.package_9.class_647;
   
   public class class_736 extends class_647
   {
       
      
      public var var_729:class_719;
      
      public var var_17:class_777;
      
      public function class_736()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_17 = class_652.var_214.var_288;
         var_729 = class_652.var_214.var_267;
      }
      
      public function method_481(param1:Sprite) : void
      {
         var_729.var_232.method_124(param1,class_719.var_285);
      }
      
      public function method_482(param1:Sprite) : void
      {
         var_729.var_232.method_124(param1,class_719.var_451);
      }
   }
}
