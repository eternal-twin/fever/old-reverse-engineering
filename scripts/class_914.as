package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.geom.ColorTransform;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_914 extends class_645
   {
      
      public static var var_1491:int = 15;
       
      
      public var var_204:Array;
      
      public var var_99:class_626;
      
      public var var_1493:Bitmap;
      
      public var var_1492:Array;
      
      public function class_914()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_1106() : void
      {
         var _loc4_:class_913 = null;
         var _loc5_:* = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Number = NaN;
         var _loc11_:Number = NaN;
         var _loc12_:Point = null;
         var _loc13_:Array = null;
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:MovieClip = null;
         var _loc17_:Point = null;
         var _loc18_:Point = null;
         var _loc19_:Number = NaN;
         var _loc20_:Number = NaN;
         var _loc21_:Matrix = null;
         var _loc22_:class_913 = null;
         var _loc23_:class_913 = null;
         var _loc24_:class_913 = null;
         var _loc1_:class_627 = new class_627();
         var _loc2_:int = 0;
         var _loc3_:Array = var_1492;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.var_1487 > 0)
            {
               _loc4_.var_1487 = _loc4_.var_1487 - 1;
            }
            _loc5_ = method_127(_loc4_.name_1,_loc4_.name_21);
            _loc6_ = _loc5_.var_244 - _loc4_.x;
            _loc7_ = _loc5_.var_245 - _loc4_.y;
            _loc8_ = Number(Math.atan2(_loc7_,_loc6_));
            _loc9_ = Number(class_663.method_112(_loc8_ - _loc4_.var_1489,3.14));
            _loc10_ = 0.1;
            if(_loc9_ > _loc10_)
            {
               _loc4_.var_1489 = Number(_loc4_.var_1489 + _loc4_.name_48);
            }
            else if(_loc9_ < -_loc10_)
            {
               _loc4_.var_1489 = _loc4_.var_1489 - _loc4_.name_48;
            }
            else
            {
               _loc4_.var_1489 = _loc8_;
            }
            _loc11_ = (4 - Math.abs(_loc9_ * 2)) * _loc4_.var_1486;
            if(_loc4_.var_300)
            {
               _loc11_ = 0;
               if(name_5)
               {
                  _loc11_ = 7;
               }
            }
            if(_loc4_.var_351 < _loc11_)
            {
               _loc4_.var_351 = Number(_loc4_.var_351 + _loc4_.var_1490);
            }
            if(_loc4_.var_351 > _loc11_)
            {
               _loc4_.var_351 = _loc4_.var_351 - _loc4_.var_1488;
            }
            _loc12_ = var_99.var_203.localToGlobal(new Point(_loc4_.x,_loc4_.y));
            if(!var_99.var_203.hitTestPoint(_loc12_.x,_loc12_.y,true))
            {
               _loc4_.var_351 = _loc4_.var_351 * 0.8;
            }
            _loc4_.x = Number(_loc4_.x + Math.cos(_loc4_.var_1489) * _loc4_.var_351);
            _loc4_.y = Number(_loc4_.y + Math.sin(_loc4_.var_1489) * _loc4_.var_351);
            _loc4_.rotation = _loc4_.var_1489 / 0.0174;
            _loc4_.var_1485 = _loc4_.var_1485 + 1;
            _loc4_.var_1484 = Number(Math.max(_loc4_.var_1484 * 0.95,Math.abs(_loc9_) * 0.1));
            if(_loc4_.var_1485 > 2)
            {
               _loc4_.var_1485 = 0;
               _loc13_ = [_loc4_._w1,_loc4_._w2,_loc4_._w3,_loc4_._w4];
               _loc14_ = 0;
               _loc15_ = 0;
               while(_loc15_ < int(_loc13_.length))
               {
                  _loc16_ = _loc13_[_loc15_];
                  _loc15_++;
                  _loc17_ = _loc16_.localToGlobal(new Point(0,0));
                  _loc17_ = var_201.globalToLocal(_loc17_);
                  _loc18_ = _loc4_.name_12[_loc14_];
                  _loc4_.name_12[_loc14_] = _loc17_;
                  if(_loc18_ != null)
                  {
                     _loc19_ = _loc17_.x - _loc18_.x;
                     _loc20_ = _loc17_.y - _loc18_.y;
                     _loc21_ = new Matrix();
                     _loc21_.scale(Math.sqrt(Number(_loc19_ * _loc19_ + _loc20_ * _loc20_)) * 0.01,1);
                     _loc21_.rotate(Number(Math.atan2(_loc20_,_loc19_)));
                     _loc21_.translate(_loc18_.x,_loc18_.y);
                     var_1493.bitmapData.draw(_loc1_,_loc21_,new ColorTransform(1,1,1,_loc4_.var_1484,0,0,0,0));
                     _loc14_++;
                  }
               }
            }
            _loc4_.var_522 = Number(Math.sqrt(Number(_loc6_ * _loc6_ + _loc7_ * _loc7_)));
            _loc14_ = int((_loc4_.name_1 + 1) % int(var_204.length));
            if(_loc4_.var_522 < 10 || Number(method_1103(_loc4_,_loc14_)) < _loc4_.var_522)
            {
               if(_loc4_.name_1 == 0)
               {
                  method_1104(_loc4_.var_215);
               }
               _loc4_.name_1 = _loc14_;
               _loc4_.name_21 = Number(_loc4_.name_21 + (Math.random() * 2 - 1) * 0.2);
               _loc4_.name_21 = Number(class_663.method_99(-1,_loc4_.name_21,1));
               _loc4_.var_522 = 100;
            }
         }
         _loc2_ = 6;
         _loc14_ = 0;
         _loc3_ = var_1492;
         while(_loc14_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc14_];
            _loc14_++;
            _loc15_ = 0;
            _loc13_ = var_1492;
            while(_loc15_ < int(_loc13_.length))
            {
               _loc22_ = _loc13_[_loc15_];
               _loc15_++;
               if(_loc4_ != _loc22_)
               {
                  _loc6_ = _loc4_.x - _loc22_.x;
                  _loc7_ = _loc4_.y - _loc22_.y;
                  _loc8_ = _loc2_ * 2 - Math.sqrt(Number(_loc6_ * _loc6_ + _loc7_ * _loc7_));
                  if(_loc8_ > 0)
                  {
                     _loc9_ = Number(Math.atan2(_loc7_,_loc6_));
                     _loc10_ = Math.cos(_loc9_) * _loc8_ * 0.5;
                     _loc11_ = Math.sin(_loc9_) * _loc8_ * 0.5;
                     _loc4_.x = Number(_loc4_.x + _loc10_);
                     _loc4_.y = Number(_loc4_.y + _loc11_);
                     _loc22_.x = _loc22_.x - _loc10_;
                     _loc22_.y = _loc22_.y - _loc11_;
                     _loc19_ = Number(method_1105(_loc4_));
                     _loc20_ = Number(method_1105(_loc22_));
                     _loc23_ = _loc19_ < _loc20_?_loc22_:_loc4_;
                     _loc24_ = _loc19_ < _loc20_?_loc4_:_loc22_;
                     if(_loc4_.var_1487 == 0)
                     {
                        _loc23_.var_351 = _loc23_.var_351 * 0.6;
                     }
                     new class_931(_loc23_,16711680);
                     _loc24_.var_1487 = 10;
                  }
               }
            }
         }
      }
      
      override public function method_79() : void
      {
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_1106();
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [700];
         super.method_95(param1);
         method_117();
      }
      
      public function method_1105(param1:class_913) : Number
      {
         return param1.name_1 * 1000 - param1.var_522;
      }
      
      public function method_127(param1:int, param2:Number) : Object
      {
         var _loc3_:* = var_204[param1];
         var _loc4_:Number = Number(Number(_loc3_.var_244) + Math.cos(Number(_loc3_.var_1489)) * param2 * class_914.var_1491);
         var _loc5_:Number = Number(Number(_loc3_.var_245) + Math.sin(Number(_loc3_.var_1489)) * param2 * class_914.var_1491);
         return {
            "L\x01":_loc4_,
            "\x03\x01":_loc5_
         };
      }
      
      public function method_1103(param1:class_913, param2:int) : Number
      {
         var _loc3_:* = method_127(param2,param1.name_21);
         var _loc4_:Number = param1.x - _loc3_.var_244;
         var _loc5_:Number = param1.y - _loc3_.var_245;
         return Number(Math.sqrt(Number(_loc4_ * _loc4_ + _loc5_ * _loc5_)));
      }
      
      public function method_1104(param1:int) : void
      {
         if(var_220 != null)
         {
            return;
         }
         method_81(param1 == 1,10);
         new class_931(this);
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:MovieClip = null;
         var _loc5_:class_913 = null;
         var _loc6_:* = null;
         var_99 = new class_626();
         addChild(var_99);
         var_204 = [];
         var_99.var_204.visible = false;
         var _loc1_:int = 1;
         _loc2_ = var_99.var_204.totalFrames;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_99.var_204.gotoAndStop(_loc3_);
            _loc4_ = var_99.var_204.var_205;
            var_204.push({
               "L\x01":_loc4_.x,
               "\x03\x01":_loc4_.y,
               "{,\x01":(_loc4_.rotation + 90) * 0.0174
            });
         }
         var_1493 = new Bitmap();
         var_1493.bitmapData = new BitmapData(class_742.var_216,class_742.var_218,true,0);
         addChild(var_1493);
         var_1492 = [];
         _loc1_ = 0;
         while(_loc1_ < 3)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc5_ = new class_913();
            _loc5_.var_215 = _loc2_;
            var_1492.push(_loc5_);
            addChild(_loc5_);
            _loc5_.name_21 = _loc2_ - 1;
            _loc5_.name_1 = 1;
            _loc6_ = method_127(0,_loc5_.name_21);
            _loc5_.x = Number(_loc6_.var_244);
            _loc5_.y = Number(_loc6_.var_245);
            _loc5_.rotation = -18;
            _loc5_.var_105.gotoAndStop(_loc2_ + 1);
            if(_loc2_ == 1)
            {
               _loc5_.var_300 = true;
               _loc5_.name_48 = 0.07;
            }
            else
            {
               _loc5_.var_1486 = Number(0.4 + var_237[0] * 0.75);
            }
         }
      }
   }
}
