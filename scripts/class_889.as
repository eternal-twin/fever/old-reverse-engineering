package
{
   import package_32.class_899;
   
   public class class_889 extends class_2
   {
       
      
      public var var_1370:Number;
      
      public var name_21:Number;
      
      public var var_334:Number;
      
      public function class_889()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         name_21 = Math.random() * 6.28;
         var_1370 = Number(0.1 + Math.random() * 0.1);
      }
      
      public function method_79() : void
      {
         name_21 = (name_21 + var_1370) % 6.28;
         y = Number(var_334 + Math.cos(name_21) * 20);
      }
   }
}
