package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_825 extends class_645
   {
       
      
      public var var_251:Number;
      
      public var var_351:Number;
      
      public var var_307:Number;
      
      public var var_1080:MovieClip;
      
      public var var_1083:Number;
      
      public var var_490:int;
      
      public var var_1081:Array;
      
      public var var_1082:MovieClip;
      
      public var var_700:Number;
      
      public function class_825()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_779() : void
      {
         var_1080.scaleX = Number(var_1080.scaleX * 0.5 + var_307 * 0.01 * 0.5);
         var_1080.scaleY = var_1080.scaleX;
      }
      
      override public function method_79() : void
      {
         var _loc1_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               var_700 = (var_700 + var_351) % 628;
               _loc1_ = class_742.var_217 * 0.5;
               var_1080.x = Number(_loc1_ + Math.cos(var_700 / 100) * (_loc1_ - var_1080.width * 0.5));
               method_779();
               method_780();
               if(var_307 == 0 && int(var_1081.length) == 0)
               {
                  method_81(false);
                  break;
               }
         }
         super.method_79();
      }
      
      override public function method_83() : void
      {
         var _loc1_:class_892 = null;
         var _loc2_:class_892 = null;
         if(var_307 > 0)
         {
            var_307 = Number(Math.max(0,var_307 - 10));
            _loc2_ = new class_892(var_232.method_84(class_899.__unprotect__("\x0fG\x1ab"),class_645.var_209));
            _loc2_.var_233 = 0.99;
            _loc1_ = _loc2_;
            _loc1_.var_244 = var_1080.x;
            _loc1_.var_245 = Number(var_1080.y + 20);
            _loc1_.var_250 = 0.5;
            _loc1_.method_118();
            var_1081.push(_loc1_);
         }
      }
      
      public function method_780() : void
      {
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:class_892 = null;
         var _loc9_:class_892 = null;
         var _loc1_:Array = var_1081.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            if(_loc3_.var_245 > var_490)
            {
               _loc4_ = Number(Math.abs(_loc3_.var_244 - var_1082.x));
               _loc5_ = 30;
               if(_loc4_ < _loc5_)
               {
                  method_781((_loc5_ - _loc4_) * 0.02);
               }
               _loc6_ = 0;
               while(_loc6_ < 10)
               {
                  _loc6_++;
                  _loc7_ = _loc6_;
                  _loc9_ = new class_892(var_232.method_84(class_899.__unprotect__("PH\x01i\x02"),class_645.var_209));
                  _loc9_.var_233 = 0.99;
                  _loc8_ = _loc9_;
                  _loc8_.var_244 = _loc3_.var_244;
                  _loc8_.var_245 = _loc3_.var_245;
                  _loc8_.var_278 = 6 * (Math.random() * 2 - 1);
                  _loc8_.var_279 = -(2 + Math.random() * 6);
                  _loc8_.var_344 = Number(0.4 + Math.random() * 0.6);
                  _loc8_.var_250 = 0.3;
                  _loc8_.var_251 = 10 + int(class_691.method_114(10));
                  _loc8_.var_272 = 0;
                  _loc8_.method_118();
               }
               _loc3_.method_89();
               var_1081.method_116(_loc3_);
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [300 - param1 * 150];
         super.method_95(param1);
         var_351 = Number(8 + param1 * 20);
         var_307 = 70 - param1 * 30;
         var_700 = 0;
         var_1083 = 0;
         var_490 = class_742.var_219 - 34;
         var_1081 = [];
         method_117();
         method_72();
      }
      
      public function method_781(param1:Number) : void
      {
         var_1083 = Number(Math.min(Number(var_1083 + param1),1));
         var _loc2_:int = int(Math.floor(var_1083 * (var_1082.totalFrames - 1))) + 1;
         var_1082.gotoAndStop(_loc2_);
         if(var_1083 == 1)
         {
            method_81(true,20);
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("?\x06=}\x01"),0);
         var_1080 = var_232.method_84(class_899.__unprotect__("R\x0fra"),class_645.var_209);
         var_1080.x = class_742.var_217 * 0.5;
         var_1080.y = 30;
         var_1080.scaleX = var_307 * 0.01;
         var_1080.scaleY = var_307 * 0.01;
         var_1082 = var_232.method_84(class_899.__unprotect__("\x1bC\x1a\x01"),class_645.var_209);
         var_1082.x = 60 + int(class_691.method_114(class_742.var_217 - 2 * 60));
         var_1082.y = (var_490 + class_742.var_219) * 0.5;
         var_1082.stop();
      }
   }
}
