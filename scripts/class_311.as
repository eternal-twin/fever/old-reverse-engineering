package
{
   import flash.display.MovieClip;
   
   public dynamic class class_311 extends MovieClip
   {
       
      
      public function class_311()
      {
         super();
         addFrameScript(0,method_1,11,method_6);
      }
      
      public function method_1() : *
      {
         blendMode = "add";
      }
      
      public function method_6() : *
      {
         parent.removeChild(this);
         stop();
      }
   }
}
