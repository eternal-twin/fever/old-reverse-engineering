package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import flash.filters.DisplacementMapFilter;
   import flash.filters.DisplacementMapFilterMode;
   import flash.geom.Matrix;
   import flash.geom.Point;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_32.class_899;
   
   public class class_857 extends class_645
   {
       
      
      public var var_1246:Number;
      
      public var var_180:BitmapData;
      
      public var var_307:int;
      
      public var var_1233:MovieClip;
      
      public var var_318:Number;
      
      public var var_319:Number;
      
      public var var_216:Number;
      
      public var var_218:Number;
      
      public var var_1247:MovieClip;
      
      public var var_1249:class_755;
      
      public var var_1145:int;
      
      public var var_1251:class_795;
      
      public var var_1250:int;
      
      public var var_1248:MovieClip;
      
      public function class_857()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_898() : void
      {
         var _loc1_:Number = var_307 * 0.5;
         var_1233.x = var_319 - _loc1_;
         var_1233.y = var_318 - _loc1_;
         var _loc2_:Number = var_319 * var_1246;
         var _loc3_:Number = var_318 * var_1246;
         var _loc5_:int = var_307 * 4;
         var _loc6_:Number = _loc2_ - _loc5_ * 0.5;
         var _loc7_:Number = _loc3_ - _loc5_ * 0.5;
         var _loc8_:BitmapData = new BitmapData(_loc5_,_loc5_,false,16678783);
         var _loc9_:Matrix = new Matrix();
         _loc9_.translate(-int(_loc6_),-int(_loc7_));
         _loc8_.draw(var_1247,_loc9_);
         var _loc10_:MovieClip = var_232.method_84(class_899.__unprotect__("?s;J\x03"),0);
         _loc10_.gotoAndStop(1);
         var _loc11_:BitmapData = new BitmapData(var_307,var_307,false,255);
         var _loc12_:Matrix = new Matrix();
         _loc12_.scale(var_307 * 0.01,var_307 * 0.01);
         _loc11_.draw(_loc10_,_loc12_);
         var _loc13_:DisplacementMapFilter = new DisplacementMapFilter(_loc11_);
         var _loc14_:int = int((_loc5_ - var_307) * 0.5);
         var _loc15_:int = int((_loc5_ - var_307) * 0.5);
         _loc13_.mapPoint = new Point(_loc14_ * 1,_loc15_ * 1);
         _loc13_.componentX = 1;
         _loc13_.componentY = 2;
         _loc13_.scaleX = -260;
         _loc13_.scaleY = -260;
         _loc13_.mode = DisplacementMapFilterMode.COLOR;
         var_1233.var_230.applyFilter(_loc8_,_loc8_.rect,new Point(-_loc14_,-_loc15_),_loc13_);
         _loc11_.dispose();
         _loc8_.dispose();
         _loc10_.parent.removeChild(_loc10_);
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = method_100();
         var_319 = Number(_loc1_.var_244);
         var_318 = Number(_loc1_.var_245);
         method_898();
         var _loc3_:MovieClip = var_1248.var_1;
         var _loc2_:Number = var_319 * var_1246 - (Number(var_1248.x + _loc3_.x * var_1145 * 0.01));
         _loc3_ = var_1248.var_1;
         var _loc4_:Number = var_318 * var_1246 - (Number(var_1248.y + _loc3_.y * var_1145 * 0.01));
         var_1248.visible = Number(Math.sqrt(Number(_loc2_ * _loc2_ + _loc4_ * _loc4_))) < 130;
         super.method_79();
      }
      
      public function method_375(param1:*) : void
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:class_892 = null;
         var _loc9_:MovieClip = null;
         method_81(true,50);
         method_102(8);
         var _loc4_:int = 0;
         while(_loc4_ < 30)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = Math.random() * 6.28;
            _loc7_ = Math.random() * 12;
            _loc8_ = new class_892(var_1249.method_84(class_899.__unprotect__("\x06p2x\x01"),1));
            _loc8_.var_278 = Math.cos(_loc6_) * _loc7_;
            _loc8_.var_279 = Math.sin(_loc6_) * _loc7_;
            _loc9_ = var_1248.var_1;
            _loc8_.var_244 = Number(Number(var_1248.x + _loc9_.x * var_1145 * 0.01) + _loc8_.var_278 * 3);
            _loc9_ = var_1248.var_1;
            _loc8_.var_245 = Number(Number(var_1248.y + _loc9_.y * var_1145 * 0.01) + _loc8_.var_279 * 3);
            _loc8_.var_243.gotoAndStop(int(class_691.method_114(_loc8_.var_243.totalFrames)) + 1);
            _loc9_ = _loc8_.var_243.var_1;
            _loc9_.gotoAndPlay(int(class_691.method_114(10)) + 1);
            _loc8_.method_119(Number(0.5 + Math.random() * 2));
            _loc8_.var_580 = (Math.random() * 2 - 1) * 16;
            _loc8_.var_243.rotation = Math.random() * 360;
            _loc8_.var_687 = 0.95;
            _loc8_.method_118();
            class_657.gfof(_loc8_.var_243,0.5 - _loc5_ / 30 * 0.5,4456516);
            if(Number(Math.abs(_loc5_ / 30 - 0.3)) < 0.2)
            {
               class_657.gfof(_loc8_.var_243,1,16711680);
            }
         }
         var_1248.parent.removeChild(var_1248);
      }
      
      override public function method_89() : void
      {
         var_1233.var_230.dispose();
         var_180.dispose();
         super.method_89();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400 - 50 * param1];
         super.method_95(param1);
         var_1246 = Number(3 + param1 * 3);
         if(var_1246 > 8)
         {
            var_1246 = 8;
         }
         var_1145 = 200;
         var_307 = 100;
         var_216 = class_742.var_216 * var_1246;
         var_218 = class_742.var_218 * var_1246;
         var_1250 = int(Number(Math.pow(var_216 * 0.003,2)));
         method_117();
      }
      
      public function method_117() : void
      {
         var _loc1_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:MovieClip = null;
         var _loc14_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__(" \x15oN\x01"),0);
         var_1247 = var_232.method_92(0);
         var_1249 = new class_755(var_1247);
         _loc1_ = 1 / var_1246;
         var_1247.scaleY = _loc1_;
         var_1247.scaleX = _loc1_;
         var_180 = new BitmapData(var_1145,var_1145,false,65280);
         var _loc2_:MovieClip = var_232.method_84(class_899.__unprotect__("\x0f\'5\x19"),0);
         var _loc3_:Matrix = new Matrix();
         _loc3_.scale(var_1145 * 0.01,var_1145 * 0.01);
         var_180.draw(_loc2_,_loc3_);
         _loc2_.parent.removeChild(_loc2_);
         var _loc4_:int = int(Math.ceil(var_216 / var_1145));
         var _loc5_:int = int(Math.ceil(var_216 / var_1145));
         var _loc6_:int = 0;
         while(_loc6_ < _loc4_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc8_ = 0;
            while(_loc8_ < _loc4_)
            {
               _loc8_++;
               _loc9_ = _loc8_;
               _loc10_ = var_1249.method_92(0);
               _loc10_.addChild(new Bitmap(var_180));
               _loc10_.x = _loc7_ * var_1145;
               _loc10_.y = _loc9_ * var_1145;
            }
         }
         var_1233 = var_232.method_92(1);
         var _loc11_:class_755 = new class_755(var_1233);
         _loc10_ = _loc11_.method_84(class_899.__unprotect__("^\x19,;\x02"),0);
         var _loc12_:MovieClip = _loc11_.method_92(0);
         _loc10_.x = var_307 * 0.5;
         _loc10_.y = var_307 * 0.5;
         _loc1_ = var_307 * 0.01;
         _loc10_.scaleY = _loc1_;
         _loc10_.scaleX = _loc1_;
         _loc12_.mask = _loc10_;
         var_1233.mouseChildren = false;
         var_1233.mouseEnabled = false;
         var_1233.var_230 = new BitmapData(var_307,var_307,false,65280);
         _loc12_.addChild(new Bitmap(var_1233.var_230));
         var_1233.x = (class_742.var_216 - var_307) * 0.5;
         var_1233.y = (class_742.var_218 - var_307) * 0.5;
         class_658.var_146(var_1233,4,4,16777215);
         class_658.var_146(var_1233,2,4,0);
         var _loc13_:MovieClip = _loc11_.method_84(class_899.__unprotect__("M8k]\x03"),0);
         _loc13_.x = var_307 * 0.5;
         _loc13_.y = var_307 * 0.5;
         var_1251 = new class_795();
         _loc6_ = int(class_691.method_114(var_1250));
         _loc7_ = 0;
         _loc8_ = var_1250;
         while(_loc7_ < _loc8_)
         {
            _loc7_++;
            _loc9_ = _loc7_;
            _loc14_ = var_1249.method_84(class_899.__unprotect__("+\x029p\x01"),0);
            _loc14_.var_319 = int(class_691.method_114(_loc4_ - 1));
            _loc14_.var_318 = int(class_691.method_114(_loc5_ - 1));
            _loc14_.gotoAndStop(int(class_691.method_114(_loc14_.totalFrames)) + 1);
            _loc14_.x = _loc14_.var_319 * var_1145;
            _loc14_.y = _loc14_.var_318 * var_1145;
            _loc14_.blendMode = BlendMode.OVERLAY;
            _loc14_.alpha = 0.5;
            _loc1_ = var_1145;
            _loc14_.scaleY = _loc1_;
            _loc14_.scaleX = _loc1_;
            var_1251.method_103(_loc14_);
            if(_loc6_ == _loc9_)
            {
               var_1248 = var_1249.method_84(class_899.__unprotect__("Jp*7\x03"),0);
               var_1248.x = _loc14_.x;
               var_1248.y = _loc14_.y;
               _loc1_ = var_1145 * 0.01;
               var_1248.scaleY = _loc1_;
               var_1248.scaleX = _loc1_;
               var_1248.addEventListener(MouseEvent.CLICK,method_375);
               var_1248.gotoAndStop(_loc14_.currentFrame);
               var_1248.visible = false;
            }
         }
         var_319 = class_742.var_216 * 0.5;
         var_318 = class_742.var_218 * 0.5;
         method_898();
      }
   }
}
