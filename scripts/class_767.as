package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_767 extends class_645
   {
      
      public static var var_845:int = 28;
      
      public static var var_846:int = 30;
      
      public static var var_847:int = 16;
      
      public static var var_725:int = 40;
      
      public static var var_350:int = 224;
      
      public static var var_848:int = 11;
       
      
      public var var_849:Number;
      
      public var var_251:Number;
      
      public var var_852:Number;
      
      public var var_650:MovieClip;
      
      public var var_855:class_892;
      
      public var var_856:Number;
      
      public var var_853:Number;
      
      public var var_857:Object;
      
      public var var_288:class_892;
      
      public var var_854:class_892;
      
      public var var_859:Boolean;
      
      public var var_858:Number;
      
      public var var_850:MovieClip;
      
      public var var_851:MovieClip;
      
      public function class_767()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      public function method_298() : void
      {
         var _loc1_:Number = Number(class_663.method_99(-var_849,class_742.var_217 * 0.5 - var_288.var_244,0));
         var_201.x = _loc1_ / 0.6;
         var_850.x = -var_201.x * 0.6 * 0.9;
         var_851.x = int(Math.floor(-var_201.x * 0.6 / class_742.var_217)) * class_742.var_217;
      }
      
      public function method_602() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var_650.graphics.clear();
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = {
                  "L\x01":var_852,
                  "\x03\x01":class_767.var_845 * 1
               };
               _loc2_ = Number(var_288.method_115(_loc1_));
               _loc3_ = Number(var_288.method_231(_loc1_));
               if(_loc2_ > class_767.var_846)
               {
                  _loc4_ = (_loc2_ - class_767.var_846) / class_767.var_846;
                  _loc5_ = 0.15;
                  var_288.var_278 = Number(var_288.var_278 + Math.cos(_loc3_) * _loc4_ * _loc5_);
                  var_288.var_279 = Number(var_288.var_279 + Math.sin(_loc3_) * _loc4_ * _loc5_);
               }
               method_601(_loc1_);
               var_853 = var_288.var_243.rotation;
               _loc4_ = _loc3_ / 0.0174 + 90 - var_853;
               _loc4_ = Number(class_663.method_112(_loc4_,180));
               var_288.var_243.rotation = Number(var_288.var_243.rotation + _loc4_ * 0.25);
               _loc5_ = -class_663.method_99(-1,_loc3_ / 3.14,0);
               var_288.var_243.gotoAndStop(int(_loc5_ * 20) + 1);
               break;
            case 2:
               break;
            case 3:
               if(var_854.var_245 < class_767.var_845)
               {
                  var_852 = var_854.var_244;
                  name_3 = 1;
                  var_854.method_89();
               }
               method_601(var_854);
               break;
            case 4:
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  method_81(true,10);
               }
               var_288.var_245 = var_855.var_245 - 5;
         }
      }
      
      public function method_603() : void
      {
         var_856 = (var_856 + 10) % 628;
         var _loc1_:* = {
            "L\x01":var_855.var_244,
            "\x03\x01":Number(class_767.var_350 - 8 + Math.cos(var_856 / 100) * 1.5)
         };
         var_855.method_358(_loc1_,0.1,0.5);
      }
      
      public function method_299() : void
      {
         var _loc2_:class_892 = null;
         var _loc3_:class_892 = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc1_:Number = (var_288.var_243.rotation + 90) * 0.0174;
         var_857 = {
            "L\x01":Number(var_288.var_244 + Math.cos(_loc1_) * class_767.var_848),
            "\x03\x01":Number(var_288.var_245 + Math.sin(_loc1_) * class_767.var_848)
         };
         if(Number(var_857.var_245) > class_767.var_350 - 4 && name_3 != 4)
         {
            var_858 = Number(Math.min(Number(var_858 + 5),100));
            _loc3_ = new class_892(var_232.method_84(class_899.__unprotect__(",\'v/"),class_645.var_209));
            _loc3_.var_233 = 0.99;
            _loc2_ = _loc3_;
            _loc2_.var_244 = Number(var_288.var_244 + (Math.random() * 2 - 1) * 6);
            _loc2_.var_245 = Number(class_767.var_350 + (Math.random() * 2 - 1) * 2);
            _loc2_.var_250 = -(0.2 + Math.random() * 0.2);
            _loc2_.var_251 = 12 + int(class_691.method_114(12));
            _loc2_.method_118();
            if(Number(var_857.var_245) > class_767.var_350 + 4)
            {
               var_288.var_278 = var_288.var_278 * 0.85;
            }
         }
         else
         {
            if(name_3 == 4)
            {
               var_858 = var_858 * 0.5;
            }
            var_858 = Number(Math.max(0,var_858 - 1));
         }
         class_657.gfof(var_288.var_243,var_858 * 0.01,0);
         if(var_858 == 100)
         {
            name_3 = 5;
            method_81(false);
         }
         if(Math.random() * var_858 > 25)
         {
            _loc3_ = new class_892(var_232.method_84(class_899.__unprotect__(",\'v/"),class_645.var_209));
            _loc3_.var_233 = 0.99;
            _loc2_ = _loc3_;
            _loc4_ = Math.random() * 10;
            _loc5_ = Math.random() * 6.28;
            _loc2_.var_244 = Number(Number(var_857.var_244) + Math.cos(_loc5_) * _loc4_);
            _loc2_.var_245 = Number(Number(var_857.var_245) + Math.sin(_loc5_) * _loc4_);
            _loc2_.var_278 = var_288.var_278;
            _loc2_.var_279 = var_288.var_279;
            _loc2_.var_250 = -(0.1 + Math.random() * 0.2);
            _loc2_.var_251 = 12 + int(class_691.method_114(12));
            _loc2_.method_118();
         }
         if(var_288.var_245 < class_767.var_845 + 4)
         {
            var_288.var_279 = var_288.var_279 * -1;
            var_288.var_245 = class_767.var_845 + 4;
         }
         var _loc6_:Boolean = var_288.var_245 < var_855.var_245 - 20;
         if((name_3 == 2 || name_3 == 3) && !_loc6_ && var_859)
         {
            if(Number(Math.abs(var_288.var_244 - var_855.var_244)) < class_767.var_725)
            {
               var_855.var_279 = Number(var_855.var_279 + Number(class_663.method_99(0,var_288.var_279,3)));
               name_3 = 4;
               var_288.var_278 = 0;
               var_288.var_279 = 0;
               var_288.var_243.gotoAndPlay("land");
               var_288.var_245 = var_855.var_245 - 5;
               var_288.var_580 = 0;
               var_288.var_243.rotation = 0;
               var_251 = 10;
               var_232.method_521(var_288.var_243);
               var_229 = [true];
            }
         }
         var_859 = _loc6_;
         var_288.var_243.x = var_288.var_244;
         var_288.var_243.y = var_288.var_245;
      }
      
      override public function method_79() : void
      {
         super.method_79();
         if(name_3 < 5)
         {
            method_298();
         }
         method_603();
         method_299();
         method_602();
      }
      
      override public function method_83() : void
      {
         var _loc1_:class_892 = null;
         var _loc2_:* = null;
         var _loc3_:Number = NaN;
         switch(name_3)
         {
            default:
               break;
            case 1:
               name_3 = name_3 + 1;
               var_288.var_580 = (var_288.var_243.rotation - var_853) * 1.5;
               var_288.var_243.play();
               break;
            case 2:
               name_3 = name_3 + 1;
               _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("\x13_\x1an\x02"),class_645.var_209));
               _loc1_.var_233 = 0.99;
               var_854 = _loc1_;
               _loc2_ = {
                  "L\x01":var_201.mouseX,
                  "\x03\x01":var_201.mouseY
               };
               _loc3_ = Number(var_288.method_231(_loc2_));
               var_854.var_244 = var_288.var_244;
               var_854.var_245 = var_288.var_245;
               var_854.var_278 = Math.cos(_loc3_) * class_767.var_847;
               var_854.var_279 = Math.sin(_loc3_) * class_767.var_847;
               var_854.method_118();
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_852 = class_742.var_217 * 0.5;
         var_856 = 0;
         var_858 = 0;
         var_849 = Number(480 + param1 * 1500);
         var_859 = true;
         method_117();
         method_72();
      }
      
      public function method_601(param1:Object) : void
      {
         var_650.graphics.lineStyle(1,12316160,100);
         var_650.graphics.moveTo(var_288.var_244,var_288.var_245);
         var_650.graphics.lineTo(Number(param1.var_244),Number(param1.var_245));
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("/dMR\x01"),0);
         var_850 = class_319.class_319;
         var_650 = var_232.method_92(class_645.var_209);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("SyB\x04\x01"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_855 = _loc1_;
         var_855.var_244 = var_849 - (class_767.var_725 + 20);
         var_855.var_245 = class_767.var_350 - 8;
         var_855.var_233 = 0.9;
         var_855.method_118();
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("\t{\x07\x13"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_288 = _loc1_;
         var_288.var_244 = class_742.var_217 * 0.5;
         var_288.var_245 = class_742.var_219 * 0.5;
         var_288.var_233 = 0.98;
         var_288.var_250 = 0.3;
         var_288.method_118();
         var_851 = var_232.method_84(class_899.__unprotect__("\x10\x1eB\x11\x03"),class_645.var_209 + 1);
      }
   }
}
