package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_731 extends class_645
   {
      
      public static var var_723:int = 8;
      
      public static var var_724:int = 10;
      
      public static var var_725:int = 25;
      
      public static var var_726:int = 18;
       
      
      public var var_251:Number;
      
      public var var_727:Number;
      
      public var var_728:class_892;
      
      public var var_495:Array;
      
      public function class_731()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:* = null;
         var _loc2_:Number = NaN;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:class_892 = null;
         var _loc6_:Number = NaN;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc11_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         loop2:
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = {
                  "L\x01":Number(method_100().var_244),
                  "\x03\x01":var_727
               };
               var_728.method_358(_loc1_,0.01,0.7);
               var_728.var_278 = var_728.var_278 * 0.96;
               _loc2_ = -var_728.var_243.rotation;
               var_728.var_580 = Number(var_728.var_580 + _loc2_ * 0.1);
               var_728.var_580 = var_728.var_580 * 0.97;
               var_251 = var_251 - 1;
               if(var_251 < 0)
               {
                  var_251 = class_731.var_726;
                  method_468();
               }
               _loc3_ = 0;
               _loc4_ = var_495;
               while(true)
               {
                  if(_loc3_ >= int(_loc4_.length))
                  {
                     break loop2;
                  }
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  _loc6_ = Number(Math.abs(var_728.var_244 - _loc5_.var_244));
                  switch(int(_loc5_.name_3))
                  {
                     case 0:
                        if(_loc5_.var_245 > var_728.var_245 - class_731.var_724)
                        {
                           _loc5_.name_3 = 1;
                           if(_loc6_ < class_731.var_725 - class_731.var_724 && var_220 == null)
                           {
                              var_728.var_279 = Number(var_728.var_279 + _loc5_.var_279 * 0.5);
                              _loc5_.method_89();
                              var_728.var_243.nextFrame();
                              if(var_728.var_243.currentFrame == 8)
                              {
                                 method_81(true,15);
                              }
                           }
                           else
                           {
                              _loc7_ = 0;
                              while(_loc7_ < 2)
                              {
                                 _loc7_++;
                                 _loc8_ = _loc7_;
                                 _loc9_ = _loc8_ * 2 - 1;
                                 _loc10_ = {
                                    "L\x01":Number(var_728.var_244 + _loc9_ * class_731.var_725),
                                    "\x03\x01":var_728.var_245
                                 };
                                 _loc11_ = Number(_loc5_.method_115(_loc10_));
                                 if(_loc11_ < class_731.var_724)
                                 {
                                    _loc5_.name_3 = 0;
                                    _loc12_ = Number(_loc5_.method_231(_loc10_));
                                    _loc13_ = class_731.var_724 - _loc11_;
                                    _loc14_ = Number(Math.cos(_loc12_));
                                    _loc15_ = Number(Math.sin(_loc12_));
                                    _loc5_.var_244 = _loc5_.var_244 - _loc14_ * _loc13_;
                                    _loc5_.var_245 = _loc5_.var_245 - _loc15_ * _loc13_;
                                    _loc16_ = Math.sqrt(Number(_loc5_.var_278 * _loc5_.var_278 + _loc5_.var_279 * _loc5_.var_279)) * 0.8;
                                    _loc5_.var_278 = -_loc14_ * _loc16_;
                                    _loc5_.var_279 = -_loc15_ * _loc16_;
                                    var_728.var_580 = Number(var_728.var_580 + _loc16_ * 0.5 * _loc9_);
                                    var_728.var_279 = Number(var_728.var_279 + _loc16_ * 0.3);
                                    _loc5_.var_580 = Number(_loc5_.var_580 + _loc16_ * 2 * _loc9_);
                                 }
                              }
                           }
                           continue;
                        }
                        continue;
                     case 1:

                  }
               }
         }
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [380 - param1 * 60];
         super.method_95(param1);
         var_495 = [];
         var_727 = class_742.var_219 - 46;
         var_251 = 10;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("cA\x16R\x02"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("8\x02\x17Q"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_728 = _loc1_;
         var_728.var_244 = class_742.var_217 * 0.5;
         var_728.var_245 = var_727;
         var_728.var_580 = 0;
         var_728.var_243.stop();
         var_728.method_118();
         var_728.var_233 = 0.95;
      }
      
      public function method_468() : void
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("NA\x1bX\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         var _loc3_:Number = Number(class_731.var_724 + 80 * (1 - Math.min(1,var_237[0])));
         _loc1_.var_244 = Number(_loc3_ + Math.random() * (class_742.var_217 - 2 * _loc3_));
         _loc1_.var_245 = -class_731.var_724;
         _loc1_.var_250 = Number(0.1 + Math.random() * 0.2);
         _loc1_.var_233 = 1;
         _loc1_.var_580 = 0;
         _loc1_.name_3 = 0;
         _loc1_.method_118();
         var_495.push(_loc1_);
      }
   }
}
