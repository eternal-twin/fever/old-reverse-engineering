package
{
   import package_32.class_899;
   
   public class class_802
   {
       
      
      public var name_10:int;
      
      public var name_11:int;
      
      public function class_802(param1:int = 0, param2:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         name_10 = param1;
         name_11 = param2;
      }
      
      public function name_1() : int
      {
         var _loc1_:int = name_10;
         name_10 = name_10 + 1;
         return _loc1_;
      }
      
      public function method_74() : Boolean
      {
         return name_10 < name_11;
      }
   }
}
