package package_8.package_9
{
   import package_32.class_899;
   
   public class class_647
   {
      
      public static var var_253:class_938;
       
      
      public var var_257:Function;
      
      public var var_258:Function;
      
      public var var_259:Boolean;
      
      public var var_255:Function;
      
      public var var_254:Number;
      
      public function class_647()
      {
         if(class_899.var_239)
         {
            return;
         }
         class_647.var_253.method_124(this);
         var_255 = function(param1:Number):Number
         {
            return param1;
         };
         var_254 = 0;
         var_259 = false;
      }
      
      public function method_79() : void
      {
      }
      
      public function method_121() : void
      {
         var_254 = 1 - var_254;
         method_120();
      }
      
      public function method_120() : void
      {
         var var_256:Function = var_255;
         var_255 = function(param1:Number):Number
         {
            return 1 - var_256(param1);
         };
      }
      
      public function method_89() : void
      {
         var_259 = true;
         if(var_258 != null)
         {
            var_258();
         }
         if(var_257 != null)
         {
            var_257();
         }
         class_647.var_253.method_116(this);
      }
      
      public function method_122(param1:Number = 1.0) : void
      {
         var var_260:Number = param1;
         var_255 = function(param1:Number):Number
         {
            return Number(Math.pow(0.5 - Math.cos(param1 * 3.14) * 0.5,var_260));
         };
      }
      
      public function method_123(param1:Number) : void
      {
         var var_260:Number = param1;
         var_255 = function(param1:Number):Number
         {
            return Number(Math.pow(param1,var_260));
         };
      }
   }
}
