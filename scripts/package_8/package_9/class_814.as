package package_8.package_9
{
   import flash.display.DisplayObject;
   import flash.display.DisplayObjectContainer;
   import package_32.class_899;
   
   public class class_814 extends class_807
   {
       
      
      public var var_251:Number;
      
      public var var_273:Number;
      
      public function class_814(param1:DisplayObject = undefined, param2:int = 10, param3:int = 10, param4:Boolean = false)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(param1);
         var_251 = param2;
         var_273 = param3;
         var_1011 = param4;
         var_1012 = var_243.alpha;
      }
      
      override public function method_79() : void
      {
         if(var_243.parent == null)
         {
            method_89();
            return;
         }
         var _loc1_:Number = var_251;
         var_251 = var_251 - 1;
         if(_loc1_ < var_273)
         {
            method_737(Number(var_255(var_251 / var_273)));
            if(var_251 == 0)
            {
               if(var_243.parent != null)
               {
                  var_243.parent.removeChild(var_243);
               }
               method_89();
            }
         }
      }
      
      public function method_769(param1:int, param2:int) : void
      {
         var_1014 = {
            "U&\x01":param1,
            "\x1c=\x01":param2,
            "xmu\x01":var_243.scaleX,
            "d\x1c\x07\x01":var_243.scaleY
         };
      }
      
      public function method_770(param1:Number, param2:Number) : void
      {
         var_1013 = {
            "L\x01":param1,
            "\x03\x01":param2
         };
      }
   }
}
