package package_8.package_9
{
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_921 extends class_647
   {
       
      
      public var var_1511:Function;
      
      public var var_27:class_647;
      
      public var name_28:int;
      
      public function class_921(param1:class_647 = undefined, param2:Object = undefined, param3:int = 10)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_27 = param1;
         var_1511 = param2;
         name_28 = param3;
         class_647.var_253.method_116(param1);
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         var _loc1_:int = name_28;
         name_28 = name_28 - 1;
         if(_loc1_ <= 0)
         {
            if(var_27 != null)
            {
               class_647.var_253.method_124(var_27);
               var_27.method_79();
            }
            if(var_1511 != null)
            {
               var_1511();
            }
            method_89();
         }
      }
      
      public function method_172(param1:MovieClip, param2:Boolean = false) : void
      {
         var var_631:MovieClip = param1;
         var name_8:Boolean = param2;
         var_631.visible = false;
         if(name_8)
         {
            var_631.stop();
         }
         var_1511 = function():void
         {
            var_631.visible = true;
            if(name_8)
            {
               var_631.play();
            }
         };
      }
   }
}
