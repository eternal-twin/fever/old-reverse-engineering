package package_8.package_9
{
   import flash.display.GradientType;
   import flash.display.Graphics;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import package_32.class_899;
   
   public class class_883 extends class_739
   {
       
      
      public var var_933:Number;
      
      public var var_1015:Number;
      
      public var name_10:Number;
      
      public var name_11:Number;
      
      public var var_759:Object;
      
      public var var_1346:Number;
      
      public var var_719:Array;
      
      public var var_1347:Number;
      
      public function class_883(param1:Number = 0.0, param2:Number = 0.0, param3:Number = 0.1, param4:Number = 0.0, param5:Number = 1.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super(new Sprite());
         name_10 = param1;
         name_11 = param2;
         var_933 = param3;
         var_1346 = param4;
         var_1015 = param5;
         var_719 = [16777215,16777215];
         var_1347 = 0.5;
         method_123(0.5);
         method_227();
         method_1016(0);
      }
      
      override public function method_79() : void
      {
         super.method_79();
         var_254 = Number(Math.min(Number(var_254 + var_933),1));
         if(var_759 != null)
         {
            method_227();
         }
         method_1016(var_254);
         if(var_254 == 1)
         {
            method_89();
         }
      }
      
      public function method_1017(param1:Number = 1.0) : void
      {
         var_759 = {"\x06AF\x12\x01":param1};
      }
      
      public function method_1016(param1:Number) : void
      {
         var _loc3_:Number = NaN;
         var _loc2_:Number = Number(var_255(var_254));
         var_243.scaleX = (name_10 + (name_11 - name_10) * _loc2_) * 0.01;
         var_243.scaleY = var_243.scaleX * var_1015;
         if(_loc2_ > var_1346)
         {
            _loc3_ = (param1 - var_1346) / (1 - var_1346);
            var_243.alpha = (1 - _loc3_) * var_1012;
         }
      }
      
      public function method_227() : void
      {
         var _loc2_:Matrix = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:Graphics = var_243.graphics;
         _loc1_.clear();
         if(int(var_719.length) > 1)
         {
            _loc2_ = new Matrix();
            _loc2_.createGradientBox(100,100,0,-50,-50);
            _loc1_.beginGradientFill(GradientType.RADIAL,var_719,[0,1],[int(var_1347 * 255),255],_loc2_);
         }
         else
         {
            _loc1_.beginFill(int(var_719[0]));
         }
         _loc1_.drawCircle(0,0,50);
         if(var_759 != null)
         {
            _loc4_ = Number(var_255(var_254));
            _loc5_ = 50 * _loc4_;
            _loc6_ = (50 - _loc5_) * (1 - var_759.name_135);
            _loc1_.drawCircle(_loc6_,0,_loc5_);
         }
      }
   }
}
