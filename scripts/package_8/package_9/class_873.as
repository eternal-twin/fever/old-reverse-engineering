package package_8.package_9
{
   import flash.display.DisplayObject;
   import package_32.class_899;
   
   public class class_873 extends class_647
   {
       
      
      public var var_251:int;
      
      public var package_15:int;
      
      public var var_631:DisplayObject;
      
      public var var_617:Number;
      
      public var var_737:Number;
      
      public var var_736:Number;
      
      public var var_334:Number;
      
      public var var_1171:Number;
      
      public function class_873(param1:DisplayObject = undefined, param2:Number = 0.0, param3:Number = 0.0, param4:Number = 0.75, param5:int = 2)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         package_15 = param5;
         var_631 = param1;
         var_617 = param4;
         var_1171 = param1.x;
         var_334 = param1.y;
         var_736 = param2;
         var_737 = param3;
         var_251 = 0;
         method_79();
      }
      
      override public function method_79() : void
      {
         var_251 = var_251 + 1;
         if(int(var_251 % package_15) != 0)
         {
            return;
         }
         var_736 = var_736 * -var_617;
         var_737 = var_737 * -var_617;
         var_631.x = Number(var_1171 + var_736);
         var_631.y = Number(var_334 + var_737);
         if(Number(Number(Math.abs(var_736)) + Number(Math.abs(var_737))) < 1)
         {
            var_631.x = var_1171;
            var_631.y = var_334;
            method_89();
         }
      }
   }
}
