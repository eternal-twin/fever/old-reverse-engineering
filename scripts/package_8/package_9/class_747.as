package package_8.package_9
{
   import flash.display.DisplayObject;
   import package_32.class_899;
   
   public class class_747 extends class_647
   {
       
      
      public var var_251:int;
      
      public var var_763:int;
      
      public var var_764:int;
      
      public var var_631:DisplayObject;
      
      public function class_747(param1:DisplayObject = undefined, param2:int = 10, param3:int = 2, param4:int = 2)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_631 = param1;
         var_251 = param2;
         var_763 = param3;
         var_764 = param4;
      }
      
      override public function method_79() : void
      {
         var_631.visible = Number(Math.abs(var_251)) % (var_763 + var_764) < var_763;
         var _loc1_:int = var_251;
         var_251 = var_251 - 1;
         if(_loc1_ == 0)
         {
            method_89();
         }
      }
   }
}
