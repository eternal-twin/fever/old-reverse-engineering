package package_8.package_9
{
   import flash.display.DisplayObject;
   import flash.filters.GlowFilter;
   import package_11.package_12.class_657;
   import package_32.class_899;
   
   public class class_931 extends class_647
   {
       
      
      public var var_351:Number;
      
      public var var_243:DisplayObject;
      
      public var var_562:Number;
      
      public var name_13:Boolean;
      
      public var var_1524:Object;
      
      public var var_369:Object;
      
      public function class_931(param1:DisplayObject = undefined, param2:Number = 0.1, param3:Object = undefined, param4:Number = 1.0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_369 = param3;
         var_562 = param4;
         super();
         var_243 = param1;
         var_351 = param2;
         name_13 = true;
      }
      
      override public function method_79() : void
      {
         if(!var_243.visible)
         {
            return;
         }
         if(!name_13)
         {
            var_254 = Number(Math.min(Number(var_254 + var_351),1));
         }
         name_13 = false;
         method_1016();
         if(var_254 == 1)
         {
            method_89();
         }
      }
      
      public function method_1016() : void
      {
         var _loc2_:* = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc1_:Number = 1 - var_255(var_254);
         if(var_369 == null)
         {
            class_657.name_18(var_243,0,int(255 * _loc1_ * var_562));
         }
         else
         {
            class_657.gfof(var_243,_loc1_ * var_562,var_369);
         }
         if(var_1524 != null)
         {
            var_243.filters = [];
            _loc2_ = var_369;
            if(_loc2_ == null)
            {
               var_369 = 16777215;
            }
            _loc3_ = var_1524.var_134 * _loc1_;
            _loc4_ = var_1524.var_1525 * _loc1_;
            if(var_254 > 0)
            {
               var_243.filters = [new GlowFilter(var_369,1,_loc3_,_loc3_,_loc4_)];
            }
         }
      }
      
      public function var_146(param1:Number, param2:Number) : void
      {
         var_1524 = {
            "\x06Gv\x01":param1,
            "h\x10\x01":param2
         };
         method_1016();
      }
   }
}
