package package_8.package_9
{
   import flash.display.DisplayObject;
   import flash.filters.BlurFilter;
   import package_32.class_899;
   
   public class class_807 extends class_647
   {
       
      
      public var var_243:DisplayObject;
      
      public var var_1014:Object;
      
      public var var_1013:Object;
      
      public var var_1011:Boolean;
      
      public var var_1012:Number;
      
      public function class_807(param1:DisplayObject = undefined)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_243 = param1;
      }
      
      public function method_737(param1:Number) : void
      {
         var _loc2_:int = 0;
         if(var_1011)
         {
            var_243.alpha = param1 * var_1012;
         }
         if(var_1013 != null)
         {
            var_243.filters = [new BlurFilter(var_1013.var_244 * (1 - param1),var_1013.var_245 * (1 - param1))];
         }
         if(var_1014 != null)
         {
            _loc2_ = int(var_1014.var_346);
            if(_loc2_ == -1)
            {
               var_243.scaleX = var_1014.name_96 / param1;
            }
            else if(_loc2_ == 1)
            {
               var_243.scaleX = var_1014.name_96 * param1;
            }
            _loc2_ = int(var_1014.var_345);
            if(_loc2_ == -1)
            {
               var_243.scaleY = var_1014.var_1015 / param1;
            }
            else if(_loc2_ == 1)
            {
               var_243.scaleY = var_1014.var_1015 * param1;
            }
         }
      }
   }
}
