package
{
   import package_10.class_870;
   import package_11.class_844;
   import package_11.package_12.class_659;
   import package_11.package_12.class_663;
   import package_16.class_675;
   import package_16.class_684;
   import package_32.class_899;
   
   public class class_911
   {
      
      public static var var_1100:Number = 0.077;
      
      public static var var_214:class_911;
       
      
      public var var_329:Array;
      
      public var name_3:class_910;
      
      public var var_1481:Array;
      
      public var var_307:int;
      
      public var var_503:class_844;
      
      public var var_1477:int;
      
      public var var_443:Array;
      
      public var var_302:Boolean;
      
      public var var_1482:Array;
      
      public var var_1483:Array;
      
      public var var_1479:class_684;
      
      public var var_1480:class_675;
      
      public var var_1476:int;
      
      public var var_1478:Array;
      
      public function class_911(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         class_911.var_214 = this;
         var_329 = [param1];
         var_307 = 100 + param1 * 20;
      }
      
      public static function method_1096(param1:int, param2:int, param3:Object) : Object
      {
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc12_:Number = NaN;
         var _loc17_:* = null;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:int = 0;
         var _loc21_:int = 0;
         var _loc22_:int = 0;
         var _loc23_:int = 0;
         var _loc24_:int = 0;
         var _loc4_:class_844 = new class_844(param1 * class_911.var_214.var_307 + param2);
         var _loc5_:int = 0;
         while(_loc5_ < 3)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = _loc4_.var_503 * 16807 % 2147483647;
            _loc4_.var_503 = _loc7_;
            int((int(_loc7_) & 1073741823) % 10007) / 10007;
         }
         _loc7_ = param1 - class_911.var_214.var_307 * 0.5;
         var _loc8_:Number = param2 - class_911.var_214.var_307 * 0.5;
         var _loc9_:Number = Number(Number(Math.abs(_loc7_)) + Number(Math.abs(_loc8_)));
         var _loc10_:Number = _loc9_ / class_911.var_214.var_307;
         var _loc11_:Number = _loc4_.var_503 * 16807 % 2147483647;
         _loc4_.var_503 = _loc11_;
         _loc5_ = int((20 + _loc10_ * 40) * (0.75 + int((int(_loc11_) & 1073741823) % 10007) / 10007 * 0.5));
         _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
         _loc4_.var_503 = _loc12_;
         _loc11_ = Number(0.5 + (int((int(_loc12_) & 1073741823) % 10007) / 10007 * 2 - 1) * 0.2);
         _loc11_ = _loc11_ - 0.3 * Math.max(1 - _loc10_ * 5,0);
         _loc6_ = int(_loc5_ * _loc11_);
         if(_loc6_ < 1)
         {
            _loc6_ = 1;
         }
         var _loc13_:Array = [];
         var _loc14_:int = 0;
         var _loc15_:int = 0;
         var _loc16_:Array = class_885.var_308._monsters;
         while(_loc15_ < int(_loc16_.length))
         {
            _loc17_ = _loc16_[_loc15_];
            _loc15_++;
            _loc12_ = (int(_loc17_._rangeTo) + int(_loc17_._rangeFrom)) * 0.5;
            _loc18_ = (int(_loc17_._rangeTo) - int(_loc17_._rangeFrom)) * 0.5;
            _loc19_ = 1 - Math.abs(int(param3.var_237) - _loc12_) / _loc18_;
            _loc20_ = int(class_663.method_99(0,_loc19_,1) * int(_loc17_._weight));
            if(int(param3.var_237) > int(_loc17_._rangeTo))
            {
               _loc20_ = 2;
            }
            _loc13_.push({
               "\x1d\x0b\x01":int(_loc17_._id),
               "`(c\x02\x02":_loc20_
            });
            _loc14_ = _loc14_ + _loc20_;
         }
         _loc16_ = [];
         _loc15_ = 0;
         while(_loc15_ < _loc6_)
         {
            _loc15_++;
            _loc20_ = _loc15_;
            _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
            _loc4_.var_503 = _loc12_;
            _loc21_ = int((int(_loc12_) & 1073741823) % _loc14_);
            _loc22_ = 0;
            _loc23_ = 0;
            _loc24_ = 0;
            while(_loc24_ < int(_loc13_.length))
            {
               _loc17_ = _loc13_[_loc24_];
               _loc24_++;
               _loc22_ = _loc22_ + int(_loc17_.var_250);
               if(_loc22_ > _loc21_)
               {
                  _loc16_.push(int(_loc17_.var_215));
                  break;
               }
            }
         }
         var _loc25_:_Reward = null;
         _loc15_ = 0;
         _loc20_ = 0;
         while(_loc20_ < 4)
         {
            _loc20_++;
            _loc21_ = _loc20_;
            if(param3.var_480[_loc21_])
            {
               _loc15_++;
            }
         }
         _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
         _loc4_.var_503 = _loc12_;
         if(int((int(_loc12_) & 1073741823) % int([0,100,10,1][_loc15_])) == 0)
         {
            _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
            _loc4_.var_503 = _loc12_;
            if(int((int(_loc12_) & 1073741823) % 5) == 0)
            {
               _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
               _loc4_.var_503 = _loc12_;
               _loc20_ = int((int(_loc12_) & 1073741823) % 12);
               switch(_loc20_)
               {
                  case 0:
                  case 1:
                  case 2:
                  case 3:
                  case 4:
                     _loc25_ = _Reward.var_396;
                     break;
                  case 5:
                  case 6:
                  case 7:
                     _loc25_ = _Reward.method_274(_IslandBonus.var_398);
                     break;
                  case 8:
                  case 9:
                     _loc25_ = _Reward.method_274(_IslandBonus.var_399);
                     break;
                  case 10:
                     _loc25_ = _Reward.method_274(_IslandBonus.var_400);
                     break;
                  case 11:
                     _loc25_ = _Reward.var_395;
               }
            }
            else
            {
               _loc12_ = _loc4_.var_503 * 16807 % 2147483647;
               _loc4_.var_503 = _loc12_;
               _loc20_ = int((int(_loc12_) & 1073741823) % 10);
               switch(_loc20_)
               {
                  case 0:
                  case 1:
                  case 2:
                  case 3:
                  case 4:
                  case 5:
                     _loc25_ = _Reward.method_275(_GameBonus.var_401);
                     break;
                  case 6:
                  case 7:
                  case 8:
                     _loc25_ = _Reward.method_275(_GameBonus.var_402);
                     break;
                  case 9:
                     _loc25_ = _Reward.method_275(_GameBonus.var_403);
               }
            }
         }
         _loc17_ = {
            "C!F[\x01":_loc5_,
            ",\x13v\x04\x03":_loc16_,
            "\x03B\x1eI\x02":_loc25_,
            "6T\x0ez":_loc15_
         };
         return _loc17_;
      }
      
      public static function method_127(param1:int, param2:int) : Object
      {
         var _loc3_:int = class_911.var_214.var_307;
         if(param1 >= _loc3_)
         {
            param1 = param1 - _loc3_;
         }
         if(param2 >= _loc3_)
         {
            param2 = param2 - _loc3_;
         }
         if(param1 < 0)
         {
            param1 = param1 + _loc3_;
         }
         if(param2 < 0)
         {
            param2 = param2 + _loc3_;
         }
         return {
            "L\x01":param1,
            "\x03\x01":param2
         };
      }
      
      public function method_1101() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         loop2:
         switch(var_1476)
         {
            case 0:
               _loc1_ = 0;
               while(true)
               {
                  if(_loc1_ >= 800)
                  {
                     break loop2;
                  }
                  _loc1_++;
                  _loc2_ = _loc1_;
                  method_1097();
                  if(var_1477 == Number(Math.pow(class_911.var_214.var_307,2)))
                  {
                     method_1098();
                     method_1099();
                     var_1476 = var_1476 + 1;
                     break loop2;
                  }
               }
               break;
            case 1:
               _loc1_ = 0;
               while(_loc1_ < 61)
               {
                  _loc1_++;
                  _loc2_ = _loc1_;
                  if(int(var_1478.length) == 0)
                  {
                     name_2();
                     break loop2;
                  }
                  method_1100();
               }
         }
      }
      
      public function method_79() : void
      {
         switch(int(name_3.var_295))
         {
            case 0:
               var_1479.method_79();
               if(var_1479.var_302)
               {
                  var_1480 = new class_675(var_1479);
                  method_1102();
                  break;
               }
               break;
            case 1:
               method_1101();
            case 2:
               method_1101();
         }
      }
      
      public function method_1100() : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc11_:_Reward = null;
         var _loc12_:* = null;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc1_:* = var_1478.pop();
         var _loc2_:* = var_1480.method_285(int(_loc1_.var_244),int(_loc1_.var_245));
         var _loc4_:int = 4 * 2 + 1;
         var _loc5_:Array = [];
         var _loc6_:int = 0;
         while(_loc6_ < _loc4_)
         {
            _loc6_++;
            _loc7_ = _loc6_;
            _loc8_ = 0;
            while(_loc8_ < _loc4_)
            {
               _loc8_++;
               _loc9_ = _loc8_;
               _loc10_ = class_911.method_127(int(_loc1_.var_244) + _loc7_ - 4,int(_loc1_.var_245) + _loc9_ - 4);
               _loc11_ = var_1482[int(_loc10_.var_244)][int(_loc10_.var_245)].var_309;
               _loc12_ = var_1480.method_285(int(_loc10_.var_244),int(_loc10_.var_245));
               _loc13_ = Number(Math.abs(int(_loc12_.var_237) - int(_loc2_.var_237)));
               _loc14_ = Number(Math.abs(_loc13_ - 3));
               if(_loc11_ == null)
               {
                  _loc14_ = Number(_loc14_ + 10);
               }
               else if(class_830.method_176(_loc11_) || _loc11_ == _Reward.var_394)
               {
                  _loc14_ = Number(_loc14_ + 1000);
               }
               _loc5_.push({
                  "L\x01":int(_loc10_.var_244),
                  "\x03\x01":int(_loc10_.var_245),
                  "\x1bX)!\x02":_loc14_
               });
            }
         }
         var _loc15_:Function = function(param1:Object, param2:Object):int
         {
            var _loc3_:int = 0;
            var _loc4_:int = 0;
            if(Number(param1.var_895) < Number(param2.var_895))
            {
               return -1;
            }
            if(Number(param1.var_895) == Number(param2.var_895))
            {
               _loc3_ = int(param1.var_244) * 100 + int(param1.var_245);
               _loc4_ = int(param2.var_244) * 100 + int(param2.var_245);
               if(_loc3_ < _loc4_)
               {
                  return -1;
               }
               return 1;
            }
            return 1;
         };
         class_659.method_241(_loc5_,var_503);
         _loc5_.sort(_loc15_);
         _loc10_ = _loc5_[0];
         var_1482[int(_loc10_.var_244)][int(_loc10_.var_245)].var_309 = _Reward.var_394;
      }
      
      public function method_1102() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         name_3 = class_910.var_1474;
         var_503 = new class_844(33 + var_329[0]);
         var_443 = [];
         var_1482 = [];
         var_1477 = 0;
         var_1476 = 0;
         var_1483 = [];
         var _loc1_:int = 0;
         var _loc2_:int = class_911.var_214.var_307;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_1482[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = class_911.var_214.var_307;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               if(var_329[0] > 0)
               {
                  _loc7_ = var_1480.method_285(_loc3_,_loc6_);
                  _loc7_.var_237 = int(_loc7_.var_237) + var_329[0] * 10;
               }
               var_1482[_loc3_][_loc6_] = {
                  "\\Uw\x01":null,
                  "/L\x02a\x01":-1
               };
            }
         }
      }
      
      public function method_95() : void
      {
         var_302 = false;
         name_3 = class_910.var_1473;
         var_1479 = new class_684(class_911.var_214.var_307,class_911.var_214.var_307,0);
         var_1479.var_502 = 0;
         var_1479.var_506 = 10;
         var_1479.var_505 = true;
         var_1479.var_504 = true;
         var _loc1_:int = int(class_911.var_214.var_307 * 0.5);
         var_1479.var_390 = {
            "L\x01":_loc1_,
            "\x03\x01":_loc1_
         };
         var_1479.var_501 = 100;
         var_1479.method_312();
      }
      
      public function method_806() : Array
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:* = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         var _loc3_:int = class_911.var_214.var_307;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = class_911.var_214.var_307;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = method_175(_loc4_,_loc7_);
               _loc1_.push(method_203(_loc8_));
            }
         }
         return _loc1_;
      }
      
      public function method_203(param1:Object) : Object
      {
         return {
            "Q2{\x01":int(param1.name_15.var_242.length),
            "\\Uw\x01":param1.var_309
         };
      }
      
      public function method_175(param1:int, param2:int) : Object
      {
         var _loc3_:* = var_1480.method_285(param1,param2);
         var _loc4_:Number = Number(Math.min(int(_loc3_.var_237) / 100,1));
         var _loc5_:* = class_911.method_1096(param1,param2,_loc3_);
         var _loc6_:* = var_1482[param1][param2];
         var _loc7_:Array = _loc3_.var_480.method_78();
         var _loc8_:* = {
            "Dk#\x01":_loc5_,
            "}\t\t\x01":_loc4_,
            "\x04);[":_loc7_,
            "\\Uw\x01":_loc6_.var_309,
            "/L\x02a\x01":int(_loc6_.name_140)
         };
         if(_loc8_.var_309 == _Reward.class_794)
         {
            _loc8_.name_15.var_242 = [];
            _loc4_ = 1;
            _loc5_.var_307 = 29;
         }
         if(param1 == int(class_911.var_214.var_307 * 0.5) && param2 == int(class_911.var_214.var_307 * 0.5))
         {
            _loc8_.name_15.var_242 = [0,0];
            _loc8_.name_15.var_307 = 60;
         }
         return _loc8_;
      }
      
      public function name_2() : void
      {
         name_3 = class_910.var_1475;
         var_302 = true;
      }
      
      public function method_1097() : void
      {
         var _loc1_:int = class_911.var_214.var_307;
         var _loc2_:int = int(var_1477 % _loc1_);
         var _loc3_:int = int(var_1477 / _loc1_);
         var _loc4_:* = var_1480.method_285(_loc2_,_loc3_);
         var _loc5_:* = class_911.method_1096(_loc2_,_loc3_,_loc4_);
         var_1482[_loc2_][_loc3_].var_309 = _loc5_.name_141;
         var _loc6_:class_844 = var_503;
         var _loc7_:Number = _loc6_.var_503 * 16807 % 2147483647;
         _loc6_.var_503 = _loc7_;
         var_443.method_249(int((int(_loc7_) & 1073741823) % int(var_443.length)),{
            "PLdl\x01":_loc4_,
            "Dk#\x01":_loc5_,
            "L\x01":_loc2_,
            "\x03\x01":_loc3_,
            "L{\x04\x02":var_1477
         });
         var_1477 = var_1477 + 1;
      }
      
      public function method_1099() : void
      {
         var _loc3_:* = null;
         var _loc4_:_Reward = null;
         var_1478 = [];
         var _loc1_:int = 0;
         var _loc2_:Array = var_443;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc4_ = var_1482[int(_loc3_.var_244)][int(_loc3_.var_245)].var_309;
            if(class_830.method_176(_loc4_))
            {
               var_1478.push({
                  "L\x01":int(_loc3_.var_244),
                  "\x03\x01":int(_loc3_.var_245)
               });
            }
         }
      }
      
      public function method_1098() : void
      {
         var _loc6_:int = 0;
         var _loc7_:Number = NaN;
         var _loc9_:int = 0;
         var _loc10_:Number = NaN;
         var _loc15_:int = 0;
         var _loc16_:class_844 = null;
         var _loc20_:int = 0;
         var _loc21_:* = null;
         var _loc22_:* = null;
         var _loc23_:Boolean = false;
         var _loc24_:int = 0;
         var _loc25_:Array = null;
         var _loc26_:* = null;
         var _loc27_:int = 0;
         var _loc28_:int = 0;
         var _loc29_:Number = NaN;
         var _loc1_:Function = function(param1:Object, param2:Object):int
         {
            if(int(param1.name_142.var_237) < int(param2.name_142.var_237))
            {
               return -1;
            }
            if(int(param1.name_142.var_237) == int(param2.name_142.var_237))
            {
               if(int(param1.var_366) < int(param2.var_366))
               {
                  return -1;
               }
               return 1;
            }
            return 1;
         };
         var_443.sort(_loc1_);
         var _loc2_:int = int(var_443[int(var_443.length) - 1].name_142.var_237);
         var _loc3_:int = class_885.var_1303 * 4;
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         while(_loc5_ < _loc3_)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc7_ = Number(Math.pow(_loc6_ / _loc3_,2.5));
            _loc4_.push(_loc7_);
         }
         var _loc8_:Array = [];
         _loc5_ = 0;
         _loc6_ = class_885.var_1360;
         while(_loc5_ < _loc6_)
         {
            _loc5_++;
            _loc9_ = _loc5_;
            _loc7_ = Number(Math.pow(_loc9_ / class_885.var_1360,1.5));
            _loc10_ = 0.03;
            _loc7_ = Number(_loc10_ + _loc7_ * (1 - _loc10_));
            _loc8_.push(_loc7_);
         }
         _loc5_ = 21;
         var _loc11_:Array = [];
         var _loc12_:Array = Type.method_253(_Item);
         _loc6_ = 0;
         while(_loc6_ < _loc5_)
         {
            _loc6_++;
            _loc9_ = _loc6_;
            _loc11_.push({
               "cHX@\x01":Type.method_254(_Item,_loc12_[_loc9_]),
               "}\t\t\x01":Number(class_885.var_1359[_loc9_])
            });
         }
         var _loc13_:Function = function(param1:Object, param2:Object):int
         {
            if(Number(param1.var_237) < Number(param2.var_237))
            {
               return -1;
            }
            if(Number(param1.var_237) == Number(param2.var_237))
            {
               class_870.name_17("items dif equal error ! (" + Number(param1.var_237) + ")",{
                  "\x06\fi2\x03":"WorldData.hx",
                  "d\x0f^S\x03":185,
                  "v2z\x01":"WorldData",
                  "d\x19o\f\x03":"addCustomRewards"
               });
            }
            return 1;
         };
         _loc11_.sort(_loc13_);
         _loc6_ = 7;
         var _loc14_:Array = [];
         _loc9_ = 0;
         while(_loc9_ < _loc6_)
         {
            _loc9_++;
            _loc15_ = _loc9_;
            _loc16_ = var_503;
            _loc7_ = _loc16_.var_503 * 16807 % 2147483647;
            _loc16_.var_503 = _loc7_;
            _loc14_.push({
               "}\t\t\x01":Number(_loc15_ / _loc6_ + int((int(_loc7_) & 1073741823) % 10007) / 10007 * 0.035),
               "cHX@\x01":[_Item.var_410,_Item.var_414,_Item.var_415,_Item.var_413,_Item.var_411,_Item.var_409,_Item.var_412][_loc15_]
            });
         }
         var _loc17_:Array = [];
         var _loc18_:* = var_443[int(int(var_443.length) * class_911.var_1100)];
         var_1482[int(_loc18_.var_244)][int(_loc18_.var_245)].var_309 = _Reward.class_794;
         var _loc19_:Array = [];
         _loc9_ = 0;
         _loc15_ = class_885.var_720;
         while(_loc9_ < _loc15_)
         {
            _loc9_++;
            _loc20_ = _loc9_;
            _loc16_ = var_503;
            _loc7_ = _loc16_.var_503 * 16807 % 2147483647;
            _loc16_.var_503 = _loc7_;
            _loc19_.push(Number(_loc20_ / class_885.var_720 + int((int(_loc7_) & 1073741823) % 10007) / 10007 * 0.024));
         }
         var_1481 = [];
         _loc9_ = 0;
         _loc15_ = int(var_443.length);
         while(_loc9_ < _loc15_)
         {
            _loc9_++;
            _loc20_ = _loc9_;
            _loc21_ = var_443[_loc20_];
            _loc22_ = var_1482[int(_loc21_.var_244)][int(_loc21_.var_245)];
            if(!(int(_loc21_.name_15.name_143) < 3 || Boolean(class_830.method_176(_loc22_.var_309))))
            {
               _loc7_ = _loc20_ / int(var_443.length);
               if(int(_loc19_.length) > 0)
               {
                  if(_loc7_ > Number(_loc19_[0]))
                  {
                     _loc23_ = true;
                     _loc24_ = 0;
                     _loc25_ = var_1481;
                     while(_loc24_ < int(_loc25_.length))
                     {
                        _loc26_ = _loc25_[_loc24_];
                        _loc24_++;
                        _loc27_ = int(_loc26_.var_244) - int(_loc21_.var_244);
                        _loc28_ = int(_loc26_.var_245) - int(_loc21_.var_245);
                        if(Number(Number(Math.abs(_loc27_)) + Number(Math.abs(_loc28_))) < 24)
                        {
                           _loc23_ = false;
                           break;
                        }
                     }
                     if(_loc23_)
                     {
                        _loc22_.name_140 = int(var_1481.length);
                        _loc19_.shift();
                        var_1481.push(_loc21_);
                     }
                  }
               }
               if(int(_loc14_.length) > 0)
               {
                  if(_loc7_ > Number(_loc14_[0].var_237))
                  {
                     _loc24_ = 2 + int(40 * _loc7_);
                     _loc23_ = true;
                     _loc27_ = 0;
                     while(_loc27_ < int(_loc17_.length))
                     {
                        _loc26_ = _loc17_[_loc27_];
                        _loc27_++;
                        _loc10_ = Number(Math.abs(int(_loc21_.var_244) - int(_loc26_.var_244)));
                        _loc29_ = Number(Math.abs(int(_loc21_.var_245) - int(_loc26_.var_245)));
                        if(Number(Math.abs(_loc10_)) < _loc24_ && Number(Math.abs(_loc29_)) < _loc24_)
                        {
                           _loc23_ = false;
                           break;
                        }
                     }
                     if(_loc23_)
                     {
                        _loc22_.var_309 = _Reward.method_273(_loc14_[0].var_252);
                        _loc14_.shift();
                        _loc17_.push({
                           "L\x01":int(_loc21_.var_244),
                           "\x03\x01":int(_loc21_.var_245)
                        });
                        continue;
                     }
                  }
               }
               if(int(_loc11_.length) > 0)
               {
                  if(_loc7_ > Number(_loc11_[0].var_237))
                  {
                     _loc22_.var_309 = _Reward.method_273(_loc11_[0].var_252);
                     _loc11_.shift();
                     continue;
                  }
               }
               if(int(_loc4_.length) > 0)
               {
                  if(_loc7_ > Number(_loc4_[0]))
                  {
                     _loc22_.var_309 = _Reward.var_397;
                     _loc4_.shift();
                     continue;
                  }
               }
               if(int(_loc8_.length) > 0)
               {
                  if(_loc7_ > Number(_loc8_[0]))
                  {
                     _loc22_.var_309 = _Reward.method_276(class_885.var_1360 - int(_loc8_.length));
                     _loc8_.shift();
                  }
               }
            }
         }
      }
   }
}
