package
{
   import flash.display.BitmapData;
   import flash.display.GradientType;
   import flash.display.InterpolationMethod;
   import flash.display.MovieClip;
   import flash.display.SpreadMethod;
   import flash.display.Sprite;
   import flash.geom.Matrix;
   import flash.geom.Rectangle;
   import package_11.package_12.class_657;
   
   public class class_649
   {
      
      public static var var_290:Number = 0.0174;
       
      
      public function class_649()
      {
      }
      
      public static function method_146(param1:Number) : Number
      {
         return param1 / Math.PI * 180;
      }
      
      public static function var_70(param1:Number, param2:Boolean = true) : Number
      {
         if(param2)
         {
            return param1 * class_649.var_290;
         }
         return param1 * Math.PI / 180;
      }
      
      public static function method_147(param1:MovieClip) : void
      {
         param1.scaleX = -1;
      }
      
      public static function name_9(param1:Number, param2:Boolean = true) : Number
      {
         if(param2)
         {
            return Number(Math.sin(param1 * class_649.var_290));
         }
         return Number(Math.sin(param1 * Math.PI / 180));
      }
      
      public static function method_148(param1:Number, param2:Boolean = true) : Number
      {
         if(param2)
         {
            return Number(Math.cos(param1 * class_649.var_290));
         }
         return Number(Math.cos(param1 * Math.PI / 180));
      }
      
      public static function method_149(param1:MovieClip, param2:int = 16777215) : void
      {
         param1.graphics.lineStyle(1,param2,100);
         param1.graphics.moveTo(0,0);
         param1.graphics.lineTo(0,1);
      }
      
      public static function method_150(param1:int, param2:int, param3:int, param4:int, param5:int, param6:BitmapData) : void
      {
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         var _loc7_:int = param4 - param2;
         var _loc8_:int = param3 - param1;
         if(_loc7_ < 0)
         {
            _loc7_ = -_loc7_;
            _loc10_ = -1;
         }
         else
         {
            _loc10_ = 1;
         }
         if(_loc8_ < 0)
         {
            _loc8_ = -_loc8_;
            _loc9_ = -1;
         }
         else
         {
            _loc9_ = 1;
         }
         _loc7_ = _loc7_ << 1;
         _loc8_ = _loc8_ << 1;
         param6.setPixel32(param1,param2,param5);
         if(_loc8_ > _loc7_)
         {
            _loc11_ = _loc7_ - (_loc8_ >> 1);
            while(param1 != param3)
            {
               if(_loc11_ >= 0)
               {
                  param2 = param2 + _loc10_;
                  _loc11_ = _loc11_ - _loc8_;
               }
               param1 = param1 + _loc9_;
               _loc11_ = _loc11_ + _loc7_;
               param6.setPixel32(param1,param2,param5);
            }
         }
         else
         {
            _loc11_ = _loc8_ - (_loc7_ >> 1);
            while(param2 != param4)
            {
               if(_loc11_ >= 0)
               {
                  param1 = param1 + _loc9_;
                  _loc11_ = _loc11_ - _loc7_;
               }
               param2 = param2 + _loc10_;
               _loc11_ = _loc11_ + _loc8_;
               param6.setPixel32(param1,param2,param5);
            }
         }
      }
      
      public static function method_152(param1:MovieClip, param2:Number, param3:Number, param4:int = 16777215, param5:Boolean = false, param6:Number = 1.0, param7:Number = 100) : void
      {
         var _loc8_:Matrix = null;
         param1.graphics.lineStyle(param6,param4,param7);
         if(param5)
         {
            _loc8_ = new Matrix();
            _loc8_.createGradientBox(param2,param3);
            param1.graphics.lineGradientStyle(GradientType.LINEAR,[param4,int(class_657.method_151(param4,50))],[param7,param7],[0,255],_loc8_,SpreadMethod.REFLECT,InterpolationMethod.LINEAR_RGB);
         }
         param1.graphics.moveTo(0,0);
         param1.graphics.lineTo(param2,param3);
      }
      
      public static function method_153(param1:MovieClip, param2:Number, param3:Number, param4:Object = undefined, param5:int = 0, param6:int = 0, param7:int = 100, param8:int = 0) : void
      {
         if(param4 != null)
         {
            param1.graphics.beginFill(param4,param5);
         }
         param1.graphics.lineStyle(param8,param6,param7);
         param1.graphics.moveTo(0,0);
         param1.graphics.lineTo(param2,0);
         param1.graphics.lineTo(param2,param3);
         param1.graphics.lineTo(0,param3);
         param1.graphics.lineTo(0,0);
         param1.graphics.endFill();
      }
      
      public static function method_154(param1:MovieClip, param2:Number, param3:Object = undefined, param4:int = 0, param5:int = 0, param6:int = 100, param7:int = 0) : void
      {
         if(param3 != null)
         {
            param1.graphics.beginFill(param3,param4);
         }
         param1.graphics.lineStyle(param7,param5,param6);
         var _loc8_:Number = param2 * Math.sqrt(3) / 2;
         param1.graphics.moveTo(param2 / 2,0);
         param1.graphics.lineTo(param2,_loc8_);
         param1.graphics.lineTo(0,_loc8_);
         param1.graphics.lineTo(param2 / 2,0);
         param1.graphics.endFill();
      }
      
      public static function method_156(param1:Number, param2:Number) : Number
      {
         return Number(class_649.method_155(Number(Number(360 + Number(class_649.method_146(Number(Math.atan2(param1,param2))))) + 90)));
      }
      
      public static function method_157(param1:Number) : Number
      {
         if(param1 >= 0)
         {
            if(param1 == 360)
            {
               return 0;
            }
            return param1;
         }
         return Number(360 + param1);
      }
      
      public static function method_155(param1:Number) : Number
      {
         if(param1 < 0)
         {
            return 360 - param1;
         }
         if(param1 > 360)
         {
            return param1 - 360;
         }
         return param1;
      }
      
      public static function method_158(param1:MovieClip, param2:Number = 0.0, param3:Number = 0.0) : Matrix
      {
         var _loc4_:Matrix = new Matrix();
         _loc4_.translate(Number(param1.x + param2),Number(param1.y + param3));
         return _loc4_;
      }
      
      public static function method_159(param1:Sprite, param2:Sprite) : Rectangle
      {
         return param1.getBounds(param2);
      }
      
      public static function method_160(param1:MovieClip, param2:Number, param3:int, param4:Boolean = false, param5:int = 0) : void
      {
         var _loc14_:int = 0;
         var _loc6_:Number = param1.x;
         var _loc7_:Number = param1.y;
         var _loc9_:Number = Math.PI / 10;
         var _loc10_:Number = param2 / Math.cos(_loc9_);
         var _loc11_:Number = 0;
         var _loc12_:Number = 0;
         param1.graphics.lineStyle(1,param3,100,true);
         if(param4)
         {
            param1.graphics.beginFill(param5);
         }
         param1.graphics.moveTo(Number(_loc6_ + Math.cos(_loc11_) * param2),Number(_loc7_ + Math.sin(_loc11_) * param2));
         var _loc13_:int = 0;
         while(_loc13_ < 10)
         {
            _loc13_++;
            _loc14_ = _loc13_;
            _loc12_ = Number(_loc11_ + _loc9_);
            _loc11_ = Number(_loc12_ + _loc9_);
            param1.graphics.curveTo(Number(_loc6_ + Math.cos(_loc12_) * _loc10_),Number(_loc7_ + Math.sin(_loc12_) * _loc10_),Number(_loc6_ + Math.cos(_loc11_) * param2),Number(_loc7_ + Math.sin(_loc11_) * param2));
         }
         if(param4)
         {
            param1.graphics.endFill();
         }
      }
      
      public static function method_161(param1:MovieClip, param2:Number, param3:int, param4:int = 0, param5:Boolean = false) : void
      {
         var _loc17_:int = 0;
         var _loc6_:Number = param1.x;
         var _loc7_:Number = param1.y;
         var _loc9_:Number = Math.PI / 10;
         var _loc10_:Number = param2 / Math.cos(_loc9_);
         var _loc11_:Number = 0;
         var _loc12_:Number = 0;
         var _loc13_:int = 0;
         if(param5)
         {
            _loc13_ = int(class_657.method_151(param3,-40));
         }
         var _loc14_:Matrix = new Matrix();
         var _loc15_:Number = param2 - param2 * 70 / 100;
         _loc14_.createGradientBox(param2 * 2 - _loc15_,param2 * 2 - _loc15_,Number(class_649.var_70(270)),Number(-param2 + _loc15_ / 2),-param2 - _loc15_ / 2);
         param1.graphics.beginGradientFill(GradientType.RADIAL,[param3,!!param5?_loc13_:param4],[100,100],[0,255],_loc14_,SpreadMethod.PAD,InterpolationMethod.RGB,0.5);
         param1.graphics.moveTo(Number(_loc6_ + Math.cos(_loc11_) * param2),Number(_loc7_ + Math.sin(_loc11_) * param2));
         var _loc16_:int = 0;
         while(_loc16_ < 10)
         {
            _loc16_++;
            _loc17_ = _loc16_;
            _loc12_ = Number(_loc11_ + _loc9_);
            _loc11_ = Number(_loc12_ + _loc9_);
            param1.graphics.curveTo(Number(_loc6_ + Math.cos(_loc12_) * _loc10_),Number(_loc7_ + Math.sin(_loc12_) * _loc10_),Number(_loc6_ + Math.cos(_loc11_) * param2),Number(_loc7_ + Math.sin(_loc11_) * param2));
         }
         param1.graphics.endFill();
      }
      
      public function method_162(param1:Object) : Object
      {
         var _loc2_:Number = int(param1.var_93) / 255;
         var _loc3_:Number = int(param1.var_39) / 255;
         var _loc4_:Number = int(param1.var_12) / 255;
         var _loc5_:* = name_10([_loc2_,_loc3_,_loc4_]);
         var _loc6_:* = name_11([_loc2_,_loc3_,_loc4_]);
         var _loc7_:Number = _loc6_ - _loc5_;
         var _loc8_:Number = 0;
         var _loc9_:Number = 0;
         var _loc10_:* = _loc6_;
         if(_loc7_ == 0)
         {
            return {
               "\x0b\x01":int(Math.round(_loc8_ * 100)),
               "\n\x01":int(Math.round(_loc9_ * 100)),
               "X\x01":int(Math.round(_loc10_ * 100))
            };
         }
         _loc9_ = _loc7_ / _loc6_;
         var _loc11_:Number = ((_loc6_ - _loc2_) / 6 + _loc7_ / 2) / _loc7_;
         var _loc12_:Number = ((_loc6_ - _loc3_) / 6 + _loc7_ / 2) / _loc7_;
         var _loc13_:Number = ((_loc6_ - _loc4_) / 6 + _loc7_ / 2) / _loc7_;
         if(_loc2_ == _loc6_)
         {
            _loc8_ = _loc13_ - _loc12_;
         }
         else if(_loc3_ == _loc6_)
         {
            _loc8_ = 1 / 3 + _loc11_ - _loc13_;
         }
         else if(_loc4_ == _loc6_)
         {
            _loc8_ = 2 / 3 + _loc12_ - _loc11_;
         }
         if(_loc8_ < 0)
         {
            _loc8_++;
         }
         if(_loc8_ > 1)
         {
            _loc8_--;
         }
         return {
            "\x0b\x01":int(Math.round(_loc8_ * 100)),
            "\n\x01":int(Math.round(_loc9_ * 100)),
            "X\x01":int(Math.round(_loc10_ * 100))
         };
      }
      
      public function name_10(param1:Array) : Object
      {
         var _loc4_:Number = NaN;
         var _loc2_:* = param1.pop();
         var _loc3_:int = 0;
         while(_loc3_ < int(param1.length))
         {
            _loc4_ = Number(param1[_loc3_]);
            _loc3_++;
            if(_loc4_ < _loc2_)
            {
               _loc2_ = _loc4_;
            }
         }
         return _loc2_;
      }
      
      public function name_11(param1:Array) : Object
      {
         var _loc4_:Number = NaN;
         var _loc2_:* = param1.pop();
         var _loc3_:int = 0;
         while(_loc3_ < int(param1.length))
         {
            _loc4_ = Number(param1[_loc3_]);
            _loc3_++;
            if(_loc4_ > _loc2_)
            {
               _loc2_ = _loc4_;
            }
         }
         return _loc2_;
      }
      
      public function method_163(param1:Object) : Object
      {
         var _loc2_:int = int(param1.var_64);
         var _loc3_:int = int(param1.var_86);
         var _loc4_:int = int(param1.var_17);
         var _loc5_:Number = 0;
         var _loc6_:Number = 0;
         var _loc7_:Number = 0;
         if(_loc2_ == 0)
         {
            return {
               "z\x01":int(Math.round(_loc3_ * 255)),
               "6\x01":int(Math.round(_loc3_ * 255)),
               "r\x01":int(Math.round(_loc3_ * 255))
            };
         }
         var _loc8_:int = _loc4_ * 6;
         if(_loc8_ == 6)
         {
            _loc8_ = 0;
         }
         var _loc9_:int = int(_loc8_);
         var _loc10_:int = _loc3_ * (1 - _loc2_);
         var _loc11_:int = _loc3_ * (1 - _loc2_ * (_loc8_ - _loc9_));
         var _loc12_:int = _loc3_ * (1 - _loc2_ * (1 - (_loc8_ - _loc9_)));
         if(_loc9_ == 0)
         {
            _loc5_ = _loc3_;
            _loc6_ = _loc12_;
            _loc7_ = _loc10_;
         }
         else if(_loc9_ == 1)
         {
            _loc5_ = _loc11_;
            _loc6_ = _loc3_;
            _loc7_ = _loc10_;
         }
         else if(_loc9_ == 2)
         {
            _loc5_ = _loc10_;
            _loc6_ = _loc3_;
            _loc7_ = _loc12_;
         }
         else if(_loc9_ == 3)
         {
            _loc5_ = _loc10_;
            _loc6_ = _loc11_;
            _loc7_ = _loc3_;
         }
         else if(_loc9_ == 4)
         {
            _loc5_ = _loc12_;
            _loc6_ = _loc10_;
            _loc7_ = _loc3_;
         }
         else
         {
            _loc5_ = _loc3_;
            _loc6_ = _loc10_;
            _loc7_ = _loc11_;
         }
         return {
            "z\x01":int(Math.round(_loc5_ * 255)),
            "6\x01":int(Math.round(_loc6_ * 255)),
            "r\x01":int(Math.round(_loc7_ * 255))
         };
      }
   }
}
