package
{
   import flash.display.MovieClip;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_845 extends class_645
   {
      
      public static var var_1191:int = 80;
      
      public static var var_1192:int = 40;
       
      
      public var var_520:Boolean;
      
      public var var_1194:int;
      
      public var var_1195:Boolean;
      
      public var name_27:Boolean;
      
      public var var_1197:int;
      
      public var var_1196:Array;
      
      public var var_1193:MovieClip;
      
      public var name_56:int;
      
      public function class_845()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc2_:Number = NaN;
         var _loc3_:Number = NaN;
         var _loc4_:Array = null;
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc1_:* = method_100();
         if(!name_27 && Number(_loc1_.var_244) >= 0 && Number(_loc1_.var_244) <= class_742.var_216 && Number(_loc1_.var_245) >= 0 && Number(_loc1_.var_245) <= class_742.var_218)
         {
            _loc2_ = _loc1_.var_244 - var_1193.x;
            _loc3_ = _loc1_.var_245 - var_1193.y;
            if(var_1193.var_149 != null)
            {
               var_1193.var_149.x = Number(var_1193.var_149.x + _loc2_);
               var_1193.var_149.y = Number(var_1193.var_149.y + _loc3_);
            }
            if(var_1193.var_150 != null)
            {
               var_1193.var_150.x = Number(var_1193.var_150.x + _loc2_);
               var_1193.var_150.y = Number(var_1193.var_150.y + _loc3_);
            }
            if(var_1193.var_147 != null)
            {
               var_1193.var_147.x = Number(var_1193.var_147.x + _loc2_);
               var_1193.var_147.y = Number(var_1193.var_147.y + _loc3_);
            }
            var_1193.x = Number(_loc1_.var_244);
            var_1193.y = Number(_loc1_.var_245);
         }
         loop1:
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_864();
               method_865();
               break;
            case 2:
               method_865();
               _loc4_ = [var_1193.var_149,var_1193.var_150,var_1193.var_147];
               _loc2_ = 1;
               _loc5_ = 0;
               while(true)
               {
                  if(_loc5_ >= int(_loc4_.length))
                  {
                     break loop1;
                  }
                  _loc6_ = _loc4_[_loc5_];
                  _loc5_++;
                  if(_loc6_.tabIndex < 2)
                  {
                     _loc6_.tabIndex = int(class_691.method_114(200));
                  }
                  _loc6_.y = Number(_loc6_.y + _loc2_);
                  _loc6_.rotation = Number(_loc6_.rotation + (_loc6_.tabIndex - 100) * 0.1);
                  _loc2_ = Number(_loc2_ + 1.5);
               }
         }
         super.method_79();
      }
      
      public function method_866() : void
      {
         var_1193.gotoAndStop(1);
      }
      
      override public function method_80() : void
      {
         method_81(true);
         var_520 = true;
      }
      
      public function method_865() : void
      {
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_1196;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if((var_1195 || var_520) && var_1193.var_149 == null)
            {
               _loc3_.alpha = 0.1;
            }
            else
            {
               if(var_1194 >= 0)
               {
                  _loc4_ = var_1194;
                  var_1194 = var_1194 - 1;
                  if(_loc4_ <= 0)
                  {
                     _loc5_ = var_1193.var_1;
                     _loc5_.gotoAndStop(1);
                     var_1194 = -1;
                  }
               }
               _loc3_.alpha = 1;
               _loc3_.x = Number(_loc3_.x + (Number(Number(_loc3_.var_354) + class_649.method_148(_loc3_.rotation) * _loc3_.var_354)));
               _loc3_.y = Number(_loc3_.y + (Number(Number(_loc3_.var_353) + class_649.name_9(_loc3_.rotation) * _loc3_.var_353)));
               _loc3_.rotation = Number(_loc3_.rotation + Number(_loc3_.name_113));
               if(Number(_loc3_.var_354) < 0)
               {
                  if(_loc3_.x < -_loc3_.width)
                  {
                     _loc3_.x = class_742.var_216;
                  }
               }
               else if(_loc3_.x > Number(class_742.var_216 + _loc3_.width))
               {
                  _loc3_.x = 0;
               }
               if(Number(_loc3_.var_353) < 0)
               {
                  if(_loc3_.y < -_loc3_.width)
                  {
                     _loc3_.y = class_742.var_218;
                  }
               }
               else if(_loc3_.y > Number(class_742.var_218 + _loc3_.width))
               {
                  _loc3_.y = 0;
               }
               if(var_155(_loc3_,_loc3_.var_149))
               {
                  var_1196.method_116(_loc3_);
                  method_867(_loc3_,1);
               }
               else if(var_155(_loc3_,_loc3_.var_150))
               {
                  var_1196.method_116(_loc3_);
                  method_867(_loc3_,2);
               }
            }
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         name_56 = 0;
         var_1194 = 0;
         var_1195 = false;
         var_520 = false;
         var_1196 = [];
         var_1197 = int(Number(50 + param1 * 20));
         method_117();
      }
      
      public function method_867(param1:MovieClip, param2:int) : void
      {
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         var _loc6_:class_892 = null;
         var _loc7_:class_473 = null;
         new class_931(var_1193,0.1,16711680);
         method_102(3);
         var_1194 = class_845.var_1192;
         method_868();
         param1.gotoAndStop(1 + param2);
         var _loc3_:int = 0;
         while(_loc3_ < 20)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = var_232.method_84(class_899.__unprotect__("%n\ri\x03"),5);
            _loc5_.x = param1.x;
            _loc5_.y = param1.y;
            _loc6_ = new class_892(_loc5_);
            _loc6_.var_251 = 10;
            _loc6_.var_250 = Number(0.3 + int(class_691.method_114(2)) / 10);
            _loc6_.var_278 = Math.cos(Number(param1.var_354)) * (int(class_691.method_114(2)) + 1) / 10;
            _loc6_.var_272 = 3;
            _loc6_.name_20 = int(Math.round(_loc4_ / 10));
         }
         if(var_1193.var_149 == null)
         {
            var_1193.var_149 = param1;
            return;
         }
         if(var_1193.var_150 == null)
         {
            var_1193.var_150 = param1;
            return;
         }
         if(var_1193.var_147 == null)
         {
            var_1193.var_147 = param1;
            name_27 = true;
            method_81(false,20);
            var_1193.visible = false;
            _loc7_ = new class_473();
            _loc7_.x = var_1193.x;
            _loc7_.y = var_1193.y;
            var_232.method_124(_loc7_,10);
            name_3 = 2;

         }
      }
      
      public function var_155(param1:MovieClip, param2:MovieClip) : Boolean
      {
         if(param2 == null)
         {
            return false;
         }
         var _loc3_:Number = Number(var_1193.x - param1.x + param1.width / 2);
         var _loc4_:Number = Number(var_1193.y - param1.y + param1.height / 2);
         if(Number(Math.abs(_loc4_)) > 50)
         {
            return false;
         }
         if(Number(Math.abs(_loc3_)) > 50)
         {
            return false;
         }
         var _loc5_:Number = Number(class_649.method_156(param2.y,param2.x));
         var _loc6_:Number = Number(Number(class_649.method_157(param1.rotation)) + _loc5_);
         _loc6_ = Number(class_649.method_155(_loc6_));
         var _loc7_:Number = 0;
         if(param2.x == 0)
         {
            _loc7_ = param2.y / class_649.method_148(_loc5_);
         }
         else
         {
            _loc7_ = param2.x / class_649.name_9(_loc5_);
         }
         var _loc8_:Number = class_649.name_9(_loc6_) * _loc7_;
         var _loc9_:Number = var_1193.x - param1.x;
         var _loc10_:Number = -class_649.method_148(_loc6_) * _loc7_;
         var _loc11_:Number = var_1193.y - param1.y;
         var _loc13_:Number = _loc10_ - _loc11_;
         var _loc14_:Number = _loc8_ - _loc9_;
         var _loc15_:Number = Number(Math.sqrt(Number(_loc13_ * _loc13_ + _loc14_ * _loc14_)));
         if(_loc15_ > 0 && _loc15_ < 10)
         {
            return true;
         }
         return false;
      }
      
      public function method_212(param1:Number) : Number
      {
         if(param1 > 0)
         {
            return param1;
         }
         return Number(360 + param1);
      }
      
      public function method_864() : void
      {
         if(var_1193.var_149 != null)
         {
            return;
         }
         var _loc1_:int = name_56;
         name_56 = name_56 - 1;
         if(_loc1_ <= 0)
         {
            if(var_1193.currentFrame == 1)
            {
               var_1193.gotoAndStop(2);
               name_56 = int(Math.floor(class_845.var_1191 / 6));
               var_1195 = true;
               return;
            }
            var_1195 = false;
            method_866();
            name_56 = class_845.var_1191 + int(class_691.method_114(class_845.var_1191));
         }
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("v~jl"),0);
         var_1193 = var_232.method_84(class_899.__unprotect__("*{\x03a"),1);
         var_1193.x = 200;
         var_1193.y = 200;
         method_866();
         var _loc1_:int = 0;
         var _loc2_:int = var_1197;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            method_868();
         }
      }
      
      public function method_868() : void
      {
         var _loc1_:MovieClip = var_232.method_84(class_899.__unprotect__("g\'+\x17\x03"),1);
         _loc1_.gotoAndStop(1);
         var _loc2_:int = int(class_691.method_114(50));
         var _loc3_:Number = Math.random() * 6.28;
         var _loc4_:Number = Math.random() * 6.28;
         var _loc5_:int = 150 + int(class_691.method_114(100));
         var _loc6_:Number = Number(0.5 + 1.5 * var_237[0]);
         _loc1_.x = Number(200 + Math.cos(_loc3_) * _loc5_);
         _loc1_.y = Number(200 + Math.sin(_loc3_) * _loc5_);
         _loc1_.var_354 = Math.cos(_loc4_) * _loc6_;
         _loc1_.var_353 = Math.sin(_loc4_) * _loc6_;
         _loc1_.name_113 = _loc6_ * 2;
         _loc1_.rotation = int(class_691.method_114(360));
         var_1196.push(_loc1_);
      }
   }
}
