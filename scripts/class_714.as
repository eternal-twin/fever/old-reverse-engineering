package
{
   import flash.display.DisplayObject;
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_11.class_755;
   import package_32.class_899;
   
   public class class_714 extends class_645
   {
       
      
      public var var_341:int;
      
      public var var_340:int;
      
      public var var_307:Number;
      
      public var var_635:Number;
      
      public var var_66:MovieClip;
      
      public var var_641:Number;
      
      public var var_642:Number;
      
      public var var_634:MovieClip;
      
      public var var_643:MovieClip;
      
      public var var_640:Array;
      
      public var var_637:Array;
      
      public var var_639:Array;
      
      public var var_496:Array;
      
      public var var_636:MovieClip;
      
      public var var_265:Array;
      
      public var var_638:int;
      
      public var var_633:Object;
      
      public var class_318:Object;
      
      public function class_714()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
      }
      
      public function method_396(param1:int, param2:int, param3:int) : void
      {
         var _loc5_:class_17 = null;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc4_:* = var_265[param3];
         do
         {
            param1 = param1 + int(_loc4_.var_244);
            param2 = param2 + int(_loc4_.var_245);
         }
         while(method_393(param1,param2,0) && var_496[param1][param2] == null);
         
         if(param1 == int(var_633.var_244) && param2 == int(var_633.var_245))
         {
            _loc5_ = var_634;
            if(_loc5_.var_1 != null)
            {
               _loc5_.var_1.play();
               method_81(true,20);
            }
         }
         else if(!method_393(param1,param2,0))
         {
            param1 = param1 + int(_loc4_.var_244) * 4;
            param2 = param2 + int(_loc4_.var_245) * 4;
         }
         var _loc6_:Number = Number(method_394(Number(param1 + 0.5)));
         var _loc7_:Number = method_395(Number(param2 + 0.5)) - var_635;
         var _loc8_:MovieClip = var_66;
         var _loc9_:int = param3 * 90 + 90;
         var _loc10_:int = 20;
         var _loc11_:int = 20;
         var _loc12_:Boolean = false;
         if(param3 == 0)
         {
            _loc8_ = var_636;
         }
         var _loc13_:* = null;
         if(method_393(param1,param2,0))
         {
            _loc13_ = var_496[param1][param2];
         }
         var _loc15_:* = null;
         if(_loc13_ != null && int(_loc13_.var_215) < 4)
         {
            _loc15_ = var_637[int(_loc13_.var_215)][param3];
            if(_loc15_ != null)
            {
               _loc16_ = int((int(_loc15_ + 2)) % 4);
               _loc17_ = _loc16_ - param3;
               if(_loc17_ > 2)
               {
                  _loc17_ = _loc17_ - 4;
               }
               if(_loc17_ < -2)
               {
                  _loc17_ = _loc17_ + 4;
               }
               _loc9_ = _loc9_ + _loc17_ * 45;
               _loc10_ = 10;
               _loc11_ = 50;
               _loc12_ = true;
               if(_loc16_ == 0)
               {
                  _loc8_ = var_636;
               }
            }
         }
         if(!_loc12_)
         {
            _loc6_ = _loc6_ - int(_loc4_.var_244) * var_307 * 0.5;
            _loc7_ = _loc7_ - int(_loc4_.var_245) * var_307 * 0.5;
         }
         var_66.graphics.lineTo(_loc6_,_loc7_);
         _loc17_ = var_638;
         var_638 = var_638 + 1;
         _loc16_ = _loc17_;
         var _loc18_:MovieClip = new class_755(_loc8_).method_84(class_899.__unprotect__("\x03E\x1eG\x03"),_loc16_);
         _loc18_.x = _loc6_;
         _loc18_.y = _loc7_;
         _loc18_.scaleX = _loc10_ * 0.01;
         _loc18_.scaleY = _loc11_ * 0.01;
         _loc18_.rotation = _loc9_;
         var_639.push(_loc18_);
         if(_loc15_ != null && !(param1 == int(class_318.var_244) && param2 == int(class_318.var_245)))
         {
            method_396(param1,param2,_loc15_);
         }
      }
      
      public function method_397() : void
      {
         var _loc1_:MovieClip = null;
         var_638 = 0;
         var_66.graphics.clear();
         while(int(var_639.length) > 0)
         {
            _loc1_ = var_639.pop();
            _loc1_.parent.removeChild(_loc1_);
         }
         var_66.graphics.lineStyle(4,16777215,50);
         var_66.graphics.moveTo(Number(method_394(Number(int(class_318.var_244) + 0.5))),method_395(Number(int(class_318.var_245) + 0.5)) - var_635);
         method_396(int(class_318.var_244),int(class_318.var_245),int(class_318.var_215));
      }
      
      public function method_399() : void
      {
         var _loc3_:class_713 = null;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:* = null;
         var _loc1_:int = 0;
         var _loc2_:Array = var_640;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            _loc3_.var_215 = int(class_691.method_114(4));
         }
         _loc2_ = method_398();
         _loc1_ = int(Math.ceil(int(_loc2_.length) * 0.2 * var_237[0]));
         var _loc4_:int = 0;
         while(_loc4_ < _loc1_)
         {
            _loc4_++;
            _loc5_ = _loc4_;
            _loc6_ = int(class_691.method_114(int(_loc2_.length)));
            _loc7_ = _loc2_[_loc6_];
            _loc2_.splice(_loc6_,1);
            _loc3_ = new class_713(int(_loc7_.var_244),int(_loc7_.var_245),int(class_691.method_114(4)));
            var_640.push(_loc3_);
         }
      }
      
      public function method_400(param1:Object) : void
      {
         param1.var_215 = int((int(param1.var_215) + 1) % 4);
         var _loc2_:MovieClip = param1;
         _loc2_.gotoAndStop(int(param1.var_215) + 1);
         method_397();
      }
      
      public function Ran3(param1:Object) : void
      {
         var var_631:Object = param1;
         var var_214:class_714 = this;
         var _loc2_:MovieClip = var_631;
         _loc2_.addEventListener(MouseEvent.MOUSE_DOWN,function(param1:*):void
         {
            var_214.method_400(var_631);
         });
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [500];
         super.method_95(param1);
         var_637 = [[null,null,1,0],[1,null,null,2],[3,2,null,null],[null,0,3,null]];
         var_265 = [{
            "L\x01":0,
            "\x03\x01":-1
         },{
            "L\x01":1,
            "\x03\x01":0
         },{
            "L\x01":0,
            "\x03\x01":1
         },{
            "L\x01":-1,
            "\x03\x01":0
         }];
         var_340 = 8;
         var_341 = 8;
         var_307 = 22;
         var_635 = 15 * var_307 / 100;
         var_642 = (class_742.var_217 - var_340 * var_307) * 0.5;
         var_641 = (class_742.var_219 - var_341 * var_307) * 0.5;
         var_639 = [];
         method_225();
         method_399();
         method_117();
         method_397();
         method_72();
      }
      
      public function method_393(param1:int, param2:int, param3:int) : Boolean
      {
         return param1 >= param3 && param1 < var_340 - param3 && param2 >= param3 && param2 < var_341 - param3;
      }
      
      public function method_395(param1:Number) : Number
      {
         return Number(var_641 + param1 * var_307);
      }
      
      public function method_394(param1:Number) : Number
      {
         return Number(var_642 + param1 * var_307);
      }
      
      public function method_402(param1:int, param2:int) : Boolean
      {
         var _loc7_:* = null;
         var _loc8_:Boolean = false;
         var _loc9_:Boolean = false;
         var _loc10_:int = 0;
         var _loc11_:Array = null;
         var _loc12_:class_713 = null;
         var _loc13_:int = 0;
         var _loc14_:Array = null;
         var _loc15_:* = null;
         var _loc16_:int = 0;
         var _loc17_:int = 0;
         var _loc18_:int = 0;
         var _loc19_:int = 0;
         var _loc3_:class_713 = var_640[int(var_640.length) - 1];
         var _loc4_:Array = [];
         var _loc5_:int = 0;
         var _loc6_:* = var_265[param1];
         do
         {
            _loc5_++;
            _loc7_ = {
               "L\x01":_loc3_.var_244 + _loc5_ * int(_loc6_.var_244),
               "\x03\x01":_loc3_.var_245 + _loc5_ * int(_loc6_.var_245)
            };
            _loc8_ = true;
            _loc9_ = int(_loc7_.var_244) >= var_340 || int(_loc7_.var_245) >= var_341 || int(_loc7_.var_244) < 0 || int(_loc7_.var_245) < 0;
            _loc10_ = 0;
            _loc11_ = var_640;
            while(_loc10_ < int(_loc11_.length))
            {
               _loc12_ = _loc11_[_loc10_];
               _loc10_++;
               if(_loc12_.var_244 == int(_loc7_.var_244) && _loc12_.var_245 == int(_loc7_.var_245))
               {
                  _loc9_ = true;
                  break;
               }
               _loc13_ = 0;
               _loc14_ = _loc12_.var_632;
               while(_loc13_ < int(_loc14_.length))
               {
                  _loc15_ = _loc14_[_loc13_];
                  _loc13_++;
                  if(int(_loc15_.var_244) == int(_loc7_.var_244) && int(_loc15_.var_245) == int(_loc7_.var_245))
                  {
                     _loc8_ = false;
                     break;
                  }
               }
            }
            if(_loc9_)
            {
               break;
            }
            if(_loc8_)
            {
               _loc4_.push(_loc7_);
            }
         }
         while(_loc5_ <= 10);
         
         if(int(_loc4_.length) == 0)
         {
            return false;
         }
         _loc11_ = [];
         _loc10_ = 0;
         while(_loc10_ < int(_loc4_.length))
         {
            _loc7_ = _loc4_[_loc10_];
            _loc10_++;
            _loc11_.push({
               "L\x01":int(_loc7_.var_244),
               "\x03\x01":int(_loc7_.var_245)
            });
         }
         _loc10_ = 0;
         while(int(_loc11_.length) > 1)
         {
            _loc13_ = 1 + int(class_691.method_114(int(_loc11_.length) - 1));
            _loc14_ = [];
            _loc7_ = _loc11_[_loc13_];
            _loc16_ = 0;
            do
            {
               _loc14_.push(_loc4_[_loc16_]);
               _loc16_++;
            }
            while(!(int(_loc4_[_loc16_].var_244) == int(_loc7_.var_244) && int(_loc4_[_loc16_].var_245) == int(_loc7_.var_245)));
            
            _loc11_.splice(_loc13_,1);
            _loc17_ = int(class_691.method_114(2));
            _loc18_ = 0;
            while(_loc18_ < 2)
            {
               _loc18_++;
               _loc19_ = _loc18_;
               if(_loc19_ == 1)
               {
                  _loc17_ = 1 - _loc17_;
               }
               _loc12_ = new class_713(int(_loc7_.var_244),int(_loc7_.var_245),int(method_401(param1 + _loc17_ + 1)));
               _loc12_.var_632 = _loc14_;
               var_640.push(_loc12_);
               if(int(var_640.length) == param2)
               {
                  _loc12_.var_215 = int(method_401(param1 + 2));
                  return true;
               }
               if(method_402(int(method_401(param1 - (_loc17_ * 2 - 1))),param2))
               {
                  return true;
               }
               var_640.pop();
            }
            _loc10_++;
            if(_loc10_ > 10)
            {
               return false;
            }
         }
         return false;
      }
      
      public function method_401(param1:int) : int
      {
         while(param1 < 0)
         {
            param1 = param1 + 4;
         }
         while(param1 >= 4)
         {
            param1 = param1 - 4;
         }
         return param1;
      }
      
      public function method_398() : Array
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc9_:class_713 = null;
         var _loc10_:Array = null;
         var _loc11_:* = null;
         var _loc1_:Array = [];
         var _loc2_:int = 0;
         _loc3_ = var_340;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc1_[_loc4_] = [];
            _loc5_ = 0;
            _loc6_ = var_341;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc1_[_loc4_][_loc7_] = true;
            }
         }
         _loc2_ = 0;
         var _loc8_:Array = var_640;
         while(_loc2_ < int(_loc8_.length))
         {
            _loc9_ = _loc8_[_loc2_];
            _loc2_++;
            _loc1_[_loc9_.var_244][_loc9_.var_245] = false;
            _loc3_ = 0;
            _loc10_ = _loc9_.var_632;
            while(_loc3_ < int(_loc10_.length))
            {
               _loc11_ = _loc10_[_loc3_];
               _loc3_++;
               _loc1_[int(_loc11_.var_244)][int(_loc11_.var_245)] = false;
            }
         }
         _loc1_[int(class_318.var_244)][int(class_318.var_245)] = false;
         _loc8_ = [];
         _loc2_ = 0;
         _loc3_ = var_340;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = var_341;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               if(_loc1_[_loc4_][_loc7_])
               {
                  _loc8_.push({
                     "L\x01":_loc4_,
                     "\x03\x01":_loc7_
                  });
               }
            }
         }
         return _loc8_;
      }
      
      public function method_403(param1:Number, param2:Number) : MovieClip
      {
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__(",@k6\x02"),class_645.var_209);
         _loc3_.scaleX = var_307 * 0.01;
         _loc3_.scaleY = var_307 * 0.01;
         _loc3_.x = Number(method_394(param1));
         _loc3_.y = Number(method_395(param2));
         return _loc3_;
      }
      
      public function method_225() : void
      {
         var _loc10_:int = 0;
         var _loc1_:int = int(class_691.method_114(4));
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         switch(_loc1_)
         {
            case 0:
               _loc2_ = int(class_691.method_114(var_340));
               _loc3_ = var_341;
               break;
            case 1:
               _loc2_ = -1;
               _loc3_ = int(class_691.method_114(var_341));
               break;
            case 2:
               _loc2_ = int(class_691.method_114(var_340));
               _loc3_ = -1;
               break;
            case 3:
               _loc2_ = var_340;
               _loc3_ = int(class_691.method_114(var_341));
         }
         var _loc4_:class_713 = new class_713(_loc2_,_loc3_);
         var_640 = [_loc4_];
         method_402(_loc1_,int(Number(5 + var_237[0] * 10)));
         var _loc5_:class_713 = var_640.pop();
         class_318 = {
            "L\x01":_loc5_.var_244,
            "\x03\x01":_loc5_.var_245,
            "\x1d\x0b\x01":_loc5_.var_215
         };
         var _loc6_:class_713 = var_640.shift();
         var_633 = {
            "L\x01":_loc6_.var_244,
            "\x03\x01":_loc6_.var_245,
            "\x1d\x0b\x01":_loc6_.var_215
         };
         var _loc7_:class_713 = var_640[0];
         var _loc8_:int = 0;
         var _loc9_:int = int(_loc5_.var_632.length);
         while(_loc8_ < _loc9_)
         {
            _loc8_++;
            _loc10_ = _loc8_;
            _loc7_.var_632.push(_loc5_.var_632[_loc10_]);
         }
         _loc8_ = 0;
         _loc9_ = int(_loc6_.var_632.length);
         while(_loc8_ < _loc9_)
         {
            _loc8_++;
            _loc10_ = _loc8_;
            _loc7_.var_632.push(_loc6_.var_632[_loc10_]);
         }
      }
      
      public function method_117() : void
      {
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:MovieClip = null;
         var _loc9_:class_713 = null;
         var _loc10_:* = null;
         var _loc11_:int = 0;
         var _loc12_:DisplayObject = null;
         class_319 = var_232.method_84(class_899.__unprotect__(";\x0f8\x1d\x03"),0);
         var _loc1_:int = 0;
         var _loc2_:int = var_340;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = 0;
            _loc5_ = var_341;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               _loc7_ = var_232.method_84(class_899.__unprotect__("xR;t"),class_645.var_209);
               _loc7_.x = Number(method_394(_loc3_));
               _loc7_.y = Number(method_395(_loc6_));
               _loc7_.scaleX = var_307 * 0.01;
               _loc7_.scaleY = var_307 * 0.01;
               _loc7_.gotoAndStop(int(class_691.method_114(_loc7_.totalFrames)) + 1);
            }
         }
         var_66 = var_232.method_92(class_645.var_209);
         var_496 = [];
         _loc1_ = 0;
         _loc2_ = var_340;
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            var_496[_loc3_] = [];
            _loc4_ = 0;
            _loc5_ = var_341;
            while(_loc4_ < _loc5_)
            {
               _loc4_++;
               _loc6_ = _loc4_;
               var_496[_loc3_][_loc6_] = null;
            }
         }
         _loc1_ = 0;
         _loc2_ = 0;
         var _loc8_:Array = var_640;
         while(_loc2_ < int(_loc8_.length))
         {
            _loc9_ = _loc8_[_loc2_];
            _loc2_++;
            _loc10_ = method_403(_loc9_.var_244,_loc9_.var_245);
            _loc7_ = _loc10_;
            _loc7_.gotoAndStop(_loc9_.var_215 + 1);
            _loc10_.var_215 = _loc9_.var_215;
            Ran3(_loc10_);
            var_640[_loc1_].var_631 = _loc10_;
            var_496[_loc9_.var_244][_loc9_.var_245] = _loc10_;
            _loc1_++;
         }
         var_643 = method_403(int(class_318.var_244),int(class_318.var_245));
         var_643.gotoAndStop("5");
         var_496[int(class_318.var_244)][int(class_318.var_245)] = var_643;
         var_634 = method_403(int(var_633.var_244),int(var_633.var_245));
         var_634.gotoAndStop("6");
         _loc2_ = 0;
         _loc3_ = var_340;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = 0;
            _loc6_ = var_341;
            while(_loc5_ < _loc6_)
            {
               _loc5_++;
               _loc11_ = _loc5_;
               _loc12_ = var_496[_loc4_][_loc11_];
               if(_loc12_ != null)
               {
                  var_232.method_110(_loc12_);
               }
            }
         }
         var_636 = var_232.method_92(class_645.var_209);
      }
   }
}
