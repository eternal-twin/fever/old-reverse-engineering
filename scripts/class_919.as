package
{
   import flash.display.MovieClip;
   import flash.geom.Point;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_919 extends class_645
   {
      
      public static var var_493:int = 12;
      
      public static var var_625:Number = 60;
      
      public static var var_1505:Array = [{
         "L\x01":11,
         "\x03\x01":213
      },{
         "L\x01":24,
         "\x03\x01":210
      },{
         "L\x01":97,
         "\x03\x01":67
      }];
       
      
      public var var_1134:int;
      
      public var var_1506:Boolean;
      
      public var var_796:int;
      
      public var var_38:MovieClip;
      
      public var var_640:Array;
      
      public var var_101:MovieClip;
      
      public function class_919()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:* = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:Number = NaN;
         var _loc9_:Number = NaN;
         var _loc10_:Boolean = false;
         var _loc11_:* = null;
         var _loc12_:Array = null;
         var _loc13_:class_892 = null;
         var _loc14_:Number = NaN;
         var _loc15_:Number = NaN;
         var _loc16_:Number = NaN;
         var _loc17_:Number = NaN;
         var _loc18_:int = 0;
         super.method_79();
         if(!var_1506)
         {
            method_1115();
            return;
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               _loc1_ = var_1134;
               var_1134 = var_1134 - 1;
               if(_loc1_ <= 0)
               {
                  _loc2_ = method_100();
                  _loc3_ = var_38.rotation * 0.0174;
                  _loc4_ = _loc2_.var_244 - var_38.x / 0.6;
                  _loc5_ = _loc2_.var_245 - var_38.y / 0.6;
                  _loc6_ = Math.atan2(_loc5_,_loc4_) - var_38.rotation * 0.0174;
                  while(_loc6_ > 3.14)
                  {
                     _loc6_ = _loc6_ - 6.28;
                  }
                  while(_loc6_ < -3.14)
                  {
                     _loc6_ = Number(_loc6_ + 6.28);
                  }
                  _loc3_ = Number(_loc3_ + _loc6_ * 0.2);
                  var_38.rotation = _loc3_ / 0.0174;
                  _loc7_ = Number(var_38.x + Math.cos(_loc3_) * class_919.var_493 * var_796);
                  _loc8_ = Number(var_38.y + Math.sin(_loc3_) * class_919.var_493 * var_796);
                  if(method_1023(_loc7_,_loc8_))
                  {
                     do
                     {
                        _loc9_ = 0.1;
                        _loc3_ = _loc3_ - class_663.method_99(-_loc9_,_loc6_ * 0.01,_loc9_);
                        _loc7_ = Number(var_38.x + Math.cos(_loc3_) * class_919.var_493 * var_796);
                        _loc8_ = Number(var_38.y + Math.sin(_loc3_) * class_919.var_493 * var_796);
                     }
                     while(method_1023(_loc7_,_loc8_));
                     
                     var_38.rotation = _loc3_ / 0.0174;
                     var_796 = var_796 * -1;
                     var_38.x = _loc7_;
                     var_38.y = _loc8_;
                     var_38.var_38.x = 6 * var_796;
                  }
                  _loc10_ = true;
                  _loc11_ = {
                     "L\x01":var_38.x,
                     "\x03\x01":var_38.y
                  };
                  _loc12_ = var_640.method_78();
                  _loc9_ = class_919.var_625;
                  _loc1_ = 0;
                  while(_loc1_ < int(_loc12_.length))
                  {
                     _loc13_ = _loc12_[_loc1_];
                     _loc1_++;
                     _loc14_ = Number(_loc13_.method_115(_loc11_));
                     if(_loc14_ < _loc9_)
                     {
                        _loc15_ = (_loc9_ - _loc14_) / class_919.var_625;
                        _loc16_ = 0.5;
                        _loc17_ = Number(_loc13_.method_231(_loc11_));
                        _loc13_.var_278 = _loc13_.var_278 - Math.cos(_loc17_) * _loc15_ * _loc16_;
                        _loc13_.var_279 = _loc13_.var_279 - Math.sin(_loc17_) * _loc15_ * _loc16_;
                     }
                     _loc18_ = 10;
                     if(_loc13_.var_244 < -_loc18_ || _loc13_.var_244 > class_742.var_217 + _loc18_ || _loc13_.var_245 < -_loc18_ || _loc13_.var_245 > class_742.var_219 + _loc18_)
                     {
                        _loc13_.method_89();
                        var_640.method_116(_loc13_);
                     }
                     if(_loc13_.var_278 == 0 && _loc13_.var_279 == 0)
                     {
                        _loc10_ = false;
                     }
                  }
               }
               if(int(var_640.length) == 0)
               {
                  method_81(true);
               }
               method_82(class_319);
         }
      }
      
      public function method_786(param1:Number, param2:Number) : void
      {
         var _loc3_:class_358 = new class_358();
         _loc3_.x = param1;
         _loc3_.y = param2;
         class_319.addChild(_loc3_);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [600];
         super.method_95(param1);
         var_796 = 1;
         var_1134 = 20;
         method_117();
         if(param1 > 0.9)
         {
            class_919.var_625 = class_919.var_625 * Math.max(1.9 - param1,0.1);
         }
         method_93();
         class_319.visible = false;
         method_82(class_319);
         var_1506 = false;
      }
      
      public function method_1023(param1:Number, param2:Number) : Boolean
      {
         if(var_101 == null)
         {
            return false;
         }
         var _loc3_:Point = new Point(param1,param2);
         _loc3_ = var_101.localToGlobal(new Point(param1,param2));
         return Boolean(var_101.hitTestPoint(_loc3_.x,_loc3_.y,true));
      }
      
      public function method_1115() : void
      {
         var _loc2_:int = 0;
         var _loc3_:MovieClip = null;
         var _loc4_:class_892 = null;
         var_38 = class_319.var_38;
         var_101 = class_319.var_101;
         if(var_101 == null)
         {
            return;
         }
         var_1506 = true;
         var_640 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 4)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            _loc3_ = Reflect.field(class_319,"$m" + _loc2_);
            _loc3_.gotoAndPlay(int(class_691.method_114(5)) + 1);
            _loc3_.alpha = 0.8;
            _loc4_ = new class_892(_loc3_);
            _loc4_.var_244 = _loc3_.x;
            _loc4_.var_245 = _loc3_.y;
            _loc4_.var_233 = 1;
            _loc4_.method_118();
            var_640.push(_loc4_);
         }
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__(";7/\x06\x02"),0);
         var _loc1_:int = int(Math.round(var_237[0] * 3));
         class_319.gotoAndStop(_loc1_ + 1);
      }
   }
}
