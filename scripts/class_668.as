package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.MovieClip;
   import flash.geom.ColorTransform;
   import package_32.class_899;
   
   public class class_668
   {
       
      
      public var var_380:int;
      
      public var _Iz8:Number;
      
      public var var_381:int;
      
      public var var_382:int;
      
      public var var_371:Number;
      
      public var var_366:int;
      
      public var var_390:int;
      
      public var var_389:int;
      
      public var var_384:Number;
      
      public var name_30:int;
      
      public var var_383:BitmapData;
      
      public var var_388:Boolean;
      
      public var var_386:int;
      
      public var var_374:int;
      
      public var var_391:int;
      
      public var var_376:Number;
      
      public var var_385:Array;
      
      public var var_387:Boolean;
      
      public function class_668(param1:MovieClip = undefined, param2:int = 0, param3:int = 0, param4:int = 0, param5:Number = 0.0, param6:int = 0, param7:int = 0, param8:int = 10)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_383 = new BitmapData(param2,param3,true,255);
         param1.addChild(new Bitmap(var_383));
         var_385 = [];
         var_384 = 0;
         var_386 = 1;
         var_390 = param6 == 0?int(Math.round(param2 / 2)):param6;
         var_389 = param7 == 0?int(Math.round(param2 / 6)):param7;
         var_374 = param8;
         var_381 = 0;
         var_380 = 0;
         var_391 = 0;
         var_382 = param4;
         _Iz8 = param5;
         var_371 = param2;
         var_376 = param3;
         name_30 = 0;
         var_366 = 0;
      }
      
      public function method_260(param1:int, param2:int) : void
      {
         var_381 = param1;
         var_380 = param2;
      }
      
      public function method_79(param1:int, param2:int, param3:int, param4:Function) : void
      {
         var _loc17_:* = null;
         var _loc18_:int = 0;
         var _loc19_:int = 0;
         var _loc20_:int = 0;
         var _loc21_:Number = NaN;
         var _loc22_:Number = NaN;
         var _loc23_:Number = NaN;
         var _loc24_:int = 0;
         var _loc25_:int = 0;
         var _loc26_:Number = NaN;
         var _loc27_:Number = NaN;
         var _loc28_:Number = NaN;
         name_30 = param3;
         var_383.scroll(-name_30,0);
         var _loc5_:ColorTransform = new ColorTransform();
         var_384 = Number(var_384 + 5);
         var _loc6_:Number = 0.5 * class_649.name_9(var_384);
         _loc5_.blueMultiplier = Number(0.98 + _loc6_ / 50);
         _loc5_.greenMultiplier = Number(0.98 + _loc6_ / 50);
         _loc5_.redMultiplier = Number(0.98 + _loc6_ / 50);
         _loc5_.alphaOffset = 128;
         var_383.colorTransform(var_383.rect,_loc5_);
         var _loc7_:class_795 = class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return Boolean(param1.name_35);
         });
         var _loc8_:* = _loc7_.name_13();
         var _loc9_:* = _loc7_.name_36();
         var _loc10_:int = var_374;
         var _loc11_:class_795 = class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return !param1.name_35;
         });
         var _loc12_:class_795 = class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return !param1.var_302;
         });
         var _loc13_:int = -2;
         var _loc14_:Boolean = false;
         var _loc15_:int = _loc12_.name_37;
         var _loc16_:* = _loc7_.method_73();
         while(_loc16_.method_74())
         {
            _loc17_ = _loc16_.name_1();
            _loc13_++;
            _loc18_ = 0;
            _loc19_ = 0;
            if(!_loc17_.var_302)
            {
               _loc20_ = int(_loc17_.name_34) == 0?_loc10_ * (int(var_385.length) - _loc12_.name_37):int(_loc17_.name_34) * _loc10_;
               if(Number(Math.abs(int(_loc8_.var_244) - int(_loc17_.var_244))) <= 3 && Number(Math.abs(int(_loc8_.var_245) + _loc20_ - int(_loc17_.var_245))) <= 3)
               {
                  _loc17_.var_302 = true;
                  _loc17_.var_244 = int(_loc8_.var_244);
                  _loc17_.var_245 = int(_loc8_.var_245) + _loc20_;
                  if(int(_loc17_.name_34) == 0)
                  {
                     _loc17_.name_34 = _loc13_ + 1;
                  }
                  _loc17_.var_65 = _loc13_ * 90;
                  if(!_loc17_.name_38)
                  {
                     param4(_loc17_);
                  }
                  else
                  {
                     _loc17_.name_38 = false;
                  }
                  method_268(_loc17_,_loc18_,_loc19_);
               }
               else
               {
                  _loc21_ = var_376 - var_382 - (_loc20_ << 1);
                  if(int(_loc8_.var_245) >= _loc21_)
                  {
                     method_268(_loc17_,_loc18_,_loc19_);
                  }
                  else
                  {
                     _loc22_ = param1 * (int(_loc17_.name_34) + 1) / 2;
                     _loc23_ = param2 * (int(_loc17_.name_34) + 1) / 2;
                     _loc24_ = int(_loc8_.var_245) + _loc20_ - int(_loc17_.var_245);
                     _loc25_ = int(_loc8_.var_244) - int(_loc17_.var_244);
                     _loc26_ = Number(Math.atan2(_loc24_,_loc25_));
                     _loc27_ = Math.cos(_loc26_) * _loc22_;
                     _loc28_ = Math.sin(_loc26_) * _loc23_;
                     method_268(_loc17_,int(Math.ceil(_loc27_)),int(Math.ceil(_loc28_)));
                  }
               }
            }
            else if(_loc17_.name_29)
            {
               method_268(_loc17_,_loc18_,_loc19_);
            }
            else
            {
               if(_loc15_ == 1)
               {
                  if(int(_loc17_.var_245) <= Number(_Iz8 + var_386) && var_380 > 0 || int(_loc17_.var_245) >= var_376 - _Iz8 - var_386 && var_380 < 0 || int(_loc17_.var_245) < var_376 - _Iz8 - var_386 && int(_loc17_.var_245) > _Iz8)
                  {
                     _loc19_ = var_380;
                     var_387 = false;
                  }
                  else
                  {
                     var_387 = true;
                  }
               }
               else if(int(_loc8_.var_245) <= _Iz8 - var_386 && var_380 > 0 || int(_loc17_.name_34) == int(_loc8_.name_34) && int(_loc17_.var_245) <= _Iz8 - var_386 && var_380 > 0 || int(_loc17_.var_245) >= var_376 - _Iz8 - var_386 && var_380 < 0 || int(_loc8_.var_245) >= var_376 - _Iz8 - var_386 && var_380 < 0 || int(_loc9_.var_245) >= var_376 - _Iz8 - var_386 && var_380 < 0 || int(_loc9_.var_245) <= var_376 - _Iz8 - var_386 && int(_loc8_.var_245) >= _Iz8 && int(_loc17_.var_245) >= _Iz8 || _loc14_)
               {
                  if(int(_loc17_.name_34) == int(_loc8_.name_34))
                  {
                     _loc14_ = true;
                  }
                  _loc19_ = var_380;
                  var_387 = false;
               }
               else
               {
                  if(int(_loc17_.name_34) == int(_loc8_.name_34))
                  {
                     _loc14_ = false;
                  }
                  _loc19_ = 0;
                  var_387 = true;
               }
               if(int(_loc17_.var_244) <= var_382 && var_381 > 0 || int(_loc17_.var_244) >= var_376 - var_382 && var_381 < 0 || int(_loc17_.var_244) > var_382 && int(_loc17_.var_244) < var_376 - var_382)
               {
                  _loc18_ = var_381;
               }
               if(int(_loc17_.var_369) != int(_loc8_.var_369))
               {
                  if(var_388)
                  {
                     _loc19_ = int(Math.ceil(class_649.name_9(Number(_loc17_.var_65)) * 5)) + int(_loc8_.var_245) - int(_loc17_.var_245);
                     _loc17_.var_65 = Number(Number(_loc17_.var_65) + 45);
                     _loc18_ = int(_loc8_.var_244) - int(_loc17_.var_244) - var_381;
                     _loc17_.name_38 = true;
                  }
                  else if(_loc17_.name_38)
                  {
                     _loc17_.var_302 = false;
                  }
               }
               method_268(_loc17_,_loc18_,_loc19_);
            }
         }
         _loc16_ = _loc11_.method_73();
         while(_loc16_.method_74())
         {
            _loc17_ = _loc16_.name_1();
            if(int(_loc17_.var_244) < var_389)
            {
               method_268(_loc17_,param1,0);
            }
            else
            {
               _loc17_.name_35 = true;
               method_268(_loc17_,0,0);
            }
         }
      }
      
      public function method_116(param1:int) : void
      {
         var _loc4_:* = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_385;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(int(_loc4_.var_366) == param1)
            {
               var_385.method_116(_loc4_);
            }
         }
      }
      
      public function method_268(param1:Object, param2:int, param3:int) : void
      {
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc4_:int = int(param1.var_244);
         var _loc5_:int = int(param1.var_245);
         var _loc6_:int = Boolean(param1.var_302)?int(param1.name_39):1619560584;
         if(param3 == 0)
         {
            _loc7_ = 0;
            _loc8_ = name_30 + param2;
            while(_loc7_ < _loc8_)
            {
               _loc7_++;
               _loc9_ = _loc7_;
               var_383.setPixel32(_loc4_ + _loc9_,_loc5_,_loc6_);
            }
            param1.var_244 = int(param1.var_244) + param2;
            var_391 = 0;
            return;
         }
         _loc7_ = _loc4_ + name_30;
         _loc8_ = _loc5_ + param3;
         var_383.setPixel32(_loc4_,_loc5_,_loc6_);
         class_649.method_150(_loc4_,_loc5_,_loc7_,_loc8_,_loc6_,var_383);
         if(var_391 <= 0)
         {
            if(param3 > 0)
            {
               var_383.setPixel32(_loc4_ + 2,_loc5_ + 1,_loc6_);
               var_383.setPixel32(_loc4_ + 2,_loc5_ + 1,_loc6_);
               var_383.setPixel32(_loc4_ + 3,_loc5_ + 1,_loc6_);
               var_383.setPixel32(_loc4_ + 2,_loc5_ + 2,1610612736);
            }
         }
         param1.var_245 = int(param1.var_245) + param3;
         var_391 = var_391 + param3;
      }
      
      public function method_269() : class_795
      {
         return class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return Boolean(param1.name_35);
         });
      }
      
      public function method_270() : class_795
      {
         return class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return Boolean(param1.var_302);
         });
      }
      
      public function method_271() : int
      {
         return int(var_385.length);
      }
      
      public function method_259() : Object
      {
         return var_385[0];
      }
      
      public function method_265(param1:int = 16777215) : void
      {
         var _loc2_:* = {};
         var _loc3_:class_795 = class_934.method_267(var_385,function(param1:Object):Boolean
         {
            return !param1.var_302;
         });
         _loc2_.var_245 = var_390 + _loc3_.name_37 * 10;
         _loc2_.var_244 = 0;
         _loc2_.var_65 = 0;
         _loc2_.name_34 = 0;
         var _loc4_:int = var_366;
         var_366 = var_366 + 1;
         _loc2_.var_366 = _loc4_;
         _loc4_ = -1;
         var_383.setPixel32(0,var_390,_loc4_);
         _loc2_.name_39 = _loc4_;
         _loc2_.var_369 = param1;
         var_385.push(_loc2_);
      }
   }
}
