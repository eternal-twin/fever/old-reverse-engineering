package package_41.package_42
{
   import flash.display.DisplayObjectContainer;
   import flash.display.FrameLabel;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.EventDispatcher;
   import flash.events.MouseEvent;
   
   public class qH1g
   {
       
      
      public function qH1g()
      {
      }
      
      public static function method_855(param1:EventDispatcher, param2:Function, param3:String) : Object
      {
         var var_1189:Function = param2;
         var _loc4_:Function = function(param1:*):void
         {
            var_1189();
         };
         param1.addEventListener(param3,_loc4_);
         return {
            "}\x11\x01":_loc4_,
            "cHX@\x01":param3
         };
      }
      
      public static function method_856(param1:EventDispatcher, param2:Object) : void
      {
         param1.removeEventListener(param2.var_252,param2.var_1189);
      }
      
      public static function method_83(param1:EventDispatcher, param2:Function) : Object
      {
         return qH1g.method_855(param1,param2,MouseEvent.CLICK);
      }
      
      public static function method_857(param1:EventDispatcher, param2:Function) : Object
      {
         return qH1g.method_855(param1,param2,MouseEvent.MOUSE_UP);
      }
      
      public static function method_406(param1:EventDispatcher, param2:Function) : Object
      {
         return qH1g.method_855(param1,param2,MouseEvent.MOUSE_DOWN);
      }
      
      public static function method_858(param1:EventDispatcher, param2:Function) : Object
      {
         return qH1g.method_855(param1,param2,MouseEvent.MOUSE_OVER);
      }
      
      public static function method_859(param1:EventDispatcher, param2:Function) : Object
      {
         return qH1g.method_855(param1,param2,MouseEvent.MOUSE_OUT);
      }
      
      public static function method_860(param1:EventDispatcher, param2:Function, param3:Number = 1.0) : Object
      {
         var var_1189:Function = param2;
         var var_1190:Number = param3;
         var _loc4_:Function = function(param1:MouseEvent):void
         {
            var_1189(param1.delta * var_1190);
         };
         param1.addEventListener(MouseEvent.MOUSE_WHEEL,_loc4_);
         return {
            "}\x11\x01":_loc4_,
            "cHX@\x01":MouseEvent.MOUSE_WHEEL
         };
      }
      
      public static function method_116(param1:DisplayObjectContainer) : void
      {
         if(param1 == null || param1.parent == null)
         {
            return;
         }
         param1.parent.removeChild(param1);
      }
      
      public static function method_861(param1:DisplayObjectContainer) : void
      {
         param1.mouseEnabled = false;
         param1.mouseChildren = false;
      }
      
      public static function method_862(param1:DisplayObjectContainer) : void
      {
         param1.mouseEnabled = true;
         param1.mouseChildren = true;
      }
      
      public static function method_439(param1:Sprite, param2:Boolean) : void
      {
         param1.useHandCursor = param2;
         param1.buttonMode = param2;
         if(param2)
         {
            qH1g.method_862(param1);
         }
         else
         {
            qH1g.method_861(param1);
         }
      }
      
      public static function method_863(param1:MovieClip, param2:String) : Boolean
      {
         var _loc5_:FrameLabel = null;
         var _loc3_:int = 0;
         var _loc4_:Array = param1.currentLabels;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.name == param2)
            {
               return true;
            }
         }
         return false;
      }
   }
}
