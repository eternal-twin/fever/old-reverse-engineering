package
{
   import flash.display.Bitmap;
   import flash.display.BitmapData;
   import flash.display.DisplayObjectContainer;
   import flash.display.MovieClip;
   import flash.display.Sprite;
   import flash.events.MouseEvent;
   import flash.geom.Matrix;
   import package_10.class_821;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_13.class_765;
   import package_17.class_800;
   import package_17.class_833;
   import package_32.App;
   import package_32.class_899;
   import package_8.package_9.class_931;
   
   public class class_645 extends Sprite
   {
      
      public static var var_207:int = 0;
      
      public static var var_208:int = 1;
      
      public static var var_209:int = 2;
      
      public static var var_210:int = 3;
      
      public static var var_211:int = 400;
      
      public static var var_212:int = 400;
      
      public static var var_213:int = 100;
      
      public static var var_214:class_645;
       
      
      public var package_13:package_17.class_652;
      
      public var var_220:Object;
      
      public var var_229:Array;
      
      public var name_3:int;
      
      public var var_225:Number;
      
      public var var_224:Object;
      
      public var var_109:MovieClip;
      
      public var var_231:class_720;
      
      public var var_221:class_795;
      
      public var name_4:Function;
      
      public var var_215:int;
      
      public var var_238:Array;
      
      public var var_227:Array;
      
      public var var_228:Object;
      
      public var name_2:Function;
      
      public var var_232:class_755;
      
      public var var_237:Array;
      
      public var name_5:Boolean;
      
      public var var_201:Sprite;
      
      public var class_319:MovieClip;
      
      public var var_234:Boolean;
      
      public function class_645()
      {
         if(class_899.var_239)
         {
            return;
         }
         class_645.var_214 = this;
         super();
         var_234 = false;
         name_5 = false;
         var_237 = [0];
         name_3 = 0;
         var_227 = [20000];
         var_238 = [200];
         var_229 = [false];
         var_201 = new Sprite();
         var_232 = new class_755(var_201);
         addChild(var_201);
         method_94();
      }
      
      public static function method_70(param1:int) : void
      {
      }
      
      public static function method_71(param1:int) : class_645
      {
         var _loc2_:class_645 = null;
         switch(param1)
         {
            case 0:
               _loc2_ = new class_896();
               break;
            case 1:
               _loc2_ = new class_792();
               break;
            case 2:
               _loc2_ = new class_848();
               break;
            case 3:
               _loc2_ = new class_903();
               break;
            case 4:
               _loc2_ = new class_674();
               break;
            case 5:
               _loc2_ = new class_714();
               break;
            case 6:
               _loc2_ = new class_679();
               break;
            case 7:
               _loc2_ = new class_753();
               break;
            case 8:
               _loc2_ = new class_655();
               break;
            case 9:
               _loc2_ = new class_890();
               break;
            case 10:
               _loc2_ = new class_884();
               break;
            case 11:
               _loc2_ = new class_726();
               break;
            case 12:
               _loc2_ = new class_804();
               break;
            case 13:
               _loc2_ = new class_682();
               break;
            case 14:
               _loc2_ = new class_703();
               break;
            case 15:
               _loc2_ = new class_767();
               break;
            case 16:
               _loc2_ = new class_764();
               break;
            case 17:
               _loc2_ = new class_797();
               break;
            case 18:
               _loc2_ = new class_785();
               break;
            case 19:
               _loc2_ = new class_784();
               break;
            case 20:
               _loc2_ = new class_681();
               break;
            case 21:
               _loc2_ = new class_937();
               break;
            case 22:
               _loc2_ = new class_712();
               break;
            case 23:
               _loc2_ = new class_832();
               break;
            case 24:
               _loc2_ = new class_748();
               break;
            case 25:
               _loc2_ = new class_708();
               break;
            case 26:
               _loc2_ = new class_888();
               break;
            case 27:
               _loc2_ = new class_775();
               break;
            case 28:
               _loc2_ = new class_824();
               break;
            case 29:
               _loc2_ = new class_707();
               break;
            case 30:
               _loc2_ = new class_843();
               break;
            case 31:
               _loc2_ = new class_745();
               break;
            case 32:
               _loc2_ = new class_727();
               break;
            case 33:
               _loc2_ = new class_923();
               break;
            case 34:
               _loc2_ = new class_717();
               break;
            case 35:
               _loc2_ = new class_743();
               break;
            case 36:
               _loc2_ = new class_763();
               break;
            case 37:
               _loc2_ = new class_922();
               break;
            case 38:
               _loc2_ = new class_791();
               break;
            case 39:
               _loc2_ = new class_789();
               break;
            case 40:
               _loc2_ = new class_919();
               break;
            case 41:
               _loc2_ = new class_793();
               break;
            case 42:
               _loc2_ = new class_842();
               break;
            case 43:
               _loc2_ = new class_688();
               break;
            case 44:
               _loc2_ = new class_897();
               break;
            case 45:
               _loc2_ = new class_731();
               break;
            case 46:
               _loc2_ = new class_915();
               break;
            case 47:
               _loc2_ = new class_705();
               break;
            case 48:
               _loc2_ = new class_693();
               break;
            case 49:
               _loc2_ = new class_716();
               break;
            case 50:
               _loc2_ = new class_877();
               break;
            case 51:
               _loc2_ = new class_828();
               break;
            case 52:
               _loc2_ = new class_839();
               break;
            case 53:
               _loc2_ = new class_723();
               break;
            case 54:
               _loc2_ = new class_799();
               break;
            case 55:
               _loc2_ = new class_825();
               break;
            case 56:
               _loc2_ = new class_849();
               break;
            case 57:
               _loc2_ = new class_766();
               break;
            case 58:
               _loc2_ = new class_936();
               break;
            case 59:
               _loc2_ = new class_941();
               break;
            case 60:
               _loc2_ = new class_796();
               break;
            case 61:
               _loc2_ = new class_909();
               break;
            case 62:
               _loc2_ = new class_837();
               break;
            case 63:
               _loc2_ = new class_790();
               break;
            case 64:
               _loc2_ = new class_836();
               break;
            case 65:
               _loc2_ = new class_880();
               break;
            case 66:
               _loc2_ = new class_854();
               break;
            case 67:
               _loc2_ = new class_857();
               break;
            case 68:
               _loc2_ = new class_862();
               break;
            case 69:
               _loc2_ = new class_882();
               break;
            case 70:
               _loc2_ = new class_779();
               break;
            case 71:
               _loc2_ = new class_856();
               break;
            case 72:
               _loc2_ = new class_706();
               break;
            case 73:
               _loc2_ = new class_751();
               break;
            case 74:
               _loc2_ = new class_902();
               break;
            case 75:
               _loc2_ = new class_715();
               break;
            case 76:
               _loc2_ = new class_646();
               break;
            case 77:
               _loc2_ = new class_677();
               break;
            case 78:
               _loc2_ = new class_778();
               break;
            case 79:
               _loc2_ = new DvTv();
               break;
            case 80:
               _loc2_ = new class_933();
               break;
            case 81:
               _loc2_ = new class_692();
               break;
            case 82:
               _loc2_ = new class_845();
               break;
            case 83:
               _loc2_ = new class_729();
               break;
            case 84:
               _loc2_ = new class_831();
               break;
            case 85:
               _loc2_ = new class_810();
               break;
            case 86:
               _loc2_ = new class_665();
               break;
            case 87:
               _loc2_ = new class_841();
               break;
            case 88:
               _loc2_ = new class_762();
               break;
            case 89:
               _loc2_ = new class_834();
               break;
            case 90:
               _loc2_ = new class_850();
               break;
            case 91:
               _loc2_ = new class_864();
               break;
            case 92:
               _loc2_ = new class_664();
               break;
            case 93:
               _loc2_ = new class_930();
               break;
            case 94:
               _loc2_ = new class_914();
               break;
            case 95:
               _loc2_ = new class_881();
               break;
            case 96:
               _loc2_ = new class_866();
               break;
            case 97:
               _loc2_ = new class_929();
               break;
            case 98:
               _loc2_ = new class_787();
               break;
            case 99:
               _loc2_ = new class_721();
         }
         _loc2_.var_215 = param1;
         return _loc2_;
      }
      
      public function method_72() : void
      {
         var_201.scaleX = class_742.var_216 / class_742.var_217;
         var_201.scaleY = class_742.var_218 / class_742.var_219;
      }
      
      public function method_76() : void
      {
         var _loc2_:* = null;
         var _loc3_:class_821 = null;
         var _loc4_:class_833 = null;
         var _loc5_:Array = null;
         var _loc6_:int = 0;
         var _loc7_:class_800 = null;
         var _loc8_:class_689 = null;
         var _loc1_:* = var_221.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            _loc3_ = _loc2_.var_38.var_21.var_222.var_84;
            while(_loc3_ != null)
            {
               _loc4_ = _loc3_.var_223;
               _loc3_ = _loc3_.name_1;
               _loc5_ = [_loc4_.var_14,_loc4_.var_73];
               _loc6_ = 0;
               while(_loc6_ < int(_loc5_.length))
               {
                  _loc7_ = _loc5_[_loc6_];
                  _loc6_++;
                  _loc8_ = method_75(_loc7_.var_215);
                  if(_loc8_ != _loc2_.var_38)
                  {
                     _loc2_.var_27(_loc8_);
                  }
               }
            }
         }
      }
      
      public function method_77() : void
      {
         if(var_224 == null)
         {
            return;
         }
         var_201.y = var_224;
         var_224 = var_224 * -var_225;
         if(Number(Math.abs(var_224)) < 0.2)
         {
            var_201.y = 0;
            var_224 = null;
         }
      }
      
      public function method_79() : void
      {
         var _loc3_:* = null;
         var _loc1_:Array = class_656.var_226.method_78();
         var _loc2_:int = 0;
         while(_loc2_ < int(_loc1_.length))
         {
            _loc3_ = _loc1_[_loc2_];
            _loc2_++;
            _loc3_.method_79();
         }
         Dm6L();
         var _loc4_:Number = var_227[0];
         var_227 = [var_227[0] - 1];
         if(_loc4_ < 0 && var_220 == null)
         {
            method_80();
         }
         if(var_228 != null)
         {
            var_228 = var_228 - 1;
            if(var_228 <= 0)
            {
               if(name_2 != null)
               {
                  name_2();
               }
            }
         }
         method_77();
      }
      
      public function Dm6L() : void
      {
      }
      
      public function method_81(param1:Boolean, param2:int = 7) : void
      {
         if(var_220 != null)
         {
            return;
         }
         if(!param1)
         {
            new class_931(this,0.15,16711680);
         }
         var_220 = param1;
         var_228 = param2;
         if(name_4 != null)
         {
            name_4(param1);
         }
      }
      
      public function method_82(param1:MovieClip, param2:Number = 1.0) : void
      {
         var _loc3_:Matrix = new Matrix();
         _loc3_.scale(param2,param2);
         var_109.var_230.draw(param1,_loc3_);
      }
      
      public function method_80() : void
      {
         if(!var_229[0])
         {
            method_81(false,8);
         }
      }
      
      public function method_83() : void
      {
      }
      
      public function method_85(param1:String) : class_656
      {
         var _loc2_:class_656 = new class_656(var_232.method_84(class_899.__unprotect__(param1),class_645.var_209));
         return _loc2_;
      }
      
      public function method_86(param1:String) : class_892
      {
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__(param1),class_645.var_209));
         _loc2_.var_233 = 0.99;
         return _loc2_;
      }
      
      public function method_87(param1:*) : void
      {
         name_5 = false;
      }
      
      public function method_88(param1:*) : void
      {
         if(!name_5 && var_234)
         {
            method_83();
         }
         name_5 = true;
      }
      
      public function method_89() : void
      {
         App.rootMc.stage.removeEventListener(MouseEvent.MOUSE_DOWN,method_88);
         App.rootMc.stage.removeEventListener(MouseEvent.MOUSE_UP,method_87);
         if(parent != null)
         {
            parent.removeChild(this);
         }
      }
      
      public function method_90(param1:int) : Boolean
      {
         if(class_765.var_214 == null)
         {
            return true;
         }
         return int(class_765.var_214.package_31._god) == param1;
      }
      
      public function method_91() : void
      {
         var_221 = new class_795();
         var_231 = new class_720();
      }
      
      public function method_93(param1:Number = 0.6) : void
      {
         var_109 = var_232.method_92(1);
         var_109.var_236 = param1;
         var_109.var_230 = new BitmapData(int(Math.ceil(class_742.var_216 * param1)),int(Math.ceil(class_742.var_218 * param1)),false,65280);
         var_109.scaleX = 1 / param1;
         var_109.scaleY = 1 / param1;
         var _loc2_:Bitmap = new Bitmap();
         _loc2_.bitmapData = var_109.var_230;
         var_109.addChild(_loc2_);
      }
      
      public function method_94() : void
      {
      }
      
      public function method_95(param1:Number) : void
      {
         var_237 = [param1];
         name_3 = 1;
         App.rootMc.stage.addEventListener(MouseEvent.MOUSE_DOWN,method_88);
         App.rootMc.stage.addEventListener(MouseEvent.MOUSE_UP,method_87);
         var_238 = [var_227[0]];
      }
      
      public function method_96(param1:Sprite) : MovieClip
      {
         var _loc2_:MovieClip = param1.var_1;
         return _loc2_;
      }
      
      public function method_75(param1:int) : class_689
      {
         return var_231.method_98(class_691.method_97(param1));
      }
      
      public function method_100() : Object
      {
         var _loc1_:Sprite = var_201;
         var _loc2_:Number = Number(class_663.method_99(0,_loc1_.mouseX,class_742.var_216));
         var _loc3_:Number = Number(class_663.method_99(0,_loc1_.mouseY,class_742.var_218));
         return {
            "L\x01":_loc2_,
            "\x03\x01":_loc3_
         };
      }
      
      public function method_101(param1:Sprite, param2:String) : MovieClip
      {
         var _loc3_:MovieClip = Reflect.field(param1,class_899.__unprotect__(param2));
         return _loc3_;
      }
      
      public function method_102(param1:Object, param2:Number = 0.5) : void
      {
         var_224 = param1;
         var_225 = param2;
         method_77();
      }
      
      public function method_104(param1:class_689, param2:Function) : void
      {
         var_221.method_103({
            "a\x01":param1,
            "-\x01":param2
         });
      }
   }
}
