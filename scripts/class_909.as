package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import package_32.class_899;
   
   public class class_909 extends class_645
   {
       
      
      public var var_949:Array;
      
      public var var_104:MovieClip;
      
      public var var_577:Array;
      
      public var var_1472:Array;
      
      public var var_752:Array;
      
      public var var_288:class_892;
      
      public function class_909()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:int = 0;
         var _loc3_:int = 0;
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:MovieClip = null;
         var _loc7_:Array = null;
         var _loc8_:* = null;
         var _loc9_:Number = NaN;
         var _loc10_:class_892 = null;
         loop4:
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_288 != null)
               {
                  var_288.method_229(method_100(),0.2,16,class_742.var_218);
                  if(var_288.var_245 < var_104.height)
                  {
                     method_1094();
                  }
               }
               _loc1_ = int(var_752.length) - 1;
               _loc2_ = 4;
               _loc3_ = 0;
               _loc4_ = int(var_1472.length);
               while(_loc3_ < _loc4_)
               {
                  _loc3_++;
                  _loc5_ = _loc3_;
                  _loc6_ = var_1472[_loc5_];
                  _loc6_.blendMode = BlendMode.LAYER;
                  _loc6_.x = Number(var_752[_loc1_ - _loc5_ * _loc2_].var_244);
                  _loc6_.y = Number(var_752[_loc1_ - _loc5_ * _loc2_].var_245);
               }
               if(var_288 != null)
               {
                  var_752.push({
                     "L\x01":var_288.var_244,
                     "\x03\x01":var_288.var_245
                  });
               }
               if(var_220 == null)
               {
                  _loc3_ = 0;
                  _loc7_ = var_949;
                  while(_loc3_ < int(_loc7_.length))
                  {
                     _loc8_ = _loc7_[_loc3_];
                     _loc3_++;
                     _loc9_ = Number(_loc8_.var_68);
                     _loc8_.var_68 = Number(_loc8_.var_68) - 1;
                     if(_loc9_ < 0)
                     {
                        method_1095(_loc8_);
                     }
                  }
               }
               _loc7_ = var_577.method_78();
               _loc3_ = 0;
               while(_loc3_ < int(_loc7_.length))
               {
                  _loc10_ = _loc7_[_loc3_];
                  _loc3_++;
                  _loc4_ = 10;
                  if(_loc10_.var_244 < -_loc4_ || _loc10_.var_244 > class_742.var_217 + _loc4_ || _loc10_.var_245 < -_loc4_ || _loc10_.var_245 > class_742.var_219 + _loc4_)
                  {
                     _loc10_.method_89();
                     var_577.method_116(_loc10_);
                  }
                  _loc5_ = 8;
                  if(var_288 != null && Number(Math.abs(var_288.var_244 - _loc10_.var_244)) < _loc5_ && Number(Math.abs(var_288.var_245 - _loc10_.var_245)) < _loc5_)
                  {
                     var_577.method_116(_loc10_);
                     _loc10_.method_89();
                     method_1094();
                  }
               }
               if(var_220)
               {
                  _loc3_ = 0;
                  while(true)
                  {
                     if(_loc3_ >= 1)
                     {
                        break loop4;
                     }
                     _loc3_++;
                     _loc4_ = _loc3_;
                     _loc9_ = Number(Math.random());
                     _loc10_ = new class_892(var_232.method_84(class_899.__unprotect__("\x07bay\x03"),class_645.var_210));
                     _loc10_.method_119(1 - _loc9_ * 0.5);
                     _loc10_.var_244 = Math.random() * class_742.var_217;
                     _loc10_.var_245 = Math.random() * var_104.height;
                     _loc10_.var_243.rotation = Math.random() * 360;
                     _loc10_.var_580 = (Math.random() * 2 - 1) * 16;
                     _loc10_.var_687 = 0.92;
                     _loc10_.var_250 = -Math.random() * 0.35 * _loc9_;
                     _loc10_.method_119(Number(1 + Number(Math.random())));
                     _loc10_.var_251 = 40;
                  }
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_1095(param1:Object) : void
      {
         var _loc3_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("v\x1e?X\x02"),class_645.var_209));
         _loc3_.var_233 = 0.99;
         var _loc2_:class_892 = _loc3_;
         var _loc5_:Number = Number(0.4 + Math.random() * (3.14 - 2 * 0.4));
         _loc2_.var_244 = param1.var_631.x;
         _loc2_.var_245 = param1.var_631.y;
         _loc2_.var_278 = Math.cos(_loc5_) * 3.2;
         _loc2_.var_279 = Math.sin(_loc5_) * 3.2;
         _loc2_.method_118();
         _loc2_.var_233 = 1;
         var_577.push(_loc2_);
         param1.var_631.gotoAndPlay("2");
         param1.var_68 = 14;
      }
      
      override public function method_80() : void
      {
         var _loc1_:class_892 = null;
         method_81(true,30);
         while(int(var_577.length) > 0)
         {
            _loc1_ = var_577.pop();
            _loc1_.method_89();
         }
         method_102(5,1);
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [Number(140 + param1 * 300)];
         super.method_95(param1);
         var_577 = [];
         var_949 = [];
         var_752 = [];
         method_117();
         method_72();
      }
      
      public function method_1094() : void
      {
         var _loc3_:MovieClip = null;
         var _loc2_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("\tNAy\x02"),class_645.var_209));
         _loc2_.var_233 = 0.99;
         var _loc1_:class_892 = _loc2_;
         _loc1_.var_244 = var_288.var_244;
         _loc1_.var_245 = var_288.var_245;
         _loc1_.method_119(2);
         _loc1_.method_118();
         var_288.method_89();
         var_288 = null;
         method_81(false,20);
         while(int(var_1472.length) > 0)
         {
            _loc3_ = var_1472.pop();
            _loc3_.parent.removeChild(_loc3_);
         }
      }
      
      public function method_117() : void
      {
         var _loc1_:MovieClip = null;
         var _loc3_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("g\x15O\x19\x01"),0);
         _loc1_ = class_319.var_1;
         _loc1_.cacheAsBitmap = true;
         var_1472 = [];
         var _loc2_:int = 0;
         while(_loc2_ < 6)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc1_ = var_232.method_84(class_899.__unprotect__("P\n$k"),class_645.var_209);
            _loc1_.alpha = 0.5 - _loc3_ * 0.08;
            var_1472.push(_loc1_);
         }
         var _loc4_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("P\n$k"),class_645.var_209));
         _loc4_.var_233 = 0.99;
         var_288 = _loc4_;
         var_288.var_244 = class_742.var_217 * 0.5;
         var_288.var_245 = class_742.var_219 - 10;
         var_288.method_118();
         _loc2_ = 0;
         while(_loc2_ < 100)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            var_752.push({
               "L\x01":var_288.var_244,
               "\x03\x01":var_288.var_245
            });
         }
         var_104 = class_319.var_104;
         _loc2_ = 0;
         while(_loc2_ < 10)
         {
            _loc2_++;
            _loc3_ = _loc2_;
            _loc1_ = Reflect.field(var_104,"$t" + _loc3_);
            _loc1_.stop();
            var_949.push({
               "gB\x01":_loc1_,
               "F\x01":Number(10 + int(class_691.method_114(10))),
               "_\x01":5
            });
         }
         _loc2_ = int(9 - var_237[0] * 10);
         _loc3_ = 0;
         while(_loc3_ < _loc2_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = int(class_691.method_114(int(var_949.length)));
            var_949[_loc6_].var_631.visible = false;
            var_949.splice(_loc6_,1);
         }
      }
   }
}
