package
{
   import flash.display.BlendMode;
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_8.package_9.class_671;
   
   public class class_836 extends class_645
   {
      
      public static var var_433:int = 340;
       
      
      public var var_888:Number;
      
      public var var_1138:class_892;
      
      public var var_241:Number;
      
      public var var_1139:class_795;
      
      public var var_1140:Number;
      
      public var var_531:MovieClip;
      
      public var var_66:Number;
      
      public var var_754:Array;
      
      public var var_532:MovieClip;
      
      public var var_1143:class_755;
      
      public var var_783:Number;
      
      public var var_1144:Array;
      
      public var var_1142:Number;
      
      public var var_1141:MovieClip;
      
      public function class_836()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         var_241 = Number(Math.min(Number(var_241 + 0.01),1));
         var_783 = Number(var_783 + 0.001);
         if(Number(Math.random()) < var_783 * Math.pow(var_241,4) && var_227[0] < 380)
         {
            method_820();
         }
         switch(name_3)
         {
            default:
               break;
            case 1:
               vf1();
               method_821();
               break;
            case 2:
               vf1();
            case 3:
               vf1();
         }
         method_822();
      }
      
      override public function method_80() : void
      {
         method_81(true);
      }
      
      public function vf1() : void
      {
         var _loc6_:Number = NaN;
         var _loc7_:MovieClip = null;
         var _loc1_:* = method_100();
         var _loc2_:Number = _loc1_.var_244 - var_1138.var_244;
         var _loc3_:Number = Number(_loc1_.var_245) + 40 * (var_66 * 0.02) - var_1138.var_245;
         var_1138.var_244 = Number(var_1138.var_244 + Number(class_663.method_99(-30,_loc2_ * 0.25,30)));
         var_1138.var_245 = Number(var_1138.var_245 + Number(class_663.method_99(-30,_loc3_ * 0.25,30)));
         if(var_1138.var_245 > 320)
         {
            _loc6_ = var_1141.x - var_1138.var_244;
            if(Number(Math.abs(_loc6_)) < 28 && (var_1138.var_279 > 5 || _loc3_ > 80 && var_227[0] < 300) && name_3 == 1)
            {
               var_1141["_disp"] = true;
               var_1141.gotoAndPlay("shuriken");
               method_81(false,20);
               name_3 = 3;
               var_1138.var_279 = 14;
               var_1138.var_233 = 0.7;
               var_1141.x = var_1138.var_244;
               _loc7_ = var_232.method_84(class_899.__unprotect__("r?Gk\x03"),0);
               _loc7_.x = var_1138.var_244;
               _loc7_.y = class_836.var_433 - 5;
               var_1138.var_243.mask = _loc7_;
            }
            else
            {
               var_1138.var_279 = var_1138.var_279 * 0.5;
               var_1138.var_245 = 320;
               var_1138.method_118();
            }
         }
         var_531.x = var_1138.var_244;
      }
      
      public function method_822() : void
      {
         var _loc2_:class_892 = null;
         var _loc3_:Number = NaN;
         var _loc4_:Number = NaN;
         var _loc5_:MovieClip = null;
         var _loc6_:MovieClip = null;
         var _loc7_:MovieClip = null;
         var _loc1_:* = var_1139.method_73();
         while(_loc1_.method_74())
         {
            _loc2_ = _loc1_.name_1();
            if(!int(_loc2_.var_508))
            {
               _loc3_ = var_1138.var_245 - var_66 * 1.5;
               if(_loc2_.var_245 > _loc3_)
               {
                  _loc2_.var_508 = 1;
                  _loc4_ = var_1138.var_244 - _loc2_.var_244;
                  if(Number(Math.abs(_loc4_)) < var_66)
                  {
                     _loc2_.var_508 = 2;
                     _loc2_.var_245 = _loc3_;
                     _loc2_.var_278 = _loc4_ * 0.3;
                     _loc2_.var_279 = -(4 + Math.random() * 10);
                     _loc2_.var_250 = 0.3;
                     _loc2_.var_580 = (Math.random() * 2 - 1) * 60;
                     _loc2_.method_118();
                     _loc5_ = Reflect.field(_loc2_.var_243,class_899.__unprotect__("\x1c7ip\x03"));
                     _loc5_.visible = false;
                     if(name_3 == 1)
                     {
                        var_1138.var_279 = Number(var_1138.var_279 + (Number(3 + var_237[0] * 2)));
                     }
                     _loc5_ = var_232.method_84(class_899.__unprotect__("O\x05y\x1d\x03"),class_645.var_210);
                     _loc5_.x = _loc2_.var_244;
                     _loc5_.y = _loc2_.var_245;
                     _loc5_.rotation = Math.random() * 360;
                     _loc5_.blendMode = BlendMode.ADD;
                  }
               }
            }
            if(_loc2_.var_245 > Number(_loc2_.name_109))
            {
               _loc3_ = var_1141.x - _loc2_.var_244;
               if(Number(Math.abs(_loc3_)) < 30 && name_3 == 1 && int(_loc2_.var_508) == 1)
               {
                  var_1141.gotoAndPlay("shuriken");
                  method_81(false,20);
                  name_3 = 2;
               }
               else
               {
                  _loc2_.var_243.y = Number(_loc2_.name_109);
                  _loc7_ = _loc2_.var_243.var_1;
                  _loc6_ = _loc7_.var_1;
                  _loc5_ = _loc6_;
                  _loc5_.gotoAndStop(2);
                  new class_671(_loc5_,class_899.__unprotect__("D\x07\x01\x02"),int(class_691.method_114(3)));
                  _loc6_ = Reflect.field(_loc2_.var_243,class_899.__unprotect__("\x1c7ip\x03"));
                  _loc6_.play();
                  _loc2_.var_243 = null;
               }
               _loc2_.method_89();
               var_1139.method_116(_loc2_);
            }
         }
      }
      
      public function method_821() : void
      {
         var _loc1_:int = 0;
         _loc1_ = int(var_1141.name_89);
         var_1141.name_89 = int(var_1141.name_89) - 1;
         if(_loc1_ > 0)
         {
            return;
         }
         if(var_1141.var_587 == null)
         {
            _loc1_ = 30;
            var_1141.var_587 = _loc1_ + int(class_691.method_114(class_742.var_216 - 2 * _loc1_));
         }
         var _loc2_:Number = var_1141.var_587 - var_1141.x;
         var_1141.x = Number(var_1141.x + Number(class_663.method_99(-var_1142,_loc2_ * 0.5,var_1142)));
         var_1141.scaleX = _loc2_ > 0?1:-1;
         if(Number(Math.abs(_loc2_)) < 1)
         {
            var_1141.var_587 = null;
            var_1141.name_89 = 10 + int(class_691.method_114(30));
         }
         var_1141.play();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [320];
         super.method_95(param1);
         var_1142 = Number(1 + param1 * 4);
         var_1140 = 60;
         var_66 = 110 - param1 * 40;
         var_783 = Number(0.25 + param1 * 0.25);
         var_888 = (Math.random() * 2 - 1) * 0.25;
         var_241 = 0;
         var_1139 = new class_795();
         method_117();
      }
      
      public function method_117() : void
      {
         var _loc2_:int = 0;
         class_319 = var_232.method_84(class_899.__unprotect__("\x01R\x0eQ\x01"),0);
         var_531 = var_232.method_84(class_899.__unprotect__("w%\x1d@\x02"),0);
         var_531.y = class_836.var_433;
         var_532 = var_232.method_92(class_645.var_209);
         var_1144 = [];
         var _loc1_:int = 0;
         while(_loc1_ < 400)
         {
            _loc1_++;
            _loc2_ = _loc1_;
            if(_loc2_ < 200 || _loc2_ > 202)
            {
               var_1144.push(_loc2_);
            }
         }
         var_1143 = new class_755(var_532);
         var_1141 = var_1143.method_84(class_899.__unprotect__("G\'\x01|\x01"),200);
         var_1141.x = class_742.var_216 * 0.5;
         var_1141.y = class_836.var_433;
         var_1141.name_89 = 20 + int(class_691.method_114(20));
         var_1141.stop();
         var_1138 = new class_892(var_1143.method_84(class_899.__unprotect__("y\x0f\x18\x02"),201));
         var_1138.var_244 = class_742.var_216 * 0.5;
         var_1138.var_245 = class_742.var_218 * 0.5;
         var_1138.method_119(var_66 * 0.02);
         var_1138.var_233 = 0.95;
      }
      
      public function method_820() : void
      {
         var _loc1_:int = int(class_691.method_114(int(var_1144.length)));
         var _loc2_:int = int(var_1144[_loc1_]);
         var_1144.splice(_loc1_,1);
         var _loc3_:class_892 = new class_892(var_1143.method_84(class_899.__unprotect__("\x1bJ\x17\x11\x03"),_loc2_));
         _loc3_.var_244 = Math.random() * class_742.var_216;
         _loc3_.var_245 = -(10 + Math.random() * 80);
         var _loc4_:Number = Number(1.57 + var_888);
         _loc3_.var_278 = Math.cos(_loc4_) * var_1140;
         _loc3_.var_279 = Math.sin(_loc4_) * var_1140;
         _loc3_.name_109 = Number(class_836.var_433 + (_loc2_ - 200) * 0.07);
         _loc3_.var_508 = 0;
         var _loc6_:MovieClip = _loc3_.var_243.var_1;
         var _loc5_:MovieClip = _loc6_.var_1;
         _loc5_.stop();
         _loc3_.var_243.var_139.name_110 = var_1140 * 0.02;
         _loc3_.var_243.var_139.name_111 = var_888 / 0.0174;
         var_1139.method_103(_loc3_);
      }
   }
}
