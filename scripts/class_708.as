package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_708 extends class_645
   {
      
      public static var var_216:int = 240;
      
      public static var var_218:int = 240;
      
      public static var var_350:int = 192;
      
      public static var var_467:int = 65;
      
      public static var var_612:int = 8;
       
      
      public var var_614:Number;
      
      public var var_66:Number;
      
      public var var_562:Number;
      
      public var var_30:class_892;
      
      public var var_615:Boolean;
      
      public var var_613:MovieClip;
      
      public function class_708()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc1_:* = method_100();
         var _loc2_:Number = var_30.var_245;
         var _loc3_:Number = _loc1_.var_244 - var_30.var_244;
         var_30.var_278 = Number(var_30.var_278 + Number(class_663.method_99(-1,_loc3_ * 0.05,1)));
         var _loc5_:Number = _loc1_.var_245 - var_30.var_245;
         var _loc6_:Number = 0.2;
         if(_loc5_ > 0)
         {
            _loc6_ = 0.5;
         }
         var_30.var_245 = Number(var_30.var_245 + _loc5_ * _loc6_);
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_30.var_245 > var_613.y - class_708.var_467)
               {
                  _loc7_ = var_30.var_244 - var_613.x;
                  if(Number(Math.abs(_loc7_)) < Number(var_66 + class_708.var_612))
                  {
                     if(var_562 > var_614)
                     {
                        var_562 = var_562 - var_614;
                        _loc8_ = class_708.var_350 + class_708.var_467 - 4;
                        var_613.y = Number(Math.min(Number(var_613.y + var_562),_loc8_));
                        if(var_613.y == _loc8_)
                        {
                           method_81(true,20);
                        }
                     }
                     var_30.var_245 = var_613.y - class_708.var_467;
                     var_30.var_278 = var_30.var_278 * 0.5;
                  }
                  else
                  {
                     name_3 = 2;
                     var_615 = _loc7_ < 0;
                  }
                  var_562 = 0;
                  break;
               }
               break;
            case 2:
               if(var_30.var_245 < var_613.y - class_708.var_467)
               {
                  name_3 = 1;
                  break;
               }
               if(!!var_615 && var_30.var_244 > var_613.x - (Number(class_708.var_612 + var_66)))
               {
                  var_30.var_244 = var_613.x - (Number(class_708.var_612 + var_66));
                  var_30.var_278 = var_30.var_278 * 0.1;
               }
               if(!var_615 && var_30.var_244 < Number(var_613.x + (Number(class_708.var_612 + var_66))))
               {
                  var_30.var_244 = Number(var_613.x + (Number(class_708.var_612 + var_66)));
                  var_30.var_278 = var_30.var_278 * 0.1;
               }
               if(var_30.var_245 > class_708.var_350)
               {
                  var_30.var_245 = class_708.var_350;
                  var_562 = 0;
                  break;
               }
               break;
            case 3:
               if(var_30.var_245 < var_613.y - class_708.var_467)
               {
                  name_3 = 1;
                  break;
               }
               if(!!var_615 && var_30.var_244 > var_613.x - (Number(class_708.var_612 + var_66)))
               {
                  var_30.var_244 = var_613.x - (Number(class_708.var_612 + var_66));
                  var_30.var_278 = var_30.var_278 * 0.1;
               }
               if(!var_615 && var_30.var_244 < Number(var_613.x + (Number(class_708.var_612 + var_66))))
               {
                  var_30.var_244 = Number(var_613.x + (Number(class_708.var_612 + var_66)));
                  var_30.var_278 = var_30.var_278 * 0.1;
               }
               if(var_30.var_245 > class_708.var_350)
               {
                  var_30.var_245 = class_708.var_350;
                  var_562 = 0;
                  break;
               }
               break;
         }
         _loc7_ = var_30.var_245 - _loc2_;
         var_562 = var_562 * 0.5;
         if(_loc7_ > 0)
         {
            var_562 = Number(var_562 + _loc7_ * 0.2);
         }
         var_30.var_243.rotation = 40 * Math.max(0,1 - var_30.var_245 * 2 / class_708.var_218);
         super.method_79();
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [280];
         super.method_95(param1);
         var_66 = 40 - param1 * 25;
         var_614 = Number(2 + param1 * 6.5);
         var_562 = 0;
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         class_319 = var_232.method_84(class_899.__unprotect__("B6E)\x03"),0);
         var _loc1_:class_892 = new class_892(var_232.method_84(class_899.__unprotect__("U~\x19r\x02"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         var_30 = _loc1_;
         var_30.var_244 = class_708.var_216;
         var_30.var_245 = class_708.var_218 * 0.5;
         var_30.var_233 = 0.92;
         var_30.method_118();
         var_30.var_243.scaleX = var_66 * 0.02;
         var_30.var_243.scaleY = var_66 * 0.02;
         var_613 = class_319.var_42;
      }
   }
}
