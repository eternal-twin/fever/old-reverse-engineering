package
{
   import flash.display.MovieClip;
   import package_11.package_12.class_663;
   import package_32.class_899;
   
   public class class_793 extends class_645
   {
      
      public static var var_493:int = 12;
      
      public static var var_965:int = 8;
      
      public static var var_966:int = 20;
      
      public static var var_967:int = 80;
       
      
      public var var_752:Array;
      
      public var class_318:class_892;
      
      public function class_793()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         super.method_79();
         switch(name_3)
         {
            default:
               break;
            case 1:
               method_719();
               method_720();
         }
      }
      
      public function method_719() : void
      {
         var _loc3_:int = 0;
         var _loc4_:class_892 = null;
         var _loc5_:* = null;
         var _loc6_:Number = NaN;
         var _loc7_:Number = NaN;
         var _loc8_:* = null;
         var _loc9_:* = null;
         var _loc10_:Number = NaN;
         var _loc11_:int = 0;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Number = NaN;
         var _loc15_:Array = null;
         var _loc16_:class_892 = null;
         var _loc17_:Number = NaN;
         var _loc18_:Number = NaN;
         var _loc19_:Number = NaN;
         var _loc20_:Number = NaN;
         var _loc21_:Number = NaN;
         var _loc22_:MovieClip = null;
         var _loc23_:MovieClip = null;
         var _loc1_:int = 0;
         var _loc2_:int = int(var_752.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            _loc3_ = _loc1_;
            _loc4_ = var_752[_loc3_];
            _loc4_.var_945 = Number(_loc4_.var_945) - 1;
            _loc5_ = method_100();
            _loc6_ = Number(_loc4_.method_231(_loc5_));
            _loc7_ = Number(_loc4_.method_115(_loc5_));
            switch(_loc3_)
            {
               case 0:
                  break;
               default:
               default:
                  _loc8_ = {
                     "L\x01":class_742.var_217,
                     "\x03\x01":class_742.var_219 * 0.5
                  };
                  _loc9_ = {
                     "L\x01":(class_318.var_244 + int(_loc8_.var_244)) * 0.5,
                     "\x03\x01":(class_318.var_245 + Number(_loc8_.var_245)) * 0.5
                  };
                  _loc6_ = Number(_loc4_.method_231(_loc9_));
                  _loc7_ = Number(_loc4_.method_115(_loc9_));
                  break;
               case 3:
               default:
               default:
                  _loc4_.var_945 = Number(_loc4_.var_945) - 1;
                  _loc6_ = Number(_loc4_.method_231(class_318));
                  _loc7_ = Number(_loc4_.method_115(class_318));
                  if(Number(Math.abs(_loc6_)) > 1.7)
                  {
                     if(class_318.var_244 > 100 && Number(Math.random()) < 0.02 && Number(_loc4_.var_945) < 0)
                     {
                        _loc4_.var_248 = _loc6_;
                        _loc4_.var_945 = 30;
                        _loc11_ = 16;
                        _loc4_.var_278 = Number(_loc4_.var_278 + Math.cos(Number(_loc4_.var_248)) * _loc11_);
                        _loc4_.var_279 = Number(_loc4_.var_279 + Math.sin(Number(_loc4_.var_248)) * _loc11_);
                        break;
                     }
                     break;
                  }
                  _loc8_ = {
                     "L\x01":class_742.var_217,
                     "\x03\x01":class_742.var_219 * 0.5
                  };
                  _loc9_ = {
                     "L\x01":Number(class_318.var_244 * 0.8 + int(_loc8_.var_244) * 0.2),
                     "\x03\x01":Number(class_318.var_245 * 0.8 + _loc8_.var_245 * 0.2)
                  };
                  _loc6_ = Number(_loc4_.method_231(_loc9_));
                  _loc7_ = Number(_loc4_.method_115(_loc9_));
                  break;
               case 6:
               default:
               case 8:
               default:
               case 10:
                  _loc10_ = class_742.var_217 * 0.5;
                  _loc8_ = {
                     "L\x01":class_742.var_217 - (class_793.var_493 + class_793.var_966) * 1,
                     "\x03\x01":Number(class_663.method_99(class_742.var_217 * 0.5 - class_793.var_967,class_318.var_245,Number(class_742.var_217 * 0.5 + class_793.var_967)))
                  };
                  _loc6_ = Number(_loc4_.method_231(_loc8_));
                  _loc7_ = Number(_loc4_.method_115(_loc8_));
                  if(Number(_loc4_.var_945) < 0 && class_318.var_244 > class_742.var_217 - (class_793.var_493 * 2 + class_793.var_966 + 6) && Number(Math.abs(class_318.var_245 - class_742.var_219 * 0.5)) < class_793.var_967)
                  {
                     _loc4_.var_945 = 30;
                     _loc11_ = 22;
                     _loc4_.var_279 = Number(_loc4_.var_279 + Number(class_663.method_99(-_loc11_,(class_318.var_245 - _loc4_.var_245) * 0.5,_loc11_)));
                     break;
                  }
            }
            _loc10_ = Number(class_663.method_112(_loc6_ - _loc4_.var_248,3.14));
            _loc12_ = 0.5;
            _loc4_.var_248 = Number(Number(_loc4_.var_248) + Number(class_663.method_99(-_loc12_,_loc10_ * 0.3,_loc12_)));
            _loc4_.var_243.rotation = _loc4_.var_248 / 0.0174;
            _loc13_ = Number(class_663.method_99(0,(_loc7_ - 5) * 0.006 - Math.abs(_loc10_),0.7));
            _loc4_.var_278 = Number(_loc4_.var_278 + Math.cos(Number(_loc4_.var_248)) * _loc13_);
            _loc4_.var_279 = Number(_loc4_.var_279 + Math.sin(Number(_loc4_.var_248)) * _loc13_);
            if(_loc3_ == 0 && Number(_loc4_.var_945) < 0 && name_5)
            {
               _loc4_.var_945 = 30;
               _loc14_ = Number(_loc4_.method_231(class_318));
               _loc4_.var_248 = _loc14_;
               _loc11_ = 22;
               _loc4_.var_278 = Number(_loc4_.var_278 + Math.cos(Number(_loc4_.var_248)) * _loc11_);
               _loc4_.var_279 = Number(_loc4_.var_279 + Math.sin(Number(_loc4_.var_248)) * _loc11_);
            }
            _loc4_.var_243.gotoAndStop(int(_loc13_ * 40) + 1);
            _loc11_ = 0;
            _loc15_ = var_752;
            while(_loc11_ < int(_loc15_.length))
            {
               _loc16_ = _loc15_[_loc11_];
               _loc11_++;
               if(_loc16_ != _loc4_)
               {
                  _loc6_ = Number(_loc4_.method_115(_loc16_));
                  if(_loc6_ < class_793.var_493 * 2)
                  {
                     _loc7_ = Number(_loc4_.method_231(_loc16_));
                     _loc10_ = class_793.var_493 * 2 - _loc6_;
                     _loc4_.var_244 = _loc4_.var_244 - Math.cos(_loc7_) * _loc10_ * 0.5;
                     _loc4_.var_245 = _loc4_.var_245 - Math.sin(_loc7_) * _loc10_ * 0.5;
                     _loc16_.var_244 = Number(_loc16_.var_244 + Math.cos(_loc7_) * _loc10_ * 0.5);
                     _loc16_.var_245 = Number(_loc16_.var_245 + Math.sin(_loc7_) * _loc10_ * 0.5);
                  }
               }
            }
            _loc6_ = Number(_loc4_.method_115(class_318));
            if(_loc6_ < class_793.var_493 + class_793.var_965)
            {
               _loc7_ = Number(_loc4_.method_231(class_318));
               _loc10_ = class_793.var_493 + class_793.var_965 - _loc6_;
               class_318.var_244 = Number(class_318.var_244 + Math.cos(_loc7_) * _loc10_);
               class_318.var_245 = Number(class_318.var_245 + Math.sin(_loc7_) * _loc10_);
               _loc12_ = Number(Math.sqrt(Number(Number(Math.pow(_loc4_.var_278,2)) + Number(Math.pow(_loc4_.var_279,2)))));
               _loc13_ = Number(Math.sqrt(Number(Number(Math.pow(class_318.var_278,2)) + Number(Math.pow(class_318.var_279,2)))));
               _loc14_ = Number(_loc12_ + _loc13_);
               _loc17_ = _loc13_ / (Number(_loc12_ + _loc13_));
               _loc18_ = Number(Math.atan2(_loc4_.var_279,_loc4_.var_278));
               _loc19_ = Number(class_663.method_112(_loc7_ - _loc18_,3.14));
               _loc20_ = Number(_loc18_ + _loc19_ * _loc17_);
               class_318.var_278 = Number(class_318.var_278 + Math.cos(_loc20_) * _loc14_);
               class_318.var_279 = Number(class_318.var_279 + Math.sin(_loc20_) * _loc14_);
               _loc21_ = 0.1;
               if(_loc3_ == 0)
               {
                  _loc21_ = 0.75;
               }
               _loc4_.var_278 = _loc4_.var_278 - Math.cos(_loc20_) * _loc14_ * _loc21_;
               _loc4_.var_279 = _loc4_.var_279 - Math.sin(_loc20_) * _loc14_ * _loc21_;
            }
            if(_loc4_.var_244 < class_793.var_493)
            {
               _loc4_.var_244 = class_793.var_493;
               _loc4_.var_278 = Number(Math.abs(_loc4_.var_278));
            }
            if(_loc4_.var_244 > class_742.var_217 - (class_793.var_493 + class_793.var_966))
            {
               _loc4_.var_244 = class_742.var_217 - (class_793.var_493 + class_793.var_966);
               _loc4_.var_278 = -Math.abs(_loc4_.var_278);
            }
            if(_loc4_.var_245 < class_793.var_493 || _loc4_.var_245 > class_742.var_219 - class_793.var_493)
            {
               _loc4_.var_245 = Number(class_663.method_99(class_793.var_493,_loc4_.var_245,class_742.var_219 - class_793.var_493));
               _loc4_.var_279 = _loc4_.var_279 * -1;
            }
            if(Number(_loc4_.var_945) > 0)
            {
               _loc22_ = var_232.method_84(class_899.__unprotect__("m(\nb"),class_645.var_208);
               _loc22_.x = _loc4_.var_244;
               _loc22_.y = _loc4_.var_245;
               _loc23_ = Reflect.field(_loc22_,class_899.__unprotect__("\x0b\x01"));
               _loc23_.gotoAndStop(_loc3_ == 0?"1":"2");
            }
            _loc4_.name_93.x = Number(_loc4_.var_244 + 5);
            _loc4_.name_93.y = Number(_loc4_.var_245 + 5);
            _loc4_.var_243.x = _loc4_.var_244;
            _loc4_.var_243.y = _loc4_.var_245;
         }
      }
      
      public function method_720() : void
      {
         if(var_229[0])
         {
            if(Number(Math.abs(class_318.var_245 - class_742.var_219 * 0.5)) > class_793.var_967 - class_793.var_965)
            {
               class_318.var_245 = Number(class_663.method_99(class_742.var_217 * 0.5 - class_793.var_967,class_318.var_245,Number(class_742.var_217 * 0.5 + class_793.var_967)));
               class_318.var_279 = class_318.var_279 * -1;
            }
            class_318.var_278 = Number(class_318.var_278 + 0.5);
            if(class_318.var_244 > class_742.var_217 + class_793.var_965)
            {
               method_81(true);
            }
         }
         else
         {
            if(class_318.var_244 < class_793.var_965)
            {
               class_318.var_244 = class_793.var_965;
               class_318.var_278 = class_318.var_278 * -1;
            }
            if(class_318.var_244 > class_742.var_217 - (class_793.var_965 + class_793.var_966))
            {
               if(Number(Math.abs(class_318.var_245 - class_742.var_219 * 0.5)) > class_793.var_967 - class_793.var_965)
               {
                  class_318.var_244 = Number(class_663.method_99(class_793.var_965,class_318.var_244,class_742.var_217 - (class_793.var_965 + class_793.var_966)));
                  class_318.var_278 = class_318.var_278 * -1;
               }
               else
               {
                  var_229 = [true];
               }
            }
            if(class_318.var_245 < class_793.var_965 || class_318.var_245 > class_742.var_219 - class_793.var_965)
            {
               class_318.var_245 = Number(class_663.method_99(class_793.var_965,class_318.var_245,class_742.var_217 - class_793.var_965));
               class_318.var_279 = class_318.var_279 * -1;
            }
         }
         var _loc1_:Number = Number(Math.sqrt(Number(Number(Math.pow(class_318.var_278,2)) + Number(Math.pow(class_318.var_279,2)))));
         var _loc2_:Number = Number(Math.atan2(class_318.var_279,class_318.var_278));
         _loc1_ = Number(Math.min(_loc1_,20));
         class_318.var_278 = Math.cos(_loc2_) * _loc1_;
         class_318.var_279 = Math.sin(_loc2_) * _loc1_;
         class_318.var_243.x = class_318.var_244;
         class_318.var_243.y = class_318.var_245;
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [400];
         super.method_95(param1);
         var_229 = [false];
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc1_:class_892 = null;
         var _loc4_:int = 0;
         var _loc5_:class_892 = null;
         var _loc6_:String = null;
         var _loc7_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("]q\x02;\x01"),0);
         _loc1_ = new class_892(var_232.method_84(class_899.__unprotect__("D;~G"),class_645.var_209));
         _loc1_.var_233 = 0.99;
         class_318 = _loc1_;
         class_318.var_244 = class_742.var_217 * 0.5;
         class_318.var_245 = class_742.var_217 * 0.5;
         class_318.method_118();
         class_318.var_233 = 0.92;
         var_752 = [];
         var _loc2_:int = 2 + int(var_237[0] * 9);
         var _loc3_:int = 0;
         while(_loc3_ < _loc2_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc5_ = new class_892(var_232.method_84(class_899.__unprotect__("yR]3\x03"),class_645.var_209));
            _loc5_.var_233 = 0.99;
            _loc1_ = _loc5_;
            if(_loc4_ < 2)
            {
               _loc1_.var_244 = Number(class_742.var_217 * 0.5 + (_loc4_ * 2 - 1) * 40);
               _loc1_.var_245 = class_742.var_219 * 0.5;
            }
            else
            {
               _loc1_.var_244 = class_742.var_217 - (class_793.var_493 + class_793.var_966);
               _loc1_.var_245 = Number(Number(40 + (_loc4_ - 2) / (_loc2_ - 2) * (class_793.var_967 * 2)) + class_793.var_967 * 2 / (_loc2_ - 1) * 0.5);
            }
            _loc1_.var_248 = 0;
            _loc1_.var_945 = 0;
            _loc1_.method_118();
            _loc6_ = "2";
            if(_loc4_ == 0)
            {
               _loc6_ = "1";
            }
            _loc7_ = Reflect.field(_loc1_.var_243,class_899.__unprotect__("r\x01"));
            _loc7_.gotoAndStop(_loc6_);
            var_752.push(_loc1_);
            _loc1_.name_93 = var_232.method_84(class_899.__unprotect__("(t\x01M"),class_645.var_208);
            _loc1_.name_93.x = _loc1_.var_244;
            _loc1_.name_93.y = _loc1_.var_245;
            _loc1_.var_233 = 0.92;
         }
      }
   }
}
