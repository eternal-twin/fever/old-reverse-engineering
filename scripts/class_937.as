package
{
   import flash.display.MovieClip;
   import flash.events.MouseEvent;
   import package_32.class_899;
   
   public class class_937 extends class_645
   {
      
      public static var var_1554:Number = 1.6;
       
      
      public var var_1557:Array;
      
      public var var_1274:MovieClip;
      
      public var var_251:Number;
      
      public var var_1556:Array;
      
      public var name_55:int;
      
      public var var_351:Number;
      
      public var var_460:int;
      
      public var var_1559:Number;
      
      public var var_1560:int;
      
      public var var_1555:Array;
      
      public var var_1558:Boolean;
      
      public var var_700:Number;
      
      public function class_937()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Boolean = false;
         var _loc3_:int = 0;
         var _loc4_:Array = null;
         var _loc5_:* = null;
         var _loc6_:MovieClip = null;
         var _loc7_:Number = NaN;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         switch(name_3)
         {
            default:
               break;
            case 1:
               if(var_251 < 0)
               {
                  name_3 = 2;
                  break;
               }
               var_251 = var_251 - 1;
               break;
            case 2:
               _loc1_ = class_742.var_218 - 30;
               _loc2_ = true;
               _loc3_ = 0;
               _loc4_ = var_1555;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  if(Number(_loc5_.var_37) < 0)
                  {
                     _loc6_ = _loc5_.var_631;
                     _loc7_ = _loc1_ - _loc6_.y;
                     _loc6_.y = Number(_loc6_.y + Number(Math.min(_loc7_ * 0.2,10)));
                     if(Number(Math.abs(_loc7_)) < 0.5)
                     {
                        _loc6_.y = _loc1_;
                     }
                     else
                     {
                        _loc2_ = false;
                     }
                  }
                  else
                  {
                     _loc5_.var_37 = Number(_loc5_.var_37) - 1;
                     _loc2_ = false;
                  }
               }
               if(_loc2_)
               {
                  name_55 = 4 + int(Math.round(var_237[0] * 10));
                  _loc3_ = 0;
                  _loc4_ = var_1555;
                  while(_loc3_ < int(_loc4_.length))
                  {
                     _loc5_ = _loc4_[_loc3_];
                     _loc3_++;
                     _loc5_.var_531.parent.removeChild(_loc5_.var_531);
                  }
                  var_1274.parent.removeChild(var_1274);
                  method_1168();
                  break;
               }
               break;
            case 3:
               var_700 = Number(Math.min(Number(var_700 + var_351),3.14));
               _loc1_ = class_742.var_218 - 30;
               _loc3_ = 0;
               _loc4_ = var_1556;
               while(_loc3_ < int(_loc4_.length))
               {
                  _loc5_ = _loc4_[_loc3_];
                  _loc3_++;
                  _loc8_ = 0;
                  while(_loc8_ < 2)
                  {
                     _loc8_++;
                     _loc9_ = _loc8_;
                     _loc10_ = int(_loc5_.name_40[_loc9_]);
                     _loc6_ = var_1555[_loc10_].var_631;
                     _loc11_ = _loc9_ * 2 - 1;
                     _loc6_.x = Number(Number(_loc5_.var_244) + Math.cos(var_700) * _loc5_.var_8 * _loc11_);
                     _loc6_.y = Number(_loc1_ + Math.sin(var_700 * _loc11_) * (4 + Math.abs(Number(_loc5_.var_8)) * 0.25));
                  }
               }
               if(var_700 == 3.14)
               {
                  method_1168();
                  break;
               }
               break;
            case 4:
               _loc1_ = 0;
               _loc4_ = var_1557;
               while(_loc1_ < int(_loc4_.length))
               {
                  _loc6_ = _loc4_[_loc1_];
                  _loc1_++;
                  _loc7_ = class_742.var_218 * 0.75 - _loc6_.y;
                  _loc6_.y = Number(_loc6_.y + Number(Math.min(_loc7_ * 0.2,10)));
               }
               if(var_220 == false && var_228 < 16 && int(var_1557.length) < 2 && !var_1558)
               {
                  method_400(var_460);
                  var_1558 = true;
                  break;
               }
         }
         super.method_79();
      }
      
      public function method_400(param1:int) : void
      {
         var _loc6_:* = null;
         var _loc2_:MovieClip = var_1555[param1].var_631;
         var _loc3_:MovieClip = var_232.method_84(class_899.__unprotect__("jK]\x02"),class_645.var_209);
         _loc3_.scaleX = var_1559 * 0.01;
         _loc3_.scaleY = var_1559 * 0.01;
         _loc3_.x = _loc2_.x;
         _loc3_.y = _loc2_.y;
         if(param1 == var_460)
         {
            method_1169(_loc2_.x,_loc2_.y);
            method_81(true,20);
         }
         else
         {
            method_81(false,20);
         }
         var_232.method_110(_loc2_);
         var_1557.push(_loc2_);
         var _loc4_:int = 0;
         var _loc5_:Array = var_1555;
         while(_loc4_ < int(_loc5_.length))
         {
            _loc6_ = _loc5_[_loc4_];
            _loc4_++;
            _loc6_.var_631.mouseEnabled = false;
         }
      }
      
      public function method_1168() : void
      {
         var _loc4_:int = 0;
         var _loc5_:int = 0;
         var _loc6_:Array = null;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc11_:* = null;
         var _loc12_:Number = NaN;
         var _loc13_:Number = NaN;
         var _loc14_:Array = null;
         var _loc15_:* = null;
         var _loc16_:MovieClip = null;
         name_55 = name_55 - 1;
         if(name_55 == 0)
         {
            method_1170();
            return;
         }
         name_3 = 3;
         var_700 = 0;
         var_1556 = [];
         var _loc1_:int = 1;
         if(int(class_691.method_114(int(Math.round(var_237[0] * 100)))) > 20)
         {
            _loc1_++;
         }
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         _loc4_ = int(var_1555.length);
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_.push(_loc5_);
         }
         _loc3_ = 0;
         while(_loc3_ < _loc1_)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc6_ = [];
            _loc5_ = 0;
            while(_loc5_ < 2)
            {
               _loc5_++;
               _loc7_ = _loc5_;
               _loc8_ = int(class_691.method_114(int(_loc2_.length)));
               _loc9_ = int(_loc2_[_loc8_]);
               _loc2_.splice(_loc8_,1);
               _loc6_.push(_loc9_);
            }
            _loc10_ = var_1555[int(_loc6_[0])];
            _loc11_ = var_1555[int(_loc6_[1])];
            _loc12_ = (_loc10_.var_244 - _loc11_.var_244) * 0.5;
            _loc13_ = (Number(_loc10_.var_244) + Number(_loc11_.var_244)) * 0.5;
            var_1556.push({
               "?FtN\x01":_loc6_,
               "L\x01":_loc13_,
               "t\x01":_loc12_
            });
            _loc5_ = 0;
            _loc14_ = var_1555;
            while(_loc5_ < int(_loc14_.length))
            {
               _loc15_ = _loc14_[_loc5_];
               _loc5_++;
               if(_loc15_ != _loc11_)
               {
                  var_232.method_110(_loc15_.var_631);
               }
            }
            var_232.method_110(_loc10_.var_631);
            _loc16_ = _loc10_.var_631;
            _loc10_.var_631 = _loc11_.var_631;
            _loc11_.var_631 = _loc16_;
            if(var_460 == int(_loc6_[0]))
            {
               var_460 = int(_loc6_[1]);
            }
            else if(var_460 == int(_loc6_[1]))
            {
               var_460 = int(_loc6_[0]);
            }
         }
      }
      
      public function method_1170() : void
      {
         name_3 = 4;
         var_1557 = [];
         var _loc1_:int = 0;
         var _loc2_:int = int(var_1555.length);
         while(_loc1_ < _loc2_)
         {
            _loc1_++;
            var var_343:Array = [_loc1_];
            var var_214:Array = [this];
            var_1555[int(var_343[0])].var_631.addEventListener(MouseEvent.CLICK,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_343:Array = param2;
               return function(param1:*):void
               {
                  var_214[0].method_400(int(var_343[0]));
               };
            }(var_214,var_343));
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var_227 = [220];
         super.method_95(param1);
         var_251 = 20;
         var_351 = Number(0.2 + param1 * 0.3);
         var_1560 = 4 + int(Math.round(param1 * 2));
         var_1559 = 30 * class_937.var_1554;
         var_1558 = false;
         var_460 = int(class_691.method_114(var_1560));
         method_117();
      }
      
      public function method_1169(param1:Number, param2:Number) : void
      {
         var_1274 = var_232.method_84(class_899.__unprotect__("\'-E\x10\x02"),class_645.var_209);
         var_1274.x = param1;
         var_1274.y = param2;
         var_1274.scaleX = var_1559 * 0.01;
         var_1274.scaleY = var_1559 * 0.01;
      }
      
      public function method_117() : void
      {
         var _loc5_:int = 0;
         var _loc6_:Number = NaN;
         var _loc7_:MovieClip = null;
         var _loc8_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__(";HbN\x01"),0);
         var _loc1_:int = class_742.var_218 - 30;
         var _loc2_:Number = (class_742.var_216 - var_1560 * var_1559) / (var_1560 + 1);
         var_1555 = [];
         var _loc3_:int = 0;
         var _loc4_:int = var_1560;
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc6_ = Number(Number(_loc2_ + var_1559 * 0.5) + _loc5_ * (_loc2_ + var_1559));
            _loc7_ = var_232.method_84(class_899.__unprotect__("jK]\x02"),class_645.var_209);
            _loc7_.scaleX = var_1559 * 0.01;
            _loc7_.scaleY = var_1559 * 0.01;
            _loc7_.x = _loc6_;
            _loc7_.y = _loc1_;
            if(var_460 == _loc5_)
            {
               var_1274 = var_232.method_84(class_899.__unprotect__("\'-E\x10\x02"),class_645.var_209);
               var_1274.x = _loc6_;
               var_1274.y = _loc1_;
               var_1274.scaleX = var_1559 * 0.01;
               var_1274.scaleY = var_1559 * 0.01;
            }
            _loc8_ = var_232.method_84(class_899.__unprotect__("m\'&(\x01"),class_645.var_209);
            _loc8_.scaleX = var_1559 * 0.01;
            _loc8_.scaleY = var_1559 * 0.01;
            _loc8_.x = _loc6_;
            _loc8_.y = class_742.var_218 * 0.5;
            var_1555.push({
               "gB\x01":_loc8_,
               "j\x01":4 * _loc5_,
               "T\n8\x02":_loc7_,
               "L\x01":_loc6_
            });
         }
      }
   }
}
