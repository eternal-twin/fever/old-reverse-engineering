package
{
   import package_32.class_899;
   
   public final class _PlayerAction
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_Play","_GameResult","_Grab","_MoveTo","_Teleport","_Burn","_Prism","_Dice","_FeverXPlay","_MajCartridge","_SavePos","_EndGame"];
      
      public static var _Teleport:_PlayerAction;
      
      public static var _Prism:_PlayerAction;
      
      public static var _FeverXPlay:_PlayerAction;
       
      
      public var var_294:String;
      
      public var var_295:int;
      
      public var var_296:Array;
      
      public const __enum__:Boolean = true;
      
      public function _PlayerAction(param1:String, param2:int, param3:*)
      {
         var_294 = param1;
         var_295 = param2;
         var_296 = param3;
      }
      
      public static function _SavePos(param1:int) : _PlayerAction
      {
         return new _PlayerAction("_SavePos",10,[param1]);
      }
      
      public static function _Play(param1:int) : _PlayerAction
      {
         return new _PlayerAction("_Play",0,[param1]);
      }
      
      public static function _MoveTo(param1:int, param2:int) : _PlayerAction
      {
         return new _PlayerAction("_MoveTo",3,[param1,param2]);
      }
      
      public static function _MajCartridge(param1:Object) : _PlayerAction
      {
         return new _PlayerAction("_MajCartridge",9,[param1]);
      }
      
      public static function _Grab(param1:int, param2:_Reward) : _PlayerAction
      {
         return new _PlayerAction("_Grab",2,[param1,param2]);
      }
      
      public static function _GameResult(param1:Boolean, param2:Array) : _PlayerAction
      {
         return new _PlayerAction("_GameResult",1,[param1,param2]);
      }
      
      public static function _EndGame(param1:int) : _PlayerAction
      {
         return new _PlayerAction("_EndGame",11,[param1]);
      }
      
      public static function _Dice(param1:Boolean) : _PlayerAction
      {
         return new _PlayerAction("_Dice",7,[param1]);
      }
      
      public static function _Burn(param1:Array, param2:_IslandBonus) : _PlayerAction
      {
         return new _PlayerAction("_Burn",5,[param1,param2]);
      }
      
      public final function toString() : String
      {
         return class_899.method_165(this);
      }
   }
}
