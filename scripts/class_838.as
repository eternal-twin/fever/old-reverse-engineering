package
{
   import flash.display.Graphics;
   import flash.display.Sprite;
   import package_11.package_12.class_657;
   import package_11.package_12.class_658;
   import package_32.class_899;
   
   public class class_838 extends Sprite
   {
      
      public static var var_1148:int = 200;
      
      public static var var_1149:int = 200;
       
      
      public var var_629:Number;
      
      public var name_55:Boolean;
      
      public var var_105:class_364;
      
      public var var_1151:Number;
      
      public var var_596:Number;
      
      public var var_595:Object;
      
      public var var_17:Number;
      
      public var var_369:int;
      
      public var var_795:int;
      
      public var var_65:Number;
      
      public function class_838(param1:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
         var_795 = param1;
         var_65 = 0;
         var_17 = 0;
         name_55 = false;
         var_105 = new class_364();
         addChild(var_105);
         var_369 = int(class_657.method_138(param1 / class_839.var_1150.z2Eo));
         class_657.name_18(var_105.var_105,var_369);
      }
      
      public function method_824() : void
      {
         filters = [];
      }
      
      public function method_129(param1:Number, param2:Number, param3:Graphics = undefined) : void
      {
         param2 = Number(param2 + var_17);
         x = Number(class_838.var_1148 + Math.cos(param1) * param2);
         y = Number(class_838.var_1149 + Math.sin(param1) * param2);
         if(param3 != null && var_595 != null && (name_55 || class_839.var_1150.var_220))
         {
            param3.lineStyle(38,var_369);
            param3.moveTo(var_595,var_596);
            param3.lineTo(x,y);
         }
         var_595 = x;
         var_596 = y;
      }
      
      public function var_10() : void
      {
         class_658.var_146(this,4,4,16777215,true);
         class_658.var_146(this,4,8,16777215);
         class_658.var_146(this,12,1,16777215);
      }
   }
}
