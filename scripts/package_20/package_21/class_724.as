package package_20.package_21
{
   import package_32.class_899;
   
   public class class_724
   {
       
      
      public var var_37:int;
      
      public var var_93:int;
      
      public var var_291:int;
      
      public var var_12:int;
      
      public function class_724(param1:int = 0, param2:int = 0, param3:int = 0, param4:int = 0)
      {
         if(class_899.var_239)
         {
            return;
         }
         var_291 = param1;
         var_37 = param2;
         var_93 = param3;
         var_12 = param4;
      }
      
      public function toString() : String
      {
         return "[l=" + var_291 + " b=" + var_12 + " r=" + var_93 + " t=" + var_37 + "]";
      }
   }
}
