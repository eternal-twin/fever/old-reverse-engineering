package
{
   import flash.display.MovieClip;
   import package_11.class_755;
   import package_11.package_12.class_657;
   import package_11.package_12.class_663;
   import package_32.class_899;
   import package_41.package_42.qH1g;
   
   public class class_726 extends class_645
   {
      
      public static var var_574:int = 24;
      
      public static var var_213:int = 7;
      
      public static var var_696:int = 46;
      
      public static var var_434:int = 26;
      
      public static var var_697:int = 20;
       
      
      public var var_85:MovieClip;
      
      public var var_698:int;
      
      public var var_488:Array;
      
      public var var_347:MovieClip;
      
      public var var_701:int;
      
      public var var_700:Number;
      
      public var var_526:MovieClip;
      
      public var var_470:Array;
      
      public var var_699:Array;
      
      public function class_726()
      {
         if(class_899.var_239)
         {
            return;
         }
         super();
      }
      
      override public function method_79() : void
      {
         var _loc1_:int = 0;
         var _loc2_:Number = NaN;
         var _loc3_:MovieClip = null;
         var _loc4_:int = 0;
         var _loc5_:Array = null;
         var _loc6_:Boolean = false;
         var _loc7_:int = 0;
         var _loc8_:Array = null;
         loop3:
         switch(name_3)
         {
            default:
               break;
            case 1:
               break;
            case 2:
               _loc1_ = class_726.var_574;
               _loc2_ = Number(class_663.method_99(0,(class_742.var_219 + class_726.var_434 - var_85.y) / _loc1_,1));
               method_438(_loc1_ * _loc2_);
               var_85.y = Number(var_85.y + _loc1_);
               if(var_85.y > class_742.var_219 + class_726.var_434 + class_726.var_697)
               {
                  var_85.y = class_742.var_219 + class_726.var_434 + class_726.var_697;
                  name_3 = 3;
                  _loc3_ = var_232.method_84(class_899.__unprotect__("\x17\x05h\x12\x01"),class_645.var_208);
                  _loc3_.x = var_85.x;
                  _loc3_.y = class_742.var_219 + class_726.var_697;
                  _loc3_.gotoAndStop(var_698 + 1);
                  _loc3_.var_38.name_63 = int(var_488.length) == 0;
                  var_488.push(_loc3_);
               }
               var_488.reverse();
               _loc4_ = 0;
               _loc5_ = var_488;
               while(_loc4_ < int(_loc5_.length))
               {
                  _loc3_ = _loc5_[_loc4_];
                  _loc4_++;
                  var_232.method_110(_loc3_);
               }
               var_488.reverse();
               break;
            case 3:
               _loc1_ = -class_726.var_574;
               var_85.y = Number(var_85.y + _loc1_);
               method_438(_loc1_);
               if(var_85.y < class_742.var_219 - class_726.var_696)
               {
                  _loc2_ = class_742.var_219 - class_726.var_696 - var_85.y;
                  method_438(_loc2_);
                  var_85.y = Number(var_85.y + _loc2_);
                  name_3 = 1;
                  if(int(var_488.length) == class_726.var_213)
                  {
                     _loc6_ = true;
                     var_470 = [];
                     _loc4_ = 0;
                     _loc7_ = 0;
                     _loc5_ = var_699;
                     while(_loc7_ < int(_loc5_.length))
                     {
                        _loc8_ = _loc5_[_loc7_];
                        _loc7_++;
                        if(int(_loc8_[0]) != int(_loc8_[1]))
                        {
                           _loc6_ = false;
                           var_470.push(var_488[_loc4_]);
                        }
                        _loc4_++;
                     }
                     method_81(_loc6_,20 + (!!_loc6_?0:10));
                     name_3 = 4;
                     var_700 = 0;
                     break;
                  }
                  break;
               }
               break;
            case 4:
               var_700 = (var_700 + 75) % 628;
               _loc1_ = 0;
               _loc5_ = var_470;
               while(true)
               {
                  if(_loc1_ >= int(_loc5_.length))
                  {
                     break loop3;
                  }
                  _loc3_ = _loc5_[_loc1_];
                  _loc1_++;
                  _loc2_ = Number(0.6 + Math.cos(var_700 / 100) * 0.4);
                  class_657.gfof(_loc3_,_loc2_,16711680);
               }
         }
         super.method_79();
      }
      
      public function method_400(param1:int) : void
      {
         if(name_3 == 1)
         {
            var_699[int(var_488.length)].push(param1);
            if(name_3 == 1)
            {
               var_698 = param1;
               name_3 = 2;
            }
         }
      }
      
      public function method_438(param1:Number) : void
      {
         var _loc4_:MovieClip = null;
         var _loc2_:int = 0;
         var _loc3_:Array = var_488;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            _loc4_.y = Number(_loc4_.y + param1);
         }
      }
      
      override public function method_95(param1:Number) : void
      {
         var _loc4_:int = 0;
         var_227 = [340 - param1 * 100];
         super.method_95(param1);
         var_701 = 3 + int(Math.floor(param1 * 8));
         if(var_701 > 11)
         {
            var_701 = 11;
         }
         var_699 = [];
         var _loc2_:int = 0;
         var _loc3_:int = class_726.var_213;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            var_699[_loc4_] = [int(class_691.method_114(var_701))];
         }
         var_488 = [];
         method_117();
         method_72();
      }
      
      public function method_117() : void
      {
         var _loc4_:int = 0;
         var _loc5_:MovieClip = null;
         class_319 = var_232.method_84(class_899.__unprotect__("=u^\x07\x02"),0);
         var_347 = var_232.method_92(class_645.var_208);
         var_347.x = 204;
         var_347.y = 157;
         var _loc1_:class_755 = new class_755(var_347);
         _loc1_.method_84(class_899.__unprotect__("OO;\x0e\x02"),1);
         var _loc2_:int = 0;
         var _loc3_:int = int(var_699.length);
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            _loc4_ = _loc2_;
            _loc5_ = _loc1_.method_84(class_899.__unprotect__("\x17\x05h\x12\x01"),class_645.var_209);
            _loc5_.y = -(class_726.var_434 + class_726.var_697 * _loc4_);
            _loc5_.gotoAndStop(int(var_699[int(var_699.length) - (1 + _loc4_)][0]) + 1);
            _loc5_.var_38.name_63 = int(var_699.length) - 1;
         }
         var_347.scaleX = 70 * 0.01;
         var_347.scaleY = 70 * 0.01;
         var_347.rotation = -30;
         class_657.gfof(var_347,0.5,16642768);
         var_85 = var_232.method_84(class_899.__unprotect__("OO;\x0e\x02"),class_645.var_208);
         var_85.x = class_742.var_217 * 0.25;
         var_85.y = class_742.var_219 - class_726.var_696;
         var_526 = var_232.method_84(class_899.__unprotect__("=q#)\x03"),class_645.var_209);
         var_526.y = class_742.var_219;
         var _loc6_:Number = class_742.var_217 / (Number(var_701 + 0.5));
         _loc2_ = 0;
         _loc3_ = var_701;
         while(_loc2_ < _loc3_)
         {
            _loc2_++;
            var var_343:Array = [_loc2_];
            _loc5_ = var_232.method_84(class_899.__unprotect__("\x17\x05h\x12\x01"),class_645.var_209);
            _loc5_.x = (int(var_343[0]) + 0.75) * _loc6_;
            _loc5_.y = 222;
            _loc5_.scaleX = 80 * 0.01;
            _loc5_.scaleY = 80 * 0.01;
            _loc5_.var_38.name_63 = false;
            _loc5_.gotoAndStop(int(var_343[0]) + 1);
            var var_214:Array = [this];
            qH1g.method_83(_loc5_,function(param1:Array, param2:Array):Function
            {
               var var_214:Array = param1;
               var var_343:Array = param2;
               return function():void
               {
                  var_214[0].method_400(int(var_343[0]));
               };
            }(var_214,var_343));
            qH1g.method_439(_loc5_,true);
         }
      }
   }
}
