(function () { "use strict";
var $hxClasses = {};
var Main = function() { };
$hxClasses["Main"] = Main;
Main.__name__ = ["Main"];
Main.main = function() {
	console.log(Type.getEnumConstructs(_IslandBonus));
	console.log(Type.createEmptyInstance(haxe.Serializer));
	console.log(Type.getClassName(haxe.Serializer));
	console.log(45);
	console.log(Math.isNaN(2));
	console.log(Reflect.field({ foo : 1, bar : 2},"foo"));
	console.log(Type.resolveClass("Type"));
	console.log(Type.resolveEnum("_IslandStatus"));
	var f = Std["is"];
	f("1",String);
};
Math.__name__ = ["Math"];
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = ["Reflect"];
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		return null;
	}
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = ["Std"];
Std["is"] = function(v,t) {
	return js.Boot.__instanceof(v,t);
};
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = ["Type"];
Type.getClassName = function(c) {
	var a = c.__name__;
	return a.join(".");
};
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.resolveEnum = function(name) {
	var e = $hxClasses[name];
	if(e == null || !e.__ename__) return null;
	return e;
};
Type.createEmptyInstance = function(cl) {
	function empty() {}; empty.prototype = cl.prototype;
	return new empty();
};
Type.getEnumConstructs = function(e) {
	var a = e.__constructs__;
	return a.slice();
};
var _GameBonus = $hxClasses["_GameBonus"] = { __ename__ : true, __constructs__ : ["GameBonus0","GameBonus1","GameBonus2"] };
_GameBonus.GameBonus0 = ["GameBonus0",0];
_GameBonus.GameBonus0.__enum__ = _GameBonus;
_GameBonus.GameBonus1 = ["GameBonus1",1];
_GameBonus.GameBonus1.__enum__ = _GameBonus;
_GameBonus.GameBonus2 = ["GameBonus2",2];
_GameBonus.GameBonus2.__enum__ = _GameBonus;
var _IslandBonus = $hxClasses["_IslandBonus"] = { __ename__ : true, __constructs__ : ["IslandBonus0","IslandBonus1","IslandBonus2"] };
_IslandBonus.IslandBonus0 = ["IslandBonus0",0];
_IslandBonus.IslandBonus0.__enum__ = _IslandBonus;
_IslandBonus.IslandBonus1 = ["IslandBonus1",1];
_IslandBonus.IslandBonus1.__enum__ = _IslandBonus;
_IslandBonus.IslandBonus2 = ["IslandBonus2",2];
_IslandBonus.IslandBonus2.__enum__ = _IslandBonus;
var _IslandStatus = $hxClasses["_IslandStatus"] = { __ename__ : true, __constructs__ : ["IslandStatus0","IslandStatus1","IslandStatus2"] };
_IslandStatus.IslandStatus0 = ["IslandStatus0",0];
_IslandStatus.IslandStatus0.__enum__ = _IslandStatus;
_IslandStatus.IslandStatus1 = function(a,r) { var $x = ["IslandStatus1",1,a,r]; $x.__enum__ = _IslandStatus; return $x; };
_IslandStatus.IslandStatus2 = ["IslandStatus2",2];
_IslandStatus.IslandStatus2.__enum__ = _IslandStatus;
var _Item = $hxClasses["_Item"] = { __ename__ : true, __constructs__ : ["Item0","Item1","Item2","Item3","Item4","Item5","Item6","Item7","Item8","Item9","Item10","Item11","Item12","Item13","Item14","Item15","Item16","Item17","Item18","Item19","Item20","Item21","Item22","Item23","Item24","Item25","Item26","Item27"] };
_Item.Item0 = ["Item0",0];
_Item.Item0.__enum__ = _Item;
_Item.Item1 = ["Item1",1];
_Item.Item1.__enum__ = _Item;
_Item.Item2 = ["Item2",2];
_Item.Item2.__enum__ = _Item;
_Item.Item3 = ["Item3",3];
_Item.Item3.__enum__ = _Item;
_Item.Item4 = ["Item4",4];
_Item.Item4.__enum__ = _Item;
_Item.Item5 = ["Item5",5];
_Item.Item5.__enum__ = _Item;
_Item.Item6 = ["Item6",6];
_Item.Item6.__enum__ = _Item;
_Item.Item7 = ["Item7",7];
_Item.Item7.__enum__ = _Item;
_Item.Item8 = ["Item8",8];
_Item.Item8.__enum__ = _Item;
_Item.Item9 = ["Item9",9];
_Item.Item9.__enum__ = _Item;
_Item.Item10 = ["Item10",10];
_Item.Item10.__enum__ = _Item;
_Item.Item11 = ["Item11",11];
_Item.Item11.__enum__ = _Item;
_Item.Item12 = ["Item12",12];
_Item.Item12.__enum__ = _Item;
_Item.Item13 = ["Item13",13];
_Item.Item13.__enum__ = _Item;
_Item.Item14 = ["Item14",14];
_Item.Item14.__enum__ = _Item;
_Item.Item15 = ["Item15",15];
_Item.Item15.__enum__ = _Item;
_Item.Item16 = ["Item16",16];
_Item.Item16.__enum__ = _Item;
_Item.Item17 = ["Item17",17];
_Item.Item17.__enum__ = _Item;
_Item.Item18 = ["Item18",18];
_Item.Item18.__enum__ = _Item;
_Item.Item19 = ["Item19",19];
_Item.Item19.__enum__ = _Item;
_Item.Item20 = ["Item20",20];
_Item.Item20.__enum__ = _Item;
_Item.Item21 = ["Item21",21];
_Item.Item21.__enum__ = _Item;
_Item.Item22 = ["Item22",22];
_Item.Item22.__enum__ = _Item;
_Item.Item23 = ["Item23",23];
_Item.Item23.__enum__ = _Item;
_Item.Item24 = ["Item24",24];
_Item.Item24.__enum__ = _Item;
_Item.Item25 = ["Item25",25];
_Item.Item25.__enum__ = _Item;
_Item.Item26 = ["Item26",26];
_Item.Item26.__enum__ = _Item;
_Item.Item27 = ["Item27",27];
_Item.Item27.__enum__ = _Item;
var _PlayerAction = $hxClasses["_PlayerAction"] = { __ename__ : true, __constructs__ : ["_Play","_GameResult","_Grab","_MoveTo","_Teleport","_Burn","_Prism","_Dice","_FeverXPlay","_MajCartridge","_SavePos","_EndGame"] };
_PlayerAction._Play = function(n) { var $x = ["_Play",0,n]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._GameResult = function(b,a) { var $x = ["_GameResult",1,b,a]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._Grab = function(n,r) { var $x = ["_Grab",2,n,r]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._MoveTo = function(x,y) { var $x = ["_MoveTo",3,x,y]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._Teleport = ["_Teleport",4];
_PlayerAction._Teleport.__enum__ = _PlayerAction;
_PlayerAction._Burn = function(a,ib) { var $x = ["_Burn",5,a,ib]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._Prism = ["_Prism",6];
_PlayerAction._Prism.__enum__ = _PlayerAction;
_PlayerAction._Dice = function(b) { var $x = ["_Dice",7,b]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._FeverXPlay = ["_FeverXPlay",8];
_PlayerAction._FeverXPlay.__enum__ = _PlayerAction;
_PlayerAction._MajCartridge = function(o) { var $x = ["_MajCartridge",9,o]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._SavePos = function(n) { var $x = ["_SavePos",10,n]; $x.__enum__ = _PlayerAction; return $x; };
_PlayerAction._EndGame = function(n) { var $x = ["_EndGame",11,n]; $x.__enum__ = _PlayerAction; return $x; };
var _Reward = $hxClasses["_Reward"] = { __ename__ : true, __constructs__ : ["Reward0","Reward1","Reward2","Reward3","Reward4","Reward5","Reward6","Reward7","Reward8"] };
_Reward.Reward0 = function(i) { var $x = ["Reward0",0,i]; $x.__enum__ = _Reward; return $x; };
_Reward.Reward1 = function(ib) { var $x = ["Reward1",1,ib]; $x.__enum__ = _Reward; return $x; };
_Reward.Reward2 = ["Reward2",2];
_Reward.Reward2.__enum__ = _Reward;
_Reward.Reward3 = ["Reward3",3];
_Reward.Reward3.__enum__ = _Reward;
_Reward.Reward4 = function(n) { var $x = ["Reward4",4,n]; $x.__enum__ = _Reward; return $x; };
_Reward.Reward5 = ["Reward5",5];
_Reward.Reward5.__enum__ = _Reward;
_Reward.Reward6 = ["Reward6",6];
_Reward.Reward6.__enum__ = _Reward;
_Reward.Reward7 = ["Reward7",7];
_Reward.Reward7.__enum__ = _Reward;
_Reward.Reward8 = function(gb) { var $x = ["Reward8",8,gb]; $x.__enum__ = _Reward; return $x; };
var haxe = {};
haxe.Serializer = function() { };
$hxClasses["haxe.Serializer"] = haxe.Serializer;
haxe.Serializer.__name__ = ["haxe","Serializer"];
haxe.io = {};
haxe.io.Eof = function() { };
$hxClasses["haxe.io.Eof"] = haxe.io.Eof;
haxe.io.Eof.__name__ = ["haxe","io","Eof"];
haxe.io.Eof.prototype = {
	toString: function() {
		return "Eof";
	}
	,__class__: haxe.io.Eof
};
var js = {};
js.Boot = function() { };
$hxClasses["js.Boot"] = js.Boot;
js.Boot.__name__ = ["js","Boot"];
js.Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else return o.__class__;
};
js.Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js.Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js.Boot.__interfLoop(cc.__super__,cl);
};
js.Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js.Boot.__interfLoop(js.Boot.getClass(o),cl)) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
$hxClasses.Math = Math;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i1) {
	return isNaN(i1);
};
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
$hxClasses.Array = Array;
Array.__name__ = ["Array"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = $hxClasses.Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
Main.fl_disableEncryption = false;
Main.main();
})();
