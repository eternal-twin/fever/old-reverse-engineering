package ;

import haxe.Unserializer;
import haxe.Serializer;
import haxe.io.Bytes;
import StringBuf;

class MtCodec {
  public static var fl_disableEncryption: Bool = false;
  public static var BASE64_URL: String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
  public var salt: String;
  public var codecKey: String;
  public var keyBuffer: Bytes;

  public function new(salt: String, keyInput: String) {
    this.salt = salt;
    this.codecKey = keyInput;
    this.keyBuffer = Bytes.alloc(256);
    for (i in 0...256) {
      this.keyBuffer.set(i, i & 127);
    }
    var input: String = salt + keyInput;
    var inputLen: Int = input.length;
    var j: Int = 0;
    for (i in 0...256) {
      j = (j + this.keyBuffer.get(i) + input.charCodeAt(i % inputLen)) & 127;
      var tmp = this.keyBuffer.get(i);
      this.keyBuffer.set(i, this.keyBuffer.get(j));
      this.keyBuffer.set(j, tmp);
    }
  }

  public function encryptAny(data: Dynamic): String {
    var s = new Serializer();
    s.useEnumIndex = true;
    s.serialize(data);
    return this.encryptString(s.toString());
  }

  public function decryptAny(encrypted: String): Dynamic {
    var raw: String = this.decryptString(encrypted);
    var s = new Unserializer(raw);
    return s.unserialize();
  }

  /**
   * XOR string with key and append Adler32 checksum.
   */
  public function encryptString(dataString: String): String {
    if (MtCodec.fl_disableEncryption) {
      return this.codecKey + dataString;
    }
    var key: Bytes = this.keyBuffer;
    var out: StringBuf = new StringBuf();
    var a1 = key.get(0);
    var a2 = key.get(1);
    for (i in 0...dataString.length) {
      var a: Int = dataString.charCodeAt(i);
      var b: Int = a ^ key.get(i & 255);
      var c: Int = b == 0 ? a : b;
      out.addChar(c);
      a1 = (a1 + b) % 65521;
      a2 = (a2 + a1) % 65521;
    }
    var checksum = a1 ^ (a2 << 8);
    out.addChar(MtCodec.BASE64_URL.charCodeAt(checksum & 63));
    out.addChar(MtCodec.BASE64_URL.charCodeAt((checksum >> 6) & 63));
    out.addChar(MtCodec.BASE64_URL.charCodeAt((checksum >> 12) & 63));
    out.addChar(MtCodec.BASE64_URL.charCodeAt((checksum >> 18) & 63));
    return out.toString();
  }

  public function decryptString(dataString: String): String {
    if (dataString.length < 4) {
      throw "Invalid string length";
    }
    var key: Bytes = this.keyBuffer;
    var out: StringBuf = new StringBuf();
    var payloadLen = dataString.length - 4;
    var a1 = key.get(0);
    var a2 = key.get(1);
    for (i in 0...payloadLen) {
      var a: Int = dataString.charCodeAt(i);
      var b: Int = a ^ key.get(i & 255);
      var c: Int = b == 0 ? a : b;
      out.addChar(c);
      if (b == 0) {
        a = 0;
      }
      a1 = (a1 + a) % 65521;
      a2 = (a2 + a1) % 65521;
    }
    var checksum = a1 ^ (a2 << 8);
    var expectedChecksumStr = [
      MtCodec.BASE64_URL.charAt(checksum & 63),
      MtCodec.BASE64_URL.charAt((checksum >> 6) & 63),
      MtCodec.BASE64_URL.charAt((checksum >> 12) & 63),
      MtCodec.BASE64_URL.charAt((checksum >> 18) & 63)
    ].join("");
    var actualChecksumStr = dataString.substring(payloadLen);
    if (actualChecksumStr != expectedChecksumStr) {
      throw ("FCHK: " + actualChecksumStr + ", " + expectedChecksumStr);
    }
    return out.toString();
  }
}
