package;

enum _PlayerAction {
  _Play(n: Int);
  _GameResult(b: Bool, a: Array<Dynamic>);
  _Grab(n: Int, r: _Reward);
  _MoveTo(x: Int, y: Int);
  _Teleport;
  _Burn(a: Array<Dynamic>, ib: _IslandBonus);
  _Prism;
  _Dice(b: Bool);
  _FeverXPlay;
  _MajCartridge(o: Dynamic);
  _SavePos(n: Int);
  _EndGame(n: Int);
}
